<?php
return [
    'app development' => 'разработка приложений',
    'promotion' => 'продвижение',
    'web development' => 'веб-разработка',
    'CRM' => 'ЦРМ',
    'Your "ninja" project' => 'Ваш "ниндзя" проект',
    'Design' => 'Дизайн',
    'Game development' => 'Разработка игр',
    'Outsource & outstaff' => 'Аутсорсинг и аутстафф',
    'blog' => 'блог',
    'Contact us' => 'Связаться с нами',
    'projects' => 'проекты',
    'Back to home' => 'Вернуться на главную',
    'Ukraine, Kiev, Bolshaya Okruzhnaya 4' => 'Украина, Киев, Большая окружная 4',
    'Pennsylvania' => 'Пенсильвания'
];
?>