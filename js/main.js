function ajax(params, callbackSuccess, callbackError) {

    let url = params.url;
    let method = params.method?params.method:'GET';
    let mime = params.mime?params.mime:'text/xml';
    let data = params.data?params.data:null;
    let headerTitle = params.header&&params.header.title?params.header.title:"Content-type";
    let headerValue = params.header&&params.header.value?params.header.value:"application/x-www-form-urlencoded";
    let async = params.async !== false?true:false;

    if (!url) {
        console.log('The url not set!');
        return false;
    }

    var xhr = false;

    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        xhr = new XMLHttpRequest();
        if (xhr.overrideMimeType) {
            xhr.overrideMimeType(mime);
        }
    } else if (window.ActiveXObject) { // IE
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }

    if (!xhr) {
        console.log('Could not create class instance XMLHTTP');
        return false;
    }
    xhr.open(method, url, async);
    xhr.setRequestHeader(headerTitle, headerValue);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            if(typeof callbackSuccess === 'function'){
                callbackSuccess(xhr.responseText);
            }
        }
        else{
            if(typeof callbackError === 'function'){
                callbackError(xhr.responseText);
            }
        }
    };
    xhr.send(data);

}
let reload = true;
window.addEventListener('resize', function () {
    if(reload){
        if(PAGE_VERSION != 'mobile' && innerWidth < 1024){
            reload = false;
            reloadPage('mobile');
        }
        else if(PAGE_VERSION == 'mobile' && window.innerWidth >= 1024){
            reload = false;
            reloadPage('pc');
        }

    }
});

function reloadPage(pageVersion) {
    let url = self.location.origin;
    if(self.location.search){
        url += self.location.search;
        if(url.indexOf('?v=mobile') > -1){
            url = url.replace('?v=mobile', '?v='+pageVersion);
        }
        else if(url.indexOf('?v=pc') > -1){
            url = url.replace('?v=pc', '?v='+pageVersion);
        }
        else{
            url.replace('&v=mobile', '');
            url.replace('&v=pc', '');
            url += '&v='+pageVersion;
        }
    }
    else {
        url += '?v='+pageVersion;
    }
    window.location.href = url;
}

window.onload = setTimeout(init, 1000);

function init() {
    //init modal windows
    modalInit();
}

class Modal{

    constructor(){
        let id = null;
        let modal = null;
        let modalsSelector = '[data-modal-id="{{ID}}"]';
        let modals = {};
        this.buttons = document.querySelectorAll('[data-modal-target="form"]');
        this.buttons.forEach(function (el) {
            id = el.getAttribute('data-modal-target');
            modal = document.querySelector(modalsSelector.replace('{{ID}}', id));
            if(modal){
                modals[id] = {
                    id: id,
                    button: el,
                    target: modal
                }
            }
        });
        this.modals = modals;
        this.bg = document.createElement('div');
        this.bg.id = 'modal-back';
        document.body.append(this.bg);
    }

    callPopup(modal){
        if(modal.classList.contains('visible')){
            modal.classList.remove('visible');
            this.bg.classList.remove('visible');
        }
        else{
            modal.classList.add('visible');
            this.bg.classList.add('visible');
        }
    }

    closeAll(){
        this.bg.classList.remove('visible');
        for (let key in this.modals){
            this.modals[key].target.classList.remove('visible');
        }
    }

}

function modalInit(){

    let modal = new Modal;
    if('modals' in modal){
        modal.bg.addEventListener('click', function () {
            modal.closeAll();
        });
        document.querySelectorAll('.modal .button-close').forEach(function (el) {
            el.addEventListener('click', function () {
                modal.closeAll();
            });
        });
        for (let key in modal.modals){
            modal.modals[key].button.addEventListener('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                modal.callPopup(modal.modals[key].target);
            })
        }
    }

}

//random number with range
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

if(window.jQuery){
    $('.menu').on('click', function () {
        $('.menu-wrap').removeClass('hidden');
    });
    $('.button-close').on('click', function () {
        $('.menu-wrap').addClass('hidden');
    });
}


