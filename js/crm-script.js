function animateFabric(svg) {
    let bearings = Array.from(svg.querySelectorAll('.bearing'));
    bearings.forEach((element, i) => {
        let x;
        if(i>=0 && i<5){
            x = 2102 - i * 216;
        } else if(i>=5 && i<10){
            x = 1073 - (i-5) * 216;
        }
        element.setAttribute('style', 'transform-origin: ' + x + 'px 3537px');
    });
    let bearings_fast = Array.from(svg.querySelectorAll('.bearing_fast'));
    bearings_fast.forEach((element, i) => {
        let x;
        if(i>=0 && i<5){
            x = 3131 - i * 216;
        }
        element.setAttribute('style', 'transform-origin: ' + x + 'px 3537px');
    });
    let pump = svg.getElementById('pump_1');
    pump.setAttribute('style', 'transform-origin: 3984px 2647px');
    let regulator_1 = svg.getElementById('regulator_1');
    let regulator_2 = svg.getElementById('regulator_2');
    let regulator_3 = svg.getElementById('regulator_3');
    let indicator_3 = svg.getElementById('indicator_3');
    indicator_3.setAttribute('style', 'transform-origin: 4317px 3629px');
    let graph = svg.getElementById('graph_1');

    anime({
        targets: bearings,
        easing: 'linear',
        loop: true,
        rotate: [
            { value: -360, duration: 3000, delay: 0 },
            { value: -360, duration: 2000, delay: 0 },
        ]
    });
    anime({
        targets: bearings_fast,
        easing: 'linear',
        loop: true,
        rotate: [
            { value: -720, duration: 3000, delay: 0 },
            { value: -720, duration: 2000, delay: 0 },
        ]
    });
    anime({
        targets: pump,
        easing: 'linear',
        duration: 1000,
        loop: true,
        scaleX: [1, 0.9],
        scaleY: [1, 1.1],
        direction: 'alternate'
    });
    anime({
        targets: regulator_1,
        easing: 'linear',
        duration: 1500,
        loop: true,
        translateY: [0, 202],
        direction: 'alternate'
    });
    anime({
        targets: regulator_2,
        easing: 'linear',
        duration: 3000,
        loop: true,
        keyframes:[
            {translateY: -109},
            {translateY: 93},
            {translateY: 0},
        ]
    });
    anime({
        targets: regulator_3,
        easing: 'linear',
        duration: 3000,
        loop: true,
        keyframes:[
            {translateY: 40},
            {translateY: -162},
            {translateY: 0},
        ]
    });
    anime({
        targets: indicator_3,
        easing: 'linear',
        duration: 3000,
        loop: true,
        keyframes:[
            {rotate: 20},
            {rotate: -30},
            {rotate: 0}
        ]
    });
    anime({
        targets: graph,
        easing: 'linear',
        direction: 'alternate',
        duration: 2000,
        points: [{
            value: '3549,3116 3585,3113 3624,3180 3745,3233 3776,3131 3820,3135 3845,3179 3889,3242 3944,3206 3975,3241 4028,3111'
        }],
        loop: true
    });
};
function animateSmoke(svg) {
    let bubbles = Array.from(svg.querySelectorAll('.bubble'));
    bubbles.forEach((element, i) => {
        let x = 3562 + i * 94;
        element.setAttribute('style', 'transform-origin: ' + x + 'px 1806px');
        anime({
            targets: element,
            easing: 'linear',
            duration: 5000,
            delay: anime.random(0, 5000),
            loop: true,
            translateY: [0, -1806],
            translateX: [
                { value: 0 },
                { value: -71 },
                { value: -124 },
                { value: 46 },
                { value: 0 }
            ],
            scale: [1, anime.random(1, 3)]
        });
    })
};
function animateBoxes(box_open, box_close, box_close_end) {
    let _box_open = box_open.getElementById('box');
    let _box_close = box_close.getElementById('box_close');
    let _box_close_1 = box_close.getElementById('box1');
    let _box_close_3 = box_close.getElementById('box3');
    _box_close_3.setAttribute('style', 'transform-origin: 1732px 3461px');
    let _box_close_end = box_close_end.getElementById('box_end');
    _box_close_end.setAttribute('style', 'transform-origin: 636px 3537px');

    anime({
        targets: _box_open,
        easing: 'linear',
        translateX: [
            { value: -1257, delay: 0, duration: 3000 },
            { value: -1257, delay: 1000, duration: 0 },
            { value: 0, delay: 0, duration: 0 },
            { value: 0, delay: 1000, duration: 0 }
        ],
        loop: true
    });

    anime({
        targets: _box_close,
        easing: 'linear',
        translateX: [
            { value: -547, delay: 0, duration: 3000 },
            { value: -547, delay: 2000, duration: 0 }
        ],
        loop: true
    });

    anime({
        targets: _box_close_1,
        easing: 'linear',
        translateX: [
            { value: 0, delay: 0, duration: 4000 },
            { value: 547, delay: 0, duration: 0 },
            { value: 547, delay: 1000, duration: 0 }
        ],
        loop: true
    });

    anime({
        targets: _box_close_3,
        easing: 'linear',
        scaleX: [
            { value: 1, delay: 3300, duration: 0 },
            { value: 1.1, delay: 0, duration: 200 },
            { value: 1, delay: 1500, duration: 0 }
        ],
        scaleY: [
            { value: 1, delay: 3300, duration: 0 },
            { value: 0.99, delay: 0, duration: 200 },
            { value: 1, delay: 1500, duration: 0 }
        ],
        loop: true
    });

    anime({
        targets: _box_close_end,
        delay: 0,
        keyframes: [
            {
                translateX: [0, -428],
                opacity: 1,
                duration: 2347,
                easing: 'linear'
            },
            {
                rotate: -90,
                opacity: 0,
                duration: 500,
                easing: 'easeInCubic'
            },
            {
                rotate: -90,
                opacity: 0,
                duration: 3000 - 2347 - 500
            },
            {
                rotate: -90,
                opacity: 0,
                delay: 2000,
                duration: 0
            }
        ],
        loop: true
    });
};
function animateCatCrm(cat) {
    let tale = cat.getElementById('tale');
    tale.setAttribute('style', 'transform-origin: 3888px 3942px');
    let shadow = cat.getElementById('shadow_1');

    anime({
        targets: tale,
        duration: 2000,
        easing: 'linear',
        rotate: [0, 170],
        loop: true,
        direction: 'alternate'
    });

    anime({
        targets: shadow,
        easing: 'linear',
        direction: 'alternate',
        duration: 2000,
        d: [{
            value: 'M4022 4261c-173,-4 -403,19 -408,73 1,7 -4,1 2,10 7,8 8,8 11,10 3,2 -3,-1 26,13 29,14 159,33 206,36 47,3 62,3 108,4 71,0 162,-4 238,-15 31,-4 60,-9 83,-16 30,-8 52,-19 62,-31 2,-4 4,-7 4,-10 2,-23 -48,-42 -119,-55 -62,-11 -139,-18 -213,-19z'
        }, {
            value: 'M4022 4261c-173,-4 -403,19 -408,73 1,7 -4,1 2,10 7,8 8,8 11,10 3,2 -3,-1 26,13 29,14 159,33 206,36 47,3 62,3 108,4 71,0 162,-4 238,-15 31,-4 47,-6 71,-13 149,6 195,89 249,52 60,-42 -128,-101 -171,-96 2,-23 -48,-42 -119,-55 -62,-11 -139,-18 -213,-19z'
        }],
        loop: true
    });
};
function animateRobots(robots) {
    let joint_1 = robots.getElementById('joint_1');
    joint_1.setAttribute('style', 'transform-origin: 2283px 1632px');
    let shoulder_1 = robots.getElementById('shoulder_1');
    shoulder_1.setAttribute('style', 'transform-origin: 2283px 1632px');
    let shoulder_2 = robots.getElementById('shoulder_2');
    shoulder_2.setAttribute('style', 'transform-origin: 2283px 1632px');
    let shoulder_2_inner = robots.getElementById('shoulder_2_inner');
    shoulder_2_inner.setAttribute('style', 'transform-origin: 2764px 1632px');
    let shoulder_3 = robots.getElementById('shoulder_3');
    let joint_2 = robots.getElementById('joint_2');
    joint_2.setAttribute('style', 'transform-origin: 1183px 1345px');

    let lamp_1 = robots.getElementById('lamp_1');
    let lamp_2 = robots.getElementById('lamp_2');

    anime({
        targets: joint_1,
        easing: 'linear',
        rotate: [
            {value: 90, duration: 1000, delay: 3000},
            {value: 0, duration: 1000, delay: 0}
        ],
        loop: true
    });
    anime({
        targets: shoulder_1,
        easing: 'linear',
        rotate: [
            {value: 90, duration: 1000, delay: 3000},
            {value: 0, duration: 1000, delay: 0}
        ],
        loop: true
    });
    anime({
        targets: shoulder_2,
        easing: 'linear',
        rotate: [
            {value: 90, duration: 1000, delay: 3000},
            {value: 0, duration: 1000, delay: 0}
        ],
        loop: true
    });
    anime({
        targets: shoulder_2_inner,
        easing: 'linear',
        rotate: [
            {value: -90, duration: 1000, delay: 3000},
            {value: 0, duration: 1000, delay: 0}
        ],
        loop: true
    });
    anime({
        targets: shoulder_3,
        easing: 'easeInOutElastic(1, 0.9)',
        translateY: [
            {value: 300, duration: 500, delay: 3000},
            {value: 0, duration: 1500, delay: 0},
        ],
        loop: true
    });
    anime({
        targets: joint_2,
        easing: 'easeInOutElastic(1, 0.9)',
        rotate: [
            {value: 90, duration: 500, delay: 3000},
            {value: 0, duration: 1500, delay: 0}
        ],
        loop: true
    });
    anime({
        targets: lamp_1,
        easing: 'linear',
        translateX: [
            {value: 100, duration: 0, delay: 3000},
            {value: 0, duration: 0, delay: 1000},
            {value: -100, duration: 0, delay: 1000}
        ],
        loop: true
    });
    anime({
        targets: lamp_2,
        easing: 'linear',
        translateX: [
            {value: 100, duration: 0, delay: 3000},
            {value: 0, duration: 0, delay: 1000},
            {value: -100, duration: 0, delay: 1000}
        ],
        loop: true
    });
};

let run = () => {
    let content = document.querySelector('.body-wrap');
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0
        });
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;

        let smoke = document.getElementById('svg-smoke').contentDocument;
        let box_open = document.getElementById('svg-box-open').contentDocument;
        let box_close = document.getElementById('svg-box-close').contentDocument;
        let box_close_end = document.getElementById('svg-box-close-end').contentDocument;
        let fabric = document.getElementById('svg-fabric').contentDocument;
        let cat = document.getElementById('svg-cat').contentDocument;
        let robots = document.getElementById('svg-robots').contentDocument;
        animateSmoke(smoke);
        animateBoxes(box_open, box_close, box_close_end);
        animateFabric(fabric);
        animateCatCrm(cat);
        animateRobots(robots);
    }
};
window.onload = setTimeout(run, 1000);
