window.onload = setTimeout(run, 1000);
function run() {
    let content = document.querySelector('.body-wrap');
    let svg = document.getElementById('svg').contentDocument;
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0,
        });
        initAnimations(svg);
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;
    }

}

function initAnimations(svg) {
    balloons(svg);
    generalAnimations(svg);
    catAnimations(svg);
}

function balloons(svg) {
    let path_1 = document.getElementById('path_1');
    let target = svg.getElementById('balloon_1');
    let path = anime.path(path_1);
    anime({
        targets: target,
        translateX: path('x'),
        translateY: path('y'),
        easing: 'easeInOutCubic',
        duration: 70000,
        loop: true
    });

    target = svg.getElementById('balloon_2');
    anime({
        targets: target,
        translateX: path('x'),
        translateY: path('y'),
        easing: 'easeInOutCubic',
        duration: 80000,
        direction: 'reverse',
        loop: true
    });

    target = svg.getElementById('balloon_3');
    anime({
        targets: target,
        translateX: path('x'),
        translateY: path('y'),
        easing: 'easeInOutCubic',
        duration: 90000,
        direction: 'alternate',
        loop: true
    });

}

function generalAnimations(svg) {

    //sun animation
    let sun = svg.getElementById('sun');
    anime({
        targets: sun,
        translateY: '-15%',
        duration: 6000,
        loop: false,
        easing: 'easeInOutQuad',
        complete: function(anim) {
            anime({
                targets: sun,
                easing: 'easeInOutQuad',
                direction: 'alternate',
                scale: 0.97,
                loop: true,
                duration: 10000
            });
        }
    });

    //bg animation
    let gradients = {
        start: '#d2c6bf',
        end: '#ffffff'
    };
    let bg = document.querySelector('.page-bg');
    anime({
        targets: gradients,
        start: '#ffffff',
        end: '#ffffff',
        easing: 'linear',
        duration: 6000,
        update: function(a) {
            let value1 = a.animations[0].currentValue;
            let value2 = a.animations[1].currentValue;
            bg.style.backgroundImage = 'linear-gradient(to left top, '+value1+' 0%, '+value2+' 40%)';
        }
    });

    //clouds animation
    let clouds = svg.querySelector('#clouds');
    anime({
        targets: clouds,
        translateX: ['50%', '-30%'],
        scale: 0.6,
        opacity: 0,
        easing: 'easeOutQuad',
        duration: 15000,
        loop: true
    });

    //parallax
    svg.querySelector('#parallax_1').dataset.depth = '0';
    svg.querySelector('#parallax_2').dataset.depth = '0';
    svg.querySelector('#parallax_3').dataset.depth = '0.05';
    svg.querySelector('#parallax_4').dataset.depth = '0.08';
    svg.querySelector('#parallax_5').dataset.depth = '0.1';
    svg.querySelector('#parallax_6').dataset.depth = '0.2';
    let scene = svg.getElementById('scene');
    let parallaxInstance = new Parallax(scene);

    //tree animation
    let tree = svg.querySelectorAll('.tree');
    if(tree.length){
        tree.forEach(function (item) {
            let rotation = getRandomArbitrary(2, 5);
            let duration = getRandomArbitrary(3000, 4000);
            anime({
                targets: item,
                rotate: rotation,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: duration
            });
        });
    }

}

function catAnimations(svg) {

    //cats static animation
    let cat = svg.querySelector('#cat');
    anime({
        targets: cat,
        scaleX: 1.05,
        scaleY: 0.95,
        direction: 'alternate',
        easing: 'easeInOutSine',
        loop: true,
        duration: 2000,
        delay: anime.random(0, 1000)
    });

    //eyes watching mouse
    let eyes = svg.querySelectorAll(".eye_left, .eye_right");
    if(eyes.length){
        let svgArea = document.getElementById('svg').getBoundingClientRect();
        eyes.forEach(function (el) {
            let eyeBall = el.querySelector(".eyeball");
            let pupil = el.querySelector(".pupil");
            let eyeArea = eyeBall.getBoundingClientRect();
            let pupilArea = pupil.getBoundingClientRect();
            window.addEventListener("mousemove", (e)=>{
                let x = (e.screenX - (svgArea.left+pupilArea.left)),
                    y = (e.pageY - (svgArea.top+pupilArea.top));
                let positionX = x / eyeArea.width;
                let positionY = y / eyeArea.height;
                let limit = 5;
                positionX = positionX>limit?limit:positionX;
                positionX = positionX<-limit?-limit:positionX;
                positionY = positionY>limit?limit:positionY;
                positionY = positionY<-limit?-limit:positionY;
                pupil.style.transform = 'translate('+ positionX + 'px, ' + positionY + 'px)';
            });

        })
    }

}