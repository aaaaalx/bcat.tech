//fps counter
const times = [];
let fps;
let fpsActive = false;
function refreshLoop() {
    window.requestAnimationFrame(() => {
        const now = performance.now();
        while (times.length > 0 && times[0] <= now - 1000) {
            times.shift();
        }
        times.push(now);
        fps = times.length;
        if(fpsActive){
            refreshLoop();
        }
    });
}
function stopFps() {
    fpsActive = false;
}
function startFps() {
    fpsActive = true;
    refreshLoop();
}
startFps();

window.onload = setTimeout(run, 1000);
function run() {
    let content = document.querySelector('.body-wrap');
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0,
            complete: function() {
                switch (PAGE) {
                    case 'index':
                        initAnimations();
                        break;
                    case 'contacts':
                        break;
                }
            }
        });
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;
    }

    let cats = document.querySelectorAll('.cats svg, #black_cat');



    cats.forEach(function (el) {
        el.addEventListener('click', function (e) {
            let menuItem = document.querySelector('[data-target="'+this.id+'"] a');
            if(menuItem){
                menuItem.click();
            }
        })
    })

}

function initAnimations() {

    const ADDITIONAL_ANIM_FPS = 45;
    const GENERAL_ANIM_FPS = 20;

    stopFps();

    if(fps >= ADDITIONAL_ANIM_FPS){
        generalAnimations();
        additionalAnimations();
    }
    else if(fps >= GENERAL_ANIM_FPS){
        generalAnimations();
    }

    //menu animation
    anime({
        targets: '.menu-item',
        scale: [0, 1],
        easing: 'spring(1, 60, 10, 1)',
        duration: 1000
    });

}

function generalAnimations() {

    //sun animation
    anime({
        targets: '.sun svg',
        left: '-70%',
        marginBottom: '320%',
        easing: 'easeInOutQuad',
        duration: 3000
    });
    anime({
        targets: '.sun svg',
        scale: 0.96,
        direction: 'alternate',
        easing: 'easeInOutQuad',
        loop: true,
        duration: 4000
    });

    //bg animation
    let gradients = {
        start: '#fff',
        end: '#63656f'
    };
    let body = document.querySelector('.bg');
    anime({
        targets: gradients,
        start: '#fff',
        end: '#ede8e5',
        easing: 'easeInOutQuad',
        duration: 3000,
        update: function(a) {
            let value1 = a.animations[0].currentValue;
            let value2 = a.animations[1].currentValue;
            body.style.backgroundImage = 'linear-gradient('+value1+', '+value2+')';
        }
    });

    runBirds();
    function runBirds(duration){
        duration = duration?duration:getRandomArbitrary(1000, 2000);
        setTimeout(function () {

            let birdsCount = getRandomArbitrary(1, 5);
            let birds = [];
            let animationDuration = 8000;
            let totalAnimationDuration = 0;
            let delay = 0;

            for (let i = 0; i < birdsCount; i++) {
                let birdsSize = getRandomArbitrary(2, 6);
                let el = getBirdTemplate(i);
                if(el){
                    birds.push({
                        id: el.getAttribute('id'),
                        position: {
                            top: getRandomArbitrary(30, 50),
                            left: getRandomArbitrary(40, 60),
                        },
                        size: birdsSize,
                        el: el
                    });
                }
            }

            let direction = {
                x: 100,
                y: getRandomArbitrary(40, 60)
            };

            birds.forEach(function (item, index, arr) {
                delay = getRandomArbitrary(800, 1000);
                totalAnimationDuration += delay;
                animateBirds(item, direction, delay, animationDuration, (index+1)>=arr.length?true:false);
            });

        }, duration);
    }

    function getBirdTemplate(id){
        let el = null;
        let bird = document.querySelector('#bird-template .bird').cloneNode(true);
        if(bird){
            let birdsContainer = document.querySelector('#bird-container');
            bird.setAttribute('id', 'bird_'+id);
            birdsContainer.append(bird);
            el = bird;
        }
        return el;
    }
    function clearBirdsContainer(){
        let birdsContainer = document.querySelector('#bird-container');
        if(birdsContainer){
            birdsContainer.innerHTML = '';
        }
    }

    //bird animation
    function animateBirds(item, direction, delay, totalAnimationDuration, complite){
        let bird = item.el;
        let birdSvg = bird.querySelector('svg');

        bird.setAttribute('style', 'left: '+item.position.left+'%;');
        birdSvg.setAttribute('style','bottom: '+item.position.top+'%;');

        if(bird){

            let verticalMove = anime({
                targets: birdSvg,
                bottom: (direction.y+item.position.top+item.size)+'%',
                easing: 'linear',
                duration: totalAnimationDuration,
                scale: [0,1],
                delay: delay,
                complete: function(anim) {
                    anime.remove(verticalMove);
                }
            });
            let wing_1 = null;
            let wing_2 = null;
            let horizontalMove = anime({
                targets: bird,
                left: (direction.x+item.size)+'%',
                easing: 'linear',
                duration: totalAnimationDuration,
                delay: delay,
                loopBegin: function(anim){
                    //wings animation
                    let duration = getRandomArbitrary(300, 500);
                    let easing = 'linear';
                    wing_1 = anime({
                        targets: bird.querySelector('[data-id="wing_1"]'),
                        points: [
                            { value: '38.8,35.3 29.2,37 12.3,8.5' },
                            { value: '38.8,35.3 29.2,37 3,21.2' },
                            { value: '38.8,35.3 29.2,37 0.4,36.7' },
                            { value: '38.8,35.3 29.2,37 6.8,50.6' }
                        ],
                        easing: easing,
                        duration: duration,
                        loop: true,
                        direction: 'alternate',
                    });
                    wing_2 = anime({
                        targets: bird.querySelector('[data-id="wing_2"]'),
                        points: [
                            { value: '51,16.7 38.8,35.3 29.2,37' },
                            { value: '53.7,22.8 38.8,35.3 29.2,37' },
                            { value: '56.1,30.7 38.8,35.3 29.2,37' },
                            { value: '53.1,42.9 38.8,35.3 29.2,37' }
                        ],
                        easing: easing,
                        duration: duration,
                        loop: true,
                        direction: 'alternate',
                    });
                },
                complete: function(anim) {
                    anime.remove(wing_1);
                    anime.remove(wing_2);
                    anime.remove(horizontalMove);
                    if(complite){
                        clearBirdsContainer();
                        runBirds();
                    }
                }
            });

        }
    }

    //eyes watching mouse
    let eyes = document.querySelectorAll(".eye_left, .eye_right");
    if(eyes.length){
        eyes.forEach(function (el) {
            let eyeBall = el.querySelector(".eyeball"),
                pupil = el.querySelector(".pupil"),
                eyeArea = eyeBall.getBoundingClientRect(),
                pupilArea = pupil.getBoundingClientRect();

            document.addEventListener("mousemove", (e)=>{
                let x = (e.clientX - pupilArea.left),
                    y = (e.clientY - pupilArea.top);
                let positionX = x / eyeArea.width;
                let positionY = y / eyeArea.height;
                let limit = 5;
                positionX = positionX>limit?limit:positionX;
                positionX = positionX<-limit?-limit:positionX;
                positionY = positionY>limit?limit:positionY;
                positionY = positionY<-limit?-limit:positionY;
                pupil.style.transform = 'translate('+ positionX + 'px, ' + positionY + 'px)';
            });

        })
    }

}

function additionalAnimations() {

    //grass animation
    let grass = document.querySelectorAll('.swing');
    if(grass.length){
        grass.forEach(function (item) {
            let duration = getRandomArbitrary(1500, 2500);
            let rotation = getRandomArbitrary(5, 10);
            let shadow = item.closest('svg').querySelector('.'+item.dataset.shadow);
            anime({
                targets: item,
                rotate: rotation,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: duration
            });
            anime({
                targets: shadow,
                rotateX: -rotation,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: duration
            });
        });
    }

    //tree animation
    let tree = document.querySelectorAll('svg .tree-top');
    if(tree.length){
        tree.forEach(function (item) {
            let rotation = getRandomArbitrary(4, 7);
            let duration = getRandomArbitrary(3000, 4000);
            anime({
                targets: item,
                rotate: rotation,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: duration
            });
        });
    }

    //clouds animation
    let clouds = document.querySelectorAll('.clouds, .clouds-back');
    if(clouds.length){
        clouds.forEach(function (item) {
            item.classList.add('active');
        });
    }

    //black cat static animation
    anime({
        targets: '#black_cat',
        transformOrigin: '100% -5% 0',
        scaleX: 1.05,
        scaleY: 0.95,
        direction: 'alternate',
        easing: 'easeInOutSine',
        loop: true,
        duration: 2000
    });
    //black cat tail static animation
    anime({
        targets: '#black-cat-tail',
        translateY: '3%',
        direction: 'alternate',
        easing: 'easeInOutSine',
        loop: true,
        duration: 2000
    });

    //cats static animation
    let cats = document.querySelectorAll('.cats svg');
    if(cats.length){
        cats.forEach(function (cat) {
            cat.staticMovement = anime({
                targets: cat,
                scaleX: 1.05,
                scaleY: 0.95,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: 2000,
                delay: anime.random(0, 1000)
            });

            //cats jumping on hover
            let jumpActive = false;
            let catBody = cat.querySelector('.cat_body');
            let catTail = cat.querySelector('.cat_tail');
            catBody.addEventListener('mouseenter', function (e) {
                if(!jumpActive){
                    cat.staticMovement.pause();
                    jumpActive = true;
                    if(typeof cat.jump == 'undefined'){
                        cat.jump = anime({
                            targets: catBody,
                            translateY: '-5px',
                            direction: 'alternate',
                            easing: 'easeInExpo',
                            loop: true,
                            duration: 500
                        });
                        let rotateDirection = catTail.classList.contains('tail_right')?'2deg':'-2deg';
                        cat.tail = anime({
                            targets: catTail,
                            rotate: rotateDirection,
                            direction: 'alternate',
                            easing: 'easeInExpo',
                            loop: true,
                            duration: 500
                        });
                    }
                    else{
                        cat.jump.play();
                        cat.tail.play();
                    }
                }
            });
            catBody.addEventListener('mouseleave', function (e) {
                if(typeof cat.jump !== 'undefined'){
                    cat.jump.pause();
                    cat.tail.pause();
                    anime({
                        targets: catBody,
                        translateY: '0',
                        easing: 'linear',
                        duration: 100,
                        complete: function () {
                            jumpActive = false;
                            cat.staticMovement.play();
                        }
                    });
                    anime({
                        targets: catTail,
                        rotate: '0deg',
                        easing: 'linear',
                        duration: 100
                    });
                }
            });

        });
    }

}

String.prototype.replaceAll = function(search, replacement) {
    let target = this;
    return target.split(search).join(replacement);
};