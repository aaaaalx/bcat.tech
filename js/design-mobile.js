window.onload = setTimeout(run, 1000);
function run() {
    let content = document.querySelector('.body-wrap');
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0,
        });
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;
    }

}