window.onload = setTimeout(run, 1000);
function run() {
    let content = document.querySelector('.body-wrap');
    let svg = document.getElementById('svg').contentDocument;
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0,
        });
        initAnimations(svg);
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;
    }

}

function initAnimations(svg) {
    generalAnimations(svg);
}

function generalAnimations(svg) {

    //clouds animation
    let clouds = svg.querySelectorAll('.cloud');
    if(clouds.length){
        clouds.forEach(function (item) {
            cloudsAnimations(item, svg);
        });
    }

    //parallax
    svg.querySelector('#parallax_1').dataset.depth = '0.1';
    let scene = svg.getElementById('scene');
    let parallaxInstance = new Parallax(scene);

    //cat's tail animation
    let tail = svg.querySelector('#tail');
    let tail_shadow = svg.querySelector('#tail_shadow');
    catsTail(tail, tail_shadow);

    //cats static animation
    let cat = svg.querySelector('#cat');
    anime({
        targets: cat,
        scaleX: 1.05,
        scaleY: 0.95,
        direction: 'alternate',
        easing: 'easeInOutSine',
        loop: true,
        duration: 2000,
        delay: anime.random(0, 1000)
    });

    //tree animation
    let tree = svg.querySelectorAll('.tree');
    if(tree.length){
        tree.forEach(function (item) {
            let rotation = getRandomArbitrary(1, 3);
            let duration = getRandomArbitrary(2000, 3000);
            anime({
                targets: item,
                rotate: rotation,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: duration
            });
        });
    }

    //grass animation
    let grass = svg.querySelectorAll('.grass');
    if(grass.length){
        grass.forEach(function (item) {
            let rotation = getRandomArbitrary(2, 3);
            let duration = getRandomArbitrary(800, 1600);
            anime({
                targets: item,
                rotate: rotation,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: duration
            });
        });
    }

}

function catsTail(tail, tail_shadow) {
    anime({
        targets: [tail, tail_shadow],
        rotate: getRandomArbitrary(4, 8)+'deg',
        scaleX: getRandomArbitrary(0.90, 0.96),
        easing: 'easeInCubic',
        duration: getRandomArbitrary(250, 400),
        direction: 'alternate',
        delay: getRandomArbitrary(0, 2000),
        complete: function () {
            catsTail(tail, tail_shadow);
        }
    });
}

function cloudsAnimations(item, svg) {
    let duration = getRandomArbitrary(10000, 20000);
    anime({
        targets: item,
        translateX: ['30%', '-35%'],
        opacity: [
            {
                value: 1,
                duration: (duration*80)/100,
                easing: 'easeInSine',
            },
            {
                value: 0,
                duration: (duration*20)/100,
                easing: 'easeInSine',
            }
        ],
        easing: 'linear',
        duration: duration,
        complete: function () {
            cloudsAnimations(item, svg);
        }
    });
}