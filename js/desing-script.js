function animateElephant(svg) {
    let elephant = svg.getElementById('elephant');
    let face = svg.getElementById('face');
    let tower = svg.getElementById('tower');

    let leg_front_right = svg.getElementById('leg_front_right');
    let leg_back_right = svg.getElementById('leg_back_right');
    let leg_front_left = svg.getElementById('leg_front_left');
    let leg_back_left = svg.getElementById('leg_back_left');

    let leg_front_right_foot = svg.getElementById('leg_front_right4');
    let leg_back_right_foot = svg.getElementById('leg_back_right4');
    let leg_front_left_foot = svg.getElementById('leg_front_left4');
    let leg_back_left_foot = svg.getElementById('leg_back_left4');


    elephant.setAttribute('style', 'transform-origin: center bottom; position: relative;');
    face.setAttribute('style', 'transform-origin: 2190px 375px');
    tower.setAttribute('style', 'transform-origin: 2212px 329px');

    leg_front_right.setAttribute('style', 'transform-origin: 2223px 387px');
    leg_back_right.setAttribute('style', 'transform-origin: 2269px 378px');
    leg_front_left.setAttribute('style', 'transform-origin: 2223px 378px');
    leg_back_left.setAttribute('style', 'transform-origin: 2285px 378px');

    leg_front_right_foot.setAttribute('style', 'transform-origin: 2212px 651px');
    leg_back_right_foot.setAttribute('style', 'transform-origin: 2283px 651px');
    leg_front_left_foot.setAttribute('style', 'transform-origin: 2228px 665px');
    leg_back_left_foot.setAttribute('style', 'transform-origin: 2302px 665px');

    let elephant_duration = 55000;
    let step = 2400 * 4.7;
    let step_half = step * 0.5;
    let step_3_4 = step * 1.5;
    let start_stop = [0, -1000];

    anime({
        targets: elephant,
        easing: 'linear',
        duration: elephant_duration,
        delay: 0,
        loop: true,
        opacity: [1, 0.3],
        scale: [1, 0.7],
        translateX: start_stop,
        complete: function() {

        }
    });

    let t_front_right = anime.timeline({
        easing: 'linear',
        loop: true,
        autoplay: false
    });
    t_front_right.add({
        targets: leg_front_right,
        duration: step_half,
        rotate: [0, -10],
    }, 0);
    t_front_right.add({
        targets: leg_front_right,
        duration: step,
        rotate: [-10, 10]
    }, step_half);
    t_front_right.add({
        targets: leg_front_right,
        duration: step_half,
        rotate: [10, 0]
    }, step_3_4);
    t_front_right.add({
        targets: leg_front_right_foot,
        duration: step_half,
        rotate: [0, 15]
    }, step_half);
    t_front_right.add({
        targets: leg_front_right_foot,
        duration: step_half,
        rotate: [15, 0]
    }, step);

    let t_front_left = anime.timeline({
        easing: 'linear',
        loop: true,
        autoplay: false
    });
    t_front_left.add({
        targets: leg_front_left,
        duration: step_half,
        rotate: [0, 10]
    }, 0);
    t_front_left.add({
        targets: leg_front_left,
        duration: step,
        rotate: [10, -10],
    }, step_half);
    t_front_left.add({
        targets: leg_front_left,
        duration: step_half,
        rotate: [-10, 0],
    }, step_3_4);
    t_front_left.add({
        targets: leg_front_left_foot,
        duration: step_half,
        rotate: [15, 0]
    }, 0);
    t_front_left.add({
        targets: leg_front_left_foot,
        duration: step_half,
        rotate: [0, 15]
    }, step_3_4);

    let t_back_right = anime.timeline({
        easing: 'linear',
        loop: true,
        autoplay: false
    });
    t_back_right.add({
        targets: leg_back_right,
        duration: step,
        rotate: [-10, 10],
    }, 0);
    t_back_right.add({
        targets: leg_back_right,
        duration: step,
        rotate: [10, -10]
    }, step);
    t_back_right.add({
        targets: leg_back_right_foot,
        rotate: [0, -20],
        duration: step_half
    }, 0);
    t_back_right.add({
        targets: leg_back_right_foot,
        rotate: [-20, 0],
        duration: step_half
    }, step_half);

    let t_back_left = anime.timeline({
        easing: 'linear',
        loop: true,
        autoplay: false
    });
    t_back_left.add({
        targets: leg_back_left,
        duration: step,
        rotate: [10, -10]
    }, 0);
    t_back_left.add({
        targets: leg_back_left,
        rotate: [-10, 10],
        duration: step
    }, step);
    t_back_left.add({
        targets: leg_back_left_foot,
        rotate: [0, -20],
        duration: step_half
    }, step);
    t_back_left.add({
        targets: leg_back_left_foot,
        rotate: [-20, 0],
        duration: step_half
    }, step_3_4);

    let face_duration = 600;
    let t_face = anime.timeline({
        easing: 'linear',
        loop: true,
        autoplay: false
    });
    t_face.add({
        targets: face,
        duration: face_duration,
        rotate: [0, -5]
    }, step_half);
    t_face.add({
        targets: face,
        duration: face_duration,
        rotate: [-5, 0]
    }, step_half + face_duration);
    t_face.add({
        targets: face,
        duration: face_duration,
        rotate: [0, -5]
    }, step_3_4);
    t_face.add({
        targets: face,
        duration: face_duration,
        rotate: [-5, 0]
    }, step_3_4 + face_duration);
    t_face.add({
        targets: face,
        duration: (step_half - 2*face_duration),
        rotate: 0
    }, step_3_4 + 2*face_duration);

    let tower_duration = 600;
    let tower_duration_half = tower_duration / 2;
    let t_tower = anime.timeline({
        easing: 'linear',
        loop: true,
        autoplay: false
    });
    t_tower.add({
        targets: tower,
        duration: tower_duration_half,
        rotate: [0, -5]
    }, step_half);
    t_tower.add({
        targets: tower,
        duration: tower_duration,
        rotate: [-5, 3]
    }, step_half + tower_duration_half);
    t_tower.add({
        targets: tower,
        duration: tower_duration_half,
        rotate: [3, 0]
    }, step_half + (tower_duration_half + tower_duration));


    t_tower.add({
        targets: tower,
        duration: tower_duration_half,
        rotate: [0, -5]
    }, step_3_4);
    t_tower.add({
        targets: tower,
        duration: tower_duration,
        rotate: [-5, 3]
    }, step_3_4 + tower_duration_half);
    t_tower.add({
        targets: tower,
        duration: tower_duration_half,
        rotate: [3, 0]
    }, step_3_4 + (tower_duration_half + tower_duration));
    t_tower.add({
        targets: tower,
        duration: (step_half - 2 * tower_duration),
        rotate: 0
    }, step_3_4 + (2 * tower_duration));

    t_front_left.play();
    t_front_right.play();
    t_back_right.play();
    t_back_left.play();
    t_face.play();
    t_tower.play();
};

function animateCat(svg) {
    let tale = svg.getElementById('tale');
    tale.setAttribute('style', 'transform-origin: 1298px 1082px');

    let t_tile = anime.timeline({
        targets: tale,
        easing: 'linear',
        loop: true
    });
    t_tile.add({
        duration: 2000,
        rotate: [0, 170],
        delay: 2000
    });
    t_tile.add({
        duration: 2000,
        rotate: [170, 0],
        delay: 2000
    });

    t_tile.play();
};

function animateClock(svg) {
    let minutes_arrow = svg.getElementById('minutes_arrow');
    let hour_arrow = svg.getElementById('hour_arrow');

    minutes_arrow.setAttribute('style', 'transform-origin: 1561px 1069px');
    hour_arrow.setAttribute('style', 'transform-origin: 1561px 1069px');

    anime({
        targets: minutes_arrow,
        easing: 'linear',
        duration: 55000,
        delay: 0,
        loop: true,
        keyframes: [
            {rotate: 45, scale: 0.6},
            {rotate: 90, scale: 0.5},
            {rotate: 180, scale: 0.7},
            {rotate: 270, scale: 1},
            {rotate: 315, scale: 0.7},
            {rotate: 360, scale: 1}
        ]
    });
    anime({
        targets: hour_arrow,
        easing: 'linear',
        duration: 55000 * 60,
        delay: 0,
        loop: true,
        keyframes: [
            {rotate: 45, scale: 0.7},
            {rotate: 90, scale: 0.6},
            {rotate: 180, scale: 0.6},
            {rotate: 270, scale: 1},
            {rotate: 315, scale: 0.7},
            {rotate: 360, scale: 1}
        ]
    });
};

function animateGrass(svg) {
    let grass = Array.from(svg.querySelectorAll('.swing'));

    grass.forEach((element) => {
        element.setAttribute('style', 'transform-origin: 1774px 1057px');
    })

    anime({
        targets: grass,
        easing: 'easeInOutSine',
        duration: 2000,
        delay: 0,
        loop: true,
        rotate: anime.stagger(anime.random(-5, 5), {from: 'center'}),
        direction: 'alternate'
    });
};

function animateCactus(svg) {
    let cactus = svg.getElementById('cactus');
    let cactus_shadow = svg.getElementById('cactus_shadow');

    cactus.setAttribute('style', 'transform-origin: 1940px 1048px');
    cactus_shadow.setAttribute('style', 'transform-origin: 1940px 1048px');

    anime({
        targets: cactus,
        easing: 'linear',
        duration: 4000,
        direction: 'alternate',
        delay: 0,
        loop: true,
        rotate: [2, -5]
    });
    anime({
        targets: cactus_shadow,
        easing: 'linear',
        duration: 4000,
        direction: 'alternate',
        delay: 0,
        loop: true,
        rotate: [2, -5]
    });
};

let run = () => {
    let content = document.querySelector('.body-wrap');
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0
        });
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;

        let back = document.getElementById('svg-element-back').contentDocument;
        let front = document.getElementById('svg-element-front').contentDocument;
        animateElephant(back);
        animateCat(front);
        animateClock(front);
        animateGrass(front);
        animateCactus(front);

        let scene = document.getElementById('scene');
        let parallaxInstance = new Parallax(scene);
    }
};
window.onload = setTimeout(run, 1000);
