window.onload = setTimeout(run, 1000);
function run() {
    let content = document.querySelector('.body-wrap');
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0,
            complete: function() {
                initAnimations();
            }
        });
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;
    }

}

function initAnimations(){

    //eyes watching mouse
    let cats = document.querySelectorAll('.slick-slide:not(.slick-cloned) .item-wrap object');
    if(cats.length){
        cats.forEach(function (obj) {
            let svgDoc = obj.contentDocument;
            let eyes = svgDoc.querySelectorAll(".eye_left, .eye_right");
            if(eyes.length){
                eyes.forEach(function (el) {
                    let eyeBall = el.querySelector(".eyeball"),
                        pupil = el.querySelector(".pupil"),
                        eyeArea = eyeBall.getBoundingClientRect(),
                        pupilArea = pupil.getBoundingClientRect();
                    document.addEventListener("click", (e)=>{
                        let x = (e.clientX - pupilArea.left),
                            y = (e.clientY - pupilArea.top);
                        let positionX = x / eyeArea.width;
                        let positionY = y / eyeArea.height;
                        let limit = 5;
                        positionX = positionX>limit?limit:positionX;
                        positionX = positionX<-limit?-limit:positionX;
                        positionY = positionY>limit?limit:positionY;
                        positionY = positionY<-limit?-limit:positionY;
                        pupil.style.transform = 'translate('+ positionX + 'px, ' + positionY + 'px)';

                        console.log('positionX');
                        console.log(positionX);
                        console.log('positionY');
                        console.log(positionY);

                    });
                });
            }
        })
    }

}

$(".slider").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    autoplay: false,
    loop: true,
    autoplaySpeed: 4000,
    adaptiveHeight: false,
    centerMode: true,
    arrows: false,
    touchThreshold: 10,
    initialSlide: 1,
    centerPadding: '0px',
    touchMove: false,
    responsive: [
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});