window.onload = setTimeout(run, 1000);
function run() {
    let content = document.querySelector('.body-wrap');
    let svg = document.getElementById('svg').contentDocument;
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0,
        });
        initAnimations(svg);
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;
    }

}

function initAnimations(svg) {
    generalAnimations(svg);
    catAnimations(svg);
}

function generalAnimations(svg) {

    //parallax
    svg.querySelector('#parallax_1').dataset.depth = '0.05';
    svg.querySelector('#parallax_2').dataset.depth = '0.04';
    svg.querySelector('#parallax_3').dataset.depth = '0.06';
    svg.querySelector('#parallax_4').dataset.depth = '0.09';
    svg.querySelector('#parallax_5').dataset.depth = '0.1';
    svg.querySelector('#parallax_6').dataset.depth = '0.03';
    let scene = svg.getElementById('scene');
    let parallaxInstance = new Parallax(scene);

    //island animation
    let islands = svg.querySelectorAll('.island');
    if(islands.length){
        islands.forEach(function (item) {
            anime({
                targets: item,
                translateY: getRandomArbitrary(20, 40),
                direction: 'alternate',
                easing: 'easeInOutQuad',
                loop: true,
                duration: getRandomArbitrary(3000, 6000)
            });
        });
    }

}

function catAnimations(svg) {

    //cats static animation
    let cats = svg.querySelectorAll('.cat');
    if(cats.length) {
        cats.forEach(function (cat) {
            anime({
                targets: cat,
                scaleX: 1.05,
                scaleY: 0.95,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: 2000,
                delay: anime.random(0, 1000)
            });
        });
    }

}