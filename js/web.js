window.onload = setTimeout(run, 1000);
function run() {
    let content = document.querySelector('.body-wrap');
    let svg = document.getElementById('svg').contentDocument;
    if(content){
        anime({
            targets: content,
            keyframes: [
                {clipPath: 'circle(100% at center)'},
            ],
            easing: 'linear',
            duration: 3000,
            delay: 0,
        });
        initAnimations(svg);
        let loader = document.querySelector('.loader');
        loader?loader.setAttribute('style', 'display:none'):false;
    }
}

function initAnimations(svg) {
    generalAnimations(svg);
    circlesRotation(svg);

}

function generalAnimations(svg) {

    //background form animation
    let target = svg.getElementById('background_form');
    anime({
        targets: target,
        rotate: '360deg',
        easing: 'linear',
        duration: 70000,
        loop: true
    });

    //cat levitation animation
    let cat = svg.getElementById('cat');
    anime({
        targets: cat,
        translateY: '50px',
        easing: 'easeInOutQuad',
        duration: 5000,
        loop: true,
        direction: 'alternate',
    });

    //cats static animation
    let cat_wrap = svg.getElementById('cat_wrap');
    anime({
        targets: cat_wrap,
        scaleX: 1.05,
        scaleY: 0.95,
        direction: 'alternate',
        easing: 'easeInOutSine',
        loop: true,
        duration: 2000,
        delay: anime.random(0, 1000)
    });

    //light animation
    let light = svg.getElementById('light');
    anime({
        targets: light,
        opacity: '0.5',
        easing: 'easeInOutCubic',
        duration: 5000,
        loop: true,
        direction: 'alternate',
    });

    //clouds animation
    let clouds = svg.getElementById('clouds');
    anime({
        targets: clouds,
        translateY: '-50px',
        opacity: '0',
        scale: '0.6',
        easing: 'easeInOutCubic',
        duration: 5000,
        loop: true,
        direction: 'alternate',
    });

    //parallax
    svg.querySelector('#parallax_1').dataset.depth = '0';
    svg.querySelector('#parallax_2').dataset.depth = '0.1';
    svg.querySelector('#parallax_2_1').dataset.depth = '0.1';
    svg.querySelector('#parallax_3').dataset.depth = '0.2';
    let scene = svg.getElementById('scene');
    let parallaxInstance = new Parallax(scene);

    //tree animation
    let tree = svg.querySelectorAll('.tree');
    if(tree.length){
        tree.forEach(function (item) {
            let rotation = getRandomArbitrary(4, 7);
            let duration = getRandomArbitrary(3000, 4000);
            anime({
                targets: item,
                rotate: rotation,
                direction: 'alternate',
                easing: 'easeInOutSine',
                loop: true,
                duration: duration
            });
        });
    }

    //moon animation
    let moon = svg.getElementById('moon');
    anime({
        targets: moon,
        scale: '0.8',
        easing: 'easeInOutCubic',
        duration: 5000,
        loop: true,
        direction: 'alternate',
    });

}

function circlesRotation(svg) {
    let circles = svg.querySelector('#circles_wrap');
    let circles_rotation = getRandomArbitrary(90, 360);
    let direction = getRandomArbitrary(0, 1)?'linear':'alternate';
    anime({
        targets: circles,
        rotate: circles_rotation,
        direction: direction,
        easing: 'easeInOutCubic',
        duration: 15000,
        complete: function () {
            circlesRotation(svg);
        }
    });
}