<?php
session_start();
$_SESSION['message_title'] = '';
$_SESSION['message_desc'] = '';
$_SESSION['message_status'] = '';
$_SESSION['form'] = '';
$locationSuccess = '/mail-page';
$locationError = '/';
if(count($_POST)==0){
	header('Location: '.$locationError, true, 301);
    exit();
}
include_once __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Multilang.php';
include_once __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Config.php';
include_once __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Geolocation.php';
include_once __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Route.php';
$Config = new Config;
$Geolocation = new Geolocation;
$Route = new Route($Config);
$Multilang = new Multilang($Route, $Config, $Geolocation);
if(!empty($_POST['username']) && (!empty($_POST['email']) || !empty($_POST['phone']))){
	$to = 'supernewmegaemail@gmail.com';
	$fromTitle = 'Politics';
	$subject = 'Форма с сайта '.$fromTitle;
	$from = [('info@'.$_SERVER['SERVER_NAME']) => $fromTitle];
	$status = 1;
    $params = [];
    $params['name'] = htmlspecialchars(strip_tags(trim($_POST['username'])), ENT_QUOTES);
    $params['email'] = isset($_POST['email'])?htmlspecialchars(strip_tags(trim($_POST['email'])), ENT_QUOTES):null;
    $params['phone'] = isset($_POST['phone'])?htmlspecialchars(strip_tags(trim($_POST['phone'])), ENT_QUOTES):null;
    $params['message'] = isset($_POST['message'])?htmlspecialchars(strip_tags(trim($_POST['message'])), ENT_QUOTES):null;
    if(strlen($params['name'])<2 || strlen($params['name'])>30){
        $status = 0;
		$_SESSION['message_title'] .= $Multilang->$Multilang->getTranslate('Введите правильно имя! ');
		$_SESSION['message_status'] = 'error';
    }
    if($params['email'] && !filter_var($params['email'], FILTER_VALIDATE_EMAIL)){
        $status = 0;
        $_SESSION['message_title'] .= $Multilang->getTranslate('Неверный email! ');
        $_SESSION['message_status'] = 'error';
    }
    if($params['phone'] && strlen($params['phone'])<1 || strlen($params['phone'])>20){
        $status = 0;
        $_SESSION['message_title'] .= $Multilang->getTranslate('Неверный номер телефона! ');
        $_SESSION['message_status'] = 'error';
    }
    if(($form == 'scroll5' && !$params['message']) || $params['message'] && (strlen($params['message'])<3 || strlen($params['message'])>5000)){
        $status = 0;
        $_SESSION['message_title'] .= $Multilang->getTranslate('Сообщение должно содержать не меньше трех и не больше 5000 букв! ');
        $_SESSION['message_status'] = 'error';
    }
	if($status == 0){
        header('Location: '.$locationError, true, 301);
        exit();
    }
	else{
	    $name = '<p><strong>Имя:</strong> '.$params['name'].'<br><br></p>';
		$email = '<p><strong>Email:</strong> '.$params['email'].'<br><br></p>';
        $phone = '<p><strong>Телефон:</strong> '.$params['phone'].'<br><br></p>';
        $message = '<p><strong>Сообщение:</strong> '.$params['message'].'<br><br></p>';
		$text = $name.$email.$phone.$message;
		require_once 'swiftmailer/lib/swift_required.php';
		$mail = Swift_Message::newInstance();
		$mail->setSubject($subject);
        $mail->setFrom($from);
        $mail->setTo([$to]);
        $mail->setBody($text);	
		$type = $mail->getHeaders()->get('Content-Type');
        $type->setValue('text/html');
        $type->setParameter('charset', 'utf-8');

		$transport = Swift_MailTransport::newInstance();
        $mailer = new Swift_Mailer($transport);

		$numSent = $mailer->send($mail);
        if($numSent){
			$_SESSION['message_title'] = $Multilang->getTranslate('Спасибо!');
			$_SESSION['message_desc'] = $Multilang->getTranslate('Мы признательны за проявленный интерес! Мы свяжемся с вами в ближайшее время.');
			$_SESSION['message_status'] = 'success';
			header('Location: '.$locationSuccess, true, 301);
            exit();
		}
		else{
		    $_SESSION['message_title'] = $Multilang->getTranslate('Ошибка при отправке сообщения! ');
			$_SESSION['message_status'] = 'error';
			header('Location: '.$locationSuccess, true, 301);
            exit();
		}
	}
}
else{
    $_SESSION['message_title'] = $Multilang->getTranslate('Нужно заполнить все обязательные поля! ');
    $_SESSION['message_status'] = 'error';
    header('Location: '.$locationError, true, 301);
    exit();
}