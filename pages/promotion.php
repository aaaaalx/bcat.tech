<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/icons/favicon_32x32.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="img/icons/favicon_64x64.ico" type="image/x-icon" sizes="64x64">
    <title>Black cats</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/promotion.css">
</head>
<body class="page">

<div class="loader">
    <img class="loader-icon" src="/img/ring-loader.gif">
</div>

<header class="controls">
    <div class="logo">
        <a href="<?= to('/'); ?>"><img src="/img/menu-icons/logo.png" alt="logo"></a>
    </div>
    <div class="lang">
        <nav class="lang-nav">
            <div class="lang-nav-wrap">
                <span class="selected-lang"><?= $currentLang; ?></span>
                <ul class="hidden-menu">
                    <?php foreach($langsList as $key => $lang): ?>
                        <?php if($currentLang !== $lang): ?>
                            <li><a href="<?= to($key); ?>"><?= $lang; ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <span class="arrow-icon"><img src="/img/arrow-icon.png"></span>
            </div>
        </nav>
    </div>
    <div class="path">
        <a href="<?= to('/'); ?>">Home</a>
        <span>/</span>
        <span><?= getTranslate('Promotion'); ?></span>
    </div>
    <div class="button-bottom-left">
        <div class="item-wrap">
            <a href="//blog.bcat.tech" target="_blank"><?= getTranslate('blog'); ?></a>
        </div>
    </div>
    <div class="button-top-right">
        <div class="item-wrap">
            <a href="<?= to('/'); ?>"><?= getTranslate('Back to home'); ?></a>
        </div>
    </div>
    <div class="button-bottom-right">
        <div class="item-wrap">
            <a href="#"><?= getTranslate('projects'); ?></a>
        </div>
    </div>
</header>

<div class="body-wrap">

    <div class="page-bg">

        <div class="left-part">
            <div class="left-part-wrap">
                <div class="left-part-content">
                    <h1>Lorem ipsum dolor sit amet</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                    </div>
            </div>
            <div class="gradient"></div>
        </div>

        <div class="right-part">
            <div class="object-cover"></div>
            <object id="svg" type="image/svg+xml" data="/img/svg/promotion_page/promotion.svg"></object>
            <svg class="svgPath">
                <path id="path_1" class="path" d="M39.3,121.8c0-27.5,23.3-66.5,61.5-85c35.4-17.2,76.1-12.9,106,5c48.2,28.9,64.5,91,50.5,140.5
	c-20.3,71.6-96.6,90.1-108.5,93c-35.4,8.6-91,8.2-106-19c-4.2-7.6-8.6-19.8-6-30c4.5-18,9-21.5,10-40c0.6-11.5-2.6-12.1-5.5-35.5
	C39.5,135.8,39.3,124.2,39.3,121.8z"/>
            </svg>
        </div>

    </div>

</div>
<script src="/libs/js/parallax.min.js"></script>
<script>
    const PAGE = 'promotion';
    const PAGE_VERSION = 'pc';
</script>
<script src="/libs/js/anime.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/promotion.js"></script>
</body>
</html>