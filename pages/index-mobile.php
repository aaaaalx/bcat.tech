<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/icons/favicon_32x32.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="img/icons/favicon_64x64.ico" type="image/x-icon" sizes="64x64">
    <title>Black cats</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/libs/css/slick.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body class="main-page">

<div class="loader">
    <img class="loader-icon" src="/img/ring-loader.gif">
</div>

<header class="controls">
    <div class="logo">
        <a href="<?= to('/'); ?>"><img src="/img/menu-icons/logo.png" alt="logo"></a>
    </div>

    <div class="menu"></div>

    <div class="menu-wrap hidden">
        <span class="button-close"></span>
        <div class="lang-mobile">
            <ul>
                <?php foreach($langsList as $key => $lang): ?>
                    <?php
                    $activeItem = '';
                    if($currentLang == $lang){
                        $activeItem = ' class="active"';
                    }
                    ?>
                    <li<?= $activeItem; ?>><a href="<?= to($key); ?>"><?= $lang; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <nav class="main-menu-min">
            <ul>
                <li><a href="<?= to('app'); ?>"><?= getTranslate('app development'); ?></a></li>
                <li><a href="<?= to('promotion'); ?>"><?= getTranslate('promotion'); ?></a></li>
                <li><a href="<?= to('web'); ?>"><?= getTranslate('web development'); ?></a></li>
                <li><a href="<?= to('crm'); ?>"><?= getTranslate('CRM'); ?></a></li>
                <li><a href="#"><?= getTranslate('Your "ninja" project'); ?></a></li>
                <li><a href="<?= to('design'); ?>"><?= getTranslate('Design'); ?></a></li>
                <li><a href="#"><?= getTranslate('Game development'); ?></a></li>
                <li><a href="<?= to('outsource-and-outstaff'); ?>"><?= getTranslate('Outsource & outstaff'); ?></a></li>
            </ul>
        </nav>
        <div class="buttons">
            <ul>
                <li><a href="//blog.bcat.tech" target="_blank"><?= getTranslate('blog'); ?></a></li>
                <li><a href="#"><?= getTranslate('projects'); ?></a></li>
                <li><a href="<?= to('contacts'); ?>"><?= getTranslate('Contact us'); ?></a></a></li>
            </ul>
        </div>
    </div>

    <div class="button-top-right">
        <div class="item-wrap">
            <a href="<?= to('contacts'); ?>"><?= getTranslate('Contact us'); ?></a>
        </div>
    </div>

</header>

<div class="body-wrap">

    <div id="scene" class="bg">

        <div class="content">
            <object type="image/svg+xml" data="/img/mobile-main-bg.svg"></object>
        </div>

        <div class="slider-wrap">
            <div id="slider" class="slider">
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-5">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="#"><?= getTranslate('Your "ninja" project'); ?></a>
                        </span>
                        </div>
                        <a href="#">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_black.svg"></object>
                        </a>
                    </div>
                </div>
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-8">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="#"><?= getTranslate('Outsource & outstaff'); ?></a>
                        </span>
                        </div>
                        <a href="#">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_blue.svg"></object>
                        </a>
                    </div>
                </div>
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-4">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="<?= to('crm'); ?>"><?= getTranslate('CRM'); ?></a>
                        </span>
                        </div>
                        <a href="<?= to('crm'); ?>">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_brown.svg"></object>
                        </a>
                    </div>
                </div>
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-1">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="<?= to('app'); ?>"><?= getTranslate('app development'); ?></a>
                        </span>
                        </div>
                        <a href="<?= to('app'); ?>">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_chocolate.svg"></object>
                        </a>
                    </div>
                </div>
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-3">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="<?= to('web'); ?>"><?= getTranslate('web development'); ?></a>
                        </span>
                        </div>
                        <a href="<?= to('web'); ?>">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_green.svg"></object>
                        </a>
                    </div>
                </div>
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-7">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="#"><?= getTranslate('Game development'); ?></a>
                        </span>
                        </div>
                        <a href="#">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_orange.svg"></object>
                        </a>
                    </div>
                </div>
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-2">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="<?= to('promotion'); ?>"><?= getTranslate('promotion'); ?></a>
                        </span>
                        </div>
                        <a href="<?= to('promotion'); ?>">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_violet.svg"></object>
                        </a>
                    </div>
                </div>
                <div>
                    <div class="item-wrap">
                        <div class="menu-item-mobile type-6">
                        <span data-target="cat_green" class="link-wrap">
                            <a href="<?= to('design'); ?>"><?= getTranslate('Design'); ?></a>
                        </span>
                        </div>
                        <a href="<?= to('design'); ?>">
                            <object type="image/svg+xml" data="/img/mobile/cats/cat_yellow.svg"></object>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<script>
    const PAGE = 'index';
    const PAGE_VERSION = 'mobile';
</script>
<script src="/libs/js/anime.min.js"></script>
<script type="text/javascript" src="/libs/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="/libs/js/slick.js"></script>
<script src="/js/main.js"></script>
<script src="/js/index-mobile.js"></script>
</body>
</html>