<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/icons/favicon_32x32.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="img/icons/favicon_64x64.ico" type="image/x-icon" sizes="64x64">
    <title>Black cats</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

<div class="loader">
    <img class="loader-icon" src="/img/ring-loader.gif">
</div>

<header class="controls">
    <div class="logo">
        <a href="<?= to('/'); ?>"><img src="/img/menu-icons/logo.png" alt="logo"></a>
    </div>
    <div class="lang">
        <nav class="lang-nav">
            <div class="lang-nav-wrap">
                <span class="selected-lang"><?= $currentLang; ?></span>
                <ul class="hidden-menu">
                    <?php foreach($langsList as $key => $lang): ?>
                        <?php if($currentLang !== $lang): ?>
                            <li><a href="<?= to($key); ?>"><?= $lang; ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <span class="arrow-icon"><img src="/img/arrow-icon.png"></span>
            </div>
        </nav>
    </div>
    <nav class="main-menu">
        <div class="menu-item type-1"><span data-target="cat_chocolate" class="link-wrap"><a href="<?= to('app'); ?>"><?= getTranslate('app development'); ?></a></span></div>
        <div class="menu-item type-2"><span data-target="cat_violet" class="link-wrap"><a href="<?= to('promotion'); ?>"><?= getTranslate('promotion'); ?></a></span></div>
        <div class="menu-item type-3"><span data-target="cat_green" class="link-wrap"><a href="<?= to('web'); ?>"><?= getTranslate('web development'); ?></a></span></div>
        <div class="menu-item type-4"><span data-target="cat_brown" class="link-wrap"><a href="<?= to('crm'); ?>"><?= getTranslate('CRM'); ?></a></span></div>
        <div class="menu-item type-5"><span data-target="cat_black" class="link-wrap"><a href="#"><?= getTranslate('Your "ninja" project'); ?></a></span></div>
        <div class="menu-item type-6"><span data-target="cat_yellow" class="link-wrap"><a href="<?= to('design'); ?>"><?= getTranslate('Design'); ?></a></span></div>
        <div class="menu-item type-7"><span data-target="cat_orange" class="link-wrap"><a href="#"><?= getTranslate('Game development'); ?></a></span></div>
        <div class="menu-item type-8"><span data-target="cat_blue" class="link-wrap"><a href="<?= to('outsource-and-outstaff'); ?>"><?= getTranslate('Outsource & outstaff'); ?></a></span></div>
    </nav>
    <div class="button-bottom-left">
        <div class="item-wrap">
            <a href="//blog.bcat.tech" target="_blank"><?= getTranslate('blog'); ?></a>
        </div>
    </div>
    <div class="button-top-right">
        <div class="item-wrap">
            <a href="<?= to('contacts'); ?>"><?= getTranslate('Contact us'); ?></a>
        </div>
    </div>
    <div class="button-bottom-right">
        <div class="item-wrap">
            <a href="#"><?= getTranslate('projects'); ?></a>
        </div>
    </div>
</header>

<div class="body-wrap">

    <div id="scene" class="bg">

        <div class="back" data-depth="0.02" id="layer_1">

            <div class="sun">
                <svg version="1.1" id="_x31_5plan_sun" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 198 205.3" style="enable-background:new 0 0 198 205.3;" xml:space="preserve">
                        <style type="text/css">
                            .sun_st0{fill:#FFFFFF;}
                        </style>
                    <polygon class="svg_sun sun_st0" points="0,70.9 38.1,19.6 98.9,0 160,19.6 198,70.9 198,134.4 160.2,185.7 99.6,205.3 38.3,185.7 0,134.4 "/>
                    </svg>
            </div>

        </div>

        <div class="back" data-depth="0.03" id="layer_2">

            <div class="rocks_back">
                <div class="object-cover"></div>
                <object type="image/svg+xml" data="/img/rocks_back_3.svg"></object>
            </div>

        </div>

        <div class="clouds-wrap" data-depth="0">
            <div class="clouds-back"></div>
            <div class="clouds"></div>
        </div>

        <div class="back" data-depth="0.04" id="layer_3">

            <div class="rocks_back">
                <div class="object-cover"></div>
                <object type="image/svg+xml" data="/img/rocks_back_2.svg"></object>
            </div>

        </div>

        <div class="back" data-depth="0.05" id="layer_4">

            <div class="rocks_back">
                <div class="object-cover"></div>
                <object type="image/svg+xml" data="/img/rocks_back_1.svg"></object>
            </div>

        </div>

        <div class="back" data-depth="0.06" id="layer_5">

            <div class="rocks_back">
                <div class="object-cover"></div>
                <object type="image/svg+xml" data="/img/rocks_back.svg"></object>
            </div>

        </div>

        <div class="back" data-depth="0.08" id="layer_6">

            <div class="tree_2">
                <svg version="1.1" id="_x36_plan_yelka" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 120.6 185.2" style="enable-background:new 0 0 120.6 185.2;" xml:space="preserve">
                        <style type="text/css">
                            .tree_2_st0{fill:#686142;}
                            .tree_2_st1{fill:#3C3C25;}
                            .tree_2_st2{fill:#51610F;}
                            .tree_2_st3{fill:#49540C;}
                            .tree_2_st4{fill:#384507;}
                            .tree_2_st5{fill:#2C3503;}
                            .tree_2_st6{fill:#4B6608;}
                            .tree_2_st7{fill:#5E8008;}
                            .tree_2_st8{fill:#53710B;}
                            .tree_2_st9{fill:#5A8204;}
                            .tree_2_st10{fill:#759F1C;}
                            .tree_2_st11{fill:#76A21A;}
                        </style>
                    <g id="trunk">
                        <polygon class="tree_2_st0" points="69.2,128.7 62.8,185.4 53.1,185.2 55.5,128.7 	"/>
                        <polygon class="tree_2_st1" points="62.8,157.9 53.5,169.7 55.5,128.7 69.2,128.7 65.6,160.9 	"/>
                    </g>
                    <g class="tree-top">
                        <g id="_x33__part">
                            <polygon class="tree_2_st2" points="91.3,111.4 120.6,147 60.2,153.2 59.2,112.9 	"/>
                            <polygon class="tree_2_st3" points="0,146.4 23.5,111.4 59.2,112.9 60.2,153.2 	"/>
                            <polygon class="tree_2_st4" points="80.3,96.7 93.7,114.2 59.5,124.2 59.8,97.9 	"/>
                            <polygon class="tree_2_st5" points="32.7,97.7 59.8,97.7 59.5,124 14.1,125.2 	"/>
                        </g>
                        <g id="_x32__part">
                            <polygon class="tree_2_st6" points="80.2,58.7 85.3,90.4 60.2,88.2 66,58.7 	"/>
                            <polygon class="tree_2_st7" points="59.2,112.9 60.2,88.2 85.3,90.1 120.6,112.9 101.7,110.9 	"/>
                            <polygon class="tree_2_st4" points="48.8,53.1 36.4,77.6 60.2,88.2 66,58.7 	"/>
                            <polygon class="tree_2_st8" points="9.1,110.5 36.4,77.6 60.2,88.2 59.2,112.9 	"/>
                        </g>
                        <g id="_x31__part">
                            <polygon class="tree_2_st9" points="64.8,0 79.3,34.5 95.5,48.8 111.7,78.1 88.2,74.7 61.5,82.1 	"/>
                            <polygon class="tree_2_st10" points="17.4,75.6 36.1,57.7 64.8,0 61.5,82.1 37.4,73.1 	"/>
                            <polygon class="tree_2_st11" points="62.9,45.7 95.5,48.8 79.3,34.5 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="tree_3">
                <svg version="1.1" id="_x37_plan_yelka" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 83.1 164.3" style="enable-background:new 0 0 83.1 164.3;" xml:space="preserve">
                        <style type="text/css">
                            .tree_3_st0{fill:#966431;}
                            .tree_3_st1{fill:#A3AF2C;}
                            .tree_3_st2{fill:#D4E545;}
                            .tree_3_st3{fill:#8E9324;}
                            .tree_3_st4{fill:#AEC03A;}
                            .tree_3_st5{fill:#D9EC5A;}
                        </style>
                    <polygon id="trunk" class="tree_3_st0" points="44.7,130.9 43.1,164.4 32.8,164.4 34.6,145.3 34.8,132.1 38.4,132.7 "/>
                    <g class="tree-top">
                        <g id="_part">
                            <polygon class="tree_3_st1" points="28.2,79 18.1,97.1 0,126.6 38.4,132.7 38.2,91.5 	"/>
                            <polygon class="tree_3_st2" points="83.1,119.8 64,101.6 58.7,90.2 38.2,91.5 38.4,132.7 	"/>
                            <polygon class="tree_3_st3" points="28.2,79 18.1,97.1 38.2,100.6 38.2,77.2 	"/>
                            <polygon class="tree_3_st4" points="51.4,74.4 38.2,77.2 38.2,100.6 64,101.6 	"/>
                        </g>
                        <g id="_x32__part">
                            <polygon class="tree_3_st3" points="26.2,50.2 23.3,73 37.6,71.5 35,50.9 	"/>
                            <polygon class="tree_3_st1" points="38.2,91.5 37.6,71.5 23.3,73 3.1,91.5 13.9,89.8 	"/>
                            <polygon class="tree_3_st4" points="49,48.7 51.2,62.9 37.6,71.5 35,50.9 	"/>
                            <polygon class="tree_3_st2" points="66.8,89.5 51.2,62.9 37.6,71.5 38.2,91.5 	"/>
                        </g>
                        <g id="_x31__part">
                            <polygon class="tree_3_st1" points="35,0 26.7,27.9 17.5,39.5 8.2,63.2 21.6,60.5 36.9,66.5 	"/>
                            <polygon class="tree_3_st2" points="62,61.2 51.4,46.8 36.1,37 36.9,66.5 50.6,59.2 	"/>
                            <polygon class="tree_3_st3" points="36.1,37 17.5,39.5 26.7,27.9 	"/>
                            <polygon class="tree_3_st5" points="35,0 51.4,46.8 36.1,37 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="tree_6">
                <svg version="1.1" id="_x37_plan_yelka" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="38 -160.6 248.5 490.7" style="enable-background:new 38 -160.6 248.5 490.7;" xml:space="preserve">
                        <style type="text/css">
                            .tree_6_st0{fill:#966431;}
                            .tree_6_st1{fill:#7C4D23;}
                            .tree_6_st2{fill:#AEC03A;}
                            .tree_6_st3{fill:#D4E545;}
                            .tree_6_st4{fill:#8E9324;}
                            .tree_6_st5{fill:#7F7F1F;}
                            .tree_6_st6{fill:#A3AF2C;}
                            .tree_6_st7{fill:#B3C444;}
                            .tree_6_st8{fill:#D9EC5A;}
                            .tree_6_st9{fill:#B6C837;}
                            .tree_6_st10{fill:#9AA02A;}
                        </style>
                    <g id="trunk">
                        <polygon class="tree_6_tree_6_st0" points="135.1,330.8 173.4,330.8 166.9,289.8 165.1,207.4 150.6,208.9 141.4,279.2 	"/>
                        <polygon class="tree_6_st1" points="165.5,258.5 165.1,207.4 150.6,208.9 141,282.5 152.9,262.5 	"/>
                    </g>
                    <g class="tree-top">
                        <g id="_x33__part">
                            <polygon class="tree_6_st2" points="178.6,110.3 235.2,171.4 208.5,173.6 194.5,144.4 	"/>
                            <polygon class="tree_6_st3" points="289.2,239.5 235.2,171.4 208.5,173.6 224.2,240.6 	"/>
                            <polygon class="tree_6_st4" points="116.8,104.6 125.2,173.2 208.5,173.6 178.6,110.3 	"/>
                            <polygon class="tree_6_st5" points="81.5,115.7 87.3,165.1 125.2,173.2 116.8,104.6 	"/>
                            <polygon class="tree_6_st6" points="87.3,165.1 60.9,265.2 122.5,258.5 125.2,173.2 	"/>
                            <polygon class="tree_6_st7" points="122.5,258.5 125.2,173.2 208.5,173.6 224.2,240.6 	"/>
                        </g>
                        <g id="_x32_part">
                            <polygon class="tree_6_st4" points="62.7,86.2 42,143 113,151.5 113,69 78.4,57.8 	"/>
                            <polygon class="tree_6_st4" points="160.8,-10.4 186.9,71.2 115,69 122.5,20.7 	"/>
                            <polygon class="tree_6_st5" points="108.4,2.3 62.7,86.2 115,69 122.5,20.7 	"/>
                            <polygon class="tree_6_st3" points="262.5,136.4 186.9,71.2 172.7,70.4 208.1,143 	"/>
                            <polygon class="tree_6_st6" points="113,69 113,151.5 208.4,143 173.2,70.4 	"/>
                        </g>
                        <g id="_x31_part">
                            <polygon class="tree_6_st8" points="88,-160.3 182,-42.6 150,-24.3 	"/>
                            <polygon class="tree_6_st2" points="107.9,-19.3 150,-24.3 88,-160.3 	"/>
                            <polygon class="tree_6_st9" points="83,-24.3 107.9,-19.3 88,-160.3 	"/>
                            <polygon class="tree_6_st3" points="185.4,33.2 150,-24.5 182,-42.6 231,36.6 	"/>
                            <polygon class="tree_6_st6" points="107.9,-19.3 110,74 185.4,33.2 150,-24.5 	"/>
                            <polygon class="tree_6_st10" points="83,-24.3 107.9,-19.3 110,74 66.3,52.8 52.4,59.1 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="tree_7">
                <svg version="1.1" id="_x37_plan_yelka" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 101.9 254.2" style="enable-background:new 0 0 101.9 254.2;" xml:space="preserve">
                        <style type="text/css">
                            .tree_4_st0{fill:#966431;}
                            .tree_4_st1{fill:#D4E545;}
                            .tree_4_st2{fill:#B3C444;}
                            .tree_4_st3{fill:#AEC03A;}
                            .tree_4_st4{fill:#8E9324;}
                            .tree_4_st5{fill:#A3AF2C;}
                            .tree_4_st6{fill:#7F7F1F;}
                            .tree_4_st7{fill:#D9EC5A;}
                            .tree_4_st8{fill:#B6C837;}
                            .tree_4_st9{fill:#9AA02A;}
                        </style>
                    <polygon id="trunk" class="tree_4_st0" points="63.6,169.8 56,254.2 42.2,254.2 51.9,209.8 50.9,169.8 "/>
                    <g class="tree-top">
                        <g id="_x33__part">
                            <polygon class="tree_4_st1" points="77.8,161.2 101.9,195.3 56.8,196.9 52.7,158.5 55.6,118 	"/>
                            <polygon class="tree_4_st2" points="20.2,155.6 0,202.9 56.8,196.9 52.7,158.5 55.6,118 	"/>
                            <polygon class="tree_4_st3" points="77.8,161.2 52.7,158.5 55.6,118 68.5,125 	"/>
                            <polygon class="tree_4_st4" points="29.1,154.3 52.7,158.5 55.6,118 50.9,117.9 	"/>
                            <polygon class="tree_4_st5" points="0,202.9 15.2,201.3 29.1,154.3 20.2,155.6 	"/>
                            <polygon class="tree_4_st6" points="46.1,118.1 50.9,117.9 29.1,154.3 20.2,155.6 	"/>
                        </g>
                        <g id="_x32__part">
                            <polygon class="tree_4_st1" points="74.3,101.7 80.9,110.1 91.5,143.4 56,142.3 55.4,109 54.9,67.6 	"/>
                            <polygon class="tree_4_st5" points="35.2,95.2 30.9,112.2 6.7,147.1 25.9,143.4 56,142.3 55.4,109 54.9,67.6 	"/>
                            <polygon class="tree_4_st3" points="80.9,110.1 55.4,108.5 54.9,67.6 60.4,73.1 	"/>
                            <polygon class="tree_4_st6" points="30.9,112.2 55.4,108.5 54.9,67.6 37.6,74 	"/>
                        </g>
                        <g id="_x31__part">
                            <polygon class="tree_4_st7" points="54.9,0.5 66,35.5 70,67.7 55.6,67.7 	"/>
                            <polygon class="tree_4_st1" points="86.7,103 70,67.7 55.6,67.7 57.5,102.1 	"/>
                            <polygon class="tree_4_st8" points="54.9,0 46.3,36 30.4,59.9 41.9,64.6 50.7,36.7 	"/>
                            <polygon class="tree_4_st9" points="15.2,102.1 30.4,59.9 41.9,64.6 31.7,94.6 	"/>
                            <polygon class="tree_4_st5" points="57.5,102.1 31.7,94.6 41.9,64.6 55.6,66.8 	"/>
                            <polygon class="tree_4_st3" points="54.9,0 55.6,66.8 41.9,64.6 50.7,36.7 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="rocks_pre_front">
                <svg version="1.1" id="_x36_plan_yelka_x5F_ground" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     x="0px" y="0px" viewBox="-2564.6 -34.4 6009.4 238.5" style="enable-background:new -2564.6 -34.4 6009.4 238.5;"
                     xml:space="preserve">
	                    <style type="text/css">
                            .rocks_pre_front_st0{fill:#45372F;}
                            .rocks_pre_front_st1{fill:#2D1F19;}
                            .rocks_pre_front_st2{fill:#5D4636;}
                            .rocks_pre_front_st3{fill:#584C44;}
                            .rocks_pre_front_st4{fill:#585355;}
                            .rocks_pre_front_st5{fill:#554944;}
                            .rocks_pre_front_st6{fill:#8D868A;}
                        </style>
                    <polygon class="rocks_pre_front_st0" points="3106,17.1 2984,3.9 1598,204.1 3120,204.1 "/>
                    <g>
                        <g>
                            <polygon class="rocks_pre_front_st1" points="3178.2,6.1 3190.7,3.9 3344.3,0.9 3444.8,-34.4 3444.8,204.1 2918.6,204.1 3005.8,102.1 		"/>
                        </g>
                        <polygon class="rocks_pre_front_st2" points="3202.5,17.1 3269,24.4 3281.5,31.2 3265.6,30.9 3005.8,102.1 	"/>
                        <polygon class="rocks_pre_front_st3" points="3122.7,-19 3190.7,3.9 3178.2,6.1 3005.8,102.1 	"/>
                        <polygon class="rocks_pre_front_st4" points="3261.6,-27.1 3344.3,0.9 3444.8,-34.4 	"/>
                        <polygon class="rocks_pre_front_st5" points="3264.6,-27.1 3344.3,0.9 3154.1,-8.4 	"/>
                        <polygon class="rocks_pre_front_st6" points="3138.1,-13.8 3264.6,-27.1 3154.1,-8.4 	"/>
                        <polygon class="rocks_pre_front_st0" points="3154.1,-8.4 3344.3,0.9 3190.7,3.9 	"/>
                    </g>
                    <polygon class="rocks_pre_front_st3" points="2824,204.1 2760,88.5 2515.9,27.4 1172,88.5 -2564.6,-34.4 -2564.6,204.1 "/>
                    </svg>
            </div>

            <div class="tree">
                <svg version="1.1" id="_x36_plan_yelka" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 182.7 361.6" style="enable-background:new 0 0 182.7 361.6;" xml:space="preserve">
                        <style type="text/css">
                            .tree_st0{fill:#756F72;}
                            .tree_st1{fill:#605859;}
                            .tree_st2{fill:#496505;}
                            .tree_st3{fill:#607C10;}
                            .tree_st4{fill:#384D03;}
                            .tree_st5{fill:#3F5B03;}
                            .tree_st6{fill:#738D24;}
                            .tree_st7{fill:#596E12;}
                            .tree_st8{fill:#618F03;}
                            .tree_st9{fill:#3E5C01;}
                            .tree_st10{fill:#506D14;}
                            .tree_st11{fill:#618D05;}
                            .tree_st12{fill:#507502;}
                            .tree_st13{fill:#76A318;}
                            .tree_st14{fill:#598301;}
                            .tree_st15{fill:#608E02;}
                            .tree_st16{fill:#6B990E;}
                            .tree_st17{fill:#507501;}
                            .tree_st18{fill:#679509;}
                        </style>
                    <g id="trunk">
                        <polygon class="tree_st0" points="67.8,362 96.1,362 91.3,331.7 90,270.8 79.3,271.9 72.5,323.9 	"/>
                        <polygon class="tree_st1" points="90.3,308.6 90,270.8 79.3,271.9 72.2,326.3 81,311.5 	"/>
                    </g>
                    <g class="tree-top">
                        <g id="_x33__part">
                            <polygon class="tree_st2" points="100,199 141.8,244.2 122.1,245.8 111.7,224.2 	"/>
                            <polygon class="tree_st3" points="181.7,294.5 141.8,244.2 122.1,245.8 133.7,295.3 	"/>
                            <polygon class="tree_st4" points="54.3,194.8 60.5,245.5 122.1,245.8 100,199 	"/>
                            <polygon class="tree_st5" points="28.2,203 32.5,239.5 60.5,245.5 54.3,194.8 	"/>
                            <polygon class="tree_st6" points="32.5,239.5 13,313.5 58.5,308.6 60.5,245.5 	"/>
                            <polygon class="tree_st7" points="58.5,308.6 60.5,245.5 122.1,245.8 133.7,295.3 	"/>
                        </g>
                        <g id="_x32__part">
                            <polygon class="tree_st8" points="14.3,181.2 -1,223.2 51.5,229.5 51.5,168.5 25.9,160.2 	"/>
                            <polygon class="tree_st9" points="86.8,109.8 106.1,170.1 53,168.5 58.5,132.8 	"/>
                            <polygon class="tree_st10" points="48.1,119.2 14.3,181.2 53,168.5 58.5,132.8 	"/>
                            <polygon class="tree_st11" points="162,218.3 106.1,170.1 95.6,169.5 121.8,223.2 	"/>
                            <polygon class="tree_st12" points="51.5,168.5 51.5,229.5 122,223.2 96,169.5 	"/>
                        </g>
                        <g id="_x31__part">
                            <polygon class="tree_st13" points="33,-1 102.5,86 78.8,99.5 	"/>
                            <polygon class="tree_st14" points="47.7,103.2 78.8,99.5 33,-1 	"/>
                            <polygon class="tree_st15" points="29.3,99.5 47.7,103.2 33,-1 	"/>
                            <polygon class="tree_st16" points="105,142 78.8,99.4 102.5,86 138.7,144.5 	"/>
                            <polygon class="tree_st17" points="47.7,103.2 49.3,172.2 105,142 78.8,99.4 	"/>
                            <polygon class="tree_st18" points="29.3,99.5 47.7,103.2 49.3,172.2 17,156.5 6.7,161.2 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="tree_4">
                <svg version="1.1" id="_x37_plan_yelka" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 101.9 254.2" style="enable-background:new 0 0 101.9 254.2;" xml:space="preserve">
                        <style type="text/css">
                            .tree_4_st0{fill:#966431;}
                            .tree_4_st1{fill:#D4E545;}
                            .tree_4_st2{fill:#B3C444;}
                            .tree_4_st3{fill:#AEC03A;}
                            .tree_4_st4{fill:#8E9324;}
                            .tree_4_st5{fill:#A3AF2C;}
                            .tree_4_st6{fill:#7F7F1F;}
                            .tree_4_st7{fill:#D9EC5A;}
                            .tree_4_st8{fill:#B6C837;}
                            .tree_4_st9{fill:#9AA02A;}
                        </style>
                    <polygon id="trunk" class="tree_4_st0" points="63.6,169.8 56,254.2 42.2,254.2 51.9,209.8 50.9,169.8 "/>
                    <g class="tree-top">
                        <g id="_x33__part">
                            <polygon class="tree_4_st1" points="77.8,161.2 101.9,195.3 56.8,196.9 52.7,158.5 55.6,118 	"/>
                            <polygon class="tree_4_st2" points="20.2,155.6 0,202.9 56.8,196.9 52.7,158.5 55.6,118 	"/>
                            <polygon class="tree_4_st3" points="77.8,161.2 52.7,158.5 55.6,118 68.5,125 	"/>
                            <polygon class="tree_4_st4" points="29.1,154.3 52.7,158.5 55.6,118 50.9,117.9 	"/>
                            <polygon class="tree_4_st5" points="0,202.9 15.2,201.3 29.1,154.3 20.2,155.6 	"/>
                            <polygon class="tree_4_st6" points="46.1,118.1 50.9,117.9 29.1,154.3 20.2,155.6 	"/>
                        </g>
                        <g id="_x32__part">
                            <polygon class="tree_4_st1" points="74.3,101.7 80.9,110.1 91.5,143.4 56,142.3 55.4,109 54.9,67.6 	"/>
                            <polygon class="tree_4_st5" points="35.2,95.2 30.9,112.2 6.7,147.1 25.9,143.4 56,142.3 55.4,109 54.9,67.6 	"/>
                            <polygon class="tree_4_st3" points="80.9,110.1 55.4,108.5 54.9,67.6 60.4,73.1 	"/>
                            <polygon class="tree_4_st6" points="30.9,112.2 55.4,108.5 54.9,67.6 37.6,74 	"/>
                        </g>
                        <g id="_x31__part">
                            <polygon class="tree_4_st7" points="54.9,0.5 66,35.5 70,67.7 55.6,67.7 	"/>
                            <polygon class="tree_4_st1" points="86.7,103 70,67.7 55.6,67.7 57.5,102.1 	"/>
                            <polygon class="tree_4_st8" points="54.9,0 46.3,36 30.4,59.9 41.9,64.6 50.7,36.7 	"/>
                            <polygon class="tree_4_st9" points="15.2,102.1 30.4,59.9 41.9,64.6 31.7,94.6 	"/>
                            <polygon class="tree_4_st5" points="57.5,102.1 31.7,94.6 41.9,64.6 55.6,66.8 	"/>
                            <polygon class="tree_4_st3" points="54.9,0 55.6,66.8 41.9,64.6 50.7,36.7 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="tree_5">
                <svg version="1.1" id="_x36_plan_yelka" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 182.7 361.6" style="enable-background:new 0 0 182.7 361.6;" xml:space="preserve">
                        <style type="text/css">
                            .tree_st0{fill:#756F72;}
                            .tree_st1{fill:#605859;}
                            .tree_st2{fill:#496505;}
                            .tree_st3{fill:#607C10;}
                            .tree_st4{fill:#384D03;}
                            .tree_st5{fill:#3F5B03;}
                            .tree_st6{fill:#738D24;}
                            .tree_st7{fill:#596E12;}
                            .tree_st8{fill:#618F03;}
                            .tree_st9{fill:#3E5C01;}
                            .tree_st10{fill:#506D14;}
                            .tree_st11{fill:#618D05;}
                            .tree_st12{fill:#507502;}
                            .tree_st13{fill:#76A318;}
                            .tree_st14{fill:#598301;}
                            .tree_st15{fill:#608E02;}
                            .tree_st16{fill:#6B990E;}
                            .tree_st17{fill:#507501;}
                            .tree_st18{fill:#679509;}
                        </style>
                    <g id="trunk">
                        <polygon class="tree_st0" points="67.8,362 96.1,362 91.3,331.7 90,270.8 79.3,271.9 72.5,323.9 	"/>
                        <polygon class="tree_st1" points="90.3,308.6 90,270.8 79.3,271.9 72.2,326.3 81,311.5 	"/>
                    </g>
                    <g class="tree-top">
                        <g id="_x33__part">
                            <polygon class="tree_st2" points="100,199 141.8,244.2 122.1,245.8 111.7,224.2 	"/>
                            <polygon class="tree_st3" points="181.7,294.5 141.8,244.2 122.1,245.8 133.7,295.3 	"/>
                            <polygon class="tree_st4" points="54.3,194.8 60.5,245.5 122.1,245.8 100,199 	"/>
                            <polygon class="tree_st5" points="28.2,203 32.5,239.5 60.5,245.5 54.3,194.8 	"/>
                            <polygon class="tree_st6" points="32.5,239.5 13,313.5 58.5,308.6 60.5,245.5 	"/>
                            <polygon class="tree_st7" points="58.5,308.6 60.5,245.5 122.1,245.8 133.7,295.3 	"/>
                        </g>
                        <g id="_x32__part">
                            <polygon class="tree_st8" points="14.3,181.2 -1,223.2 51.5,229.5 51.5,168.5 25.9,160.2 	"/>
                            <polygon class="tree_st9" points="86.8,109.8 106.1,170.1 53,168.5 58.5,132.8 	"/>
                            <polygon class="tree_st10" points="48.1,119.2 14.3,181.2 53,168.5 58.5,132.8 	"/>
                            <polygon class="tree_st11" points="162,218.3 106.1,170.1 95.6,169.5 121.8,223.2 	"/>
                            <polygon class="tree_st12" points="51.5,168.5 51.5,229.5 122,223.2 96,169.5 	"/>
                        </g>
                        <g id="_x31__part">
                            <polygon class="tree_st13" points="33,-1 102.5,86 78.8,99.5 	"/>
                            <polygon class="tree_st14" points="47.7,103.2 78.8,99.5 33,-1 	"/>
                            <polygon class="tree_st15" points="29.3,99.5 47.7,103.2 33,-1 	"/>
                            <polygon class="tree_st16" points="105,142 78.8,99.4 102.5,86 138.7,144.5 	"/>
                            <polygon class="tree_st17" points="47.7,103.2 49.3,172.2 105,142 78.8,99.4 	"/>
                            <polygon class="tree_st18" points="29.3,99.5 47.7,103.2 49.3,172.2 17,156.5 6.7,161.2 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

        </div>

        <div class="front" data-depth="0" id="layer_7">

            <div id="bird-container"></div>
            <div id="bird-template">
                <div class="bird">
                    <svg version="1.1" id="Layer_7" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 57.7 52" style="enable-background:new 0 0 57.7 52;" xml:space="preserve">
                            <style type="text/css">
                                .bird_st0{fill:#908F8D;}
                                .bird_st1{fill:#807E7C;}
                            </style>
                        <polygon data-id="wing_2" class="bird_st0" points="45.3,10 38.8,35.3 29.2,37 "/>
                        <polygon data-id="wing_1" class="bird_st1" points="38.8,35.3 29.2,37 22.7,0.8 "/>
                        </svg>
                </div>
            </div>

            <div class="front_tree_2">
                <svg version="1.1" id="_x34_plan_tree" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="124 -877.3 420.4 1509.4" style="enable-background:new 124 -877.3 420.4 1509.4;" xml:space="preserve">
                        <style type="text/css">
                            .front_tree_2_st0{fill:#3C5605;}
                            .front_tree_2_st1{fill:#415613;}
                            .front_tree_2_st2{fill:#AA7845;}
                            .front_tree_2_st3{fill:#619101;}
                            .front_tree_2_st4{fill:#8ABD24;}
                            .front_tree_2_st5{fill:#8C8170;}
                            .front_tree_2_st6{fill:#391D01;}
                            .front_tree_2_st7{fill:#8B5B2A;}
                            .front_tree_2_st8{fill:#966431;}
                            .front_tree_2_st9{fill:#713E0C;}
                            .front_tree_2_st10{fill:#673401;}
                            .front_tree_2_st11{fill:#794613;}
                            .front_tree_2_st12{fill:#956434;}
                            .front_tree_2_st13{fill:#6E3A0A;}
                            .front_tree_2_st14{fill:#5E2F00;}
                            .front_tree_2_st15{fill:#6B9D07;}
                            .front_tree_2_st16{fill:#4F7601;}
                            .front_tree_2_st17{fill:#556B2A;}
                            .front_tree_2_st18{fill:#4D6A10;}
                            .front_tree_2_st19{fill:#3C5901;}
                            .front_tree_2_st20{fill:#45581B;}
                            .front_tree_2_st21{fill:#344C01;}
                            .front_tree_2_st22{fill:#92B946;}
                            .front_tree_2_st23{fill:#7CA922;}
                            .front_tree_2_st24{fill:#73A50F;}
                            .front_tree_2_st25{fill:#78AB12;}
                            .front_tree_2_st26{fill:#699C03;}
                            .front_tree_2_st27{fill:#608E01;}
                            .front_tree_2_st28{fill:#283A00;}
                            .front_tree_2_st29{fill:#77A716;}
                            .front_tree_2_st30{fill:#70A30A;}
                            .front_tree_2_st31{fill:#253501;}
                            .front_tree_2_st32{fill:#4C7101;}
                        </style>
                    <g>
                        <polygon class="front_tree_2_st0" points="544.4,-463.1 541.3,-585.9 462.3,-770.7 450.7,-829.9 383.3,-860.6 325.5,-846.2 282,-874.5
                                235.8,-861.9 126.7,-680.6 148.4,-552 124,-463.1 139.4,-421.6 133.4,-265.1 191.4,-123.3 261.4,-43.6 261.9,4.4 327.4,32.7
                                337.4,87.4 444.9,-43.9 512.8,-146.6 520,-222.6 496.4,-289.6 	"/>
                        <polygon class="front_tree_2_st1" points="261.4,-43.6 276.9,-100.2 191.4,-123.3 	"/>
                        <polygon class="front_tree_2_st2" points="291.1,632.1 302.1,420.7 307.9,273.3 307.9,-250.1 339.4,-250.1 348.4,168.2 343.4,384.5 348.4,632
                                "/>
                        <polygon class="front_tree_2_st2" points="343.4,384.1 330.4,367.4 348.4,167.5 	"/>
                        <polygon class="front_tree_2_st3" points="372.4,-14.6 366.9,-129.6 326.1,-161.6 	"/>
                        <polygon class="front_tree_2_st4" points="444.9,-43.9 485.9,-198.1 372.4,-16.1 	"/>
                        <polygon class="front_tree_2_st5" points="330.4,-3.1 348.4,167.5 345.4,-16.1 	"/>
                        <polygon class="front_tree_2_st6" points="308.4,273.3 319.8,122.4 317,-1.3 307.4,-3.1 	"/>
                        <polygon class="front_tree_2_st7" points="330.4,-3.1 333.1,153.1 348.4,167.5 	"/>
                        <polygon class="front_tree_2_st8" points="330.4,367.4 333.1,153.1 348.4,167.5 	"/>
                        <polygon class="front_tree_2_st9" points="319.8,122.4 330.4,-3.1 317,-1.3 	"/>
                        <polygon class="front_tree_2_st10" points="333.1,153.1 319.8,122.4 330.4,-3.1 	"/>
                        <polygon class="front_tree_2_st11" points="308.4,273.3 319.8,122.4 333.1,153.1 330.4,367.4 313.6,470.4 	"/>
                        <polygon class="front_tree_2_st12" points="343.4,384.1 330.4,367.4 328.4,631.9 348.4,631.9 	"/>
                        <polygon class="front_tree_2_st13" points="313.6,470.4 305.4,631.9 328.4,631.9 330.4,367.4 	"/>
                        <polygon class="front_tree_2_st14" points="308.4,273.3 302.4,420.7 291.1,631.9 307,631.9 313.6,470.3 	"/>
                        <polygon class="front_tree_2_st15" points="444.9,-43.9 372.4,-16.1 337.4,87.4 	"/>
                        <polygon class="front_tree_2_st16" points="326.1,-161.6 358.9,-325.6 366.9,-129.6 	"/>
                        <polygon class="front_tree_2_st17" points="261.9,4.4 280.4,-16.1 261.4,-43.6 	"/>
                        <polygon class="front_tree_2_st18" points="276.9,-100.2 280.4,-16.1 261.4,-43.6 	"/>
                        <polygon class="front_tree_2_st19" points="326.1,-161.6 280.4,-16.1 276.9,-100.2 	"/>
                        <polygon class="front_tree_2_st20" points="325.2,32.7 280.4,-16.1 261.9,4.4 	"/>
                        <polygon class="front_tree_2_st21" points="337.4,87.4 325.2,32.7 372.4,-16.1 	"/>
                        <polygon class="front_tree_2_st22" points="512.8,-146.6 485.9,-198.1 520,-222.6 	"/>
                        <polygon class="front_tree_2_st23" points="496.4,-289.6 485.9,-198.1 520,-222.6 	"/>
                        <polygon class="front_tree_2_st24" points="444.9,-43.9 485.9,-198.1 512.8,-146.6 	"/>
                        <polygon class="front_tree_2_st18" points="191.4,-123.3 326.1,-161.6 276.9,-100.2 	"/>
                        <polygon class="front_tree_2_st25" points="366.9,-129.6 485.9,-198.1 372.4,-16.1 	"/>
                        <polygon class="front_tree_2_st26" points="496.4,-289.6 366.9,-129.6 485.9,-198.1 	"/>
                        <polygon class="front_tree_2_st27" points="496.4,-289.6 358.9,-325.6 366.9,-129.6 	"/>
                        <polygon class="front_tree_2_st0" points="326.1,-161.6 256.4,-188.9 358.9,-325.6 	"/>
                        <polygon class="front_tree_2_st19" points="191.4,-123.3 256.4,-188.9 326.1,-161.6 	"/>
                        <polygon class="front_tree_2_st21" points="133.4,-265.1 256.4,-188.9 191.4,-123.3 	"/>
                        <polygon class="front_tree_2_st21" points="256.4,-188.9 238.4,-334.6 358.9,-325.6 	"/>
                        <polygon class="front_tree_2_st28" points="133.4,-265.1 238.4,-334.6 256.4,-188.9 	"/>
                        <polygon class="front_tree_2_st29" points="544.4,-463.1 496.4,-289.6 475.9,-368.8 	"/>
                        <polygon class="front_tree_2_st30" points="358.9,-325.6 475.9,-368.8 496.4,-289.6 	"/>
                        <polygon class="front_tree_2_st0" points="236.9,-334.6 356.9,-417.1 358.9,-325.6 	"/>
                        <polygon class="front_tree_2_st31" points="133.4,-265.1 139.4,-421.6 238.4,-334.6 	"/>
                        <polygon class="front_tree_2_st28" points="356.9,-417.1 238.4,-334.6 139.4,-421.6 	"/>
                        <polygon class="front_tree_2_st32" points="280.4,-16.1 326.1,-161.6 372.4,-14.6 	"/>
                        <polygon class="front_tree_2_st0" points="280.4,-16.1 372.4,-14.6 325.2,32.7 	"/>
                        <polygon class="front_tree_2_st27" points="475.9,-368.8 541.3,-585.9 544.4,-463.1 	"/>
                        <polygon class="front_tree_2_st16" points="356.9,-417.1 475.9,-368.8 358.9,-325.6 	"/>
                        <polygon class="front_tree_2_st24" points="462.3,-770.7 462.3,-538.6 541.3,-585.9 	"/>
                        <polygon class="front_tree_2_st16" points="462.3,-538.6 475.9,-368.8 541.3,-585.9 	"/>
                        <polygon class="front_tree_2_st27" points="356.9,-417.1 462.3,-538.6 475.9,-368.8 	"/>
                        <polygon class="front_tree_2_st29" points="282,-874.5 319.3,-794.6 325.5,-846.2 	"/>
                        <polygon class="front_tree_2_st16" points="383.3,-860.6 319.3,-794.6 325.5,-846.2 	"/>
                        <polygon class="front_tree_2_st27" points="383.3,-860.6 450.7,-829.9 319.3,-794.6 	"/>
                        <polygon class="front_tree_2_st16" points="319.3,-794.6 462.3,-770.7 450.7,-829.9 	"/>
                        <polygon class="front_tree_2_st16" points="319.3,-794.6 235.8,-861.9 282,-874.5 	"/>
                        <polygon class="front_tree_2_st0" points="126.7,-680.6 319.3,-794.6 235.8,-861.9 	"/>
                        <polygon class="front_tree_2_st21" points="126.7,-680.6 319.3,-794.6 286.7,-611.3 	"/>
                        <polygon class="front_tree_2_st28" points="126.7,-680.6 286.7,-611.3 148.4,-552 	"/>
                        <polygon class="front_tree_2_st21" points="124,-463.1 286.7,-611.3 148.4,-552 	"/>
                        <polygon class="front_tree_2_st31" points="356.9,-417.1 286.7,-611.3 124,-463.1 139.4,-421.6 	"/>
                        <polygon class="front_tree_2_st0" points="286.7,-611.3 462.3,-538.6 319.3,-794.6 	"/>
                        <polygon class="front_tree_2_st21" points="286.7,-611.3 462.3,-538.6 356.9,-417.1 	"/>
                        <polygon class="front_tree_2_st27" points="462.3,-770.7 462.3,-538.6 319.3,-794.6 	"/>
                    </g>
                    </svg>
            </div>

            <div class="front_tree_3">
                <svg version="1.1" id="_x34_plan_tree" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="154 -955.6 555.7 1595.7" style="enable-background:new 154 -955.6 555.7 1595.7;" xml:space="preserve">
                        <style type="text/css">
                            .front_tree_3_st0{fill:#364F01;}
                            .front_tree_3_st1{fill:#A16E3B;}
                            .front_tree_3_st2{fill:#9B6836;}
                            .front_tree_3_st3{fill:#619001;}
                            .front_tree_3_st4{fill:#608F01;}
                            .front_tree_3_st5{fill:#B68350;}
                            .front_tree_3_st6{fill:#7F4E19;}
                            .front_tree_3_st7{fill:#654220;}
                            .front_tree_3_st8{fill:#915E2C;}
                            .front_tree_3_st9{fill:#B88552;}
                            .front_tree_3_st10{fill:#7A4C1E;}
                            .front_tree_3_st11{fill:#93602D;}
                            .front_tree_3_st12{fill:#486B01;}
                            .front_tree_3_st13{fill:#5B8603;}
                            .front_tree_3_st14{fill:#78A717;}
                            .front_tree_3_st15{fill:#7CAA1E;}
                            .front_tree_3_st16{fill:#517801;}
                            .front_tree_3_st17{fill:#486C01;}
                            .front_tree_3_st18{fill:#4E7301;}
                            .front_tree_3_st19{fill:#426003;}
                            .front_tree_3_st20{fill:#385302;}
                            .front_tree_3_st21{fill:#314704;}
                            .front_tree_3_st22{fill:#6FA00C;}
                            .front_tree_3_st23{fill:#5A8600;}
                            .front_tree_3_st24{fill:#79AC13;}
                            .front_tree_3_st25{fill:#273901;}
                            .front_tree_3_st26{fill:#578201;}
                            .front_tree_3_st27{fill:#729D26;}
                            .front_tree_3_st28{fill:#293B01;}
                            .front_tree_3_st29{fill:#2C3D04;}
                            .front_tree_3_st30{fill:#436202;}
                            .front_tree_3_st31{fill:#88B222;}
                        </style>
                    <g>
                        <polygon class="front_tree_3_st0" points="677.2,-653.5 623.8,-676.8 633.3,-755 552,-819.6 498,-815 440,-885 342.4,-948.9 227.4,-913.9
                                234.2,-763.4 178.9,-656.3 184,-536.3 161.4,-539.3 240.2,-299.8 292.3,-250.7 336.3,-146.7 367.5,-119.1 428.2,-65.7
                                538.2,-182.7 552.6,-250.7 608.6,-274 703.6,-436.7 709.6,-470.2 	"/>
                        <polygon class="front_tree_3_st1" points="365.6,-215.2 371.6,111.6 383.6,225.3 374.6,518 370.9,640.6 452.2,640.8 431.9,236.4 422.8,94.9
                                429.7,-108.7 433.6,-215.2 	"/>
                        <polygon class="front_tree_3_st2" points="452.2,640.8 431.9,236.4 425.1,266.6 424.4,640.8 	"/>
                        <polygon class="front_tree_3_st3" points="526.6,-220.7 464.9,-196.3 429.7,-108.7 	"/>
                        <polygon class="front_tree_3_st4" points="450.1,-264.2 508.2,-304.2 464.9,-196.3 	"/>
                        <polygon class="front_tree_3_st5" points="422.8,95.8 433.6,-215.2 410.6,-215.2 	"/>
                        <polygon class="front_tree_3_st1" points="410.6,-215.2 383.6,-215.2 403.5,146 422.8,95.3 	"/>
                        <polygon class="front_tree_3_st6" points="371.6,112.6 386.4,66.2 384,-215.2 365.6,-215.2 	"/>
                        <polygon class="front_tree_3_st7" points="383.6,225.3 386.4,64.7 371.6,111.6 	"/>
                        <polygon class="front_tree_3_st8" points="386.4,64.7 383.6,-217.2 403.5,145 	"/>
                        <polygon class="front_tree_3_st9" points="431.9,236.4 422.8,94.9 401.5,145 425.1,267.7 	"/>
                        <polygon class="front_tree_3_st10" points="386.4,64.7 403.5,145 399.9,472.3 374.6,518 383.6,225.3 	"/>
                        <polygon class="front_tree_3_st11" points="425.1,266.4 403,145 399.2,472.3 424.4,640.8 	"/>
                        <polygon class="front_tree_3_st12" points="402.5,-178.5 429.7,-108.7 397,-146.8 	"/>
                        <polygon class="front_tree_3_st13" points="367.5,-119.1 336.3,-146.7 397,-146.8 	"/>
                        <polygon class="front_tree_3_st14" points="526.6,-220.7 552.6,-250.7 538.2,-182.7 	"/>
                        <polygon class="front_tree_3_st15" points="428.2,-65.7 429.7,-108.7 538.2,-182.7 	"/>
                        <polygon class="front_tree_3_st16" points="450.1,-264.2 429.7,-108.7 402.5,-178.5 	"/>
                        <polygon class="front_tree_3_st17" points="397,-146.8 370.2,-227.9 402.5,-178.5 	"/>
                        <polygon class="front_tree_3_st18" points="336.3,-146.7 397,-146.8 347.2,-184.4 	"/>
                        <polygon class="front_tree_3_st19" points="338.8,-245.8 397,-146.8 347.2,-184.4 	"/>
                        <polygon class="front_tree_3_st20" points="370.2,-227.9 338.8,-245.8 397,-146.8 	"/>
                        <polygon class="front_tree_3_st21" points="292.3,-250.7 347.2,-184.4 336.3,-146.7 	"/>
                        <polygon class="front_tree_3_st22" points="526.6,-220.7 429.7,-108.7 538.2,-182.7 	"/>
                        <polygon class="front_tree_3_st23" points="464.9,-196.3 450.1,-264.2 429.7,-108.7 	"/>
                        <polygon class="front_tree_3_st24" points="552.6,-250.7 508.2,-304.2 464.9,-196.3 526.6,-220.7 	"/>
                        <polygon class="front_tree_3_st25" points="292.3,-250.7 311.6,-281.7 338.8,-245.8 347.2,-184.4 	"/>
                        <polygon class="front_tree_3_st20" points="311.6,-281.7 271.8,-375.7 240.2,-301.3 292.3,-250.7 	"/>
                        <polygon class="front_tree_3_st25" points="333.2,-350.3 271.8,-375.7 311.6,-281.7 338.8,-245.8 	"/>
                        <polygon class="front_tree_3_st26" points="552.6,-250.7 581.6,-298 608.6,-274 	"/>
                        <polygon class="front_tree_3_st27" points="703.6,-436.7 581.6,-298 608.6,-274 	"/>
                        <polygon class="front_tree_3_st14" points="508.2,-304.2 524.9,-380.7 585.9,-390.7 581.6,-298 552.6,-250.7 	"/>
                        <polygon class="front_tree_3_st24" points="703.6,-436.7 709.6,-470.2 585.9,-390.7 581.6,-295.8 	"/>
                        <polygon class="front_tree_3_st16" points="450.1,-264.2 431.6,-351.2 480.6,-436.7 524.9,-380.7 508.2,-304.2 	"/>
                        <polygon class="front_tree_3_st4" points="571.3,-653.5 585.9,-390.7 524.9,-380.7 480.6,-436.7 	"/>
                        <polygon class="front_tree_3_st28" points="333.2,-350.3 275.9,-436.7 271.8,-375.7 	"/>
                        <polygon class="front_tree_3_st21" points="161.4,-539.3 275.9,-436.7 271.8,-374.7 240.2,-299.8 	"/>
                        <polygon class="front_tree_3_st29" points="275.9,-436.7 284,-647.6 365.6,-436.2 333.2,-348.3 	"/>
                        <polygon class="front_tree_3_st21" points="365.6,-436.2 402.5,-286.7 370.2,-227.9 338.8,-245.8 333.2,-350.3 	"/>
                        <polygon class="front_tree_3_st21" points="431.6,-351.2 402.5,-286.7 365.6,-436.2 	"/>
                        <polygon class="front_tree_3_st30" points="402.5,-178.5 370.2,-227.9 402.5,-286.7 431.6,-351.2 450.1,-264.2 	"/>
                        <polygon class="front_tree_3_st7" points="374.6,518 370.9,640.6 399.9,472.3 	"/>
                        <polygon class="front_tree_3_st10" points="424.4,640.8 399.9,472.3 370.9,640.6 	"/>
                        <polygon class="front_tree_3_st31" points="585.9,-390.7 677.2,-653.5 709.6,-470.2 	"/>
                        <polygon class="front_tree_3_st26" points="623.8,-676.8 585.9,-390.7 677.2,-653.5 	"/>
                        <polygon class="front_tree_3_st27" points="552,-819.6 571.3,-653.5 623.8,-676.8 633.3,-755 	"/>
                        <polygon class="front_tree_3_st27" points="623.8,-676.8 585.9,-390.7 571.3,-653.5 	"/>
                        <polygon class="front_tree_3_st26" points="498,-815 571.3,-653.5 552,-819.6 	"/>
                        <polygon class="front_tree_3_st26" points="440,-885 227.4,-913.9 342.4,-948.9 	"/>
                        <path class="front_tree_3_st26" d="M410.1-664.3"/>
                        <polygon class="front_tree_3_st28" points="480.6,-436.7 431.6,-351.2 365.6,-436.2 370.2,-716.3 	"/>
                        <polygon class="front_tree_3_st16" points="370.2,-716.3 571.3,-653.5 480.6,-436.7 	"/>
                        <polygon class="front_tree_3_st24" points="440,-885 571.3,-653.5 498,-815 	"/>
                        <polygon class="front_tree_3_st27" points="370.2,-716.3 440,-885 571.3,-653.5 	"/>
                        <polygon class="front_tree_3_st4" points="370.2,-716.3 227.4,-913.9 440,-885 	"/>
                        <polygon class="front_tree_3_st16" points="234.2,-763.4 370.2,-716.3 227.4,-913.9 	"/>
                        <polygon class="front_tree_3_st16" points="161.4,-539.3 184,-536.3 275.9,-436.7 	"/>
                        <polygon class="front_tree_3_st20" points="178.9,-656.3 275.9,-436.7 184,-536.3 	"/>
                        <polygon class="front_tree_3_st28" points="234.2,-763.4 284,-647.6 275.9,-436.7 178.9,-656.3 	"/>
                        <polygon class="front_tree_3_st20" points="234.2,-763.4 284,-647.6 370.2,-716.3 	"/>
                        <polygon class="front_tree_3_st21" points="370.2,-716.3 365.6,-436.2 284,-647.6 	"/>
                    </g>
                    </svg>
            </div>

            <div class="front_tree_4">
                <svg version="1.1" id="_x35_plan_tree" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="147.5 -817.9 497 1458" style="enable-background:new 147.5 -817.9 497 1458;" xml:space="preserve">
                        <style type="text/css">
                            .front_tree_4_st0{fill:#76A910;}
                            .front_tree_4_st1{fill:#905D2A;}
                            .front_tree_4_st2{fill:#BE8A58;}
                            .front_tree_4_st3{fill:#C2A384;}
                            .front_tree_4_st4{fill:#CC9967;}
                            .front_tree_4_st5{fill:#9F764C;}
                            .front_tree_4_st6{fill:#946637;}
                            .front_tree_4_st7{fill:#A3703E;}
                            .front_tree_4_st8{fill:#9B6835;}
                            .front_tree_4_st9{fill:#D29F6C;}
                            .front_tree_4_st10{fill:#BE8B58;}
                            .front_tree_4_st11{fill:#CC9966;}
                            .front_tree_4_st12{fill:#BC8956;}
                            .front_tree_4_st13{fill:#7D4A17;}
                            .front_tree_4_st14{fill:#7C5935;}
                            .front_tree_4_st15{fill:#6C4115;}
                            .front_tree_4_st16{fill:#537901;}
                            .front_tree_4_st17{fill:#516501;}
                            .front_tree_4_st18{fill:#78953D;}
                            .front_tree_4_st19{fill:#71A112;}
                            .front_tree_4_st20{fill:#5C8902;}
                            .front_tree_4_st21{fill:#90B04F;}
                            .front_tree_4_st22{fill:#6C8E28;}
                            .front_tree_4_st23{fill:#96C632;}
                            .front_tree_4_st24{fill:#669801;}
                            .front_tree_4_st25{fill:#5C8901;}
                            .front_tree_4_st26{fill:#82B51C;}
                            .front_tree_4_st27{fill:#8EB53D;}
                            .front_tree_4_st28{fill:#7EAE1C;}
                            .front_tree_4_st29{fill:#669703;}
                            .front_tree_4_st30{fill:#9BBB5C;}
                            .front_tree_4_st31{fill:#70A30A;}
                            .front_tree_4_st32{fill:#5B8501;}
                            .front_tree_4_st33{fill:#639205;}
                            .front_tree_4_st34{fill:#6B9E04;}
                            .front_tree_4_st35{fill:#A5D741;}
                            .front_tree_4_st36{fill:#94C62E;}
                            .front_tree_4_st37{fill:#6F9F0D;}
                            .front_tree_4_st38{fill:#8EC128;}
                            .front_tree_4_st39{fill:#588201;}
                        </style>
                    <g>
                        <polygon class="front_tree_4_st0" points="567.5,-608.3 505.3,-744.4 429.1,-804.3 321.6,-815.3 307.1,-784.1 214.1,-764.2 209.6,-696.5
                151.1,-469.5 225.2,-300.6 247,-199.4 319.8,-157.2 324.1,-58.4 352.8,-36.2 378.1,-66.7 398.1,-110 441.1,-16 482.5,-65
                520.1,-134 560.1,-182 607.6,-435.5 642,-506.9 	"/>
                        <polygon class="front_tree_4_st1" points="372.5,258.9 351.6,103 345.8,-88.3 363.9,-93.8 370.6,99 382.1,182.1 	"/>
                        <polygon class="front_tree_4_st2" points="384.7,212.5 370.6,99 360.2,121 	"/>
                        <polygon class="front_tree_4_st3" points="370.6,99 365.5,-106.6 357.2,-89.7 	"/>
                        <polygon class="front_tree_4_st4" points="360.2,121 370.6,99 357.2,-89.7 345.8,-88.3 	"/>
                        <polygon class="front_tree_4_st5" points="351.6,103 360.2,121 345.8,-88.3 	"/>
                        <polygon class="front_tree_4_st6" points="351.6,103 360.2,121 374,270.5 	"/>
                        <polygon class="front_tree_4_st7" points="384.7,212.5 374,270.5 360.2,121 	"/>
                        <polygon class="front_tree_4_st1" points="453.1,607 453.1,348 453.1,220 440.1,-25 440.1,-256.5 387.1,-256.5 387.1,-58 379.3,176.6 342.5,625
                417.2,640.1 	"/>
                        <polygon class="front_tree_4_st1" points="386.1,-196.8 397.3,-110 399,125.5 377.2,216.5 386.1,-58 	"/>
                        <polygon class="front_tree_4_st8" points="406.6,-209 424.1,186 399.3,226 399.2,125.5 398.1,-110 387.1,-196.8 	"/>
                        <polygon class="front_tree_4_st9" points="441.1,-16 424.1,-182 406.6,-209 429.1,30.7 444.7,53 	"/>
                        <polygon class="front_tree_4_st10" points="424.1,186 429.1,30.7 406.6,-209 	"/>
                        <polygon class="front_tree_4_st11" points="452.1,220 444,53 428.7,30.7 441.2,228.5 424.8,477 452.1,418 	"/>
                        <polygon class="front_tree_4_st12" points="424.1,186 429,30.7 441.3,228.5 425.4,477 	"/>
                        <polygon class="front_tree_4_st13" points="399.2,125.5 399.3,226 370.1,637 377.8,216.5 	"/>
                        <polygon class="front_tree_4_st14" points="377.8,216.5 372.1,267.1 342.6,625 370.1,637 	"/>
                        <polygon class="front_tree_4_st1" points="424.1,186 399.3,549.5 370.1,637 399.3,226 	"/>
                        <polygon class="front_tree_4_st15" points="425.4,477 417.5,640.1 370.1,637 399.3,549.5 424.1,186 	"/>
                        <polygon class="front_tree_4_st12" points="453.6,607 451.6,418 425.4,477 417.5,640.1 	"/>
                        <polygon class="front_tree_4_st16" points="394.1,-155 432.3,-65 398.1,-110 	"/>
                        <polygon class="front_tree_4_st17" points="378.1,-66.7 398.1,-110 394.1,-155 	"/>
                        <polygon class="front_tree_4_st18" points="352.8,-36.2 339.6,-73.3 324.1,-58.4 	"/>
                        <polygon class="front_tree_4_st19" points="378.1,-66.7 352.8,-36.2 358.4,-104.2 	"/>
                        <polygon class="front_tree_4_st20" points="394.1,-155 358.4,-104.2 378.1,-66.7 	"/>
                        <polygon class="front_tree_4_st21" points="319.8,-157.2 339.6,-73.3 324.1,-58.4 	"/>
                        <polygon class="front_tree_4_st22" points="358.4,-104.2 339.6,-73.3 352.8,-36.2 	"/>
                        <polygon class="front_tree_4_st21" points="432.3,-65 441.1,-16 482.5,-65 452.1,-42.5 	"/>
                        <polygon class="front_tree_4_st23" points="520.1,-134 482.5,-65 452.1,-42.5 	"/>
                        <polygon class="front_tree_4_st24" points="424.1,-182 467.6,-104.2 452.1,-42.5 432.3,-65 	"/>
                        <polygon class="front_tree_4_st25" points="394.1,-155 401.8,-206 424.1,-182 432.3,-65 	"/>
                        <polygon class="front_tree_4_st26" points="467.6,-104.2 520.1,-134 452.1,-42.5 	"/>
                        <polygon class="front_tree_4_st27" points="319.8,-157.2 274.6,-230.8 247,-199.4 	"/>
                        <polygon class="front_tree_4_st28" points="358.4,-104.2 352.8,-204.5 319.8,-157.2 339.6,-73.3 	"/>
                        <polygon class="front_tree_4_st29" points="401.8,-206 376.3,-233 352.8,-204.5 358.4,-104.2 394.1,-155 	"/>
                        <polygon class="front_tree_4_st30" points="274.6,-230.8 225.4,-300.6 247,-199.4 	"/>
                        <polygon class="front_tree_4_st31" points="352.8,-204.5 363.1,-263.5 308.6,-323 274.6,-230.8 319.8,-157.2 	"/>
                        <polygon class="front_tree_4_st32" points="376.3,-233 363.1,-263.5 352.8,-204.5 	"/>
                        <polygon class="front_tree_4_st33" points="424.1,-182 374.6,-296 401.8,-206 	"/>
                        <polygon class="front_tree_4_st34" points="363.1,-263.5 374.6,-296 401.8,-206 376.3,-233 	"/>
                        <polygon class="front_tree_4_st35" points="467.6,-104.2 560.1,-182 520.1,-134 	"/>
                        <polygon class="front_tree_4_st26" points="486.1,-271.5 467.6,-104.2 424.1,-182 	"/>
                        <polygon class="front_tree_4_st36" points="486.1,-271.5 551.8,-304.3 560.1,-182 467.6,-104.2 	"/>
                        <polygon class="front_tree_4_st35" points="607.6,-435.5 551.8,-304.3 560.1,-182 	"/>
                        <polygon class="front_tree_4_st37" points="486.1,-271.5 364.6,-428.8 374.6,-296 424.1,-182 	"/>
                        <polygon class="front_tree_4_st38" points="607.6,-435.5 364.6,-428.8 486.1,-271.5 551.8,-304.3 	"/>
                        <polygon class="front_tree_4_st39" points="309.1,-323 151.1,-469.5 364.8,-428.8 374.7,-296 363.4,-263.5 	"/>
                        <polygon class="front_tree_4_st39" points="364.8,-428.8 262.9,-515.9 300,-674.9 	"/>
                        <polygon class="front_tree_4_st26" points="364.8,-428.8 642,-506.9 607.6,-435.5 	"/>
                        <polygon class="front_tree_4_st35" points="642,-506.9 486.1,-568.3 567.5,-608.3 	"/>
                        <polygon class="front_tree_4_st36" points="364.8,-428.8 486.1,-568.3 642,-506.9 	"/>
                        <polygon class="front_tree_4_st32" points="209.6,-696.5 262.9,-515.9 151.1,-469.5 	"/>
                        <polygon class="front_tree_4_st25" points="364.8,-428.8 151.1,-469.5 262.9,-515.9 	"/>
                        <polygon class="front_tree_4_st29" points="274.6,-230.8 247,-334.9 151.1,-469.5 309.1,-323 	"/>
                        <polygon class="front_tree_4_st29" points="307.1,-784.1 300,-674.9 214.1,-764.2 	"/>
                        <polygon class="front_tree_4_st0" points="262.9,-515.9 209.6,-696.5 214.1,-764.2 	"/>
                        <polygon class="front_tree_4_st25" points="214.1,-764.2 262.9,-515.9 300,-674.9 	"/>
                        <polygon class="front_tree_4_st26" points="486.1,-568.3 300,-674.9 364.8,-428.8 	"/>
                        <polygon class="front_tree_4_st26" points="393,-775.6 321.6,-815.3 429.1,-804.3 	"/>
                        <polygon class="front_tree_4_st35" points="429.1,-703.6 393,-775.6 429.1,-804.3 505.3,-744.4 	"/>
                        <polygon class="front_tree_4_st36" points="486.1,-568.3 429.1,-703.6 505.3,-744.4 567.5,-608.3 	"/>
                        <polygon class="front_tree_4_st26" points="300,-674.9 393,-775.6 429.1,-703.6 	"/>
                    </g>
                    </svg>
            </div>

            <div class="front_tree_cat">
                <svg version="1.1" id="plan_tree_with_cat" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     x="0px" y="0px" viewBox="295 -1137.7 785.2 1749.8" style="enable-background:new 295 -1137.7 785.2 1749.8;"
                     xml:space="preserve">
                        <style type="text/css">
                            .front_tree_cat_st0{fill:#213001;}
                            .front_tree_cat_st1{fill:#462401;}
                            .front_tree_cat_st2{fill:#253501;}
                            .front_tree_cat_st3{fill:#1D2A01;}
                            .front_tree_cat_st4{fill:#151E01;}
                            .front_tree_cat_st5{fill:#1E2C01;}
                            .front_tree_cat_st6{fill:#4C3901;}
                            .front_tree_cat_st7{fill:#231300;}
                            .front_tree_cat_st8{fill:#371D02;}
                            .front_tree_cat_st9{fill:#3A3505;}
                            .front_tree_cat_st10{fill:#8B6C2D;}
                            .front_tree_cat_st11{fill:#5B3206;}
                            .front_tree_cat_st12{fill:#1A0E01;}
                            .front_tree_cat_st13{opacity:0.8;fill:#A79941;enable-background:new    ;}
                            .front_tree_cat_st14{fill:#6C8006;}
                            .front_tree_cat_st15{fill:#392001;}
                            .front_tree_cat_st16{fill:#2C1803;}
                            .front_tree_cat_st17{fill:#372001;}
                            .front_tree_cat_st18{fill:#5F3101;}
                            .front_tree_cat_st19{fill:#2D1801;}
                            .front_tree_cat_st20{fill:#321D07;}
                            .front_tree_cat_st21{fill:#2C311C;}
                            .front_tree_cat_st22{fill:#1E1916;}
                            .front_tree_cat_st23{opacity:0.8;fill:#A1703F;enable-background:new    ;}
                            .front_tree_cat_st24{fill:#3D2001;}
                            .front_tree_cat_st25{fill:#44290E;}
                            .front_tree_cat_st26{fill:#37230E;}
                            .front_tree_cat_st27{fill:#251708;}
                            .front_tree_cat_st28{fill:none;stroke:#FF0000;stroke-miterlimit:10;}
                            .front_tree_cat_st29{fill:#987451;}
                            .front_tree_cat_st30{fill:#23201C;}
                            .front_tree_cat_st31{fill:#75A70E;}
                            .front_tree_cat_st32{fill:#304701;}
                            .front_tree_cat_st33{fill:#699D02;}
                            .front_tree_cat_st34{fill:#192301;}
                            .front_tree_cat_st35{fill:#1C2802;}
                            .front_tree_cat_st36{fill:#1A2601;}
                            .front_tree_cat_st37{fill:#283A00;}
                            .front_tree_cat_st38{fill:#223101;}
                            .front_tree_cat_st39{fill:#313A03;}
                            .front_tree_cat_st40{fill:#252E12;}
                            .front_tree_cat_st41{fill:#375103;}
                            .front_tree_cat_st42{fill:#1F2D01;}
                            .front_tree_cat_st43{fill:#273901;}
                            .front_tree_cat_st44{fill:#121901;}
                            .front_tree_cat_st45{fill:#192201;}
                            .front_tree_cat_st46{fill:#1E2B01;}
                            .front_tree_cat_st47{fill:#141D01;}
                            .front_tree_cat_st48{fill:#0E1401;}
                            .front_tree_cat_st49{fill:#192105;}
                            .front_tree_cat_st50{fill:#3A5600;}
                            .front_tree_cat_st51{fill:#243501;}
                            .front_tree_cat_st52{fill:#1D2A03;}
                            .front_tree_cat_st53{fill:#101701;}
                            .front_tree_cat_st54{fill:#141D02;}
                            .front_tree_cat_st55{fill:#2E1801;}
                            .front_tree_cat_st56{fill:#3D2501;}
                            .front_tree_cat_st57{fill:#4C2904;}
                            .front_tree_cat_st58{fill:#422E01;}
                            .front_tree_cat_st59{fill:#341C02;}
                            .front_tree_cat_st60{fill:#533200;}
                            .front_tree_cat_st61{fill:#415F00;}
                            .front_tree_cat_st62{fill:#693602;}
                            .front_tree_cat_st63{fill:#261400;}
                            .front_tree_cat_st64{fill:#182202;}
                            .front_tree_cat_st65{fill:#1A2301;}
                            .front_tree_cat_st66{opacity:0.5;fill:#FFFFFF;enable-background:new    ;}
                            .front_tree_cat_st67{opacity:0.15;fill:#FFFFFF;enable-background:new    ;}
                            .front_tree_cat_st68{opacity:7.000000e-002;fill:#FFFFFF;enable-background:new    ;}
                            .front_tree_cat_st69{fill:#DD0000;}
                            .front_tree_cat_st70{fill:#353535;}
                            .front_tree_cat_st71{fill:#212121;}
                            .front_tree_cat_st72{fill:#898989;}
                            .front_tree_cat_st73{fill:#FFFFFF;}
                            .front_tree_cat_st74{fill:#DBDBDB;}
                            .front_tree_cat_st75{fill:#A80000;}
                            .front_tree_cat_st76{opacity:0.3;fill:#FFFFFF;enable-background:new    ;}
                            .front_tree_cat_st77{opacity:0.81;fill:#FFFFFF;enable-background:new    ;}
                            .front_tree_cat_st78{fill:#F4F4F4;}
                        </style>
                    <g id="tree">
                        <polygon class="front_tree_cat_st0" points="976,-641.8 1021,-706.8 1021,-931.8 879.8,-1045.7 808.9,-1054.4 682.1,-1137.7 591.9,-1126.6
                534,-1034.8 475,-993.8 412,-1014.8 310.7,-830.8 346.6,-711.3 313.1,-615.5 301,-441.9 427.8,-300.7 450.2,-189.3 511.4,-166.4
                569.6,-99.4 618.3,-90.8 678.9,-146.3 785,-115.7 917.5,-172.3 982.1,-281.5 1051.6,-361.4 1074.2,-441.9 	"/>
                        <polygon class="front_tree_cat_st1" points="699.9,-25.3 757.3,-63.9 818.3,-158.6 806.8,-157.1 801.7,-172.3 740.6,-94.9 692.6,-73.4
                678.9,-60.7 684.3,-26.1 	"/>
                        <polygon class="front_tree_cat_st2" points="712.1,-165.7 769.6,-211.9 827.6,-196.5 785,-115.7 	"/>
                        <polygon class="front_tree_cat_st3" points="861.6,-273.9 769.6,-209.9 897.6,-209.9 	"/>
                        <polygon class="front_tree_cat_st4" points="618.3,-339.9 627.6,-204.6 638.6,-76.2 633.1,301 616.6,440.8 622.6,583.9 632.6,597.6 684.6,612.1
                726.6,602.3 704.6,121.6 712.6,92.6 701.6,-160.6 715.7,-339.9 	"/>
                        <polygon class="front_tree_cat_st5" points="769.6,-211.9 671.6,-246.9 712.1,-165.7 	"/>
                        <polygon class="front_tree_cat_st6" points="692.6,-73.4 672.5,-85.8 682.1,-221.4 	"/>
                        <polygon class="front_tree_cat_st7" points="699.9,-25.3 684.3,-26.1 695.6,80.6 	"/>
                        <polygon class="front_tree_cat_st8" points="671.3,-38.8 675.3,31.9 695.6,80.6 	"/>
                        <polygon class="front_tree_cat_st9" points="589.4,-220.9 591.3,-142.3 620.5,1 641.1,34.9 641.1,80.6 606.3,13.6 579.1,-139.9 579.1,-220.9 	"/>
                        <polygon class="front_tree_cat_st6" points="606.6,13.6 608.6,-17.4 620.3,0.4 640.4,78.8 	"/>
                        <polygon class="front_tree_cat_st10" points="619.3,-0.6 640.1,79.6 640.1,33.6 	"/>
                        <polygon class="front_tree_cat_st11" points="712.6,92.6 705.2,-79 692.6,-73.4 699.9,-25.3 695.6,80.6 	"/>
                        <polygon class="front_tree_cat_st12" points="684.3,-26.1 678.9,-60.7 671.6,-38.3 695.6,80.6 	"/>
                        <polygon class="front_tree_cat_st13" points="705.2,-79 692.6,-73.4 689.2,-121.1 	"/>
                        <polygon class="front_tree_cat_st14" points="682.1,-221.4 705.2,-79 689.2,-121.1 	"/>
                        <polygon class="front_tree_cat_st15" points="672.5,-85.8 678.9,-60.7 692.6,-73.4 	"/>
                        <polygon class="front_tree_cat_st16" points="704.6,121.6 695.6,80.6 712.6,92.6 	"/>
                        <polygon class="front_tree_cat_st4" points="638.6,-76.6 628.3,-196.5 642.8,-216.6 	"/>
                        <polygon class="front_tree_cat_st17" points="671.3,-36.8 655.6,-219.9 681.6,-219.9 	"/>
                        <polygon class="front_tree_cat_st18" points="695.6,80.6 687.6,302.6 705.6,262.8 704.6,121.6 	"/>
                        <polygon class="front_tree_cat_st19" points="675.3,31.9 695.6,80.6 687.6,302.6 	"/>
                        <polygon class="front_tree_cat_st20" points="638.6,-76.6 633.1,300.8 652.4,266.6 	"/>
                        <polygon class="front_tree_cat_st21" points="642.8,-216.6 655.6,-221.9 652.4,266.6 638.6,-76.6 	"/>
                        <polygon class="front_tree_cat_st1" points="671.3,-38.8 655.6,-221.9 652.4,266.6 687.6,302.6 	"/>
                        <polygon class="front_tree_cat_st22" points="616.6,440.7 652.4,266.6 633.1,300.8 	"/>
                        <polygon class="front_tree_cat_st23" points="704.6,121.6 726.6,602.3 687.6,302.6 705.6,262.8 	"/>
                        <polygon class="front_tree_cat_st24" points="652.4,266.6 682.1,612.1 726.6,602.3 687.6,302.6 	"/>
                        <polygon class="front_tree_cat_st25" points="622.6,583.9 652.4,266.6 616.6,440.7 	"/>
                        <polygon class="front_tree_cat_st26" points="632.6,597.6 651.9,269.2 622.6,583.9 	"/>
                        <polygon class="front_tree_cat_st27" points="682.1,612.1 652.4,266.6 632.6,597.6 	"/>
                        <polygon class="front_tree_cat_st28" points="902,-203.2 906.6,-194.1 888,-181.4 901,-196.6 895.1,-199.6 	"/>
                        <polygon class="front_tree_cat_st29" points="591.3,-143.1 620.3,0.4 608.6,-17.4 	"/>
                        <polygon class="front_tree_cat_st30" points="579.4,-140.4 608.6,-17.4 606.6,13.6 	"/>
                        <polygon class="front_tree_cat_st9" points="584.6,-156.1 591.3,-143.1 608.6,-17.4 579.4,-140.4 	"/>
                        <polygon class="front_tree_cat_st31" points="917.5,-172.3 916.6,-200.9 942.6,-238.9 954.7,-235.3 	"/>
                        <polygon class="front_tree_cat_st32" points="916.6,-200.9 942.6,-238.9 897.6,-211.9 	"/>
                        <polygon class="front_tree_cat_st33" points="982.1,-281.5 942.6,-238.9 954.7,-235.3 	"/>
                        <polygon class="front_tree_cat_st34" points="897.6,-209.9 769.6,-209.9 827.6,-194.5 	"/>
                        <polygon class="front_tree_cat_st35" points="827.6,-196.5 785,-115.7 840.6,-160.4 	"/>
                        <polygon class="front_tree_cat_st36" points="917.5,-172.3 827.6,-196.5 840.6,-160.4 	"/>
                        <polygon class="front_tree_cat_st37" points="785,-115.7 917.5,-172.3 840.6,-160.4 	"/>
                        <polygon class="front_tree_cat_st38" points="678.9,-146.3 712.1,-165.7 785,-115.7 	"/>
                        <polygon class="front_tree_cat_st39" points="618.3,-90.8 584.6,-126.4 569.6,-99.4 	"/>
                        <polygon class="front_tree_cat_st40" points="678.9,-146.3 618.3,-90.8 584.6,-126.4 	"/>
                        <polygon class="front_tree_cat_st41" points="982.1,-281.5 937.6,-275.9 942.3,-238.9 	"/>
                        <polygon class="front_tree_cat_st42" points="511.4,-166.4 584.6,-126.4 569.6,-99.4 	"/>
                        <polygon class="front_tree_cat_st43" points="678.9,-146.3 588.2,-206.3 584.6,-126.4 	"/>
                        <polygon class="front_tree_cat_st4" points="712.1,-165.7 671.6,-246.9 652.4,-163.8 678.9,-146.3 	"/>
                        <polygon class="front_tree_cat_st44" points="450.2,-189.4 588.2,-206.3 511.4,-166.4 	"/>
                        <polygon class="front_tree_cat_st42" points="427.8,-300.8 507.6,-357.9 491.6,-243.9 450.2,-189.4 	"/>
                        <polygon class="front_tree_cat_st45" points="588.2,-206.3 553.1,-258.4 491.6,-243.9 450.2,-189.4 	"/>
                        <polygon class="front_tree_cat_st46" points="507.6,-357.9 526.6,-295.4 491.6,-243.9 	"/>
                        <polygon class="front_tree_cat_st47" points="553.1,-258.4 614.1,-316.4 526.6,-295.4 491.6,-243.9 	"/>
                        <polygon class="front_tree_cat_st48" points="614.1,-316.4 671.6,-246.9 588.2,-206.3 553.1,-258.4 	"/>
                        <polygon class="front_tree_cat_st49" points="427.8,-300.8 301,-441.9 386,-525.8 480.2,-441.9 507.6,-357.9 	"/>
                        <polygon class="front_tree_cat_st36" points="861.6,-273.9 959.8,-355.9 937.6,-273.9 	"/>
                        <polygon class="front_tree_cat_st50" points="982.1,-281.5 1051.6,-362.4 959.8,-357.9 	"/>
                        <polygon class="front_tree_cat_st43" points="937.6,-275.9 959.8,-357.9 982.1,-281.5 	"/>
                        <polygon class="front_tree_cat_st51" points="769.6,-211.9 794.1,-421.9 861.6,-275.9 	"/>
                        <polygon class="front_tree_cat_st37" points="671.6,-246.9 794.1,-421.9 769.6,-211.9 	"/>
                        <polygon class="front_tree_cat_st52" points="647.1,-416.4 655.6,-441.9 794.1,-441.9 794.1,-421.9 671.6,-246.9 	"/>
                        <polygon class="front_tree_cat_st4" points="614.1,-316.4 647.1,-416.4 671.6,-246.9 	"/>
                        <polygon class="front_tree_cat_st53" points="507.6,-357.9 614.1,-316.4 526.6,-295.4 	"/>
                        <polygon class="front_tree_cat_st51" points="480.2,-441.9 655.6,-441.9 614.1,-316.4 507.6,-357.9 	"/>
                        <polygon class="front_tree_cat_st3" points="794.1,-421.9 959.8,-357.9 861.6,-275.9 	"/>
                        <polygon class="front_tree_cat_st54" points="917.5,-172.3 828.1,-196.9 897.6,-211.9 	"/>
                        <polygon class="front_tree_cat_st55" points="684.3,-26.1 699.9,-25.3 757.3,-63.9 	"/>
                        <polygon class="front_tree_cat_st10" points="740.6,-94.9 728.8,-80.1 692.6,-73.4 705.2,-79 	"/>
                        <polygon class="front_tree_cat_st56" points="678.9,-60.7 728.8,-80.1 692.6,-73.4 	"/>
                        <polygon class="front_tree_cat_st57" points="684.3,-26.1 744.8,-65.4 757.3,-63.9 	"/>
                        <polygon class="front_tree_cat_st58" points="806.8,-157.1 749.8,-82.4 744.8,-65.4 757.3,-63.9 	"/>
                        <polygon class="front_tree_cat_st59" points="801.7,-172.3 806.8,-157.1 749.8,-82.4 	"/>
                        <polygon class="front_tree_cat_st60" points="765.3,-108.6 749.8,-82.4 684.3,-26.1 678.9,-60.7 728.8,-80.1 	"/>
                        <polygon class="front_tree_cat_st6" points="818.3,-158.6 806.8,-157.1 757.3,-63.9 	"/>
                        <polyline class="front_tree_cat_st61" points="897.6,-211.9 916.6,-200.9 917.5,-172.3 897.6,-211.9 	"/>
                        <polygon class="front_tree_cat_st62" points="672,-25.3 654.7,-79 652.7,228.9 668.6,248.9 681.3,163.6 	"/>
                        <polygon class="front_tree_cat_st63" points="672.5,-85.8 671.6,-38.3 678.9,-60.7 	"/>
                        <polygon class="front_tree_cat_st59" points="744.8,-65.4 749.8,-82.4 684.3,-26.1 	"/>
                        <polygon class="front_tree_cat_st6" points="801.7,-172.3 764.7,-108.1 728.8,-80.1 740.6,-94.9 	"/>
                        <polygon class="front_tree_cat_st64" points="584.6,-126.4 588.2,-206.3 511.4,-166.4 	"/>
                        <polygon class="front_tree_cat_st65" points="671.6,-246.9 588.2,-206.3 652.4,-163.8 	"/>
                        <polygon class="front_tree_cat_st33" points="976,-641.8 986.7,-446.8 1074.2,-441.9 	"/>
                        <polygon class="front_tree_cat_st61" points="986.7,-446.8 959.8,-357.9 1051.6,-362.4 1074.2,-441.9 	"/>
                        <polygon class="front_tree_cat_st42" points="346.6,-711.3 386,-525.8 301,-441.9 313.1,-615.5 	"/>
                        <polygon class="front_tree_cat_st50" points="1021,-931.8 897.6,-848.8 937.6,-711.3 1021,-706.8 	"/>
                        <polygon class="front_tree_cat_st43" points="937.6,-711.3 976,-641.8 1021,-706.8 	"/>
                        <polygon class="front_tree_cat_st61" points="986.7,-446.8 857.2,-601.8 937.6,-711.3 976,-641.8 	"/>
                        <polygon class="front_tree_cat_st37" points="794.1,-421.9 857.2,-601.8 986.7,-446.8 959.8,-357.9 	"/>
                        <polygon class="front_tree_cat_st36" points="808.9,-1054.4 833.6,-1015.8 879.8,-1045.7 	"/>
                        <polygon class="front_tree_cat_st61" points="897.6,-848.8 833.6,-1015.8 879.8,-1045.7 1021,-931.8 	"/>
                        <polygon class="front_tree_cat_st48" points="310.7,-830.8 450.2,-676.6 346.6,-711.3 	"/>
                        <polygon class="front_tree_cat_st4" points="386,-525.8 346.6,-711.3 450.2,-676.6 480.2,-441.9 	"/>
                        <polygon class="front_tree_cat_st4" points="475,-993.8 450.2,-676.6 310.7,-830.8 412,-1014.8 	"/>
                        <polygon class="front_tree_cat_st48" points="534,-1034.8 450.2,-676.6 475,-993.8 	"/>
                        <polygon class="front_tree_cat_st37" points="591.9,-1126.6 785,-966.8 833.6,-1015.8 808.9,-1054.4 682.1,-1137.7 	"/>
                        <polygon class="front_tree_cat_st47" points="534,-1034.8 682.6,-764.8 785,-966.8 591.9,-1126.6 	"/>
                        <polygon class="front_tree_cat_st36" points="785,-966.8 833.6,-1015.8 897.6,-848.8 	"/>
                        <polygon class="front_tree_cat_st3" points="682.6,-764.8 785,-966.8 897.6,-848.8 937.6,-711.3 	"/>
                        <polygon class="front_tree_cat_st4" points="857.2,-601.8 682.6,-764.8 937.6,-711.3 	"/>
                        <polygon class="front_tree_cat_st49" points="682.6,-764.8 450.2,-676.6 534,-1034.8 	"/>
                        <polygon class="front_tree_cat_st48" points="450.2,-676.6 655.6,-441.9 480.2,-441.9 	"/>
                        <polygon class="front_tree_cat_st47" points="682.6,-764.8 655.6,-441.9 450.2,-676.6 	"/>
                        <polygon class="front_tree_cat_st36" points="682.6,-764.8 655.6,-441.9 794.1,-421.9 857.2,-601.8 	"/>
                    </g>
                    <g id="tale_and_tree">
                        <polygon class="front_tree_cat_st59" points="887,-181.4 817.3,-158.6 805.8,-157.1 827.3,-170.6 	"/>
                        <polygon class="front_tree_cat_st58" points="887,-181.4 880.6,-192.4 841.8,-182.7 827.3,-170.6 	"/>
                        <polygon class="front_tree_cat_st59" points="900,-196.6 894.1,-199.6 901,-203.2 905.6,-194.1 887,-181.4 	"/>
                        <polygon class="front_tree_cat_st60" points="800.7,-172.3 841.8,-182.7 827.3,-170.6 805.8,-157.1 	"/>
                        <polygon points="865.2,-208.3 861.8,-208.1 858.4,-207.6 855.2,-206.8 852.2,-205.7 849.3,-204.3 846.5,-202.6 843.9,-200.7
                841.6,-198.5 839.4,-196.1 837.5,-193.5 835.9,-190.8 834.5,-187.8 833.3,-184.8 832.5,-181.6 832,-178.2 831.9,-174.8
                831.8,-173.5 831.8,-170.4 831.8,-165.9 831.5,-87.9 831.5,-86.9 832.8,-86.9 834.1,-86.9 835.3,-86.9 836.6,-86.9 837.9,-86.9
                839.1,-86.9 840.4,-86.9 841.7,-86.9 842.9,-86.9 844.2,-86.9 845.4,-86.9 846.7,-86.9 848,-86.9 849.2,-86.9 850.5,-86.9
                851.8,-86.9 851.9,-170.8 851.9,-176.4 852.1,-177.7 852.4,-178.9 852.8,-180 853.3,-181.1 853.9,-182.1 854.6,-183.1 855.4,-184
                856.3,-184.9 857.2,-185.6 858.2,-186.3 859.2,-186.9 860.3,-187.4 861.5,-187.8 862.7,-188.1 863.9,-188.2 865.2,-188.3
                866.6,-188.2 867.9,-188 869.2,-187.7 870.5,-187.3 871.7,-186.7 872.8,-186 873.9,-185.2 874.8,-184.4 875.7,-183.4 876.5,-182.4
                877.2,-181.3 877.8,-180.1 878.3,-178.9 878.6,-177.6 878.8,-176.2 878.9,-174.9 878.8,-173.7 878.7,-172.6 878.4,-171.5
                878,-170.4 877.6,-169.4 877.1,-168.4 876.5,-167.5 875.8,-166.6 875.1,-165.8 874.3,-165 873.5,-164.3 872.7,-163.7 871.8,-163.1
                870.9,-162.6 870,-162.2 869.1,-161.9 868.4,-161.7 867.6,-161.5 866.8,-161.3 866,-161.1 865.1,-160.9 864.2,-160.6 863.4,-160.2
                862.5,-159.8 861.7,-159.3 861,-158.6 860.3,-157.9 859.7,-157 859.3,-155.9 858.9,-154.7 858.7,-153.3 858.6,-151.7 858.6,-150.7
                858.8,-149.7 859,-148.7 859.3,-147.7 859.7,-146.9 860.2,-146 860.8,-145.2 861.4,-144.5 862.1,-143.8 862.9,-143.2 863.8,-142.7
                864.7,-142.3 865.7,-141.9 866.8,-141.7 867.9,-141.5 869.1,-141.5 872.5,-141.6 875.7,-142.2 878.7,-143.1 881.6,-144.2
                884.2,-145.7 886.7,-147.4 888.9,-149.4 890.9,-151.6 892.7,-154 894.3,-156.6 895.7,-159.4 896.8,-162.3 897.7,-165.3
                898.3,-168.4 898.7,-171.6 898.8,-174.8 898.7,-178.2 898.1,-181.6 897.3,-184.8 896.2,-187.8 894.8,-190.8 893.1,-193.5
                891.1,-196.1 888.9,-198.5 886.6,-200.7 884,-202.6 881.2,-204.3 878.3,-205.7 875.2,-206.8 871.9,-207.6 868.6,-208.1 	"/>

                        <g>
                            <polygon id="black-cat-tail" class="front_tree_cat_st66" points="851.8,-86.9 848.4,-86.9 848.4,-136.4 851.9,-176.4 	"/>
                            <polygon class="front_tree_cat_st67" points="865.4,-208.2 862,-208.1 858.7,-207.6 855.5,-206.8 852.5,-205.8 849.7,-204.4 847,-202.9
                844.5,-201 842.2,-199 840,-196.8 838.1,-194.4 836.5,-191.8 835,-189.1 833.9,-186.2 832.9,-183.3 832.3,-180.2 831.9,-177
                831.9,-171.4 831.9,-165.8 831.5,-86.9 836.1,-86.9 836.1,-176.6 836.1,-178 836.2,-180.5 836.7,-183.1 837.4,-185.7 838.4,-188.3
                839.7,-190.9 841.2,-193.4 842.9,-195.8 844.8,-198.1 846.9,-200.2 849.1,-202.2 851.5,-203.9 854.1,-205.4 856.8,-206.6
                859.6,-207.5 862.5,-208.1 	"/>
                        </g>

                        <polygon class="front_tree_cat_st68" points="878.8,-174.4 878.8,-173.3 878.6,-172.2 878.4,-171.1 878,-170.1 877.6,-169.1 877.1,-168.2
                876.5,-167.3 875.8,-166.4 875.1,-165.6 874.4,-164.9 873.6,-164.2 872.7,-163.5 871.8,-163 870.9,-162.5 870,-162.1 869.1,-161.8
                869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8
                869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869.1,-161.8 869,-161.7 868.8,-161.7
                868.5,-161.7 868,-161.6 867.5,-161.5 866.9,-161.3 866.3,-161.1 865.6,-160.9 864.8,-160.6 864.1,-160.3 863.4,-159.9
                862.6,-159.5 862,-159 861.3,-158.4 860.7,-157.8 860.2,-157.1 860.1,-156.8 860,-156.4 859.9,-155.9 859.9,-155.4 859.9,-154.8
                860.1,-154.3 860.2,-153.7 860.5,-153.1 860.8,-152.6 861.3,-152 861.8,-151.6 862.4,-151.2 863.2,-150.8 864,-150.6 865,-150.5
                866.1,-150.5 868,-150.7 869.7,-151.3 871.4,-152.1 872.9,-153.2 874.4,-154.5 875.7,-156 876.8,-157.7 877.8,-159.5 878.6,-161.3
                879.2,-163.3 879.7,-165.2 880,-167.2 880,-169.1 879.8,-171 879.5,-172.8 	"/>
                        <polygon class="front_tree_cat_st58" points="900,-196.6 887,-181.4 860,-176.5 859.6,-187.1 860.6,-187.5 861.6,-187.8 880.6,-192.4
                894.1,-199.6 	"/>
                        <polygon class="front_tree_cat_st59" points="857.2,-171.6 887,-181.4 860,-176.5 	"/>
                        <polygon class="front_tree_cat_st59" points="900,-196.6 887,-181.4 903,-192.3 	"/>
                    </g>
                    <g style="transform-origin: 100% 0%" id="black_cat">
                        <polygon id="red_stripe_back" class="front_tree_cat_st69" points="787.1,28.9 762.4,36.5 772,24.6 760.5,14.8 787.1,17.8 	"/>
                        <polygon id="body_12_" class="front_tree_cat_st70" points="797.3,64 798.9,63.9 800.5,63.5 802.2,63.1 803.6,62.2 804.8,61.2 806,60 807,58.5
                807.6,57 807.6,57 807.6,57 809.9,51.9 812.5,46.1 814.9,40.5 817.8,33.8 819,31.7 820.2,29.9 821.6,28.5 823.2,27.3 824.7,26.4
                826.3,25.8 827.7,25.4 829.2,25.1 856.2,25.1 857.2,25.4 858.6,25.7 860.1,26.2 861.6,27 863.3,28 864.8,29.5 866.3,31.3
                867.6,33.5 870.1,39.4 872.9,45.8 875.3,51.6 877.6,56.9 877.6,56.9 877.6,56.9 877.6,56.9 877.6,56.9 877.6,56.9 877.6,56.9
                877.6,56.9 877.6,56.9 878.5,58.4 879.4,59.8 880.5,61 881.7,62 883.2,62.8 885,63.4 886.6,63.8 888.2,64 890.4,63.7 892.6,63.1
                894.5,62.2 896.2,60.8 897.5,59.2 898.5,57.4 899.3,55.3 899.5,53.2 899.5,40.7 899.5,31.7 899.5,22.6 899.5,13.6 899.5,4.5
                899.5,-4.5 899.5,-13.6 899.5,-22.7 899.5,-31.8 899.5,-40.9 899.5,-50 899.5,-59 899.5,-66.7 899.1,-71 897.9,-75 895.9,-78.5
                893.3,-81.7 890.2,-84.3 886.6,-86.3 882.7,-87.6 878.5,-88.2 866.1,-88.2 819.8,-88.2 806.4,-88.2 802.3,-87.6 798.3,-86.3
                795,-84.3 792,-81.7 789.6,-78.5 787.7,-75 786.6,-71 786.2,-66.7 786.2,-59 786.2,-50.6 786.2,-42.4 786.2,-34 786.2,-25.7
                786.2,-17.5 786.2,-9.1 786.2,-0.8 786.2,7.5 786.2,15.8 786.2,24.2 786.2,32.5 786.2,40.7 786.2,53.2 786.5,55.3 787.1,57.4
                788.1,59.2 789.5,60.9 791.1,62.2 793,63.1 795.1,63.7 	"/>
                        <polygon id="body_light_part_12_" class="front_tree_cat_st71" points="797.3,64 798.9,63.9 800.5,63.5 802.2,63.1 803.6,62.2 804.8,61.2 806,60
                807,58.5 807.6,57 807.6,57 807.6,57 809.9,51.9 812.5,46.1 814.9,40.5 817.8,33.8 819,31.7 820.2,29.9 821.6,28.5 823.2,27.3
                824.7,26.4 826.3,25.8 827.7,25.4 829.2,25.1 842.4,25.1 842.4,-88.2 819.8,-88.2 806.4,-88.2 802.3,-87.6 798.3,-86.3 795,-84.3
                792,-81.7 789.6,-78.5 787.7,-75 786.6,-71 786.2,-66.7 786.2,-59 786.2,-50.6 786.2,-42.4 786.2,-34 786.2,-25.7 786.2,-17.5
                786.2,-9.1 786.2,-0.8 786.2,7.5 786.2,15.8 786.2,24.2 786.2,32.5 786.2,40.7 786.2,53.2 786.5,55.3 787.1,57.4 788.1,59.2
                789.5,60.9 791.1,62.2 793,63.1 795.1,63.7 	"/>
                        <polygon id="shadow_12_" class="front_tree_cat_st71" points="842.4,-88.2 869.9,-88.2 874.1,-87.9 877.9,-86.6 881.4,-84.5 884.3,-81.9
                886.7,-78.6 888.4,-75 889.5,-71 890,-66.8 890,-32.7 888.6,-33.1 884.8,-34.3 879,-35.4 872,-36.1 868.2,-35.9 864.3,-35.4
                860.3,-34.7 858,-34.1 856,-33.3 853.9,-32.3 852.1,-31.3 850.3,-30 848.9,-28.5 847.1,-27.1 845.5,-25.3 844,-23.3 842.4,-20.9
                "/>
                        <polygon id="light_1_12_" class="front_tree_cat_st70" points="842.4,25.1 842.4,20.7 858,20.7 860.9,21.1 863.3,22 865.8,23.6 868,26.1
                870.2,29.2 872.3,33 882.3,56 882.9,57.3 883.8,58.5 884.8,59.4 885.9,60.2 887.1,60.8 888.2,61.3 889.4,61.5 890.7,61.5
                892.2,61.3 893.5,61 894.8,60.3 896.2,59.4 897.2,58.4 898.2,57 899,55.5 899.4,53.7 899.1,55.6 898.5,57.4 897.6,59.1 896.2,60.7
                894.6,62 892.6,63.1 890.6,63.7 888.2,64 886.6,63.8 885,63.4 883.4,62.8 881.9,62 880.5,61 879.4,59.8 878.5,58.4 877.6,56.9
                877.3,56.3 876.4,54.2 867.6,33.3 867.2,32.9 866.9,32.2 866.5,31.7 866.3,31.1 866,30.8 865.5,30.3 865.2,29.8 864.8,29.5
                863.7,28.3 862.4,27.4 861.1,26.7 859.8,26 858.3,25.7 857,25.4 855.7,25.1 854.6,25.1 	"/>
                        <polygon id="shadow_on_light_part_12_" points="842.4,-88.2 814.9,-88.2 810.7,-87.9 807,-86.6 803.5,-84.5 800.5,-81.9
                798.2,-78.6 796.4,-75 795.2,-71 794.9,-66.8 794.9,-32.7 796.2,-33.1 800.1,-34.3 805.9,-35.4 812.8,-36.1 816.7,-35.9
                820.6,-35.4 824.4,-34.7 826.8,-34.1 828.8,-33.3 830.9,-32.3 832.8,-31.3 834.4,-30 836.1,-28.5 837.7,-27.1 839.4,-25.3
                840.8,-23.3 842.4,-20.9 	"/>
                        <g id="pow_left_12_">
                            <polygon class="front_tree_cat_st70" points="853.6,-67.4 853.8,-69.5 854.2,-71.6 854.6,-73.6 855.4,-75.5 856.3,-77.3 857.3,-79.1 858.4,-80.7
                    859.8,-82.2 861.3,-83.5 862.9,-84.7 864.5,-85.7 866.4,-86.6 868.3,-87.3 870.3,-87.8 872.3,-88.2 874.4,-88.2 876.4,-88.2
                    878.5,-87.8 880.6,-87.3 882.5,-86.6 884.2,-85.7 886,-84.7 887.6,-83.5 889,-82.2 890.3,-80.7 891.5,-79.1 892.6,-77.3
                    893.5,-75.5 894.1,-73.6 894.6,-71.6 895,-69.5 895.2,-67.4 895.2,-67.3 895.1,-67.2 895.1,-67.1 895.1,-67 895,-66.9 895,-66.7
                    895,-66.6 895,-66.5 895,-66.4 895,-66.3 895,-66.1 895,-66.1 895,-66.1 895,-65.8 895,-65.8 894.9,-65.7 894.7,-67.6
                    894.4,-69.5 893.7,-71.3 892.9,-72.9 892,-74.6 891,-76 889.7,-77.3 888.4,-78.5 887,-79.7 885.4,-80.6 883.8,-81.4 882,-82.2
                    880.3,-82.7 878.4,-83.2 876.4,-83.4 874.4,-83.5 872.3,-83.4 870.5,-83.2 868.6,-82.7 866.7,-82.2 865,-81.4 863.3,-80.6
                    861.8,-79.7 860.4,-78.5 859,-77.3 857.9,-76 856.8,-74.6 855.9,-72.9 855.1,-71.3 854.6,-69.5 854.1,-67.6 853.8,-65.7
                    853.8,-65.8 853.8,-65.8 853.7,-66.1 853.7,-66.1 853.7,-66.1 853.7,-66.3 853.7,-66.4 853.7,-66.5 853.7,-66.6 853.7,-66.7
                    853.7,-66.9 853.7,-67 853.7,-67.1 853.6,-67.2 853.6,-67.3 		"/>
                            <polygon class="front_tree_cat_st70" points="879.4,-73.4 879.4,-73.4 879.4,-73.4 879.4,-73.4 879.4,-73.4 880.1,-73.6 880.6,-73.9 881,-74.5
                    881.1,-75.1 881.1,-85 881,-85.6 880.6,-86.3 880.1,-86.6 879.4,-86.6 879.4,-86.6 879.4,-86.6 879.4,-86.6 879.4,-86.6
                    878.8,-86.6 878.2,-86.3 877.8,-85.6 877.6,-85 877.6,-75.1 877.8,-74.5 878.2,-73.9 878.8,-73.6 		"/>
                            <polygon class="front_tree_cat_st70" points="869.4,-73.4 869.4,-73.4 869.4,-73.4 869.4,-73.4 869.4,-73.4 870.1,-73.6 870.7,-73.9 871,-74.5
                    871.1,-75.1 871.1,-85 871,-85.6 870.7,-86.3 870.1,-86.6 869.4,-86.6 869.4,-86.6 869.4,-86.6 869.4,-86.6 869.4,-86.6
                    868.7,-86.6 868.2,-86.3 867.9,-85.6 867.6,-85 867.6,-75.1 867.9,-74.5 868.2,-73.9 868.7,-73.6 		"/>
                        </g>
                        <polygon id="ear_25_" points="890.9,47.9 890.7,48.7 890.5,49.4 890.1,50 889.5,50.6 889,51 888.1,51.4 887.4,51.6 886.6,51.6
                886,51.6 885.3,51.4 884.8,51.1 884.2,50.7 883.8,50.3 883.4,49.8 882.9,49.2 882.8,48.7 878.1,33.7 890.9,33.7 	"/>
                        <polygon id="ear_24_" points="794.9,47.9 795.1,48.7 795.2,49.4 795.7,50 796.3,50.6 796.9,51 797.6,51.4 798.3,51.6 799.2,51.6
                799.8,51.6 800.5,51.4 801.1,51.1 801.6,50.7 802.1,50.3 802.6,49.8 802.9,49.2 803.2,48.7 807.8,33.7 794.9,33.7 	"/>
                        <polygon id="nose_12_" class="front_tree_cat_st72" points="840.8,-19.3 841.9,-19.3 842.8,-19.3 843.9,-19.3 844.9,-19.3 847,-18.8 848.6,-17.7
                849.8,-15.9 850.2,-14 850.2,-14 850.2,-14 850.2,-14 850.2,-14 849.9,-12.8 849.2,-11.8 848.4,-11.2 847.1,-11 845,-11 842.8,-11
                840.6,-11 838.5,-11 837.4,-11.2 836.4,-11.8 835.8,-12.8 835.6,-14 835.6,-14 835.6,-14 835.6,-14 835.6,-14 835.9,-15.9
                837.2,-17.7 838.8,-18.8 	"/>

                        <g class="eye_left">
                            <polygon points="816.8,5.6 813.4,5.2 810.3,4.3 807.4,2.7 805,0.7 802.9,-1.8 801.4,-4.7 800.4,-7.8
                800,-11.2 800.4,-14.6 801.4,-17.7 802.9,-20.5 805,-23 807.4,-25.1 810.3,-26.6 813.4,-27.6 816.8,-27.9 820.2,-27.6 823.3,-26.6
                826.2,-25.1 828.6,-23 830.7,-20.5 832.2,-17.7 833.2,-14.6 833.6,-11.2 833.2,-7.8 832.2,-4.7 830.7,-1.8 828.6,0.7 826.2,2.7
                823.3,4.3 820.2,5.2 	"/>
                            <polygon class="eyeball front_tree_cat_st73" points="815.1,9.9 811.7,9.6 808.5,8.6 805.7,7 803.2,5 801.2,2.5 799.6,-0.3 798.7,-3.5
                798.3,-6.8 798.7,-10.2 799.6,-13.4 801.2,-16.2 803.2,-18.7 805.7,-20.7 808.5,-22.3 811.7,-23.3 815.1,-23.6 818.4,-23.3
                821.6,-22.3 824.4,-20.7 826.9,-18.7 829,-16.2 830.5,-13.4 831.5,-10.2 831.8,-6.8 831.5,-3.5 830.5,-0.3 829,2.5 826.9,5
                824.4,7 821.6,8.6 818.4,9.6 	"/>
                            <polygon class="front_tree_cat_st74" points="799.7,-13.4 800.3,-14.5 801,-15.7 801.6,-16.8 802.5,-17.8 803.3,-18.8
                804.2,-19.6 805.2,-20.3 806.3,-21 807.3,-21.6 808.3,-22.2 809.4,-22.6 810.6,-23 811.7,-23.2 812.8,-23.4 814,-23.5 815,-23.5
                816.8,-23.5 818.4,-23.2 820,-22.8 821.6,-22.2 823.1,-21.6 824.4,-20.7 825.6,-19.8 826.8,-18.7 828,-17.5 828.9,-16.2
                829.7,-14.8 830.4,-13.4 831,-11.9 831.5,-10.2 831.8,-8.6 831.8,-6.8 831.8,-6.4 831.8,-6 831.8,-5.5 831.8,-4.8 831.6,-4.2
                831.5,-3.5 831.3,-2.8 831.2,-2.1 831,-1.4 830.8,-0.5 830.4,0.2 830,0.9 829.7,1.7 829.2,2.4 828.6,3 827.9,3.8 828.7,2.4
                829.4,1 829.7,-0.5 829.9,-2.1 829.7,-3.7 829.7,-5.4 829.2,-6.9 828.6,-8.6 827.8,-10.1 826.9,-11.7 825.9,-13.1 824.7,-14.4
                823.4,-15.6 822.1,-16.6 820.7,-17.5 819.2,-18 817.8,-18.5 816.5,-18.8 815.1,-18.8 813.7,-19 812.3,-18.8 810.7,-18.8
                809.4,-18.5 808.1,-18.2 806.6,-17.8 805.3,-17.4 804.1,-16.8 802.9,-16.2 802,-15.6 801.1,-14.9 800.3,-14.1 	"/>
                            <circle class="pupil" cx="815" cy="-5" r="6.5"/>
                        </g>

                        <g class="eye_right">
                            <polygon points="873,5.6 869.6,5.2 866.5,4.3 863.6,2.7 861.1,0.7 859.1,-1.8 857.5,-4.7 856.6,-7.8
                856.2,-11.2 856.6,-14.6 857.5,-17.7 859.1,-20.5 861.1,-23 863.6,-25.1 866.5,-26.6 869.6,-27.6 873,-27.9 876.4,-27.6
                879.5,-26.6 882.4,-25.1 884.8,-23 886.9,-20.5 888.4,-17.7 889.4,-14.6 889.7,-11.2 889.4,-7.8 888.4,-4.7 886.9,-1.8 884.8,0.7
                882.4,2.7 879.5,4.3 876.4,5.2 	"/>
                            <polygon class="eyeball front_tree_cat_st73" points="871.3,9.9 867.9,9.6 864.8,8.6 862,7 859.5,5 857.4,2.5 855.9,-0.3 854.9,-3.5
                854.6,-6.8 854.9,-10.2 855.9,-13.4 857.4,-16.2 859.5,-18.7 862,-20.7 864.8,-22.3 867.9,-23.3 871.3,-23.6 874.7,-23.3
                877.8,-22.3 880.7,-20.7 883.2,-18.7 885.2,-16.2 886.8,-13.4 887.7,-10.2 888.1,-6.8 887.7,-3.5 886.8,-0.3 885.2,2.5 883.2,5
                880.7,7 877.8,8.6 874.7,9.6 	"/>
                            <polygon class="front_tree_cat_st74" points="855.9,-13.4 856.5,-14.5 857.2,-15.7 857.9,-16.8 858.7,-17.8 859.6,-18.8
                860.5,-19.6 861.4,-20.3 862.4,-21 863.5,-21.6 864.5,-22.2 865.7,-22.6 866.8,-23 867.9,-23.2 869.1,-23.4 870.2,-23.5
                871.3,-23.5 873,-23.5 874.6,-23.2 876.3,-22.8 877.8,-22.2 879.3,-21.6 880.6,-20.7 881.9,-19.8 883.1,-18.7 884.2,-17.5
                885.1,-16.2 886,-14.8 886.6,-13.4 887.2,-11.9 887.6,-10.2 887.9,-8.6 888.1,-6.8 888.1,-6.4 888.1,-6 887.9,-5.5 887.9,-4.8
                887.9,-4.2 887.8,-3.5 887.6,-2.8 887.4,-2.1 887.2,-1.4 887,-0.5 886.6,0.2 886.3,0.9 885.9,1.7 885.4,2.4 884.8,3 884.1,3.8
                885,2.4 885.6,1 886,-0.5 886.1,-2.1 886,-3.7 885.9,-5.4 885.4,-6.9 884.8,-8.6 884,-10.1 883.2,-11.7 882.2,-13.1 880.9,-14.4
                879.7,-15.6 878.4,-16.6 877,-17.5 875.4,-18 874.1,-18.5 872.7,-18.8 871.3,-18.8 869.9,-19 868.5,-18.8 867,-18.8 865.5,-18.5
                864.2,-18.2 862.9,-17.8 861.5,-17.4 860.3,-16.8 859.2,-16.2 858.3,-15.6 857.3,-14.9 856.5,-14.1 	"/>
                            <circle class="pupil" cx="870" cy="-5" r="6.5"/>
                        </g>

                        <rect id="red_stripe_light" x="786.2" y="15" class="front_tree_cat_st69" width="113.3" height="17"/>
                        <rect id="red_stripe" x="786.2" y="15" class="front_tree_cat_st75" width="56.3" height="17"/>
                        <polygon id="light_3_12_" class="front_tree_cat_st76" points="786.2,53.2 786.2,-66.8 786.5,-71 787.6,-75 789.4,-78.6 791.7,-81.9 794.7,-84.5
                798.1,-86.6 801.7,-87.9 806,-88.2 806.5,-88.2 807.1,-88.2 807.8,-88.2 808.3,-88.2 809,-88.2 809.5,-88.2 810.2,-88.2
                810.7,-88.2 805.2,-87.3 800.8,-85.6 797.1,-82.8 794.5,-79.3 792.6,-75.4 791.4,-71.3 790.7,-67.5 790.5,-63.9 790.5,50.1
                790.5,51.6 790.5,52.9 790.7,55.3 791.3,57.2 792.1,58.8 793.3,60.1 794.8,61 796.4,61.6 798.3,61.8 800.2,61.6 801.3,61.5
                803.2,61.2 805,60.2 806.5,59.1 806,60 804.8,61.2 803.6,62.2 802.2,63.1 800.6,63.5 799.5,63.8 798.2,64 797,64 794.8,63.7
                792.8,63.1 791,62.1 789.3,60.8 788,59.1 787.1,57.3 786.4,55.3 	"/>
                        <polygon id="light_2_12_" class="front_tree_cat_st67" points="790.5,53.2 790.5,-64.1 791.4,-71.3 792.6,-75.4 794.5,-79.3 797.1,-82.8
                800.8,-85.6 805.1,-87.3 810.3,-88.2 811.7,-88.2 813.2,-88.2 814.6,-88.2 816,-88.2 817.5,-88.2 819,-88.2 820.4,-88.2
                821.9,-88.2 813.3,-87.5 806.7,-85.6 801.9,-82.6 798.6,-78.9 796.6,-75 795.4,-70.8 795,-67.1 794.9,-63.9 794.9,50.1 794.9,51.8
                794.9,53.7 795.1,55.6 795.4,57.4 796,59.1 796.9,60.4 798.3,61.3 800.2,61.6 798.3,61.8 796.4,61.6 794.8,61 793.3,60.1
                792.1,58.8 791.3,57.1 790.7,55.3 	"/>
                        <polygon id="light_3_8_" class="front_tree_cat_st77" points="899.5,53.2 899.5,-66.8 899.2,-71 898,-75 896.2,-78.6 893.9,-81.9 890.9,-84.5
                887.5,-86.6 883.9,-87.9 879.7,-88.2 879.1,-88.2 878.5,-88.2 877.9,-88.2 877.4,-88.2 876.6,-88.2 876.1,-88.2 875.5,-88.2
                874.9,-88.2 880.5,-87.3 884.9,-85.6 888.6,-82.8 891.2,-79.3 893,-75.4 894.3,-71.3 894.9,-67.5 895.1,-63.9 895.1,50.1
                895.1,51.6 895.1,52.9 895.1,54 895,55.1 894.8,56.2 894.3,57.2 893.7,58.2 892.8,59.4 891.6,60.4 890,61.2 888.3,61.5 886.2,61.8
                884.3,61.5 882.4,61.2 880.6,60.2 878.7,58.8 879.4,59.8 880.5,61 881.7,62 883.7,63.1 885,63.5 886.2,63.8 887.4,64 888.6,64
                890.9,63.7 892.8,63.1 894.7,62.1 896.3,60.8 897.6,59.1 898.5,57.4 899.3,55.3 	"/>
                        <polygon id="light_2_5_" class="front_tree_cat_st66" points="895.1,54 895.1,-64.1 894.8,-69.4 893.4,-75 891.6,-78.6 888.6,-82.8 884.9,-85.6
                880.5,-87.3 875,-88.2 873.7,-88.2 872.1,-88.2 870.7,-88.2 869.3,-88.2 867.8,-88.2 866.4,-88.2 864.9,-88.2 863.5,-88.2
                872.1,-87.5 878.6,-85.6 883.4,-82.6 886.7,-78.9 888.8,-75 889.9,-70.8 890.3,-67.1 890.5,-63.9 890.5,50.1 890.5,51.8
                890.5,53.7 890.3,55.6 890,57.4 889.3,59.1 888.4,60.4 887,61.3 884.5,61.6 886.2,61.8 888.3,61.5 890,61.2 891.6,60.4 892.8,59.4
                893.7,58.2 894.8,56.2 	"/>
                        <polygon id="blick" class="front_tree_cat_st78" points="895.9,-58.8 896,-54.5 896.4,-50.8 897,-47.6 897.7,-45 898.3,-43.1 898.9,-41.9
                899.3,-41.7 899.5,-42.4 899.5,-44.1 899.5,-46.8 899.5,-50.1 899.5,-53.7 899.5,-57.3 899.5,-60.6 899.5,-63.2 899.5,-64.9
                899.5,-66.2 899.4,-67.6 899.3,-69.1 899,-70.8 898.6,-72.6 898,-74.4 897.1,-76.4 896,-78.3 894.8,-80 893.6,-81.4 892.6,-82.5
                891.6,-83.3 890.8,-83.8 890.2,-84.1 889.8,-84.3 889.7,-84.3 889.7,-84.2 889.5,-84 889.4,-83.6 889.3,-83 889.4,-82.2
                889.7,-81.3 890.2,-80.3 891.1,-79.1 892.1,-77.4 893,-75.2 893.8,-72.5 894.5,-69.5 895.1,-66.5 895.5,-63.6 895.8,-60.9 	"/>
                        <g id="pow_right_12_">
                            <polygon class="front_tree_cat_st70" points="790.5,-67.4 790.7,-69.5 791,-71.6 791.6,-73.6 792.3,-75.5 793.1,-77.3 794.1,-79.1 795.2,-80.7
                    796.7,-82.2 798.2,-83.5 799.7,-84.7 801.4,-85.7 803.2,-86.6 805.1,-87.3 807.1,-87.8 809.1,-88.2 811.3,-88.2 813.4,-88.2
                    815.4,-87.8 817.5,-87.3 819.3,-86.6 821.1,-85.7 822.8,-84.7 824.4,-83.5 825.8,-82.2 827.2,-80.7 828.4,-79.1 829.4,-77.3
                    830.3,-75.5 831,-73.6 831.5,-71.6 831.9,-69.5 832,-67.4 832,-67.3 831.9,-67.2 831.9,-67.1 831.9,-67 831.9,-66.9 831.9,-66.7
                    831.9,-66.6 831.9,-66.5 831.9,-66.4 831.9,-66.3 831.9,-66.1 831.9,-66.1 831.9,-66.1 831.9,-65.8 831.9,-65.8 831.8,-65.7
                    831.6,-67.6 831.2,-69.5 830.6,-71.3 829.7,-72.9 828.8,-74.6 827.8,-76 826.6,-77.3 825.3,-78.5 823.8,-79.7 822.2,-80.6
                    820.6,-81.4 819,-82.2 817.1,-82.7 815.1,-83.2 813.3,-83.4 811.3,-83.5 809.1,-83.4 807.3,-83.2 805.4,-82.7 803.6,-82.2
                    801.9,-81.4 800.2,-80.6 798.6,-79.7 797.3,-78.5 795.9,-77.3 794.7,-76 793.6,-74.6 792.8,-72.9 792,-71.3 791.4,-69.5
                    791,-67.6 790.7,-65.7 790.7,-65.8 790.7,-65.8 790.7,-66.1 790.7,-66.1 790.7,-66.1 790.7,-66.3 790.7,-66.4 790.7,-66.5
                    790.7,-66.6 790.7,-66.7 790.7,-66.9 790.7,-67 790.7,-67.1 790.5,-67.2 790.5,-67.3 		"/>
                            <polygon class="front_tree_cat_st70" points="816.2,-73.4 816.2,-73.4 816.2,-73.4 816.2,-73.4 816.2,-73.4 816.9,-73.6 817.5,-73.9 817.8,-74.5
                    817.9,-75.1 817.9,-85 817.8,-85.6 817.5,-86.3 816.9,-86.6 816.2,-86.6 816.2,-86.6 816.2,-86.6 816.2,-86.6 816.2,-86.6
                    815.6,-86.6 815,-86.3 814.6,-85.6 814.5,-85 814.5,-75.1 814.6,-74.5 815,-73.9 815.6,-73.6 		"/>
                            <polygon class="front_tree_cat_st70" points="806.3,-73.4 806.3,-73.4 806.3,-73.4 806.3,-73.4 806.3,-73.4 807,-73.6 807.5,-73.9 807.8,-74.5
                    808.1,-75.1 808.1,-85 807.8,-85.6 807.5,-86.3 807,-86.6 806.3,-86.6 806.3,-86.6 806.3,-86.6 806.3,-86.6 806.3,-86.6
                    805.6,-86.6 805,-86.3 804.7,-85.6 804.5,-85 804.5,-75.1 804.7,-74.5 805,-73.9 805.6,-73.6 		"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="rocks_front">
                <svg version="1.1" id="_x32_plan_ground" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="0 0 2120 500.2" style="enable-background:new 0 0 2120 500.2;" xml:space="preserve">
                        <style type="text/css">
                            .rocks_front_st0{fill:#432A10;}
                            .rocks_front_st1{fill:#332211;}
                            .rocks_front_st2{fill:#563D25;}
                            .rocks_front_st3{fill:#724F2C;}
                            .rocks_front_st4{fill:#3E3329;}
                            .rocks_front_st5{fill:#645D63;}
                            .rocks_front_st6{fill:#403027;}
                            .rocks_front_st7{fill:#402E20;}
                            .rocks_front_st8{fill:#513C2A;}
                            .rocks_front_st9{fill:#51402E;}
                            .rocks_front_st10{fill:#5D4630;}
                            .rocks_front_st11{fill:#402B18;}
                            .rocks_front_st12{fill:#2A1D15;}
                            .rocks_front_st13{fill:#362C2A;}
                            .rocks_front_st14{fill:#3E3434;}
                            .rocks_front_st15{fill:#F0EAE4;}
                            .rocks_front_st16{fill:#A39A9C;}
                            .rocks_front_st17{fill:#504D58;}
                            .rocks_front_st18{fill:#3F393D;}
                            .rocks_front_st19{fill:#716662;}
                            .rocks_front_st20{fill:#6B6362;}
                            .rocks_front_st21{fill:#695E59;}
                            .rocks_front_st22{fill:#847871;}
                            .rocks_front_st23{fill:#7D7169;}
                            .rocks_front_st24{fill:#3C2F27;}
                            .rocks_front_st25{fill:#8A8188;}
                            .rocks_front_st26{fill:#92827D;}
                            .rocks_front_st27{fill:#F3EAE7;}
                            .rocks_front_st28{fill:#5A4C43;}
                            .rocks_front_st29{fill:#BBA595;}
                            .rocks_front_st30{fill:#6D5F53;}
                            .rocks_front_st31{fill:#41342F;}
                            .rocks_front_st32{fill:#3A2F2E;}
                            .rocks_front_st33{fill:#312B2C;}
                            .rocks_front_st34{fill:#413734;}
                            .rocks_front_st35{fill:#C8B4A9;}
                            .rocks_front_st36{fill:#565158;}
                            .rocks_front_st37{fill:#403A3D;}
                            .rocks_front_st38{fill:#4A3F40;}
                            .rocks_front_st39{fill:#585056;}
                            .rocks_front_st40{fill:#3B2A19;}
                            .rocks_front_st41{fill:#1B1107;}
                            .rocks_front_st42{fill:#4A321B;}
                            .rocks_front_st43{fill:#6B5F56;}
                            .rocks_front_st44{fill:#4D4647;}
                            .rocks_front_st45{fill:#746B6A;}
                            .rocks_front_st46{fill:#7C6F6E;}
                            .rocks_front_st47{fill:#5A4E49;}
                            .rocks_front_st48{fill:#796D66;}
                            .rocks_front_st49{fill:#847060;}
                            .rocks_front_st50{fill:#67584E;}
                            .rocks_front_st51{fill:#5D4D45;}
                            .rocks_front_st52{fill:#2E292A;}
                            .rocks_front_st53{fill:#4F433E;}
                            .rocks_front_st54{fill:#544945;}
                            .rocks_front_st55{fill:#4E341A;}
                            .rocks_front_st56{fill:#5D4D3B;}
                            .rocks_front_st57{fill:#534F56;}
                            .rocks_front_st58{fill:#4F464C;}
                            .rocks_front_st59{fill:#4B3D3A;}
                            .rocks_front_st60{fill:#6A656D;}
                            .rocks_front_st61{fill:#5B442E;}
                            .rocks_front_st62{fill:#594B3F;}
                            .rocks_front_st63{fill:#322114;}
                            .rocks_front_st64{fill:#1C140F;}
                            .rocks_front_st65{fill:#603E1C;}
                            .rocks_front_st66{fill:#484045;}
                            .rocks_front_st67{fill:#776F78;}
                            .rocks_front_st68{fill:#59555B;}
                            .rocks_front_st69{fill:#3C383D;}
                            .rocks_front_st70{fill:#473A34;}
                            .rocks_front_st71{fill:#867B7C;}
                            .rocks_front_st72{fill:#635750;}
                            .rocks_front_st73{fill:#695A50;}
                            .rocks_front_st74{fill:#CEBAA5;}
                            .rocks_front_st75{fill:#322317;}
                            .rocks_front_st76{fill:#312623;}
                            .rocks_front_st77{fill:#63544D;}
                            .rocks_front_st78{fill:#493E3A;}
                            .rocks_front_st79{fill:#4D4143;}
                            .rocks_front_st80{fill:#3D353A;}
                            .rocks_front_st81{fill:#4C3522;}
                            .rocks_front_st82{fill:#473B3C;}
                            .rocks_front_st83{fill:#534A4D;}
                            .rocks_front_st84{fill:#423942;}
                            .rocks_front_st85{fill:#F9E8D6;}
                            .rocks_front_st86{fill:#F6EDE9;}
                            .rocks_front_st87{fill:#685A52;}
                            .rocks_front_st88{fill:#998A7C;}
                            .rocks_front_st89{fill:#ADA3AC;}
                            .rocks_front_st90{fill:#E9DFD7;}
                            .rocks_front_st91{fill:#8F8179;}
                            .rocks_front_st92{fill:#7A6E69;}
                            .rocks_front_st93{fill:#FFFFFF;}
                            .rocks_front_st94{fill:#E9E0D8;}
                            .rocks_front_st95{fill:#71635A;}
                            .rocks_front_st96{fill:#73665F;}
                            .rocks_front_st97{fill:#68605B;}
                            .rocks_front_st98{fill:#645B57;}
                            .rocks_front_st99{fill:#584D49;}
                            .rocks_front_st100{fill:#6B605E;}
                            .rocks_front_st101{fill:#6A605E;}
                            .rocks_front_st102{fill:#786D6B;}
                            .rocks_front_st103{fill:#6F6360;}
                            .rocks_front_st104{fill:#6E6461;}
                            .rocks_front_st105{fill:#564E4F;}
                            .rocks_front_st106{fill:#776F6F;}
                            .rocks_front_st107{fill:#564B49;}
                            .rocks_front_st108{fill:#3A302F;}
                            .rocks_front_st109{fill:#4F4443;}
                            .rocks_front_st110{fill:#362F31;}
                            .rocks_front_st111{fill:#413939;}
                            .rocks_front_st112{fill:#3B3538;}
                            .rocks_front_st113{fill:#29252C;}
                            .rocks_front_st114{fill:#141319;}
                            .rocks_front_st115{fill:#121117;}
                            .rocks_front_st116{fill:#271C1A;}
                            .rocks_front_st117{fill:#252127;}
                            .rocks_front_st118{fill:#211E21;}
                            .rocks_front_st119{fill:#2B211F;}
                            .rocks_front_st120{fill:#262327;}
                            .rocks_front_st121{fill:#2F2A29;}
                            .rocks_front_st122{fill:#645345;}
                            .rocks_front_st123{fill:#5D4D40;}
                            .rocks_front_st124{fill:#221C18;}
                            .rocks_front_st125{fill:#352820;}
                            .rocks_front_st126{fill:#29201D;}
                            .rocks_front_st127{fill:#1B191C;}
                            .rocks_front_st128{fill:#4D3C2D;}
                            .rocks_front_st129{fill:#5E5043;}
                            .rocks_front_st130{fill:#594535;}
                            .rocks_front_st131{fill:#3A2D24;}
                            .rocks_front_st132{fill:#463425;}
                            .rocks_front_st133{fill:#45372B;}
                            .rocks_front_st134{fill:#34261D;}
                            .rocks_front_st135{fill:#372A20;}
                            .rocks_front_st136{fill:#483A30;}
                            .rocks_front_st137{fill:#564A40;}
                            .rocks_front_st138{fill:#796A66;}
                            .rocks_front_st139{fill:#70614F;}
                            .rocks_front_st140{fill:#6B5640;}
                            .rocks_front_st141{fill:#665646;}
                            .rocks_front_st142{fill:#62564D;}
                            .rocks_front_st143{fill:#534F5A;}
                            .rocks_front_st144{fill:#58515A;}
                            .rocks_front_st145{fill:#393337;}
                            .rocks_front_st146{fill:#4E423E;}
                            .rocks_front_st147{fill:#4B4142;}
                            .rocks_front_st148{fill:#484245;}
                            .rocks_front_st149{fill:#4F4444;}
                            .rocks_front_st150{fill:#4F403B;}
                            .rocks_front_st151{fill:#584A47;}
                            .rocks_front_st152{fill:#574944;}
                            .rocks_front_st153{fill:#483C38;}
                            .rocks_front_st154{fill:#58473D;}
                            .rocks_front_st155{fill:#4B444B;}
                            .rocks_front_st156{fill:#342E31;}
                            .rocks_front_st157{fill:#564640;}
                            .rocks_front_st158{fill:#4C454C;}
                            .rocks_front_st159{fill:#5F5256;}
                            .rocks_front_st160{fill:#53494F;}
                            .rocks_front_st161{fill:#6B676D;}
                            .rocks_front_st162{fill:#58575E;}
                            .rocks_front_st163{fill:#4F4C4F;}
                            .rocks_front_st164{fill:#574E53;}
                            .rocks_front_st165{fill:#696065;}
                            .rocks_front_st166{fill:#3D2001;}
                            .rocks_front_st167{fill:#3E363B;}
                            .rocks_front_st168{fill:#827778;}
                            .rocks_front_st169{fill:#4F474C;}
                            .rocks_front_st170{fill:#4D474D;}
                            .rocks_front_st171{fill:#413B3F;}
                            .rocks_front_st172{fill:#383339;}
                            .rocks_front_st173{fill:#4E474D;}
                            .rocks_front_st174{fill:#514642;}
                            .rocks_front_st175{fill:#61554E;}
                            .rocks_front_st176{fill:#3F3838;}
                            .rocks_front_st177{fill:#343032;}
                            .rocks_front_st178{fill:#3E3739;}
                            .rocks_front_st179{fill:#514643;}
                            .rocks_front_st180{fill:#413835;}
                            .rocks_front_st181{fill:#47403D;}
                            .rocks_front_st182{fill:#3E3533;}
                            .rocks_front_st183{fill:#58493E;}
                            .rocks_front_st184{fill:#5D4D42;}
                            .rocks_front_st185{fill:#3A3131;}
                            .rocks_front_st186{fill:#615147;}
                            .rocks_front_st187{fill:#7A675C;}
                            .rocks_front_st188{fill:#7A665A;}
                            .rocks_front_st189{fill:#403531;}
                            .rocks_front_st190{fill:#7F6B5C;}
                            .rocks_front_st191{fill:#877365;}
                            .rocks_front_st192{fill:#867366;}
                            .rocks_front_st193{fill:#6B584B;}
                            .rocks_front_st194{fill:#968375;}
                            .rocks_front_st195{fill:#786860;}
                            .rocks_front_st196{fill:#5A4F49;}
                            .rocks_front_st197{fill:#3A312F;}
                            .rocks_front_st198{fill:#2E2928;}
                            .rocks_front_st199{fill:#2F2627;}
                            .rocks_front_st200{fill:#3A2F2D;}
                            .rocks_front_st201{fill:#41332E;}
                            .rocks_front_st202{fill:#392B26;}
                            .rocks_front_st203{fill:#3A2B25;}
                            .rocks_front_st204{fill:#39281F;}
                            .rocks_front_st205{fill:#38261C;}
                            .rocks_front_st206{fill:#432D1F;}
                            .rocks_front_st207{fill:#433B42;}
                            .rocks_front_st208{fill:#5D4B42;}
                            .rocks_front_st209{fill:#5D4F49;}
                            .rocks_front_st210{fill:#635650;}
                            .rocks_front_st211{fill:#675850;}
                            .rocks_front_st212{fill:#6B5C57;}
                            .rocks_front_st213{fill:#6D5D58;}
                            .rocks_front_st214{fill:#5F524D;}
                            .rocks_front_st215{fill:#5C4F49;}
                            .rocks_front_st216{fill:#5F514A;}
                            .rocks_front_st217{fill:#5A4D47;}
                            .rocks_front_st218{fill:#5A4942;}
                            .rocks_front_st219{fill:#5A4B44;}
                            .rocks_front_st220{fill:#776960;}
                            .rocks_front_st221{fill:#6E5E52;}
                            .rocks_front_st222{fill:#75665E;}
                            .rocks_front_st223{fill:#5B4C44;}
                            .rocks_front_st224{fill:#7F7264;}
                            .rocks_front_st225{fill:#63544C;}
                            .rocks_front_st226{fill:#756458;}
                            .rocks_front_st227{fill:#6D5C51;}
                            .rocks_front_st228{fill:#745E4D;}
                            .rocks_front_st229{fill:#3B312D;}
                            .rocks_front_st230{fill:#4B413A;}
                            .rocks_front_st231{fill:#726051;}
                            .rocks_front_st232{fill:#382F2B;}
                            .rocks_front_st233{fill:#322C29;}
                            .rocks_front_st234{fill:#302A2A;}
                            .rocks_front_st235{fill:#534539;}
                            .rocks_front_st236{fill:#51443C;}
                            .rocks_front_st237{fill:#53463D;}
                            .rocks_front_st238{fill:#605148;}
                            .rocks_front_st239{fill:#4D413A;}
                            .rocks_front_st240{fill:#56483E;}
                            .rocks_front_st241{fill:#52453C;}
                            .rocks_front_st242{fill:#4B3F3B;}
                            .rocks_front_st243{fill:#695B55;}
                            .rocks_front_st244{fill:#564741;}
                            .rocks_front_st245{fill:#423938;}
                            .rocks_front_st246{fill:#4D423D;}
                            .rocks_front_st247{fill:#7C7272;}
                            .rocks_front_st248{fill:#574B45;}
                            .rocks_front_st249{fill:#4A3E3C;}
                            .rocks_front_st250{fill:#655955;}
                            .rocks_front_st251{fill:#463F3C;}
                            .rocks_front_st252{fill:#494041;}
                            .rocks_front_st253{fill:#453E40;}
                            .rocks_front_st254{fill:#51494B;}
                            .rocks_front_st255{fill:#726A6D;}
                            .rocks_front_st256{fill:#696163;}
                            .rocks_front_st257{fill:#4C4448;}
                            .rocks_front_st258{fill:#484146;}
                            .rocks_front_st259{fill:#6F696D;}
                            .rocks_front_st260{fill:#5E5551;}
                            .rocks_front_st261{fill:#675445;}
                            .rocks_front_st262{fill:#6B6B76;}
                            .rocks_front_st263{fill:#493629;}
                            .rocks_front_st264{fill:#614C39;}
                            .rocks_front_st265{fill:#5A4530;}
                            .rocks_front_st266{fill:#59412C;}
                            .rocks_front_st267{fill:#635345;}
                            .rocks_front_st268{fill:#523A24;}
                            .rocks_front_st269{fill:#4C403A;}
                            .rocks_front_st270{fill:#58432E;}
                            .rocks_front_st271{fill:#59412A;}
                            .rocks_front_st272{fill:#553D26;}
                            .rocks_front_st273{fill:#493622;}
                            .rocks_front_st274{fill:#503924;}
                            .rocks_front_st275{fill:#5C4E41;}
                            .rocks_front_st276{fill:#472E15;}
                            .rocks_front_st277{fill:#7F6F66;}
                            .rocks_front_st278{fill:#4F3921;}
                            .rocks_front_st279{fill:#32261C;}
                            .rocks_front_st280{fill:#382A21;}
                            .rocks_front_st281{fill:#120E0C;}
                            .rocks_front_st282{fill:#503D2E;}
                            .rocks_front_st283{fill:#442E1D;}
                            .rocks_front_st284{fill:#5A4631;}
                            .rocks_front_st285{fill:#4F3C2C;}
                            .rocks_front_st286{fill:#67564B;}
                            .rocks_front_st287{fill:#18110F;}
                            .rocks_front_st288{fill:#271A0F;}
                            .rocks_front_st289{fill:#553619;}
                            .rocks_front_st290{fill:#442E19;}
                            .rocks_front_st291{fill:#755332;}
                            .rocks_front_st292{fill:#403223;}
                            .rocks_front_st293{fill:#4F3F2E;}
                            .rocks_front_st294{fill:#7C5935;}
                            .rocks_front_st295{fill:#584736;}
                            .rocks_front_st296{fill:#937B56;}
                            .rocks_front_st297{fill:#3D332A;}
                            .rocks_front_st298{fill:#4E4139;}
                            .rocks_front_st299{fill:#372E23;}
                            .rocks_front_st300{fill:#4B3D33;}
                            .rocks_front_st301{fill:#4D4138;}
                            .rocks_front_st302{fill:#574B40;}
                            .rocks_front_st303{fill:#4B3E37;}
                            .rocks_front_st304{fill:#56483B;}
                            .rocks_front_st305{fill:#443830;}
                            .rocks_front_st306{fill:#2F2825;}
                            .rocks_front_st307{fill:#483B33;}
                            .rocks_front_st308{fill:#C0B0A2;}
                            .rocks_front_st309{fill:#4E3D30;}
                            .rocks_front_st310{fill:#AB9C90;}
                            .rocks_front_st311{fill:#332A24;}
                            .rocks_front_st312{fill:#362C24;}
                            .rocks_front_st313{fill:#7D6E61;}
                            .rocks_front_st314{fill:#514137;}
                            .rocks_front_st315{fill:#8E7E71;}
                            .rocks_front_st316{fill:#69584A;}
                            .rocks_front_st317{fill:#AA9C8D;}
                            .rocks_front_st318{fill:#937D6B;}
                            .rocks_front_st319{fill:#F7E8D9;}
                            .rocks_front_st320{fill:#C3B4A6;}
                            .rocks_front_st321{fill:#F6E8D9;}
                            .rocks_front_st322{fill:#998B82;}
                            .rocks_front_st323{fill:#928376;}
                            .rocks_front_st324{fill:#574734;}
                            .rocks_front_st325{fill:#63594B;}
                            .rocks_front_st326{fill:#583D23;}
                            .rocks_front_st327{fill:#8C6033;}
                            .rocks_front_st328{fill:#613508;}
                            .rocks_front_st329{fill:#6C441D;}
                            .rocks_front_st330{fill:#806D5F;}
                            .rocks_front_st331{fill:#583D21;}
                            .rocks_front_st332{fill:#594B3D;}
                            .rocks_front_st333{fill:#66584B;}
                            .rocks_front_st334{fill:#82705E;}
                            .rocks_front_st335{fill:#6D615E;}
                            .rocks_front_st336{fill:#EAD4C1;}
                            .rocks_front_st337{fill:#907D6C;}
                            .rocks_front_st338{fill:#A8907E;}
                            .rocks_front_st339{fill:#D3B69F;}
                            .rocks_front_st340{fill:#A78769;}
                            .rocks_front_st341{fill:#80736D;}
                            .rocks_front_st342{fill:#9B7E6B;}
                            .rocks_front_st343{fill:#BAACA7;}
                            .rocks_front_st344{fill:#39251C;}
                            .rocks_front_st345{fill:#57422D;}
                            .rocks_front_st346{fill:#908078;}
                        </style>
                    <g>
                        <polygon class="rocks_front_st0" points="958.5,100.7 980.5,148.8 985,182.9 976,183.2 	"/>
                        <polygon class="rocks_front_st1" points="931.5,94.2 912.5,89.5 933.2,181.2 	"/>
                        <polygon class="rocks_front_st2" points="1046.5,81.7 1031.5,103.7 984.5,103.7 974.9,103.7 986,78.6 	"/>
                        <polygon class="rocks_front_st3" points="1622.8,216.5 1543,215.2 1569.8,221.2 1633.8,243.8 	"/>
                        <polygon class="rocks_front_st4" points="1119.1,182.8 1111.3,123.5 1078.6,96.2 1078.5,97.5 1075.6,183.7 	"/>
                        <polygon class="rocks_front_st5" points="441.2,255.5 574.3,260.8 500.5,252.2 	"/>
                        <polygon class="rocks_front_st6" points="578.9,186.7 544,183.7 523.2,209.4 545.9,214.7 	"/>
                        <polygon class="rocks_front_st7" points="757.7,84.5 732,141.7 713.7,147.2 728.2,121.2 	"/>
                        <polygon class="rocks_front_st8" points="774.1,97 796.3,87.8 754.6,143.7 	"/>
                        <polygon class="rocks_front_st9" points="757.7,84.5 774.1,97 732,141.7 	"/>
                        <polygon class="rocks_front_st10" points="871,98.2 850.3,94.5 818.1,113.4 848.8,107.5 	"/>
                        <polygon class="rocks_front_st11" points="848.8,107.5 785.2,147.2 735.5,184.2 803.8,147.2 	"/>
                        <polygon class="rocks_front_st12" points="842.5,123.8 865.6,170.2 858.6,182.4 	"/>
                        <polygon class="rocks_front_st13" points="773.2,249.8 781.5,245.8 794.5,241.6 814,238.3 781.2,256.1 	"/>
                        <polygon class="rocks_front_st14" points="748.7,246.6 742.7,238.3 772.3,231.7 794.5,241.6 781.5,245.8 	"/>
                        <polygon class="rocks_front_st15" points="910.8,260.8 781.2,256.1 841.3,254.7 971,254.1 1011.5,252.2 1057.3,247.6 1081.9,248.9
                                1205.4,244.8 1281.4,240.8 1326.8,246.7 1370.6,245.8 1392,238.5 1452.7,239 1610.8,250 1561.5,255.9 1454.5,250.2 1290,256.3
                                1104,253.7 	"/>
                        <polygon class="rocks_front_st16" points="781.2,256.1 910.8,260.8 1104,253.7 1185.5,247.3 933.2,241.6 	"/>
                        <polygon class="rocks_front_st17" points="826.1,257.6 910.8,260.8 650.8,293.7 722.5,273.7 	"/>
                        <polygon class="rocks_front_st18" points="910.8,260.8 1058,300.9 1104,253.7 	"/>
                        <polygon class="rocks_front_st19" points="1062.5,182.7 1101.5,183.2 1094.5,199.9 	"/>
                        <polygon class="rocks_front_st20" points="1105.2,249.9 1046,189.9 1094.5,199.4 1120.1,249.4 	"/>
                        <polygon class="rocks_front_st21" points="1175.3,215.9 1197.8,183.5 1131,182.8 	"/>
                        <polygon class="rocks_front_st22" points="1266.8,215.9 1239.2,182.8 1230.8,193.2 	"/>
                        <polygon class="rocks_front_st23" points="1326.8,246.7 1314.2,225.3 1316.2,189.5 1361.5,229.7 1371.5,246.7 	"/>
                        <polygon class="rocks_front_st24" points="1385.9,232.8 1479.5,202.5 1448.5,239.7 1392,239.5 	"/>
                        <polygon class="rocks_front_st25" points="1427.8,172.7 1543.2,183.5 1508.8,172.5 	"/>
                        <polygon class="rocks_front_st26" points="1432.2,177.3 1541.5,215.2 1479.5,202.5 	"/>
                        <polygon class="rocks_front_st27" points="1090.8,302.4 1169,310.6 1221,314.7 1416.5,314.7 1483.5,308.7 1517.8,309.4 1723.5,301
                                1747.2,304.9 1729.8,299.1 1747.5,295.7 1765.5,295.7 1768,291.4 1785.7,287.8 1721.7,281.6 1710.5,284.4 1552.8,283 1570.3,291.7
                                1511.3,295.2 	"/>
                        <polygon class="rocks_front_st28" points="1407.3,333.8 1416.5,311.7 1385,316 	"/>
                        <polygon class="rocks_front_st29" points="2120,337.7 2120,398.7 1440.5,398.7 1541.5,341.2 1880.8,316.6 1957.5,328.9 	"/>
                        <polygon class="rocks_front_st30" points="1742.5,319.8 1723.5,300.7 1745.3,304.4 1752.3,318.6 	"/>
                        <polygon class="rocks_front_st31" points="1732.9,387.3 1843.5,500.2 1659.8,498.7 1661,389.3 	"/>
                        <polygon class="rocks_front_st32" points="1732.5,387.3 1843.5,500.2 1812.5,381.5 	"/>
                        <polygon class="rocks_front_st33" points="1633.8,498.7 1520,387.9 1490.4,376.3 1578,498.7 	"/>
                        <polygon class="rocks_front_st34" points="1471.9,366 1501.6,364.2 1520,387.9 1490.4,376.3 	"/>
                        <polygon class="rocks_front_st35" points="1543.2,350.2 1839.8,360.2 1780.6,361.4 1755.3,370 1664.5,382.4 1611.3,386.2 1584.4,381.5
                                1602.8,376.2 1564.8,373.4 1572.5,368.2 1548.8,365.9 1575.2,363.4 1559,361.4 1576.5,360.2 1564.3,358.7 1578,354.9 1551.8,352.2
                                    "/>
                        <polygon class="rocks_front_st36" points="844.8,294.2 910.8,260.8 650.8,293.7 	"/>
                        <polygon class="rocks_front_st37" points="844.8,293.7 922.9,336.7 681.8,316.4 	"/>
                        <polygon class="rocks_front_st38" points="375.4,340.4 289.4,313.2 424.5,295 527.7,357.8 	"/>
                        <polygon class="rocks_front_st39" points="448.2,269.3 424.5,295 294,275.8 246,270.8 	"/>
                        <polygon class="rocks_front_st40" points="728.2,121.2 713.7,147.2 650.8,186.7 671,170.2 695.5,143.4 	"/>
                        <polygon class="rocks_front_st41" points="933.9,182.9 912.5,89.5 915.2,181.7 	"/>
                        <polygon class="rocks_front_st42" points="572.5,169.9 578.9,186.7 544,183.7 	"/>
                        <polygon class="rocks_front_st43" points="1290,256.2 1461.8,270.8 1454.5,250 	"/>
                        <polygon class="rocks_front_st44" points="1104,253.7 1058,300.9 1290,256.2 	"/>
                        <polygon class="rocks_front_st45" points="1094.5,199.4 1120.1,249.4 1125,249.2 1101.5,182.7 	"/>
                        <polygon class="rocks_front_st46" points="1046,189.9 1094.5,199.4 1062.5,182.7 	"/>
                        <polygon class="rocks_front_st47" points="1175.3,215.9 1197.8,183.5 1232.8,245.9 	"/>
                        <polygon class="rocks_front_st48" points="1272.3,188.2 1239.2,182.8 1266.8,215.9 	"/>
                        <polygon class="rocks_front_st49" points="1439.5,246.7 1561.5,255.2 1675.5,274.2 1531,268.8 1454.5,250 	"/>
                        <polygon class="rocks_front_st50" points="1461.8,270.8 1454.5,250 1526.3,281.7 	"/>
                        <polygon class="rocks_front_st51" points="1742.5,319.8 1734.3,322.7 1723.5,300.7 	"/>
                        <polygon class="rocks_front_st52" points="1501.6,364.2 1520,387.9 1529.7,398.2 1540.4,377.1 1543.6,372.3 1532.6,363.3 1509.5,360.2
                                1524.6,367.7 	"/>
                        <polygon class="rocks_front_st53" points="1385,316 1366.2,335.2 1396.3,336.3 1418.2,342.5 	"/>
                        <polygon class="rocks_front_st54" points="1058,300.9 1290,256.2 1511.3,295.2 1090.8,302.4 	"/>
                        <polygon class="rocks_front_st55" points="818.1,113.4 848.8,107.5 785.2,147.2 	"/>
                        <polygon class="rocks_front_st56" points="780.8,76.5 790.5,80.8 774.1,97 	"/>
                        <polygon class="rocks_front_st57" points="650.8,293.7 681.8,316.4 844.8,294.2 	"/>
                        <polygon class="rocks_front_st58" points="424.5,295 290.3,314 195.5,303.7 94.9,303.2 161.8,296.3 293.8,275.9 	"/>
                        <polygon class="rocks_front_st59" points="375.4,340.4 210.2,341.2 289.4,312.9 	"/>
                        <polygon class="rocks_front_st60" points="137,270.9 246,270.8 448.2,269.3 441.2,255.5 293.8,260.8 143.4,256.7 	"/>
                        <polygon class="rocks_front_st0" points="949.5,102.2 931.4,94.9 933.2,185.7 943.5,185.7 	"/>
                        <polygon class="rocks_front_st61" points="949.3,102.2 958.5,103.2 976.1,102.2 987,77.7 965.5,78.5 953,82 	"/>
                        <polygon class="rocks_front_st62" points="965.5,0.7 980.5,0.7 993.8,9.9 991.5,79.7 986.5,79.7 964.5,79.7 	"/>
                        <polygon class="rocks_front_st63" points="871,98.2 777.5,181.2 803.8,147.2 848.8,107.5 	"/>
                        <polygon class="rocks_front_st64" points="858.6,182.4 842.2,122.8 777.5,181.2 796.3,191 827.5,180.3 850.3,185.6 853.3,182.7 	"/>
                        <polygon class="rocks_front_st65" points="1540,181.3 1621.3,216.5 1540,215.2 	"/>
                        <polygon class="rocks_front_st66" points="650.8,293.7 626.5,278.4 722.5,273.7 	"/>
                        <polygon class="rocks_front_st67" points="441.2,255.5 650.8,293.7 626.5,278.4 603,264.9 574.3,260.8 	"/>
                        <polygon class="rocks_front_st68" points="0,226.3 128.9,235.4 144.3,256.7 0,243.3 	"/>
                        <polygon class="rocks_front_st69" points="406.9,256.8 319.7,251.9 143.4,256.7 293.8,260.8 	"/>
                        <polygon class="rocks_front_st70" points="526.5,357.5 424.5,295 618.9,365.5 	"/>
                        <polygon class="rocks_front_st71" points="890.8,407.6 798.1,391.7 864.5,374.5 974.9,383.7 900.3,397.3 	"/>
                        <polygon class="rocks_front_st72" points="1396.3,336.3 1387.8,339.6 1302.5,336.3 1283.5,332.2 	"/>
                        <polygon class="rocks_front_st73" points="1716,335 1723.5,300.7 1734.3,322.7 	"/>
                        <polygon class="rocks_front_st74" points="1561.5,255.2 1675.5,274.2 1710.5,284.2 1720.5,281.7 1782.5,287.7 1690,266.7 1606.5,250 	"/>
                        <polygon class="rocks_front_st75" points="871,98.2 873,175.7 841,123.8 	"/>
                        <polygon class="rocks_front_st76" points="1980.9,473.1 1976.5,447.1 1915.8,374.3 1882.1,376.1 	"/>
                        <polygon class="rocks_front_st77" points="1716,335 1737.3,340.7 1734.3,322.7 	"/>
                        <polygon class="rocks_front_st78" points="1344.8,348.9 1387.8,339.6 1302.5,336.3 	"/>
                        <polygon class="rocks_front_st79" points="0,369.5 193,300.7 289.9,313.2 211.1,341.2 	"/>
                        <polygon class="rocks_front_st80" points="737.3,397.2 788.8,398.7 798.1,391.7 864.5,374.8 974.9,383.7 947.8,374.3 959.3,371.2 922.9,336.7
                                "/>
                        <polygon class="rocks_front_st81" points="1479.5,202.5 1539.5,245.6 1557.2,246.8 1569.8,221.2 1543,215.2 	"/>
                        <polygon class="rocks_front_st82" points="359.2,228.7 444.1,238.1 537.2,228.7 	"/>
                        <polygon class="rocks_front_st5" points="319.7,251.9 440.4,237.1 359.5,228.2 	"/>
                        <polygon class="rocks_front_st83" points="666.7,248.5 699.9,230.4 687.9,232.5 683.2,228.7 537.5,228.7 445.1,236.1 318.5,251.9 406.5,257.9
                                500.5,252.4 	"/>
                        <polygon class="rocks_front_st84" points="604.2,253.8 781.5,256.2 773.2,249.8 781.5,245.8 666.7,248.5 535.8,251.4 	"/>
                        <polygon class="rocks_front_st85" points="1569.8,221.2 1606.5,250 1735.1,276.9 1695.5,241.5 1543.2,183.5 1622.8,216.5 1633.8,243.8 	"/>
                        <polygon class="rocks_front_st86" points="1572.5,173.8 1695.5,241.5 1735.1,276.9 1782.5,287.7 1811.6,293.9 1877.8,311.7 	"/>
                        <polygon class="rocks_front_st87" points="1742.5,319.8 1764.5,321.2 1880.8,316.6 1806,311.7 1787.3,310.6 1765.3,315.3 	"/>
                        <polygon class="rocks_front_st16" points="947.8,374.2 974.9,383.5 1215.1,387.4 1193.9,392.6 1332.5,381.5 1340.8,376.8 1387.8,373.8
                                1444.7,376 1390.8,371.2 1501.6,364.2 1509.5,360.9 1510,355.7 1498.5,355.7 1495.2,357.3 1470.8,351.2 1430.8,354.3 1352.9,358.8
                                1282.6,358.3 1119.2,358.2 1078.5,363.2 958.8,370.8 	"/>
                        <polygon class="rocks_front_st88" points="1541.5,183.5 1630.7,205.8 1695.5,241.5 	"/>
                        <polygon class="rocks_front_st89" points="1575.2,159.8 1572.5,173.8 1630.7,205.8 1540.5,183.5 1508.8,172.5 	"/>
                        <polygon class="rocks_front_st85" points="1429.5,174.7 1321.8,174.7 1318.7,163.7 1575.2,160.6 1508.8,174.4 	"/>
                        <polygon class="rocks_front_st90" points="1392,239.5 1343.8,187.5 1334.5,188.2 1361.5,229.2 1371.5,246.2 	"/>
                        <polygon class="rocks_front_st91" points="1314.6,189.3 1335.4,188.1 1361.8,230.1 	"/>
                        <polygon class="rocks_front_st92" points="1272.2,188.2 1316.2,189.2 1314.2,224.8 	"/>
                        <polygon class="rocks_front_st93" points="1343.8,187.5 1310.9,183.6 1315.7,190 1335.4,188.1 	"/>
                        <polygon class="rocks_front_st94" points="1312.5,185.7 1239.5,185.7 1272.2,188.9 1316.2,189.8 	"/>
                        <polygon class="rocks_front_st95" points="1197.8,183.5 1230.8,193.2 1239.5,182.7 	"/>
                        <polygon class="rocks_front_st23" points="1314,225.3 1343.8,246.7 1326.5,246.7 	"/>
                        <polygon class="rocks_front_st96" points="1361.5,229.7 1314,225 1343.8,246.7 1371.5,246.2 	"/>
                        <polygon class="rocks_front_st97" points="1266.8,215.9 1314,223.8 1272.2,188.2 	"/>
                        <polygon class="rocks_front_st98" points="1258.7,245.3 1279.5,244.8 1266.8,215.9 	"/>
                        <polygon class="rocks_front_st99" points="1314,223.8 1300,244.3 1326.5,247.3 	"/>
                        <polygon class="rocks_front_st100" points="1175.3,215.9 1232.8,245.9 1206.5,246.6 	"/>
                        <polygon class="rocks_front_st101" points="1185.5,247.3 1206.5,246.6 1175.3,215.9 	"/>
                        <polygon class="rocks_front_st102" points="1131,182.8 1138.3,205.3 1168,247.8 1185.5,247.3 1175.3,215.9 	"/>
                        <polygon class="rocks_front_st103" points="1101.5,182.7 1131,182.8 1138.3,205.3 	"/>
                        <polygon class="rocks_front_st104" points="1125,249.2 1101.5,182.7 1138.3,205.3 1168,247.8 	"/>
                        <polygon class="rocks_front_st105" points="1054.3,250.2 1046,189.4 1081.5,250.7 	"/>
                        <polygon class="rocks_front_st106" points="1081.5,250.7 1046,189.9 1105.2,249.9 	"/>
                        <polygon class="rocks_front_st107" points="1005.5,183.2 1062.5,182.7 1046,190.4 	"/>
                        <polygon class="rocks_front_st108" points="1014,202.9 1005.5,182.7 1046,189.9 	"/>
                        <polygon class="rocks_front_st109" points="949.5,186.3 1014,202.9 1005.5,182.7 	"/>
                        <polygon class="rocks_front_st110" points="1011.5,250.7 1014,202.9 1037.5,251 	"/>
                        <polygon class="rocks_front_st111" points="1055.3,250.7 1037.5,251 1014,202.9 1046,189.9 	"/>
                        <polygon class="rocks_front_st112" points="949.5,186.3 994.7,251.9 1011.5,250.7 1014,202.9 	"/>
                        <polygon class="rocks_front_st113" points="971,253.6 949.5,186.3 994.7,251.9 	"/>
                        <polygon class="rocks_front_st114" points="936.7,254.1 949.5,186.3 971,253.6 	"/>
                        <polygon class="rocks_front_st115" points="896.2,254.6 901,221.9 949.5,186.3 936.7,254.1 	"/>
                        <polygon class="rocks_front_st116" points="890,181.2 901,221.9 949.5,186.3 	"/>
                        <polygon class="rocks_front_st117" points="841.3,254.7 901,221.9 896.2,254.6 	"/>
                        <polygon class="rocks_front_st118" points="890,181.2 841.3,254.7 901,222 	"/>
                        <polygon class="rocks_front_st119" points="862.5,183.7 890,181.2 841.3,254.7 	"/>
                        <polygon class="rocks_front_st120" points="814,238.3 841.3,254.7 781.2,256.1 	"/>
                        <polygon class="rocks_front_st121" points="862.5,183.7 814,238.3 841.3,254.7 	"/>
                        <polygon class="rocks_front_st122" points="853.3,182.7 862.5,183.7 890,181.2 	"/>
                        <polygon class="rocks_front_st123" points="794.5,241.6 853.3,182.7 862.5,183.7 814,238.3 	"/>
                        <polygon class="rocks_front_st124" points="666.7,248.5 730.7,217.9 793.6,199.5 827.5,180.3 734.7,212.1 699.9,230.4 	"/>
                        <polygon class="rocks_front_st125" points="850.3,185.6 827.5,180.3 793.6,199.5 	"/>
                        <polygon class="rocks_front_st124" points="730.7,217.9 737.2,232.5 713.7,247.4 666.7,248.5 	"/>
                        <polygon class="rocks_front_st126" points="772.3,231.7 826.1,209.9 850.3,185.6 793.6,199.5 730.7,217.9 737.2,232.5 	"/>
                        <polygon class="rocks_front_st127" points="794.5,241.6 772.3,231.7 826.1,209.9 	"/>
                        <polygon class="rocks_front_st128" points="737.2,232.5 742.7,238.3 772.3,231.7 	"/>
                        <polygon class="rocks_front_st13" points="730.7,247 742.7,238.3 737.2,232.5 713.7,247.4 	"/>
                        <polygon class="rocks_front_st121" points="748.7,246.6 742.7,238.3 730.7,247 	"/>
                        <polygon class="rocks_front_st129" points="1569.8,221.2 1606.5,250 1557.2,246.8 	"/>
                        <polygon class="rocks_front_st130" points="1487.7,242.2 1479.5,202.5 1539.5,245.6 	"/>
                        <polygon class="rocks_front_st131" points="1448.5,239.7 1479.5,202.5 1487.7,242.2 	"/>
                        <polygon class="rocks_front_st132" points="1368,190.8 1432.1,177.3 1479.5,202.7 	"/>
                        <polygon class="rocks_front_st133" points="1385.9,232.8 1358,202.8 1368,190.8 1479.5,202.7 	"/>
                        <polygon class="rocks_front_st134" points="1378.5,174.7 1307.1,173.4 1368,191.4 1408.7,173.4 	"/>
                        <polygon class="rocks_front_st135" points="1432.1,177.3 1429.2,173.2 1407.3,173.2 1367.7,191.2 	"/>
                        <polygon class="rocks_front_st136" points="1353.6,198 1368,190.8 1358,202.8 	"/>
                        <polygon class="rocks_front_st137" points="1292.7,185.7 1307.5,173.2 1368,191.1 1353.6,198.1 1343.8,188.4 1312.5,185.7 	"/>
                        <polygon class="rocks_front_st97" points="1281.3,185.7 1295.5,173.2 1307.5,173.2 1292.7,185.7 	"/>
                        <polygon class="rocks_front_st138" points="1710.5,284.2 1675.5,274.2 1654,283.7 	"/>
                        <polygon class="rocks_front_st139" points="1570.3,291.7 1552.8,282.9 1531,268.8 1454.5,250 1526.3,281.7 	"/>
                        <polygon class="rocks_front_st140" points="1552.8,282.9 1531,268.8 1675.5,274.2 1654,283.7 	"/>
                        <polygon class="rocks_front_st141" points="1511.3,295.2 1461.8,270.8 1526.3,281.7 1570.3,291.7 	"/>
                        <polygon class="rocks_front_st142" points="1511.3,295.2 1290,256.2 1461.8,270.8 	"/>
                        <polygon class="rocks_front_st143" points="781.2,256.1 722.5,273.7 826.1,257.6 	"/>
                        <polygon class="rocks_front_st144" points="604.2,253.8 574.3,260.8 500.5,252.2 535.8,251.4 	"/>
                        <polygon class="rocks_front_st66" points="781.2,256.1 603,264.9 574.3,260.8 604.2,253.8 	"/>
                        <polygon class="rocks_front_st145" points="722.5,273.7 626.5,278.4 603,264.9 781.2,256.1 	"/>
                        <polygon class="rocks_front_st5" points="441.2,255.5 571.2,303.2 681.8,316.4 650.8,293.7 	"/>
                        <polygon class="rocks_front_st67" points="571.2,303.2 448.2,269.3 441.2,255.5 	"/>
                        <polygon class="rocks_front_st84" points="590,250.6 650.8,239.1 535.8,251.4 	"/>
                        <polygon class="rocks_front_st146" points="617.5,365.2 571.2,303.2 681.8,316.4 	"/>
                        <polygon class="rocks_front_st147" points="922.9,336.7 737.7,397.2 681.8,316.4 	"/>
                        <polygon class="rocks_front_st148" points="936.7,295.2 910.8,260.8 844.8,293.7 	"/>
                        <polygon class="rocks_front_st66" points="922.9,336.7 844.8,293.7 936.7,295.2 	"/>
                        <polygon class="rocks_front_st149" points="737.7,397.2 617.5,365.2 681.8,316.4 	"/>
                        <polygon class="rocks_front_st144" points="424.5,295 448.2,269.3 571.2,303.2 	"/>
                        <polygon class="rocks_front_st150" points="617.5,365.2 424.5,295 571.2,303.2 	"/>
                        <polygon class="rocks_front_st151" points="737.7,397.2 618,364.8 558.2,424.8 	"/>
                        <polygon class="rocks_front_st152" points="526.5,357.1 558.2,424.8 618.9,365.2 	"/>
                        <polygon class="rocks_front_st153" points="375.4,339.8 291.7,383.4 558.2,424.8 	"/>
                        <polygon class="rocks_front_st154" points="526.5,357.1 558.2,424.8 374.4,339.8 	"/>
                        <polygon class="rocks_front_st155" points="161.7,296.5 137,270.9 246,270.8 293.8,275.7 	"/>
                        <polygon class="rocks_front_st156" points="293.3,382.2 0,424.8 0,498.7 594.5,498.7 557.6,423.6 	"/>
                        <polygon class="rocks_front_st157" points="0,369.5 211.3,341.2 375,340.4 293,383.2 0,424.8 	"/>
                        <polygon class="rocks_front_st158" points="0,314 162,296.5 137.7,270.9 0,299.2 	"/>
                        <polygon class="rocks_front_st159" points="143.8,256.7 137.7,270.9 0,299.2 	"/>
                        <polygon class="rocks_front_st60" points="359.5,228.2 344.5,227.2 319.7,251.9 	"/>
                        <polygon class="rocks_front_st160" points="237.5,225.7 344.5,227.2 319.7,251.9 267.5,253.5 	"/>
                        <polygon class="rocks_front_st161" points="143.8,256.7 128.5,235.2 267.5,253.5 	"/>
                        <polygon class="rocks_front_st162" points="237.5,225.7 129.1,235.4 267.5,253.5 	"/>
                        <polygon class="rocks_front_st163" points="131.3,226.2 0,226.3 128.9,235.4 237.5,225.7 	"/>
                        <polygon class="rocks_front_st164" points="144.2,256.7 0,263.2 0,299.2 	"/>
                        <polygon class="rocks_front_st165" points="0,243.3 144.2,256.7 0,263.2 	"/>
                        <polygon class="rocks_front_st166" points="845.6,399.7 695.8,464 798.1,391.7 	"/>
                        <polygon class="rocks_front_st167" points="936.7,295.2 1078.5,363.2 958.8,370.8 922.9,336.7 	"/>
                        <polygon class="rocks_front_st168" points="1282.2,359.5 1258.7,356.4 1175.3,352.7 1119.2,358.2 	"/>
                        <polygon class="rocks_front_st169" points="936.7,295.2 1175.3,352.7 1119.2,358.2 1078.5,363.2 	"/>
                        <polygon class="rocks_front_st170" points="1090.8,302.4 1082,306.5 1102,322.5 1134.7,336.7 1175.3,352.7 936.7,295.2 910.8,260.8 1058,300.9
                                    "/>
                        <polygon class="rocks_front_st171" points="1087.2,342.2 1111.2,349.2 1109.8,354.5 1119.2,358.2 1175.3,352.7 1088.9,331.9 	"/>
                        <polygon class="rocks_front_st172" points="1082,306.5 1070.3,311.7 1075.6,321.2 1090.8,327.2 1088.9,331.9 1175.3,352.7 1133.8,336.3
                                1102,322.5 	"/>
                        <polygon class="rocks_front_st173" points="974.9,383.7 1087.3,498.7 803.8,498.7 900.8,397.2 	"/>
                        <polygon class="rocks_front_st168" points="1193.9,393.3 1087.3,498.7 973.7,383.1 1217.2,387 	"/>
                        <polygon class="rocks_front_st174" points="1332.5,381.5 1309.8,399.8 1255.3,416.9 1193.9,393.3 	"/>
                        <polygon class="rocks_front_st166" points="1087.3,498.7 1255.3,416.9 1193.9,393.3 	"/>
                        <polygon class="rocks_front_st175" points="1332.5,381.5 1365.3,394.5 1340.8,376.8 	"/>
                        <polygon class="rocks_front_st176" points="1398.2,498.7 1387.8,373.8 1340.8,376.8 1361.5,392.2 	"/>
                        <polygon class="rocks_front_st177" points="1332.5,381.5 1361.5,392.2 1398.2,498.7 	"/>
                        <polygon class="rocks_front_st178" points="1309.8,399.8 1285.5,498.7 1398.2,498.7 1332.5,381.5 	"/>
                        <polygon class="rocks_front_st179" points="1387.8,373.8 1425.8,459.3 1430.8,498.7 1398.2,498.7 	"/>
                        <polygon class="rocks_front_st180" points="1430.8,498.7 1459.5,377.8 1436.4,375.7 1387.8,373.8 1418.2,398.7 	"/>
                        <polygon class="rocks_front_st181" points="1425.8,459.3 1418.2,398.7 1387.8,373.8 	"/>
                        <polygon class="rocks_front_st182" points="1459.5,377.8 1532.6,498.7 1430.8,498.7 	"/>
                        <polygon class="rocks_front_st183" points="1490.4,376.3 1459.5,378.1 1390.8,371.2 1471.9,366 	"/>
                        <polygon class="rocks_front_st184" points="1568.2,498.7 1457.9,375.8 1532.6,498.7 	"/>
                        <polygon class="rocks_front_st185" points="1490.4,376 1578,498.7 1568.2,498.7 1459.5,378 	"/>
                        <polygon class="rocks_front_st186" points="1524.6,367.7 1501.6,364.2 1509.5,360.2 	"/>
                        <polygon class="rocks_front_st187" points="1510,354.2 1525.5,355.9 1532.6,363.3 1509.5,360.2 	"/>
                        <polygon class="rocks_front_st188" points="1611.3,385.7 1662.5,389.3 1732.5,387.3 1664.5,382.4 	"/>
                        <polygon class="rocks_front_st189" points="1546.1,498.7 1611.3,385.7 1661,389.3 1659.8,498.7 	"/>
                        <polygon class="rocks_front_st190" points="1548.8,365.9 1543.5,372.3 1540.3,377.1 1545.8,384.1 1564.8,373.4 1572.5,368.2 	"/>
                        <polygon class="rocks_front_st191" points="1561.5,392.8 1586.3,381.5 1602.8,376.2 1566.2,372.7 	"/>
                        <polygon class="rocks_front_st192" points="1507.5,498.7 1586.3,381 1611.3,385.7 1546.1,498.7 	"/>
                        <polygon class="rocks_front_st193" points="1440.5,498.7 1465.2,498.7 1546.1,398.7 1539.5,389.4 	"/>
                        <polygon class="rocks_front_st194" points="1566.2,372.7 1546.1,399.4 1539.3,389.8 1545.8,383.9 	"/>
                        <polygon class="rocks_front_st195" points="1507.5,498.7 1586.3,381 1561.5,392.6 1566.5,372 1463.8,498.7 	"/>
                        <polygon class="rocks_front_st196" points="1559,361.4 1554.3,365.4 1575.2,363.4 	"/>
                        <polygon class="rocks_front_st197" points="1551.8,352.2 1538,367.7 1543.5,372.3 1548.8,365.9 1554.3,365.4 1559,361.4 1576.5,360.2
                                1564.3,358.7 1578,354.9 	"/>
                        <polygon class="rocks_front_st198" points="1543.2,350.2 1532.6,363.3 1538,367.7 1551.8,352.2 	"/>
                        <polygon class="rocks_front_st199" points="1939.8,432.8 1980.9,473.1 1985.2,498.7 1843.5,500.2 1812.5,381.5 	"/>
                        <polygon class="rocks_front_st200" points="1882.1,376.1 1884.8,393.1 1854.7,399.2 1812.5,381.5 	"/>
                        <polygon class="rocks_front_st201" points="1939.8,432.8 1884.8,392.5 1854.7,398.7 	"/>
                        <polygon class="rocks_front_st201" points="1882.1,375.8 1938.8,431.8 1884.8,393.1 	"/>
                        <polygon class="rocks_front_st202" points="1888,370.7 1918,370 1933.5,369 1918,364.8 	"/>
                        <polygon class="rocks_front_st203" points="1968.4,398.7 1956.2,387.3 1918.4,369.6 1887.6,370.7 1915.8,374.4 1976.5,447.1 	"/>
                        <polygon class="rocks_front_st201" points="1933.5,369 1957.5,388.7 1918,370 	"/>
                        <polygon class="rocks_front_st204" points="1985.2,498.7 2000.3,360.9 1918,364.8 1933.5,369.2 1957,388.5 1968.4,398.7 	"/>
                        <polygon class="rocks_front_st205" points="2067.8,498.7 2000.3,359.9 1985.2,498.7 	"/>
                        <polygon class="rocks_front_st206" points="1985.2,356 2120,356 2120,498.7 2095.5,498.7 2000.3,360.9 	"/>
                        <polygon class="rocks_front_st207" points="98,303.2 193.5,300.7 0,369.5 0,314 	"/>
                        <polygon class="rocks_front_st208" points="1894.8,338.9 1880.8,316.6 1901.8,335.7 	"/>
                        <polygon class="rocks_front_st209" points="1855.5,317.6 1894.8,338.9 1880.8,316.6 	"/>
                        <polygon class="rocks_front_st210" points="1764.5,321.2 1842.3,327.7 1855.5,317.6 	"/>
                        <polygon class="rocks_front_st211" points="1742.5,319.8 1737.3,340.7 1744.8,342.1 1764.5,321.2 	"/>
                        <polygon class="rocks_front_st212" points="1751.8,343.4 1764.5,321.2 1744.8,342.1 	"/>
                        <polygon class="rocks_front_st213" points="1778.5,333.9 1764.5,344.1 1751.8,343.4 1764.5,321.2 	"/>
                        <polygon class="rocks_front_st214" points="1856.8,342.9 1842.3,327.7 1764.5,321.2 1778.5,333.9 1776.8,344.7 	"/>
                        <polygon class="rocks_front_st215" points="1764.5,344.1 1778.5,333.9 1776.8,344.7 	"/>
                        <polygon class="rocks_front_st216" points="1842.3,327.7 1873,327.1 1855.5,317.6 	"/>
                        <polygon class="rocks_front_st217" points="1871.5,341.2 1871.6,327.1 1841.5,327.7 1855.6,342.9 	"/>
                        <polygon class="rocks_front_st218" points="1894.8,338.9 1873,327.1 1878,340.7 	"/>
                        <polygon class="rocks_front_st219" points="1871.5,341.2 1871.6,327.1 1873,327.1 1878,340.7 	"/>
                        <polygon class="rocks_front_st220" points="1793.5,297.6 1782.3,287.7 1811.6,293.9 	"/>
                        <polygon class="rocks_front_st51" points="1745.3,304.4 1752,318.6 1758.8,317.8 1765.7,315.2 	"/>
                        <polygon class="rocks_front_st221" points="1729.8,299.1 1751.8,299.7 1765.3,314.9 1745.3,304.4 	"/>
                        <polygon class="rocks_front_st222" points="1765.7,292.7 1778.6,301.7 1787.5,311.7 1791.5,311.7 1781,294.5 1798.9,307.7 1782.3,287.7
                                1768,290.1 	"/>
                        <polygon class="rocks_front_st223" points="1765.3,315.2 1751.8,299.7 1769.7,309.8 1757.3,298.2 1773.5,305.3 1765.7,292.7 1778.6,300.2
                                1788,311.4 1772.8,314.9 	"/>
                        <polygon class="rocks_front_st224" points="1729.7,299 1747.5,294.2 1765.7,292.7 1774.4,306 1758.1,298.9 1770.7,310.6 1751.8,300.1 	"/>
                        <polygon class="rocks_front_st225" points="1737.3,340.7 1734.3,322.7 1742.5,319.8 	"/>
                        <polygon class="rocks_front_st226" points="1647.8,303.6 1716,335 1723.5,300.7 	"/>
                        <polygon class="rocks_front_st227" points="1657,344.2 1647.8,303.6 1716,335 1737.3,340.7 	"/>
                        <polygon class="rocks_front_st226" points="1580,306.2 1657,344.2 1647.8,303.6 	"/>
                        <polygon class="rocks_front_st228" points="1610.2,346.6 1580,306.2 1657,344.2 	"/>
                        <polygon class="rocks_front_st54" points="1576.9,348.4 1580,306.2 1610.2,346.6 	"/>
                        <polygon class="rocks_front_st229" points="1517.6,308.7 1564.8,349 1576.9,348.4 1580,306.2 	"/>
                        <polygon class="rocks_front_st230" points="1479.5,316.7 1517.8,308.7 1483.5,307.2 	"/>
                        <polygon class="rocks_front_st231" points="1416.5,311.7 1479.5,316.7 1483.5,307.2 	"/>
                        <polygon class="rocks_front_st232" points="1495.2,357.3 1479.5,316.8 1517.8,308.6 1564.8,349.1 1543.2,350.3 1532.6,363.3 1526.3,357.4
                                1510,355.7 1498.5,355.7 	"/>
                        <polygon class="rocks_front_st233" points="1470.5,352.8 1450,325.7 1479.5,316.7 1495.2,356.6 	"/>
                        <polygon class="rocks_front_st234" points="1457.8,353.6 1450,325.7 1470.5,352.8 	"/>
                        <polygon class="rocks_front_st235" points="1416.5,311.7 1450,325.7 1479.5,316.7 	"/>
                        <polygon class="rocks_front_st236" points="1418.2,342.5 1450,325.7 1416.5,311.7 	"/>
                        <polygon class="rocks_front_st234" points="1430.8,355.3 1418.2,342.5 1450,325.7 1457.8,353.6 	"/>
                        <polygon class="rocks_front_st237" points="1416.5,311.7 1407.3,333.8 1418.2,342.5 	"/>
                        <polygon class="rocks_front_st238" points="1418.2,342.5 1387.8,339.6 1396.3,336.3 	"/>
                        <polygon class="rocks_front_st239" points="1430.8,355.3 1387.8,339.6 1418.2,342.5 	"/>
                        <polygon class="rocks_front_st240" points="1406.5,356.9 1387.8,339.6 1430.8,355.3 	"/>
                        <polygon class="rocks_front_st241" points="1387.8,339.6 1387,358.2 1406.5,356.9 	"/>
                        <polygon class="rocks_front_st242" points="1344.8,348.9 1387.8,339.6 1387,358.2 1361.5,359.9 	"/>
                        <polygon class="rocks_front_st243" points="1302.5,336.3 1282.2,359.5 1258.7,356.4 1283.5,332.2 	"/>
                        <polygon class="rocks_front_st244" points="1352.5,360.5 1344.8,348.9 1361.5,359.9 	"/>
                        <polygon class="rocks_front_st245" points="1302.5,336.3 1344.8,348.9 1352.5,360.5 1282.2,359.5 	"/>
                        <polygon class="rocks_front_st246" points="1366.2,335.2 1385,316 1333.5,311.7 1313.2,321.4 1316.2,333.3 	"/>
                        <polygon class="rocks_front_st247" points="1265.8,311.7 1313.2,321.9 1333.5,312.2 	"/>
                        <polygon class="rocks_front_st248" points="1275,318.7 1265.8,311.7 1313.2,321.4 	"/>
                        <polygon class="rocks_front_st249" points="1283.5,332.2 1275,318.7 1313.2,321.4 1316.2,333.3 	"/>
                        <polygon class="rocks_front_st250" points="1234.6,331.9 1275,318.7 1265.8,311.7 	"/>
                        <polygon class="rocks_front_st169" points="1221,312.7 1234.6,332.9 1265.8,312.7 	"/>
                        <polygon class="rocks_front_st251" points="1219.4,354.6 1234.6,331.9 1275,318.7 1283.5,332.2 1258.7,356.4 	"/>
                        <polygon class="rocks_front_st252" points="1221,311.7 1208.9,354.2 1219.4,354.6 1234.6,331.9 	"/>
                        <polygon class="rocks_front_st247" points="1207.7,311 1182,326.5 1221,311.7 	"/>
                        <polygon class="rocks_front_st169" points="1166.3,349.1 1182,326.5 1221,311.7 1175.3,352.7 	"/>
                        <polygon class="rocks_front_st253" points="1208.9,354.2 1221,311.7 1175.3,352.7 	"/>
                        <polygon class="rocks_front_st254" points="1169,309.1 1182,326.5 1207.7,311 	"/>
                        <polygon class="rocks_front_st148" points="1133.8,336.3 1169,309.1 1182,326.5 1166.3,349.1 	"/>
                        <polygon class="rocks_front_st255" points="1157.9,308.2 1133.8,336.3 1169,309.1 	"/>
                        <polygon class="rocks_front_st256" points="1144.4,307 1119.2,323.3 1157.9,308.2 	"/>
                        <polygon class="rocks_front_st257" points="1116.6,328.8 1119.2,323.3 1157.9,308.2 1133.8,336.3 	"/>
                        <polygon class="rocks_front_st258" points="1108.3,309.7 1144.4,307 1119.2,323.3 	"/>
                        <polygon class="rocks_front_st171" points="1102,322.5 1108.3,309.7 1119.2,323.3 1116.6,328.8 	"/>
                        <polygon class="rocks_front_st259" points="1082,306.5 1090.8,305.4 1144.4,307 1090.8,302.4 	"/>
                        <polygon class="rocks_front_st169" points="1102,322.5 1108.3,309.7 1144.4,307 1090.8,305.4 1082,306.5 	"/>
                        <polygon class="rocks_front_st260" points="1266.8,215.9 1314,223.8 1300,244.3 1279.5,244.8 	"/>
                        <polygon class="rocks_front_st261" points="1540.4,377.1 1545.8,384.1 1539.5,389.9 1440.5,498.7 	"/>
                        <polygon class="rocks_front_st262" points="1575.2,160 1587.1,180.4 1572.5,173.8 	"/>
                        <polygon class="rocks_front_st13" points="671.8,228.2 713.7,147.2 650.8,186.7 682.2,189.9 	"/>
                        <polygon class="rocks_front_st120" points="636,228.7 682.2,190.4 672.1,228.7 	"/>
                        <polygon class="rocks_front_st14" points="619.3,229.7 621.3,205.4 650.8,185.6 685.2,188.9 636.9,229.7 	"/>
                        <polygon class="rocks_front_st13" points="608.5,192.9 621.5,203.7 619.3,229.7 535.8,229.7 558,221.9 	"/>
                        <polygon class="rocks_front_st263" points="650.8,186.7 621.3,205 608.5,192.9 	"/>
                        <polygon class="rocks_front_st264" points="604.2,185.6 608.5,192.9 559.2,222.7 546,214.4 	"/>
                        <polygon class="rocks_front_st14" points="505.1,229.7 546,214.4 559.2,222.7 537.2,229.7 	"/>
                        <polygon class="rocks_front_st265" points="598.8,169.9 604.2,185.6 546,214.4 	"/>
                        <polygon class="rocks_front_st266" points="641.5,168.4 604.2,185.6 608.5,192.9 671.5,169.9 	"/>
                        <polygon class="rocks_front_st267" points="650.8,186.7 608.5,192.9 671.5,169.9 	"/>
                        <polygon class="rocks_front_st268" points="598.8,169.9 641.5,168.4 604.2,185.6 	"/>
                        <polygon class="rocks_front_st156" points="546,214.4 523.2,208.3 494.8,229.2 506.9,229.2 	"/>
                        <polygon class="rocks_front_st14" points="495.9,229.7 498,209.1 507.7,199.2 523.2,209.4 	"/>
                        <polygon class="rocks_front_st156" points="471.5,230.4 498,209.1 495.9,229.7 	"/>
                        <polygon class="rocks_front_st269" points="452.8,229.7 458,220.9 470.1,214.5 502.3,206.9 474,229.7 	"/>
                        <polygon class="rocks_front_st66" points="507.7,199.2 469.8,214.5 498,209.4 	"/>
                        <polygon class="rocks_front_st14" points="544,183.7 507.7,199.2 523.2,209.4 	"/>
                        <polygon class="rocks_front_st270" points="572.5,169.9 587,154.9 578.9,186.7 	"/>
                        <polygon class="rocks_front_st271" points="598.8,169.9 587,154.9 578.9,186.7 	"/>
                        <polygon class="rocks_front_st265" points="641.5,168.4 632.5,163.2 619.3,161.7 598.8,169.9 	"/>
                        <polygon class="rocks_front_st272" points="587,154.9 601,151.4 619.3,161.7 598.8,169.9 	"/>
                        <polygon class="rocks_front_st267" points="668,170.2 668,162.2 655.8,156.7 631.8,163.2 639.6,168.4 	"/>
                        <polygon class="rocks_front_st273" points="703,122.9 728.2,121.2 695.5,143.4 671,162.2 	"/>
                        <polygon class="rocks_front_st9" points="671.2,169.9 695.5,143.4 658.9,169.5 	"/>
                        <polygon class="rocks_front_st274" points="601,151.4 625,156.8 632.5,163.2 619.3,161.7 	"/>
                        <polygon class="rocks_front_st275" points="728.2,121.2 719.5,113.4 703,122.9 	"/>
                        <polygon class="rocks_front_st276" points="713.7,147.2 733.3,140.2 687.9,195.9 	"/>
                        <polygon class="rocks_front_st66" points="544.8,184.1 542.5,176.2 504.4,201 	"/>
                        <polygon class="rocks_front_st36" points="469.8,214.5 544,176.2 507.7,199.2 	"/>
                        <polygon class="rocks_front_st277" points="573.6,169.4 542.5,176.2 544,183.7 	"/>
                        <polygon class="rocks_front_st277" points="657.3,156.7 625,156.8 632.5,163.2 	"/>
                        <polygon class="rocks_front_st278" points="735.5,184.2 733.3,198.2 803.8,147.2 	"/>
                        <polygon class="rocks_front_st8" points="732,141.7 733.3,173.1 687.9,195.9 	"/>
                        <polygon class="rocks_front_st14" points="687.2,234.1 710.6,201.5 733.3,198.2 699.9,230.4 	"/>
                        <polygon class="rocks_front_st13" points="739.7,210.4 733.3,198.2 699.9,230.4 734.7,212.1 	"/>
                        <polygon class="rocks_front_st279" points="777.5,181.2 782.2,195.9 796.3,191 	"/>
                        <polygon class="rocks_front_st280" points="733.3,198.2 777.5,181.2 782.2,195.9 739.7,210.4 	"/>
                        <polygon class="rocks_front_st281" points="803.8,147.2 777.5,181.2 733.3,198.2 	"/>
                        <polygon class="rocks_front_st123" points="710.6,201.5 735.5,184.2 733.3,198.2 	"/>
                        <polygon class="rocks_front_st36" points="692.1,205 702.3,213.4 710.6,201.5 735.5,184.2 	"/>
                        <polygon class="rocks_front_st145" points="669.7,228.7 692.5,203.7 683.2,228.7 	"/>
                        <polygon class="rocks_front_st66" points="687.2,234.1 681.9,229.7 692.1,205 702.3,213.4 	"/>
                        <polygon class="rocks_front_st7" points="687.9,195.9 692.1,205 671.8,228.2 	"/>
                        <polygon class="rocks_front_st9" points="733.3,172.7 735.5,184.2 692.1,205 687.9,195.9 	"/>
                        <polygon class="rocks_front_st282" points="818.1,113.4 735.5,184.2 785.2,147.2 	"/>
                        <polygon class="rocks_front_st283" points="733.3,172.7 818.1,113.4 735.5,184.2 	"/>
                        <polygon class="rocks_front_st284" points="754.6,143.7 774.1,97 732,141.7 733.3,173 	"/>
                        <polygon class="rocks_front_st285" points="818.1,113.4 754.6,143.7 733,173.2 	"/>
                        <polygon class="rocks_front_st276" points="796.3,87.8 818.1,113.4 754.6,143.7 	"/>
                        <polygon class="rocks_front_st286" points="757.7,84.5 719.5,113.7 728.2,121.2 	"/>
                        <polygon class="rocks_front_st287" points="872,170.2 842.5,123.8 865.6,170.2 858.6,182.4 871.6,181.9 	"/>
                        <polygon class="rocks_front_st126" points="887.3,181.4 871.6,181.9 871,98.2 	"/>
                        <polygon class="rocks_front_st64" points="915.2,181.5 871,98.2 887.3,181.4 	"/>
                        <polygon class="rocks_front_st288" points="912.5,89.5 871,98.2 915.2,181.7 	"/>
                        <polygon class="rocks_front_st288" points="963.2,183.3 949.3,102.2 943.5,183.2 	"/>
                        <polygon class="rocks_front_st0" points="963.2,183.3 959.7,113.9 949.3,102.2 	"/>
                        <polygon class="rocks_front_st1" points="958.5,100.7 976.2,183.7 963,182.2 959.7,113.9 	"/>
                        <polygon class="rocks_front_st289" points="796.3,87.8 821.9,84.5 818.1,113.4 	"/>
                        <polygon class="rocks_front_st290" points="821.9,84.5 850.3,94.5 818.1,113.4 	"/>
                        <polygon class="rocks_front_st265" points="790.5,80.8 796.3,87.8 774.1,97 	"/>
                        <polygon class="rocks_front_st267" points="757.7,84.5 780.8,76.5 774.1,97 	"/>
                        <polygon class="rocks_front_st291" points="790.5,80.8 821.9,84.5 796.3,87.8 	"/>
                        <polygon class="rocks_front_st292" points="958.9,101.6 984.5,101.7 999,183.3 985,182.9 980.5,150.7 	"/>
                        <polygon class="rocks_front_st293" points="1011.5,183.2 999,183.3 984.5,101.7 1003.5,135.3 	"/>
                        <polygon class="rocks_front_st294" points="1016.8,107.5 1011.5,183.2 1003.5,135.3 984.5,101.7 	"/>
                        <polygon class="rocks_front_st295" points="1029.3,183 1016.8,107.5 1011.5,183.2 	"/>
                        <polygon class="rocks_front_st296" points="983.5,101.3 1031.5,102 1016.8,109 	"/>
                        <polygon class="rocks_front_st293" points="1028.3,183 1038.6,183.7 1016.8,107.5 	"/>
                        <polygon class="rocks_front_st294" points="1031.5,101.7 1037.5,182.7 1016.8,107.5 	"/>
                        <polygon class="rocks_front_st297" points="1049.8,153.1 1031.4,101.3 1037.5,183.2 1062.5,182.7 	"/>
                        <polygon class="rocks_front_st298" points="1046,108.7 1048.5,153.1 1062.5,182.7 1075.6,183.7 	"/>
                        <polygon class="rocks_front_st299" points="1066.2,159.6 1078.9,95.9 1075.6,183.7 	"/>
                        <polygon class="rocks_front_st300" points="1046,78.9 1079.4,96 1066.5,162.5 	"/>
                        <polygon class="rocks_front_st301" points="1031.5,101.7 1046.9,79.4 1046,108.7 1048.5,153.1 	"/>
                        <polygon class="rocks_front_st302" points="1066.2,159.6 1047,80.7 1046,108.7 	"/>
                        <polygon class="rocks_front_st303" points="1131,182.8 1111.3,125.4 1119.1,182.8 	"/>
                        <polygon class="rocks_front_st304" points="1079.4,97 1085.5,90.2 1136.8,141.3 1111.3,125.4 	"/>
                        <polygon class="rocks_front_st305" points="1131,182.8 1136.8,141.3 1111.3,125.4 	"/>
                        <polygon class="rocks_front_st306" points="1151.5,183.7 1136.8,141.3 1131,182.8 	"/>
                        <polygon class="rocks_front_st307" points="1164.4,105.9 1168,183.3 1151.5,183.7 1136.8,141.3 	"/>
                        <polygon class="rocks_front_st308" points="1046.5,81.2 1063.5,81.7 1079.4,98.5 	"/>
                        <polygon class="rocks_front_st309" points="1085.5,90.2 1164.4,105.9 1136.8,141.3 	"/>
                        <polygon class="rocks_front_st310" points="1062.1,81.2 1185.5,90.2 1164.4,105.9 1085.5,90.2 1079.4,98.5 	"/>
                        <polygon class="rocks_front_st311" points="1168,183.3 1183,183.7 1164.4,105.9 	"/>
                        <polygon class="rocks_front_st312" points="1192.3,115.7 1164.4,105.9 1183,183.7 1197.8,183.5 	"/>
                        <polygon class="rocks_front_st313" points="1185.5,90.2 1192.3,115.7 1164.4,105.9 	"/>
                        <polygon class="rocks_front_st314" points="1221.5,184.2 1214.3,151.2 1185.5,90.2 1192.3,115.7 1197.8,183.5 	"/>
                        <polygon class="rocks_front_st315" points="1211,106.2 1211,151.2 1185,90.2 	"/>
                        <polygon class="rocks_front_st316" points="1221.5,185.7 1248.5,185.7 1225.5,98.5 1211,106.2 1211,151.2 	"/>
                        <polygon class="rocks_front_st317" points="1185.5,90.2 1206.5,91.2 1225.8,98.2 1212.5,106.9 	"/>
                        <polygon class="rocks_front_st318" points="1248.5,185.7 1257.5,185.7 1245.8,122.2 1234.8,102.4 1225.8,98.6 	"/>
                        <polygon class="rocks_front_st319" points="1282.5,185.7 1258.7,126.2 1236.8,106 1245.8,122.4 1257.5,185.7 	"/>
                        <polygon class="rocks_front_st320" points="1269.1,136.8 1282.5,185.5 1258.7,126.7 	"/>
                        <polygon class="rocks_front_st321" points="1286.5,141.3 1296.3,173.2 1282.5,185.2 1269.1,136.8 	"/>
                        <polygon class="rocks_front_st322" points="1307.5,173.2 1300,154.5 1286.5,141.7 1296,173.4 	"/>
                        <polygon class="rocks_front_st323" points="1244.5,112.9 1252.5,113.2 1313.2,150.9 1322.3,174.2 1307.1,173.2 1300,155 1286.5,142 1269.1,137
                                1261,130.9 1258.7,126.7 	"/>
                        <polygon class="rocks_front_st324" points="1302.4,136.5 1307.5,127.9 1273.8,115.7 1256,115.7 	"/>
                        <polygon class="rocks_front_st325" points="1322.3,174.2 1307.5,127.5 1302.4,135 1252.5,113.2 1313.2,150.9 	"/>
                        <polygon class="rocks_front_st326" points="821.9,84.5 790.5,80.8 838.3,1.8 837.9,90.2 	"/>
                        <polygon class="rocks_front_st327" points="852.3,0 850.3,94.5 837.9,90.2 	"/>
                        <polygon class="rocks_front_st328" points="838.3,1.8 852.3,0 837.9,90.2 	"/>
                        <polygon class="rocks_front_st329" points="871,98.2 883.5,95.6 852.3,0 850.3,94.5 	"/>
                        <polygon class="rocks_front_st309" points="951.5,4.2 852.3,0 883.5,95.6 912.5,89.5 931.4,94.9 927.2,83 953.3,82.3 965.2,79 	"/>
                        <polygon class="rocks_front_st330" points="852.3,0 965.5,0.2 951.5,4.2 	"/>
                        <polygon class="rocks_front_st331" points="931.4,94.9 953.3,82.3 927.2,83 	"/>
                        <polygon class="rocks_front_st332" points="949.3,102.2 953.3,82.3 931.4,94.9 	"/>
                        <polygon class="rocks_front_st333" points="965.5,0.2 964.5,79.2 951.5,4.2 	"/>
                        <polygon class="rocks_front_st334" points="991.5,80.7 1029.3,81.7 1022.2,21 1006,10.7 993.7,10.7 	"/>
                        <polygon class="rocks_front_st335" points="1046.5,81.2 1022.2,21 1028.3,81 	"/>
                        <polygon class="rocks_front_st85" points="1099.2,18.5 1077.8,19.8 1037.5,15.5 1022.2,21 1006,10.7 1001.9,6.9 	"/>
                        <polygon class="rocks_front_st22" points="1046.5,81.2 1037.5,15.5 1022.2,21 	"/>
                        <polygon class="rocks_front_st317" points="1063.5,81.7 1056.1,34.2 1037.5,15.5 1046.5,81.2 	"/>
                        <polygon class="rocks_front_st22" points="1077.8,19.8 1056.1,33.4 1063.5,81.7 1083.4,83.1 1086.3,30.3 	"/>
                        <polygon class="rocks_front_st336" points="1099.2,18.5 1106.5,21.2 1107.3,33.4 1086.3,30.3 1077.8,19.8 	"/>
                        <polygon class="rocks_front_st74" points="1083.4,83.2 1091.3,83.3 1095,73.3 1095,37.2 1105.3,32.5 1085.6,30.4 	"/>
                        <polygon class="rocks_front_st85" points="1171.5,22.2 1106.5,22.2 1106.8,33.2 1128.5,24.5 	"/>
                        <polygon class="rocks_front_st337" points="1136.2,86.6 1109.6,66.3 1109.5,84.6 	"/>
                        <polygon class="rocks_front_st20" points="1095.5,73.3 1109.6,66.3 1109.5,84.6 1091.5,83.3 	"/>
                        <polygon class="rocks_front_st338" points="1094.5,37.2 1105.1,32.5 1127.6,22.8 1136.1,86.6 1109,66.3 1094.5,73.3 	"/>
                        <polygon class="rocks_front_st74" points="1171.5,19.7 1170,89 1136.2,86.6 1128.1,22.8 	"/>
                        <polygon class="rocks_front_st338" points="1185,90.2 1171.5,19.7 1170,89 	"/>
                        <polygon class="rocks_front_st339" points="1190.3,24.5 1171.5,19.7 1185,90.2 	"/>
                        <polygon class="rocks_front_st85" points="1206.5,91.2 1190.3,24.5 1185,90.2 	"/>
                        <polygon class="rocks_front_st340" points="1037.5,15.5 1056.1,34.2 1077.8,19.8 	"/>
                        <polygon class="rocks_front_st341" points="1880.8,316.6 1806.3,312.3 1793.5,297.6 1811.6,293.9 1877.7,311.7 1956.2,328.9 	"/>
                        <polygon class="rocks_front_st335" points="1232.8,245.9 1197.8,183.5 1230.8,193.2 1266.8,215.9 1258.7,245.3 	"/>
                        <polygon class="rocks_front_st294" points="959.7,115.2 949.3,102.2 958.8,101.8 	"/>
                        <polygon class="rocks_front_st342" points="993.7,10.7 1006,10.7 998,3.3 979.8,1.4 	"/>
                        <polygon class="rocks_front_st16" points="890,181.2 949.5,186.3 1005.5,182.7 	"/>
                        <polygon class="rocks_front_st343" points="1839.8,360.2 1543.2,350.2 1576.9,348.4 1610.2,346.6 1657,344.2 1737.3,340.7 1751.8,343.4
                                1764.5,344.1 1776.8,344.7 1855.6,342.9 1878,340.7 1811.6,353.4 	"/>
                        <polygon class="rocks_front_st250" points="1385,317 1416.5,312.7 1333.5,312.7 	"/>
                        <polygon class="rocks_front_st223" points="1782.3,287.7 1796.9,305.7 1781,294.5 1791.2,311.7 1806,311.7 1794.1,297.5 	"/>
                        <polygon class="rocks_front_st344" points="2000.3,359.9 2095.5,498.7 2067.8,498.7 	"/>
                        <polygon class="rocks_front_st345" points="1430.8,177.2 1427.5,171.6 1541.1,182.8 1540.5,215.2 	"/>
                        <polygon class="rocks_front_st346" points="1320.1,167.2 1457.8,164.6 1509.5,174.7 1430.8,177.2 1322.3,174.2 	"/>
                        <polygon class="rocks_front_st153" points="557.6,423.6 737.3,397.2 788.8,398.7 695.8,464 594.5,498.7 	"/>
                        <polygon class="rocks_front_st199" points="1255.1,416.9 1309.7,399.8 1285.5,498.7 1087,498.7 	"/>
                        <polygon class="rocks_front_st80" points="804,498.7 890.8,407.6 845.6,399.7 695.8,464 594,498.7 	"/>
                    </g>
                    </svg>
            </div>

            <div class="front_tree_cat_shadow">
                <svg version="1.1" id="Layer_28" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 982.7 554.7" style="enable-background:new 0 0 982.7 554.7;" xml:space="preserve">
                        <style type="text/css">
                            .front_tree_cat_shadow_st0{opacity:0.25;}
                        </style>
                    <path class="front_tree_cat_shadow_st0" d="M876.4,247.2l-102.9-17l-12.9-17.6l-36.2-10.7l55.6-39.1l3.9-9.2L944.3,37.9l38.3-9.4l-33.8-10.8l-13.1,0
	l-8.1-3.5l-8.1-0.2L912,7.4l-2-3.7L900.8,0l-9.7,6.2l-64.6,45.6l0,0l0,0l-40.8,44.6l-90.5,73l-53.9,18.5l-64.5,35l-18.5,1
	l-85.6,21.3l-70.6,7.3l-68,35.5l-184.6,45l-58.9,55.3l-5.7,30.5L0,457l26,58.6l71.6-6.7l42.2,13.1l20.3,29.2l85.6,3.6l161-26.5
	l74.4-2.8L669,489.1l92.1-71.7l-18.4-20.7l180-63.7l10.4-25.6l-36.8-25.5L876.4,247.2z M678.1,183.3l-41.3,33.3l-15.2,9.8l-11-5.2
	l-18.3,1l29.3-15.2l28.8-15L678.1,183.3z M741.6,231.2l-66.7,5.5l27.2-19.1l0.2-0.1l28.9,5.1L741.6,231.2z"/>
                    </svg>
            </div>

            <div class="front_tree_3_shadow">
                <svg version="1.1" id="Layer_28" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 697.7 469.3" style="enable-background:new 0 0 697.7 469.3;" xml:space="preserve">
                        <style type="text/css">
                            .front_tree_3_shadow_st0{opacity:0.25;}
                        </style>
                    <polygon class="front_tree_3_shadow_st0" points="554.2,262.4 558.6,242.3 472.7,207.6 514.4,159.9 566.3,118.2 697.7,0 629.7,0.9 587,34.6 510,121.4
                        463.3,154.8 463.3,154.7 389.8,223.4 347.5,231.6 268.3,262.4 196.5,277 38.8,347.9 64.5,347.1 23.2,382.6 52.2,414.4 0,458.9
                        115.9,469.3 242.1,450.4 326.6,429.6 384.6,431 493.1,411.9 505.8,388.7 571.4,381.8 661.5,327.5 664.9,317.5 608.8,269.3 "/>
                    </svg>
            </div>

            <div class="front_tree_2_shadow">
                <svg version="1.1" id="Layer_28" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 671.1 216.4" style="enable-background:new 0 0 671.1 216.4;" xml:space="preserve">
                        <style type="text/css">
                            .front_tree_2_shadow_st0{opacity:0.25;}
                        </style>
                    <polygon class="front_tree_2_shadow_st0" points="517.2,66.7 517.3,66.6 587.6,35.4 671.1,6.1 659.7,0.6 628.1,0.8 624.7,0 619.4,0.8 558.9,30.3
                            513.4,51.5 465.3,71.4 426.5,87.5 373.8,90.6 373.8,90.6 356.5,97.3 258.8,108.6 151.4,128.9 102.9,151.4 73.1,157.3 66.5,170.1
                            0,188.6 46,214.6 87.8,216.4 141.1,212.4 194,214.4 272,210 304.2,201.5 447.6,175 493.5,157.3 505.9,132.4 529.4,127.6
                            552.8,122.8 572.1,111.9 539.9,97.1 483.6,79.9 517.1,66.7 "/>
                    </svg>
            </div>

            <div class="grass grass_1">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 163 136" xml:space="preserve">
                        <style type="text/css">
                            .grass_st0{opacity:0.49;}
                            .grass_st1{fill:#D0D119;}
                            .grass_st2{fill:#D9EA5C;}
                            .grass_st3{fill:#C7C500;}
                            .grass_st4{fill:#AEAE00;}
                        </style>
                    <g class="grass_shadow grass_st0">
                        <polygon class="swing_shadow s1" points="24.5,135.6 122.6,106.2 100.8,105.2 	"/>
                        <g class="swing_shadow s2">
                            <polygon points="2,119.4 59.7,116.4 55.7,113.1 	"/>
                            <polygon points="106.2,105.3 59.7,116.4 55.7,113.1 84.7,104.5 	"/>
                        </g>
                        <polygon class="swing_shadow s3" points="97.8,128 128.6,106.7 110,105.7 	"/>
                        <g class="swing_shadow s4">
                            <polygon points="135.9,120.1 121.1,116.1 131.4,115.4 	"/>
                            <polygon points="119.8,105.9 121.1,116.1 131.4,115.4 136.5,106.9 	"/>
                        </g>
                    </g>
                    <g class="grass_body">
                        <polygon data-shadow="s1" class="grass_st1 swing" points="74.2,0 121.8,108 100.8,106.2 	"/>
                        <g data-shadow="s2" class="swing">
                            <polygon class="grass_st2" points="31.3,43.2 79.2,64.5 71,73.3 	"/>
                            <polygon class="grass_st1" points="105.8,107.2 79.2,64.5 71,73.3 85.3,104.8 	"/>
                        </g>
                        <polygon data-shadow="s3" class="grass_st3 swing" points="129.7,38.3 127.8,108 109.7,106.9 	"/>
                        <g data-shadow="s4" class="swing">
                            <polygon class="grass_st2" points="153,70 134.2,78.5 142.5,82.8 	"/>
                            <polygon class="grass_st4" points="118.8,108.5 134.2,78.5 142.5,82.8 135.3,109 	"/>
                        </g>
                    </g>
                    </svg>

            </div>

            <div class="grass grass_2">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="70 0 100 136" xml:space="preserve">
                        <style type="text/css">
                            .grass_2_st0{fill:#D9EA5C;}
                            .grass_2_st1{fill:#B8CA44;}
                            .grass_2_st2{fill:#ADBE53;}
                            .grass_2_st3{fill:#DCEB8F;}
                            .grass_2_st4{opacity:0.5;}
                        </style>
                    <g class="grass_shadow grass_2_st4">
                        <polygon class="swing_shadow s1" points="119.5,120.1 101.3,136.3 128.7,120.1"/>
                        <polygon class="swing_shadow s2" points="135.5,131.1 124,120.1 137,120.1"/>
                        <polygon class="swing_shadow s3" points="131.7,120.1 142.8,123.5 137,120.1"/>
                        <g class="swing_shadow s4">
                            <polygon points="90.2,125.8 107.3,125.2 104.3,123.8"/>
                            <polygon points="123.4,120.1 107.3,125.3 104.3,124 113.8,120.1"/>
                        </g>
                    </g>
                    <g class="grass_body">
                        <polygon data-shadow="s1" class="grass_2_st0 swing" points="119.5,120.1 101.3,74.7 128.7,120.1"/>
                        <polygon data-shadow="s2" class="grass_2_st1 swing" points="135.5,89.4 124,120.1 137,120.1"/>
                        <polygon data-shadow="s3" class="grass_2_st2 swing" points="131.7,120.1 142.8,109.7 137,120.1"/>
                        <g data-shadow="s4" class="swing">
                            <polygon class="grass_2_st0" points="90.2,103.3 107.3,105 104.3,108.8"/>
                            <polygon class="grass_2_st3" points="123.4,120.1 107.3,105.1 104.3,109 113.8,120.1"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="grass grass_3">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 163 136" xml:space="preserve">
                        <style type="text/css">
                            .grass_st0{opacity:0.49;}
                            .grass_st1{fill:#D0D119;}
                            .grass_st2{fill:#D9EA5C;}
                            .grass_st3{fill:#C7C500;}
                            .grass_st4{fill:#AEAE00;}
                        </style>
                    <g class="grass_shadow grass_st0">
                        <polygon class="swing_shadow s1" points="24.5,135.6 122.6,106.2 100.8,105.2 	"/>
                        <g class="swing_shadow s2">
                            <polygon points="2,119.4 59.7,116.4 55.7,113.1 	"/>
                            <polygon points="106.2,105.3 59.7,116.4 55.7,113.1 84.7,104.5 	"/>
                        </g>
                        <polygon class="swing_shadow s3" points="97.8,128 128.6,106.7 110,105.7 	"/>
                        <g class="swing_shadow s4">
                            <polygon points="135.9,120.1 121.1,116.1 131.4,115.4 	"/>
                            <polygon points="119.8,105.9 121.1,116.1 131.4,115.4 136.5,106.9 	"/>
                        </g>
                    </g>
                    <g class="grass_body">
                        <polygon data-shadow="s1" class="grass_st1 swing" points="74.2,0 121.8,108 100.8,106.2 	"/>
                        <g data-shadow="s2" class="swing">
                            <polygon class="grass_st2" points="31.3,43.2 79.2,64.5 71,73.3 	"/>
                            <polygon class="grass_st1" points="105.8,107.2 79.2,64.5 71,73.3 85.3,104.8 	"/>
                        </g>
                        <polygon data-shadow="s3" class="grass_st3 swing" points="129.7,38.3 127.8,108 109.7,106.9 	"/>
                        <g data-shadow="s4" class="swing">
                            <polygon class="grass_st2" points="153,70 134.2,78.5 142.5,82.8 	"/>
                            <polygon class="grass_st4" points="118.8,108.5 134.2,78.5 142.5,82.8 135.3,109 	"/>
                        </g>
                    </g>
                    </svg>

            </div>

            <div class="grass grass_4">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="70 0 100 136" xml:space="preserve">
                        <style type="text/css">
                            .grass_2_st0{fill:#D9EA5C;}
                            .grass_2_st1{fill:#B8CA44;}
                            .grass_2_st2{fill:#ADBE53;}
                            .grass_2_st3{fill:#DCEB8F;}
                            .grass_2_st4{opacity:0.5;}
                        </style>
                    <g class="grass_shadow grass_2_st4">
                        <polygon class="swing_shadow s1" points="119.5,120.1 101.3,136.3 128.7,120.1"/>
                        <polygon class="swing_shadow s2" points="135.5,131.1 124,120.1 137,120.1"/>
                        <polygon class="swing_shadow s3" points="131.7,120.1 142.8,123.5 137,120.1"/>
                        <g class="swing_shadow s4">
                            <polygon points="90.2,125.8 107.3,125.2 104.3,123.8"/>
                            <polygon points="123.4,120.1 107.3,125.3 104.3,124 113.8,120.1"/>
                        </g>
                    </g>
                    <g class="grass_body">
                        <polygon data-shadow="s1" class="grass_2_st0 swing" points="119.5,120.1 101.3,74.7 128.7,120.1"/>
                        <polygon data-shadow="s2" class="grass_2_st1 swing" points="135.5,89.4 124,120.1 137,120.1"/>
                        <polygon data-shadow="s3" class="grass_2_st2 swing" points="131.7,120.1 142.8,109.7 137,120.1"/>
                        <g data-shadow="s4" class="swing">
                            <polygon class="grass_2_st0" points="90.2,103.3 107.3,105 104.3,108.8"/>
                            <polygon class="grass_2_st3" points="123.4,120.1 107.3,105.1 104.3,109 113.8,120.1"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="cat_chocolate cats">
                <svg version="1.1" id="cat_chocolate" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="-847 438.2 224.4 190.8" style="transform-origin: center bottom;" xml:space="preserve">
                        <style type="text/css">
                            .cat_chocolate_st0{opacity:0.6;}
                            .cat_chocolate_st1{fill:#AA4724;}
                            .cat_chocolate_st2{fill:#C87651;}
                            .cat_chocolate_st3{fill:#FFA188;}
                            .cat_chocolate_st4{fill:#CC6633;}
                            .cat_chocolate_st5{opacity:0.4;fill:#602C03;enable-background:new    ;}
                            .cat_chocolate_st6{fill:#FFD2C0;}
                            .cat_chocolate_st7{opacity:0.6;fill:#FFA188;enable-background:new    ;}
                            .cat_chocolate_st8{opacity:0.5;fill:#FF9966;enable-background:new    ;}
                            .cat_chocolate_st9{opacity:0.5;fill:#723D15;enable-background:new    ;}
                            .cat_chocolate_st10{fill:#4C1700;}
                            .cat_chocolate_st11{fill:#733C21;}
                            .cat_chocolate_st12{fill:#FFFFFF;}
                            .cat_chocolate_st13{opacity:0.3;fill:#CC9966;enable-background:new    ;}
                            .cat_chocolate_st14{fill:#FF9966;}
                            .cat_chocolate_st15{opacity:0.71;fill:#FF9966;enable-background:new    ;}
                            .cat_chocolate_st16{fill:#FFF0EB;}
                        </style>
                    <g class="cat_shadow cat_chocolate_st0">
                        <polygon points="-672.1,608.2 -671.3,608.1 -670.7,607.9 -670.2,607.8 -669.8,607.7 -669.6,607.5 -669.4,607.4 -669.5,607.3
                -669.6,607.2 -669.9,607.1 -670.2,607 -670.7,607 -671.4,606.9 -672.1,606.8 -672.9,606.8 -673.8,606.8 -675,606.8 -675.5,606.8
                -676.1,606.8 -676.7,606.8 -677.5,606.8 -678.1,606.8 -678.8,606.9 -679.4,606.9 -680.1,606.9 -680.8,607 -681.4,607 -682.2,607
                -682.9,607.1 -683.5,607.1 -684.1,607.2 -684.9,607.2 -685.5,607.3 -687.6,607.4 -689.6,607.5 -691.8,607.7 -693.9,607.8
                -696.1,607.9 -698.2,608 -700.4,608.1 -702.6,608.2 -704.7,608.3 -706.8,608.4 -708.8,608.5 -710.8,608.5 -712.7,608.6
                -714.5,608.6 -716.2,608.6 -717.8,608.6 -718.8,608.6 -719.8,608.6 -720.8,608.6 -721.8,608.5 -722.8,608.5 -723.8,608.5
                -724.8,608.4 -725.8,608.4 -726.8,608.3 -727.7,608.2 -728.7,608.2 -729.7,608.1 -730.6,608 -731.5,608 -732.5,607.9 -733.4,607.8
                -734.6,607.7 -735.8,607.6 -737,607.6 -738.3,607.5 -739.6,607.4 -741,607.3 -742.3,607.2 -743.8,607.1 -745.2,607.1 -746.7,607
                -748.1,606.9 -749.8,606.9 -751.3,606.9 -752.9,606.8 -754.6,606.8 -756.2,606.8 -757.7,606.8 -759.3,606.8 -760.9,606.8
                -762.4,606.8 -763.9,606.8 -765.7,606.8 -767.3,606.9 -769.1,606.9 -770.9,607 -772.7,607 -774.7,607.1 -776.8,607.2 -779.1,607.2
                -781.4,607.3 -783.9,607.4 -786.5,607.6 -788.8,607.7 -791.1,607.8 -793.4,607.9 -795.6,608 -797.8,608.1 -800,608.3 -802.2,608.4
                -804.3,608.6 -806.4,608.7 -808.5,608.8 -810.4,609 -812.4,609.1 -814.3,609.3 -816.2,609.4 -818.1,609.6 -819.8,609.7
                -820.4,609.8 -821.1,609.8 -821.6,609.9 -822.2,609.9 -822.7,610 -823.3,610.1 -823.9,610.1 -824.4,610.2 -824.9,610.2
                -825.4,610.3 -825.8,610.4 -826.3,610.4 -826.7,610.5 -827.2,610.6 -827.7,610.6 -828.1,610.7 -828.8,610.8 -829.4,611
                -829.9,611.1 -830.3,611.2 -830.5,611.4 -830.6,611.5 -830.7,611.6 -830.5,611.7 -830.3,611.8 -829.9,611.9 -829.4,611.9
                -828.8,612 -828.1,612.1 -827.2,612.1 -826.3,612.1 -825.3,612.1 -824.4,612.1 -823.7,612.1 -822.8,612.1 -822,612.1 -821.2,612
                -820.3,612 -819.4,612 -818.5,611.9 -817.7,611.8 -816.8,611.8 -815.9,611.7 -815.1,611.7 -814.2,611.6 -813.3,611.5 -812.4,611.4
                -811.7,611.4 -810.1,611.2 -808.5,611.1 -806.6,611 -804.6,610.8 -802.4,610.6 -800,610.5 -797.6,610.4 -795,610.2 -792.4,610.1
                -789.6,609.9 -786.8,609.8 -783.8,609.7 -780.9,609.7 -777.8,609.6 -774.7,609.6 -771.7,609.6 -770.7,609.6 -769.7,609.6
                -768.6,609.6 -767.6,609.6 -766.6,609.7 -765.7,609.7 -764.6,609.8 -763.7,609.9 -762.7,609.9 -761.8,610 -760.8,610.1
                -759.8,610.2 -758.9,610.2 -758,610.3 -757,610.4 -756.1,610.4 -754.8,610.6 -753.4,610.7 -752.1,610.8 -750.7,610.9 -749.4,611
                -748,611 -746.5,611.1 -745.1,611.2 -743.4,611.2 -741.9,611.3 -740.1,611.4 -738.3,611.4 -736.3,611.4 -734.2,611.4 -732,611.4
                -729.6,611.4 -727.7,611.4 -725.6,611.4 -723.5,611.3 -721.2,611.3 -718.8,611.2 -716.2,611.1 -713.5,611 -710.8,610.9
                -707.8,610.8 -704.8,610.6 -701.6,610.5 -698.3,610.3 -694.8,610.1 -691.3,609.9 -687.5,609.7 -683.6,609.4 -682.7,609.3
                -682,609.3 -681.2,609.2 -680.3,609.2 -679.6,609.1 -678.8,609 -678,609 -677.3,608.9 -676.5,608.8 -675.8,608.7 -675.1,608.7
                -674.5,608.6 -673.8,608.5 -673.2,608.4 -672.7,608.3 	"/>
                        <polygon id="body_4_" points="-744.9,627.5 -746.5,627.4 -748,627.4 -749.3,627.3 -750,627.2 -750.6,627.1 -750.9,626.9
                -750.8,626.7 -750.4,626.5 -750.4,626.5 -750.4,626.5 -750.4,626.5 -750.4,626.5 -750.4,626.5 -750.3,626.5 -750.3,626.5
                -750.3,626.5 -748.8,625.8 -747,625 -745.2,624.2 -743.1,623.3 -742.7,623 -742.6,622.8 -742.9,622.6 -743.7,622.4 -744.6,622.3
                -745.8,622.2 -746.9,622.2 -748.2,622.1 -776.1,622.1 -777.3,622.2 -779,622.2 -780.9,622.3 -783.1,622.4 -785.6,622.5
                -788.4,622.7 -791.3,623 -794.2,623.3 -801.3,624.1 -809.1,625 -816,625.8 -822.5,626.5 -822.5,626.5 -822.5,626.5 -822.5,626.5
                -822.5,626.5 -822.5,626.5 -822.5,626.5 -822.5,626.5 -822.5,626.5 -824.4,626.7 -826.5,626.9 -828.5,627 -830.6,627.2
                -832.8,627.3 -835,627.4 -836.9,627.4 -838.8,627.5 -840.9,627.4 -842.6,627.3 -843.9,627.2 -844.5,627 -844.8,626.8 -844.5,626.6
                -843.6,626.3 -842.2,626 -756.8,610.7 -750.9,609.6 -747.3,609 -743,608.5 -738.2,608 -733.1,607.6 -727.9,607.2 -722.7,607
                -717.6,606.8 -712.8,606.7 -700,606.7 -652.2,606.7 -638.5,606.7 -634.7,606.8 -631.6,606.9 -629.6,607.2 -628.6,607.6 -628.5,608
                -629.3,608.5 -631.2,609 -634,609.6 -725.2,626 -727.2,626.3 -729.4,626.6 -731.8,626.8 -734.5,627 -737.2,627.2 -739.8,627.3
                -742.4,627.4 	"/>
                    </g>
                    <g class="cat_body">
                        <g class="cat_tail">
                            <polygon class="cat_chocolate_st1" points="-676.2,598.6 -676.2,599.7 -676.3,600.7 -676.6,601.7 -676.9,602.7 -677.4,603.7 -677.9,604.5
                -678.6,605.3 -679.2,606.1 -680,606.8 -680.8,607.4 -681.7,607.9 -682.7,608.4 -683.6,608.7 -684.6,609 -685.6,609.2 -686.8,609.2
                -687.3,609.2 -687.9,609.2 -688.4,609 -689,608.9 -689.6,608.8 -690.1,608.6 -690.7,608.5 -691.1,608.2 -691.6,607.9 -692.1,607.6
                -692.6,607.4 -693,607 -693.5,606.8 -693.8,606.5 -694.2,606.1 -694.6,605.6 -695.8,604.6 -697.1,603.6 -698.3,602.5 -699.8,601.5
                -701.3,600.6 -702.8,599.9 -704.3,599.1 -706,598.3 -707.6,597.7 -709.3,597.2 -710.8,596.7 -712.5,596.3 -714.2,596 -715.8,595.7
                -717.3,595.7 -718.8,595.5 -719.9,595.5 -721,595.7 -721.9,595.8 -723.2,596 -724.3,596.3 -725.6,596.7 -726.7,597.1 -728,597.4
                -729.4,597.8 -730.7,598.3 -732,598.8 -733.4,599.3 -734.6,599.9 -735.9,600.4 -737.3,600.9 -738.6,601.4 -740.1,602 -741.9,602.8
                -743.6,603.4 -745.3,604.1 -747.1,604.8 -748.9,605.4 -750.6,606 -752.6,606.5 -754.3,607 -756.2,607.6 -757.9,608 -759.7,608.4
                -761.4,608.6 -763.2,608.9 -764.9,609 -766.7,609.2 -768.2,609.3 -769.8,609.3 -771.3,609.2 -772.7,609 -774.1,609 -775.6,608.7
                -777.2,608.5 -778.6,608.2 -780.1,607.9 -781.6,607.5 -783.3,607 -785,606.5 -786.7,605.8 -788.5,605.1 -790.3,604.3 -792.2,603.4
                -793.9,602.7 -795.6,601.8 -797.2,600.9 -798.7,600 -800.1,599.1 -801.5,598.1 -802.9,597.1 -804.3,596 -805.6,594.9 -806.9,593.9
                -808,592.9 -809.1,591.7 -810.2,590.7 -811.2,589.5 -812.2,588.4 -813.1,587.3 -813.4,587 -813.7,586.5 -814,586.1 -814.3,585.7
                -814.5,585.3 -814.8,584.8 -815,584.4 -815.1,583.9 -815.4,583.5 -815.5,583.1 -815.6,582.7 -815.7,582.1 -815.8,581.7
                -815.9,581.1 -815.9,580.6 -815.9,580.1 -815.9,579.1 -815.7,578 -815.4,576.9 -815.1,576 -814.6,575.1 -814.1,574.2 -813.5,573.4
                -812.8,572.6 -812.1,572 -811.2,571.3 -810.3,570.8 -809.4,570.3 -808.5,570 -807.5,569.7 -806.4,569.5 -805.3,569.5 -804.4,569.5
                -803.8,569.5 -803.2,569.7 -802.4,569.9 -801.8,570.2 -801.1,570.4 -800.5,570.7 -799.9,571.1 -799.3,571.5 -798.7,571.8
                -798.2,572.4 -797.8,572.8 -797.3,573.4 -796.8,573.9 -796.4,574.5 -796,575 -795.3,576 -794.4,577.1 -793.4,578.2 -792.2,579.3
                -790.8,580.5 -789.4,581.6 -787.8,582.7 -786,583.7 -784.3,584.7 -782,585.6 -779.9,586.4 -777.6,587.1 -775,587.8 -772.4,588.1
                -769.6,588.4 -766.7,588.5 -765.6,588.4 -764.5,588.4 -763.4,588.1 -762.2,587.9 -761,587.5 -759.7,587.1 -758.3,586.7 -757,586.2
                -755.7,585.7 -754.4,585.2 -753.1,584.7 -751.8,584.2 -750.4,583.5 -749.1,583 -747.8,582.5 -746.5,581.9 -744.4,581 -742.7,580.4
                -740.8,579.6 -739,578.8 -737,578.2 -735.3,577.6 -733.4,576.9 -731.6,576.4 -729.5,576 -727.8,575.5 -725.8,575.2 -723.8,574.9
                -721.6,574.8 -719.6,574.6 -717.4,574.6 -715.3,574.8 -713.3,574.9 -711.6,575.2 -709.6,575.4 -707.7,575.9 -705.7,576.3
                -703.6,576.9 -701.5,577.7 -699.3,578.5 -697.2,579.4 -695,580.5 -692.7,581.6 -690.4,582.9 -688,584.4 -685.7,586 -683.3,587.8
                -680.9,589.7 -680.4,590.1 -680,590.6 -679.5,591 -679.1,591.5 -678.6,592 -678.3,592.5 -677.8,593 -677.6,593.5 -677.2,594.1
                -676.9,594.6 -676.7,595.3 -676.5,595.9 -676.3,596.5 -676.3,597.2 -676.2,597.8 	"/>
                            <polygon class="cat_chocolate_st2" points="-748.4,582.7 -749.5,583.3 -750.4,583.9 -751.3,584.7 -752.5,585.8 -753.5,587 -754.8,588.1
                -755.9,589.5 -757.2,590.9 -758.6,592.4 -760,593.8 -761.4,595.3 -763,596.7 -764.7,598 -766.2,599.2 -768,600.5 -769.9,601.4
                -771.8,602.3 -773.5,603.1 -775,603.7 -776.7,604.3 -778.3,604.8 -779.7,605.1 -781.1,605.3 -782.5,605.5 -783.8,605.5
                -785.1,605.5 -786.4,605.3 -787.6,605.1 -788.6,604.8 -789.9,604.4 -791.1,603.9 -792.2,603.4 -793.9,602.5 -795.5,601.7
                -797,600.8 -798.6,599.9 -800.1,598.9 -801.5,597.9 -802.9,596.9 -804.2,595.9 -805.5,594.9 -806.6,593.8 -808,592.7 -809.1,591.7
                -810.2,590.6 -811.2,589.5 -812.2,588.4 -813.1,587.3 -813.4,587 -813.7,586.5 -814,586.1 -814.2,585.6 -814.5,585.2 -814.8,584.7
                -814.9,584.2 -815.1,583.7 -815.3,583.3 -815.4,582.7 -815.6,582.2 -815.7,581.8 -815.8,581.3 -815.9,580.9 -815.9,580.5
                -815.9,580.1 -815.9,579.1 -815.7,578 -815.4,576.9 -815.1,576 -814.6,575.1 -814.1,574.2 -813.5,573.4 -812.8,572.6 -812.1,572
                -811.2,571.3 -810.3,570.8 -809.4,570.3 -808.5,570 -807.5,569.7 -806.4,569.5 -805.3,569.5 -804.4,569.5 -803.8,569.5
                -803.2,569.7 -802.4,569.9 -801.8,570.2 -801.1,570.4 -800.5,570.7 -799.9,571.1 -799.3,571.5 -798.7,571.8 -798.2,572.4
                -797.8,572.8 -797.3,573.4 -796.8,573.9 -796.4,574.5 -796,575 -795.3,576 -794.4,577.1 -793.4,578.2 -792.2,579.3 -790.8,580.5
                -789.4,581.6 -787.8,582.7 -786,583.7 -784.3,584.7 -782,585.6 -779.9,586.4 -777.6,587.1 -775,587.8 -772.4,588.1 -769.6,588.4
                -766.7,588.5 -765.8,588.4 -764.8,588.4 -763.8,588.1 -762.8,588 -761.7,587.6 -760.5,587.4 -759.4,587 -758.2,586.6 -756.9,586.1
                -755.7,585.7 -754.6,585.2 -753.4,584.7 -752.1,584.2 -750.9,583.7 -749.8,583.3 	"/>
                            <polygon class="cat_chocolate_st3" points="-783.5,591.2 -785.4,590.9 -787.1,590.4 -788.8,589.8 -790.3,589.2 -791.7,588.4 -793,587.5
                -794.3,586.7 -795.4,585.7 -796.4,584.8 -797.4,583.9 -798.2,583 -799,582.1 -799.6,581.3 -800.4,580.6 -800.9,580 -801.3,579.6
                -801.4,579.3 -801.8,579.1 -802.1,578.8 -802.3,578.6 -802.7,578.3 -803.2,578 -803.5,577.7 -804,577.4 -804.4,577.2 -804.9,576.9
                -805.3,576.8 -806,576.6 -806.6,576.4 -807.1,576.3 -807.7,576.2 -808.3,576.2 -808.9,576.3 -809.7,576.5 -810.3,576.8
                -811.1,577.2 -811.7,577.7 -812.3,578.2 -812.9,578.8 -813.4,579.4 -813.9,580.2 -814.2,581 -814.5,581.8 -814.5,582.7
                -814.6,583.5 -814.5,584.5 -814.3,585.3 -813.9,586.4 -814,586.1 -814.2,585.9 -814.4,585.6 -814.5,585.2 -814.8,584.8
                -814.9,584.5 -815.1,584.1 -815.3,583.6 -815.4,583.2 -815.5,582.7 -815.6,582.3 -815.8,581.9 -815.8,581.4 -815.9,581
                -815.9,580.5 -815.9,580.1 -815.9,579.1 -815.7,578 -815.4,576.9 -815.1,576 -814.6,575 -814.1,574.1 -813.5,573.4 -812.8,572.6
                -812.1,572 -811.2,571.3 -810.3,570.8 -809.4,570.3 -808.5,570 -807.5,569.7 -806.4,569.5 -805.3,569.5 -804.4,569.5 -803.8,569.5
                -803.2,569.7 -802.4,569.9 -801.8,570.2 -801.1,570.4 -800.5,570.7 -799.9,571.1 -799.3,571.5 -798.7,571.8 -798.2,572.4
                -797.8,572.8 -797.3,573.4 -796.8,573.9 -796.4,574.5 -796,575 -795.3,576 -794.4,577.1 -793.4,578.2 -792.2,579.3 -791.1,580.5
                -789.5,581.5 -787.9,582.7 -786.2,583.7 -784.3,584.7 -782.3,585.6 -780,586.4 -777.7,587 -775,587.6 -772.4,588.1 -769.7,588.4
                -766.7,588.5 -767.1,588.8 -767.9,589 -768.4,589.3 -769.3,589.6 -770.2,589.9 -771.3,590.2 -772.3,590.4 -773.5,590.7
                -774.6,590.9 -775.8,591.1 -777.2,591.2 -778.3,591.3 -779.7,591.4 -781,591.4 -782.3,591.3 	"/>
                        </g>
                        <polygon id="body_10_" class="cat_chocolate_st1" points="-636.3,455.4 -637.8,455.5 -639.6,455.8 -641.2,456.4 -642.6,457.2 -644,458.3
                -645.1,459.5 -646,460.9 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4
                -646.8,462.4 -646.8,462.4 -649,467.6 -651.6,473.6 -654.1,479.3 -657,486.1 -658.2,488.1 -659.6,490 -661,491.5 -662.5,492.6
                -664.1,493.5 -665.7,494.1 -667.2,494.6 -668.6,494.9 -696,494.9 -697.1,494.6 -698.6,494.3 -700,493.8 -701.6,493 -703.3,492
                -704.8,490.4 -706.3,488.6 -707.6,486.4 -710.2,480.4 -713,473.9 -715.5,468 -717.8,462.5 -717.8,462.5 -717.8,462.5 -717.8,462.5
                -717.8,462.5 -717.8,462.5 -717.8,462.5 -717.8,462.5 -717.8,462.5 -718.6,461 -719.6,459.6 -720.7,458.5 -722,457.4 -723.5,456.6
                -725.2,455.9 -726.8,455.5 -728.6,455.4 -730.8,455.7 -733.1,456.3 -735,457.2 -736.6,458.7 -738,460.2 -739.1,462 -739.8,464.2
                -740.1,466.4 -740.1,479 -740.1,488.1 -740.1,497.4 -740.1,506.6 -740.1,515.8 -740.1,525 -740.1,534.1 -740.1,543.4 -740.1,552.6
                -740.1,561.8 -740.1,571.1 -740.1,580.3 -740.1,588.1 -739.6,592.4 -738.4,596.4 -736.4,600 -733.7,603.3 -730.5,605.9
                -726.8,607.9 -722.9,609.3 -718.6,609.9 -706.1,609.9 -659.1,609.9 -645.4,609.9 -641.4,609.3 -637.4,608 -633.9,605.9
                -630.9,603.3 -628.4,600 -626.5,596.4 -625.4,592.4 -624.9,588.1 -624.9,580.3 -624.9,571.7 -624.9,563.3 -624.9,554.9
                -624.9,546.5 -624.9,538.1 -624.9,529.6 -624.9,521.1 -624.9,512.7 -624.9,504.3 -624.9,495.8 -624.9,487.4 -624.9,479
                -624.9,466.4 -625.2,464.2 -625.8,462 -627,460.2 -628.2,458.6 -630,457.2 -631.8,456.3 -633.9,455.7 	"/>
                        <polygon id="body_light_part_6_" class="cat_chocolate_st4" points="-636.3,455.4 -637.8,455.5 -639.6,455.8 -641.2,456.4 -642.6,457.2
                -644,458.3 -645.1,459.5 -646,460.9 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4 -646.8,462.4
                -646.8,462.4 -646.8,462.4 -649,467.6 -651.6,473.6 -654.1,479.3 -657,486.1 -658.2,488.1 -659.6,490 -661,491.5 -662.5,492.6
                -664.1,493.5 -665.7,494.1 -667.2,494.6 -668.6,494.9 -682.1,494.9 -682.1,609.9 -659.1,609.9 -645.4,609.9 -641.4,609.3
                -637.4,608 -633.9,605.9 -630.9,603.3 -628.4,600 -626.5,596.4 -625.4,592.4 -624.9,588.1 -624.9,580.3 -624.9,571.7 -624.9,563.3
                -624.9,554.9 -624.9,546.5 -624.9,538.1 -624.9,529.6 -624.9,521.1 -624.9,512.7 -624.9,504.3 -624.9,495.8 -624.9,487.4
                -624.9,479 -624.9,466.4 -625.2,464.2 -625.8,462 -627,460.2 -628.2,458.6 -630,457.2 -631.8,456.3 -633.9,455.7 	"/>
                        <polygon id="shadow_6_" class="cat_chocolate_st5" points="-682.1,609.9 -710,609.9 -714.2,609.5 -718.1,608.2 -721.6,606.1 -724.6,603.4
                -727.1,600.2 -728.8,596.5 -729.9,592.5 -730.4,588.1 -730.4,553.5 -729,554 -725.1,555.2 -719.2,556.3 -712.1,556.9 -708.2,556.8
                -704.3,556.3 -700.3,555.6 -697.9,555 -695.9,554.1 -693.8,553.1 -691.9,552.1 -690.2,550.8 -688.6,549.4 -687,547.8 -685.2,546
                -683.7,544 -682.1,541.5 	"/>
                        <polygon id="light_2_6_" class="cat_chocolate_st6" points="-629.3,466.4 -629.3,588.1 -629.7,592.5 -630.9,596.5 -632.7,600.2 -635,603.4
                -638,606.1 -641.4,608.2 -645.1,609.5 -649.4,609.9 -650.8,609.9 -652.3,609.9 -653.8,609.9 -655.2,609.9 -656.7,609.9
                -658.2,609.9 -659.8,609.9 -661.2,609.9 -652.5,609.2 -645.7,607.1 -640.9,604.2 -637.5,600.5 -635.5,596.4 -634.4,592.3
                -633.9,588.4 -633.7,585.2 -633.7,469.4 -633.7,467.8 -633.7,465.9 -633.9,463.9 -634.4,462 -635,460.4 -635.8,459 -637.2,458.1
                -639.2,457.8 -637.2,457.6 -635.4,457.8 -633.6,458.4 -632.2,459.4 -631,460.6 -630.1,462.3 -629.6,464.3 	"/>
                        <polygon id="light_3_1_" class="cat_chocolate_st7" points="-740.1,466.4 -740.1,588.1 -739.7,592.5 -738.4,596.5 -736.7,600.2 -734.5,603.4
                -731.4,606.1 -728,608.2 -724.3,609.5 -719.9,609.9 -719.4,609.9 -718.8,609.9 -718.2,609.9 -717.5,609.9 -716.9,609.9
                -716.4,609.9 -715.7,609.9 -715.1,609.9 -720.7,609 -725.2,607.2 -728.9,604.4 -731.5,600.9 -733.6,596.9 -734.8,592.7
                -735.4,588.8 -735.6,585.2 -735.6,469.4 -735.6,468 -735.6,466.6 -735.6,465.5 -735.5,464.3 -735.3,463.3 -734.9,462.3
                -734.2,461.2 -733.3,460.1 -732.2,459 -730.5,458.3 -728.7,457.8 -726.7,457.7 -724.7,457.8 -722.7,458.3 -720.8,459.2
                -719.4,460.3 -720.6,458.9 -721.7,457.8 -722.7,456.9 -724,456.3 -725.4,455.8 -726.6,455.5 -727.8,455.4 -729.1,455.4
                -731.4,455.7 -733.3,456.4 -735.2,457.3 -736.8,458.7 -738.1,460.3 -739.2,462.2 -739.8,464.2 	"/>
                        <polygon id="light_2_1_" class="cat_chocolate_st8" points="-735.8,466.4 -735.8,588.1 -735.4,592.5 -734.2,596.5 -732.5,600.2 -730.2,603.4
                -727.1,606.1 -723.7,608.2 -720,609.5 -715.7,609.9 -714.3,609.9 -712.8,609.9 -711.3,609.9 -709.9,609.9 -708.4,609.9
                -706.9,609.9 -705.4,609.9 -704,609.9 -712.6,609.2 -719.4,607.1 -724.2,604.2 -727.6,600.5 -729.7,596.4 -730.8,592.3
                -731.2,588.4 -731.4,585.2 -731.4,469.4 -731.4,467.8 -731.4,465.9 -731.2,463.9 -730.8,462 -730.2,460.4 -729.3,459 -727.9,458.1
                -726,457.8 -727.9,457.6 -729.7,457.8 -731.5,458.4 -732.9,459.4 -734.2,460.6 -735.1,462.3 -735.6,464.3 	"/>
                        <polygon id="shadow_on_light_part_6_" class="cat_chocolate_st9" points="-682.1,609.9 -654.1,609.9 -649.9,609.5 -646,608.2 -642.5,606.1
                -639.6,603.4 -637.2,600.2 -635.4,596.5 -634.1,592.5 -633.7,588.1 -633.7,553.5 -635.2,554 -639.2,555.2 -644.9,556.3
                -651.9,556.9 -655.9,556.8 -659.8,556.3 -663.8,555.6 -666.3,555 -668.3,554.1 -670.4,553.1 -672.3,552.1 -674,550.8 -675.6,549.4
                -677.3,547.8 -679.1,546 -680.5,544 -682.1,541.5 	"/>
                        <g id="pow_left_6_">
                            <polygon class="cat_chocolate_st10" points="-693.5,589 -693.7,591.2 -694,593.4 -694.6,595.4 -695.3,597.3 -696.1,599.1 -697.2,600.9
                    -698.3,602.5 -699.7,604.1 -701.2,605.4 -702.8,606.6 -704.6,607.6 -706.5,608.5 -708.4,609.3 -710.4,609.8 -712.4,610.2
                    -714.6,610.2 -716.7,610.2 -718.8,609.8 -720.8,609.3 -722.8,608.5 -724.5,607.6 -726.3,606.6 -728,605.4 -729.4,604.1
                    -730.8,602.5 -731.9,600.9 -733.1,599.1 -733.9,597.3 -734.6,595.4 -735.1,593.4 -735.5,591.2 -735.6,589 -735.6,589
                    -735.6,588.9 -735.6,588.7 -735.6,588.6 -735.5,588.5 -735.5,588.4 -735.5,588.3 -735.5,588.1 -735.5,588.1 -735.5,587.9
                    -735.5,587.8 -735.5,587.8 -735.5,587.6 -735.5,587.5 -735.5,587.4 -735.4,587.3 -735.2,589.3 -734.8,591.2 -734.2,593
                    -733.3,594.6 -732.4,596.3 -731.4,597.7 -730.1,599.1 -728.8,600.4 -727.3,601.5 -725.7,602.5 -724,603.4 -722.3,604.1
                    -720.5,604.6 -718.6,605.1 -716.6,605.3 -714.6,605.4 -712.5,605.3 -710.5,605.1 -708.6,604.6 -706.8,604.1 -705,603.4
                    -703.4,602.5 -701.7,601.5 -700.3,600.4 -698.9,599.1 -697.7,597.7 -696.7,596.3 -695.8,594.6 -695,593 -694.4,591.2 -694,589.3
                    -693.7,587.3 -693.7,587.4 -693.7,587.5 -693.6,587.6 -693.6,587.8 -693.6,587.8 -693.6,587.9 -693.6,588.1 -693.6,588.1
                    -693.6,588.3 -693.6,588.4 -693.6,588.5 -693.6,588.6 -693.6,588.7 -693.5,588.9 		"/>
                            <polygon class="cat_chocolate_st10" points="-719.6,595.1 -719.6,595.1 -719.6,595.1 -719.6,595.1 -719.6,595.1 -720.3,595.3 -720.8,595.7
                    -721.2,596.3 -721.4,596.9 -721.4,606.9 -721.2,607.6 -720.8,608.2 -720.3,608.5 -719.6,608.6 -719.6,608.6 -719.6,608.6
                    -719.6,608.6 -719.6,608.6 -719,608.5 -718.4,608.2 -718,607.6 -717.8,606.9 -717.8,596.9 -718,596.3 -718.4,595.7 -719,595.3
                    "/>
                            <polygon class="cat_chocolate_st10" points="-709.5,595.1 -709.5,595.1 -709.5,595.1 -709.5,595.1 -709.5,595.1 -710.2,595.3 -710.7,595.7
                    -711.1,596.3 -711.3,596.9 -711.3,606.9 -711.1,607.6 -710.7,608.2 -710.2,608.5 -709.5,608.6 -709.5,608.6 -709.5,608.6
                    -709.5,608.6 -709.5,608.6 -708.8,608.5 -708.2,608.2 -707.9,607.6 -707.7,606.9 -707.7,596.9 -707.9,596.3 -708.2,595.7
                    -708.8,595.3 		"/>
                        </g>
                        <g id="pow_right_6_">
                            <polygon class="cat_chocolate_st11" points="-629.3,589 -629.6,591.2 -629.9,593.4 -630.4,595.4 -631.1,597.3 -631.9,599.1 -633,600.9
                    -634.1,602.5 -635.6,604.1 -637.2,605.4 -638.7,606.6 -640.5,607.6 -642.3,608.5 -644.2,609.3 -646.2,609.8 -648.2,610.2
                    -650.4,610.2 -652.5,610.2 -654.7,609.8 -656.7,609.3 -658.7,608.5 -660.4,607.6 -662.1,606.6 -663.8,605.4 -665.2,604.1
                    -666.6,602.5 -667.9,600.9 -668.9,599.1 -669.8,597.3 -670.5,595.4 -671,593.4 -671.4,591.2 -671.6,589 -671.6,589 -671.4,588.9
                    -671.4,588.7 -671.4,588.6 -671.4,588.5 -671.4,588.4 -671.4,588.3 -671.4,588.1 -671.4,588.1 -671.4,587.9 -671.4,587.8
                    -671.4,587.8 -671.4,587.6 -671.4,587.5 -671.4,587.4 -671.3,587.3 -671.2,589.3 -670.7,591.2 -670,593 -669.3,594.6
                    -668.3,596.3 -667.2,597.7 -666.1,599.1 -664.7,600.4 -663.2,601.5 -661.6,602.5 -660,603.4 -658.2,604.1 -656.3,604.6
                    -654.4,605.1 -652.5,605.3 -650.4,605.4 -648.4,605.3 -646.3,605.1 -644.4,604.6 -642.6,604.1 -640.9,603.4 -639.2,602.5
                    -637.5,601.5 -636.3,600.4 -634.7,599.1 -633.6,597.7 -632.6,596.3 -631.7,594.6 -630.9,593 -630.2,591.2 -629.8,589.3
                    -629.6,587.3 -629.6,587.4 -629.6,587.5 -629.5,587.6 -629.5,587.8 -629.5,587.8 -629.5,587.9 -629.5,588.1 -629.5,588.1
                    -629.5,588.3 -629.5,588.4 -629.5,588.5 -629.5,588.6 -629.5,588.7 -629.3,588.9 		"/>
                            <polygon class="cat_chocolate_st11" points="-655.4,595.1 -655.4,595.1 -655.4,595.1 -655.4,595.1 -655.4,595.1 -656.1,595.3 -656.7,595.7
                    -657.2,596.3 -657.2,596.9 -657.2,606.9 -657.2,607.6 -656.7,608.2 -656.1,608.5 -655.4,608.6 -655.4,608.6 -655.4,608.6
                    -655.4,608.6 -655.4,608.6 -654.8,608.5 -654.2,608.2 -653.8,607.6 -653.6,606.9 -653.6,596.9 -653.8,596.3 -654.2,595.7
                    -654.8,595.3 		"/>
                            <polygon class="cat_chocolate_st11" points="-645.3,595.1 -645.3,595.1 -645.3,595.1 -645.3,595.1 -645.3,595.1 -646,595.3 -646.6,595.7
                    -646.9,596.3 -647.1,596.9 -647.1,606.9 -646.9,607.6 -646.6,608.2 -646,608.5 -645.3,608.6 -645.3,608.6 -645.3,608.6
                    -645.3,608.6 -645.3,608.6 -644.6,608.5 -644.2,608.2 -643.7,607.6 -643.5,606.9 -643.5,596.9 -643.7,596.3 -644.2,595.7
                    -644.6,595.3 		"/>
                        </g>
                        <polygon id="ear_12_" class="cat_chocolate_st11" points="-633.7,471.7 -633.9,470.9 -634.1,470.2 -634.6,469.7 -635.2,469 -635.8,468.5
                -636.5,468.3 -637.4,468 -638.1,468 -638.9,468 -639.4,468.3 -640.1,468.5 -640.6,468.8 -641.1,469.3 -641.5,469.8 -642,470.4
                -642.3,470.9 -646.9,486.1 -633.7,486.1 	"/>
                        <polygon id="nose_6_" class="cat_chocolate_st3" points="-680.5,539.9 -681.6,539.9 -682.5,539.9 -683.5,539.9 -684.5,539.9 -686.7,539.5
                -688.4,538.3 -689.5,536.5 -689.9,534.5 -689.9,534.5 -689.9,534.5 -689.9,534.5 -689.9,534.5 -689.8,533.3 -689,532.3
                -688.1,531.7 -687,531.5 -684.7,531.5 -682.5,531.5 -680.3,531.5 -678.2,531.5 -676.9,531.7 -676,532.3 -675.4,533.3 -675.1,534.5
                -675.1,534.5 -675.1,534.5 -675.1,534.5 -675.1,534.5 -675.5,536.5 -676.8,538.3 -678.4,539.5 	"/>
                        <g class="eye_right">
                            <circle class="cat_chocolate_st10" cx="-713.2" cy="531.7" r="16.9"/>
                            <circle class="eyeball cat_chocolate_st12" cx="-711.4" cy="527.3" r="16.9"/>
                            <polygon class="cat_chocolate_st13" points="-695.8,533.9 -696.4,535.1 -697.1,536.4 -697.8,537.4 -698.6,538.5
                -699.5,539.4 -700.5,540.2 -701.4,541 -702.5,541.8 -703.5,542.3 -704.6,542.9 -705.7,543.3 -706.8,543.7 -708,543.9 -709.1,544.1
                -710.3,544.2 -711.3,544.3 -713.1,544.2 -714.7,543.9 -716.4,543.5 -718,542.9 -719.5,542.3 -720.8,541.4 -722.1,540.4
                -723.4,539.2 -724.5,538.1 -725.4,536.8 -726.3,535.4 -727,533.9 -727.6,532.4 -728,530.7 -728.3,529 -728.4,527.3 -728.4,526.9
                -728.4,526.5 -728.3,525.9 -728.3,525.3 -728.2,524.6 -728.1,523.9 -728,523.2 -727.7,522.5 -727.6,521.7 -727.3,520.9 -727,520.1
                -726.6,519.4 -726.2,518.6 -725.7,517.9 -725.1,517.2 -724.4,516.5 -725.3,517.9 -725.9,519.3 -726.3,520.9 -726.5,522.5
                -726.3,524.2 -726.2,525.8 -725.7,527.4 -725.1,529 -724.3,530.7 -723.4,532.2 -722.4,533.6 -721.2,535 -720,536.1 -718.6,537.2
                -717.2,538.1 -715.6,538.6 -714.2,539.1 -712.8,539.4 -711.4,539.5 -710,539.6 -708.5,539.5 -707,539.5 -705.6,539.2 -704.2,538.8
                -702.8,538.5 -701.5,538 -700.3,537.4 -699.1,536.8 -698.2,536.2 -697.2,535.5 -696.4,534.7 	"/>
                            <circle class="pupil" cx="-711" cy="527" r="6.4"/>
                        </g>
                        <polygon id="spot" class="cat_chocolate_st8" points="-625.2,490.8 -628.9,491.3 -633.3,491.8 -637.8,492.4 -642.5,493.2 -647.2,494.3
                -651.9,495.6 -656.6,497.4 -661.1,499.6 -665.4,502.3 -669.3,505.6 -672.9,509.6 -676,514.3 -678.5,519.7 -680.5,526 -681.7,533.2
                -682.1,541.5 -681.7,545.6 -680.6,549.7 -678.8,553.9 -676.5,557.9 -673.6,561.9 -670.2,565.7 -666.4,569.2 -662.3,572.4
                -657.9,575.3 -653.4,577.7 -648.6,579.7 -643.8,581.2 -639,582 -634.3,582.2 -629.7,581.7 -625.2,580.4 	"/>
                        <path id="ear2" class="cat_chocolate_st8" d="M-738.3,515.2l1.7-3.2l2.3-3.1l2.8-2.9l3.3-2.6l3.6-2.3l4-1.9l3-1.2l2.7-1.1l2.3-1.3l1.8-1.6
                l1.2-2.2l0.4-2.9l-0.4-3.8l-1.4-4.9l-1.9-5.3l-1.8-4.8l-1.9-4.2l-2-3.5l-2.3-2.7l-2.7-1.8l-3.2-0.8l-3.8,0.3l-3.4,1.2l-2.4,1.9
                l-1.6,2.6l-0.9,3.2l-0.4,3.8l-0.1,4.3v4.7l0.1,5.1v5.6v6.1v6.3v6v5.4v4.3v2.9v1L-738.3,515.2z"/>
                        <g class="eye_left">
                            <circle class="cat_chocolate_st11" cx="-656" cy="531.7" r="16.9"/>
                            <circle class="eyeball cat_chocolate_st12" cx="-654.2" cy="527.3" r="16.9"/>
                            <polygon class="cat_chocolate_st13" points="-638.7,533.9 -639.3,535.1 -640.1,536.4 -640.6,537.4 -641.5,538.5
                -642.4,539.4 -643.3,540.2 -644.3,541 -645.3,541.8 -646.3,542.3 -647.5,542.9 -648.5,543.3 -649.7,543.7 -650.8,543.9
                -652.1,544.1 -653.2,544.2 -654.2,544.3 -656,544.2 -657.6,543.9 -659.3,543.5 -661,542.9 -662.4,542.3 -663.8,541.4 -665.1,540.4
                -666.3,539.2 -667.5,538.1 -668.4,536.8 -669.2,535.4 -669.9,533.9 -670.5,532.4 -670.9,530.7 -671.2,529 -671.3,527.3
                -671.3,526.9 -671.3,526.5 -671.2,525.9 -671.2,525.3 -671.2,524.6 -671,523.9 -670.9,523.2 -670.7,522.5 -670.5,521.7
                -670.3,520.9 -669.9,520.1 -669.5,519.4 -669.1,518.6 -668.6,517.9 -668,517.2 -667.3,516.5 -668.2,517.9 -668.9,519.3
                -669.2,520.9 -669.4,522.5 -669.3,524.2 -669.1,525.8 -668.6,527.4 -668,529 -667.2,530.7 -666.3,532.2 -665.3,533.6 -664.1,535
                -662.9,536.1 -661.5,537.2 -660.1,538.1 -658.4,538.6 -657.2,539.1 -655.6,539.4 -654.2,539.5 -653,539.6 -651.4,539.5
                -649.9,539.5 -648.5,539.2 -647.1,538.8 -645.7,538.5 -644.4,538 -643.1,537.4 -642,536.8 -641.1,536.2 -640.1,535.5 -639.3,534.7
                    "/>
                            <circle class="pupil" cx="-654" cy="527" r="6.4"/>
                        </g>
                        <polygon id="ear_13_" class="cat_chocolate_st10" points="-731.3,471.7 -731.2,470.9 -730.9,470.2 -730.5,469.7 -729.9,469 -729.4,468.5
                -728.5,468.3 -727.7,468 -727,468 -726.3,468 -725.7,468.3 -725.1,468.5 -724.5,468.8 -724,469.3 -723.7,469.8 -723.3,470.4
                -723,470.9 -718.3,486.1 -731.3,486.1 	"/>
                        <polygon id="light_1_6_" class="cat_chocolate_st14" points="-682.1,494.9 -682.1,499.3 -697.9,499.3 -700.8,499 -703.4,498 -705.8,496.3
                -708.1,493.9 -710.3,490.7 -712.5,486.9 -722.6,463.5 -723.3,462.2 -724.2,460.9 -725.1,460 -726.2,459.2 -727.4,458.7
                -728.6,458.2 -729.9,458 -731.2,458 -732.6,458.1 -733.9,458.5 -735.3,459.1 -736.6,460 -737.8,461.1 -738.7,462.4 -739.6,464
                -740,465.7 -739.7,463.9 -739.1,462 -738.1,460.4 -736.7,458.7 -735,457.4 -733.1,456.3 -731,455.7 -728.6,455.4 -726.8,455.5
                -725.2,456 -723.7,456.7 -722.1,457.4 -720.7,458.5 -719.6,459.6 -718.6,461 -717.8,462.5 -717.5,463.2 -716.6,465.3 -707.6,486.5
                -707.2,487 -707,487.6 -706.6,488.1 -706.3,488.7 -706,489.2 -705.6,489.5 -705.2,490.1 -704.8,490.4 -703.7,491.6 -702.5,492.6
                -701.1,493.4 -699.7,494 -698.3,494.4 -696.9,494.6 -695.6,494.8 -694.6,494.9 	"/>
                        <polygon id="light_1_9_" class="cat_chocolate_st15" points="-682.5,494.9 -682.5,499.3 -666.9,499.3 -664,499 -661.4,498 -659,496.3
                -656.8,493.9 -654.6,490.7 -652.4,486.9 -642.4,463.5 -641.7,462.2 -640.8,460.9 -639.8,460 -638.8,459.2 -637.6,458.7
                -636.4,458.2 -635.1,458 -633.8,458 -632.4,458.1 -631.1,458.5 -629.7,459.1 -628.5,460 -627.3,461.1 -626.3,462.4 -625.5,464
                -625.1,465.7 -625.4,463.9 -626,462 -626.9,460.4 -628.3,458.7 -630,457.4 -631.9,456.3 -634,455.7 -636.4,455.4 -638.1,455.5
                -639.8,456 -641.3,456.7 -642.8,457.4 -644.2,458.5 -645.3,459.6 -646.3,461 -647.1,462.5 -647.5,463.2 -648.3,465.3 -657.3,486.5
                -657.6,487 -657.9,487.6 -658.2,488.1 -658.5,488.7 -658.8,489.2 -659.2,489.5 -659.6,490.1 -660,490.4 -661.1,491.6 -662.3,492.6
                -663.7,493.4 -665.1,494 -666.5,494.4 -667.9,494.6 -669.1,494.8 -670.2,494.9 	"/>
                        <polygon id="light_3_6_" class="cat_chocolate_st16" points="-624.9,466.4 -624.9,588.1 -625.3,592.5 -626.5,596.5 -628.2,600.2 -630.5,603.4
                -633.6,606.1 -636.9,608.2 -640.7,609.5 -645.1,609.9 -645.6,609.9 -646.2,609.9 -646.8,609.9 -647.5,609.9 -648.1,609.9
                -648.6,609.9 -649.3,609.9 -649.9,609.9 -644.3,609 -639.8,607.2 -636.1,604.4 -633.5,600.9 -631.4,596.9 -630.2,592.7
                -629.6,588.8 -629.3,585.2 -629.3,469.4 -629.3,468 -629.3,466.6 -629.3,465.5 -629.5,464.3 -629.7,463.3 -630.1,462.3
                -630.8,461.2 -631.7,460.1 -632.8,459 -634.5,458.3 -636.3,457.8 -638.3,457.7 -640.3,457.8 -642.3,458.3 -644.2,459.2
                -645.6,460.3 -644.4,458.9 -643.3,457.8 -642.3,456.9 -640.9,456.3 -639.6,455.8 -638.4,455.5 -637.2,455.4 -635.9,455.4
                -633.6,455.7 -631.7,456.4 -629.8,457.3 -628.2,458.7 -626.8,460.3 -625.8,462.2 -625.2,464.2 	"/>
                        <polygon id="blick_5_" class="cat_chocolate_st12" points="-633.7,602 -632.7,600.2 -631.5,597.9 -630.4,595.1 -629.8,592.6 -629.6,590.1
                -629.3,587.5 -629.1,585 -629.1,582.2 -629,579.2 -628.6,576 -627.9,572.9 -627.1,570.3 -626.2,568.4 -625.6,567.5 -625.2,568.1
                -624.9,570.4 -624.9,573.6 -624.9,576.6 -624.9,579.3 -624.9,581.8 -624.9,583.7 -624.9,585.3 -624.8,588.1 -625.1,590.1
                -625.3,592.5 -626.1,595.2 -627.1,598.1 -628.7,601 -630.9,603.9 -633,606 -634.4,606.7 -635,606.7 -635.2,606 -635,605
                -634.7,603.9 -634.4,603 -634.1,602.5 	"/>
                    </g>
                    </svg>
            </div>

            <div class="cat_yellow cats">
                <svg version="1.1" id="cat_yellow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 -15 190.6 176.6" style="transform-origin: center bottom;" xml:space="preserve">
                        <style type="text/css">
                            .cat_yellow_st0{opacity:0.6;}
                            .cat_yellow_st1{fill:#CE8900;}
                            .cat_yellow_st2{fill:#FFCC00;}
                            .cat_yellow_st3{fill:#FFFFFF;}
                            .cat_yellow_st4{fill:#996666;}
                            .cat_yellow_st5{fill:#FFFF33;}
                            .cat_yellow_st6{fill:#FFFF99;}
                            .cat_yellow_st7{fill:#A06E00;}
                            .cat_yellow_st8{fill:#B78300;}
                            .cat_yellow_st9{fill:#C19C00;}
                            .cat_yellow_st10{opacity:0.74;fill:#FF9900;}
                            .cat_yellow_st11{opacity:0.5;fill:#FFFF99;}
                            .cat_yellow_st12{opacity:0.2;fill:#996666;enable-background:new    ;}
                        </style>
                    <g class="cat_shadow cat_yellow_st0">
                        <polygon points="147.7,138.4 148.4,138.2 148.8,138.1 149.2,137.9 149.4,137.8 149.6,137.7 149.6,137.5 149.5,137.4 149.4,137.3
                149.1,137.2 148.7,137.1 148.2,137.1 147.6,137 146.9,137 146.2,136.9 145.3,136.9 144.3,136.9 143.8,136.9 143.2,136.9
                142.7,136.9 142,136.9 141.5,136.9 140.9,137 140.3,137 139.8,137 139.1,137.1 138.6,137.1 138,137.1 137.3,137.2 136.8,137.2
                136.3,137.3 135.6,137.3 135.1,137.4 133.3,137.5 131.6,137.7 129.7,137.8 127.9,138 126,138.1 124.2,138.2 122.3,138.3
                120.3,138.4 118.5,138.5 116.6,138.6 114.9,138.6 113.1,138.7 111.5,138.7 109.9,138.8 108.4,138.8 107,138.8 106,138.8
                105.1,138.8 104.2,138.8 103.3,138.7 102.4,138.7 101.4,138.6 100.5,138.6 99.5,138.5 98.7,138.5 97.8,138.4 96.8,138.3
                95.9,138.3 95.1,138.2 94.2,138.1 93.3,138.1 92.4,138 91.3,137.9 90.1,137.8 89,137.7 87.8,137.6 86.6,137.5 85.2,137.4 84,137.3
                82.6,137.3 81.3,137.2 80,137.1 78.6,137.1 77.1,137 75.7,137 74.2,136.9 72.7,136.9 71.3,136.9 70,136.9 68.5,136.9 67.1,136.9
                65.8,136.9 64.3,136.9 62.8,137 61.4,137 59.8,137 58.2,137.1 56.6,137.1 54.8,137.2 53,137.3 51,137.4 49,137.5 46.8,137.6
                44.5,137.7 42.6,137.8 40.5,137.9 38.5,138.1 36.7,138.2 34.7,138.3 32.9,138.4 31,138.6 29.1,138.7 27.3,138.9 25.6,139
                23.9,139.2 22.2,139.3 20.6,139.5 19,139.7 17.4,139.8 15.9,140 15.4,140 14.9,140.1 14.4,140.1 13.9,140.2 13.5,140.2 13,140.3
                12.5,140.4 12.1,140.4 11.7,140.5 11.2,140.5 10.9,140.6 10.5,140.7 10.2,140.7 9.8,140.8 9.4,140.9 9.1,141 8.5,141.1 8.1,141.3
                7.7,141.4 7.4,141.5 7.3,141.7 7.3,141.8 7.3,141.9 7.5,142 7.8,142.1 8.2,142.2 8.7,142.3 9.2,142.3 9.9,142.4 10.7,142.4
                11.6,142.4 12.5,142.5 13.3,142.4 13.9,142.4 14.7,142.4 15.4,142.4 16.1,142.4 16.9,142.3 17.6,142.3 18.4,142.2 19.1,142.2
                19.9,142.1 20.7,142 21.4,142 22.2,141.9 22.9,141.8 23.6,141.8 24.3,141.7 25.5,141.5 27,141.4 28.5,141.2 30.3,141.1 32.1,140.9
                34.2,140.8 36.3,140.6 38.5,140.5 40.8,140.3 43.2,140.2 45.7,140.1 48.3,140 50.9,139.9 53.6,139.8 56.4,139.8 59.1,139.8
                60,139.8 60.9,139.8 61.8,139.8 62.8,139.9 63.7,139.9 64.6,140 65.6,140 66.5,140.1 67.4,140.2 68.2,140.3 69.1,140.3 70.1,140.4
                71,140.5 71.8,140.6 72.7,140.6 73.6,140.7 74.9,140.8 76.1,140.9 77.4,141 78.7,141.1 80,141.2 81.3,141.3 82.6,141.4 84,141.5
                85.5,141.5 86.9,141.6 88.6,141.7 90.2,141.7 92,141.7 93.9,141.7 95.9,141.7 98,141.7 99.7,141.7 101.6,141.7 103.5,141.6
                105.5,141.6 107.6,141.5 109.9,141.4 112.2,141.3 114.6,141.2 117.2,141.1 119.8,140.9 122.6,140.8 125.5,140.6 128.5,140.4
                131.5,140.1 134.8,139.9 138.1,139.6 138.9,139.6 139.5,139.5 140.2,139.4 140.9,139.4 141.6,139.3 142.2,139.2 142.9,139.2
                143.5,139.1 144.2,139 144.7,138.9 145.3,138.8 145.8,138.8 146.4,138.7 146.9,138.6 147.3,138.5 	"/>
                        <polygon id="body_6_" points="94.1,158.4 92.7,158.4 91.4,158.4 90.1,158.3 89.4,158.2 88.8,158 88.4,157.8 88.4,157.6 88.6,157.4
                88.6,157.4 88.6,157.4 88.6,157.4 88.6,157.4 88.6,157.4 88.7,157.4 88.7,157.4 88.7,157.4 89.6,156.7 90.8,155.9 91.9,155.1
                93.2,154.1 93.4,153.8 93.3,153.6 92.9,153.4 92.2,153.2 91.2,153.1 90.2,153 89.1,152.9 87.9,152.9 62.9,152.9 61.7,152.9
                60.3,153 58.6,153 56.7,153.2 54.5,153.3 52.2,153.5 49.7,153.8 47.3,154.1 41.4,154.9 34.9,155.8 29.2,156.7 23.8,157.4
                23.8,157.4 23.8,157.4 23.8,157.4 23.8,157.4 23.8,157.4 23.8,157.4 23.8,157.4 23.8,157.4 22.2,157.6 20.5,157.8 18.8,158
                16.9,158.1 15.1,158.3 13.2,158.3 11.4,158.4 9.7,158.4 7.9,158.4 6.2,158.3 5,158.2 4.4,158 4,157.7 4.1,157.5 4.7,157.2
                5.8,156.9 77.7,139.8 80.7,139.2 84.2,138.7 88.2,138.2 92.5,137.7 96.9,137.4 101.5,137.1 106,136.9 110.2,136.8 121.7,136.8
                164.7,136.8 177,136.8 180.5,136.9 183.3,137.1 185.3,137.4 186.5,137.7 186.8,138.2 186.3,138.7 185,139.2 182.9,139.8
                110.9,156.9 109.4,157.2 107.6,157.5 105.5,157.7 103.2,158 100.9,158.2 98.7,158.3 96.4,158.4 	"/>
                    </g>
                    <g class="cat_body">
                        <g class="cat_tail">
                            <polygon class="cat_yellow_st1" points="149.7,129.4 149.7,130.4 149.5,131.4 149.3,132.3 148.9,133.1 148.5,134 148.1,134.8 147.5,135.5
                146.9,136.2 146.2,136.9 145.4,137.4 144.6,137.9 143.8,138.3 142.9,138.6 142,138.9 141.1,139 140,139 139.6,139 139,139
                138.5,138.9 138,138.8 137.5,138.7 137,138.5 136.6,138.4 136.2,138.1 135.7,137.9 135.3,137.7 134.8,137.4 134.4,137.1 134,136.9
                133.7,136.5 133.3,136.2 133,135.8 131.9,134.9 130.8,133.9 129.6,133 128.3,132.1 126.9,131.3 125.6,130.6 124.2,129.9
                122.7,129.2 121.2,128.7 119.7,128.1 118.3,127.7 116.8,127.3 115.3,127.1 113.8,126.9 112.4,126.8 111.1,126.7 110.1,126.7
                109.2,126.9 108.2,126.9 107.1,127.2 106.1,127.4 105,127.7 103.8,128.1 102.7,128.4 101.5,128.8 100.4,129.2 99.2,129.6 98,130.1
                96.8,130.6 95.6,131.1 94.4,131.5 93.2,132 91.8,132.6 90.2,133.2 88.7,133.9 87.1,134.4 85.5,135 83.8,135.6 82.2,136.2
                80.6,136.6 79,137.1 77.3,137.6 75.7,138 74.1,138.3 72.5,138.5 70.9,138.8 69.3,138.9 67.8,139 66.5,139.1 65,139.1 63.7,139
                62.4,138.9 61.1,138.9 59.7,138.6 58.4,138.5 57,138.1 55.7,137.8 54.2,137.5 52.8,137 51.3,136.5 49.7,136 48.1,135.4 46.4,134.6
                44.7,133.9 43.2,133.1 41.7,132.3 40.3,131.5 38.9,130.8 37.6,129.9 36.3,129 35,128.1 33.8,127.1 32.6,126.1 31.5,125.2
                30.5,124.2 29.5,123.2 28.5,122.3 27.6,121.2 26.7,120.2 25.8,119.2 25.6,118.9 25.2,118.5 25,118.2 24.8,117.8 24.6,117.4
                24.4,117 24.1,116.6 24,116.2 23.8,115.8 23.7,115.4 23.6,115 23.5,114.6 23.4,114.2 23.3,113.7 23.3,113.2 23.3,112.7 23.3,111.8
                23.5,110.8 23.7,109.9 24.1,109 24.5,108.2 24.9,107.4 25.5,106.6 26.1,106 26.8,105.3 27.6,104.8 28.3,104.3 29.1,103.9 30,103.6
                31,103.4 31.9,103.2 32.9,103.1 33.6,103.2 34.2,103.2 34.9,103.4 35.5,103.5 36,103.8 36.7,104 37.2,104.2 37.8,104.6 38.3,105
                38.8,105.3 39.3,105.7 39.7,106.1 40.2,106.6 40.6,107.1 41,107.7 41.3,108.1 41.9,109.1 42.7,110 43.7,111 44.7,112 45.9,113.1
                47.2,114.1 48.7,115 50.3,116 51.9,116.9 53.8,117.7 55.8,118.4 58,119.1 60.2,119.6 62.6,120 65.2,120.3 67.8,120.4 68.8,120.3
                69.7,120.2 70.7,120 71.8,119.8 73,119.5 74.1,119.1 75.3,118.8 76.5,118.3 77.6,117.8 78.8,117.3 80,116.9 81.3,116.4 82.5,115.8
                83.7,115.4 84.9,114.9 86.1,114.4 87.8,113.6 89.5,113 91.2,112.3 92.9,111.6 94.6,111 96.2,110.4 97.9,109.9 99.6,109.4
                101.3,109 103,108.6 104.8,108.3 106.5,108 108.4,107.9 110.4,107.8 112.3,107.8 114.3,108 116,108 117.7,108.3 119.4,108.5
                121.2,108.9 123,109.3 124.9,109.9 126.8,110.5 128.7,111.2 130.7,112.1 132.7,113.1 134.7,114.1 136.8,115.3 138.9,116.6
                141,118.1 143.2,119.6 145.4,121.4 145.8,121.8 146.2,122.2 146.6,122.6 147,123.1 147.4,123.5 147.7,123.9 148.1,124.4
                148.4,124.9 148.7,125.4 148.9,125.9 149.2,126.5 149.3,127 149.5,127.6 149.6,128.2 149.7,128.8 	"/>
                            <polygon class="cat_yellow_st2" points="84.2,115.1 83.4,115.6 82.6,116.1 81.6,116.9 80.7,117.9 79.7,118.9 78.6,120 77.6,121.2 76.4,122.5
                75.2,123.8 73.8,125.1 72.6,126.5 71.1,127.7 69.6,128.9 68.1,130 66.5,131.2 64.9,132 63.2,132.8 61.6,133.5 60.2,134.1
                58.8,134.6 57.3,135 56.1,135.3 54.8,135.5 53.5,135.7 52.3,135.7 51.1,135.7 50,135.5 48.9,135.4 47.9,135.1 46.8,134.7
                45.7,134.3 44.7,133.9 43.2,133.1 41.8,132.3 40.3,131.5 39,130.6 37.6,129.7 36.3,128.8 35.1,128 33.9,127 32.7,126.1 31.6,125.1
                30.5,124.2 29.5,123.2 28.5,122.2 27.6,121.2 26.7,120.2 25.8,119.2 25.6,118.9 25.3,118.5 25.1,118.1 24.9,117.7 24.6,117.3
                24.4,116.9 24.2,116.5 24.1,116 23.9,115.6 23.7,115.1 23.6,114.6 23.5,114.2 23.4,113.8 23.3,113.4 23.3,113.1 23.3,112.7
                23.3,111.8 23.5,110.8 23.7,109.9 24.1,109 24.5,108.2 24.9,107.4 25.5,106.6 26.1,106 26.8,105.3 27.6,104.8 28.3,104.3
                29.1,103.9 30,103.6 31,103.4 31.9,103.2 32.9,103.1 33.6,103.2 34.2,103.2 34.9,103.4 35.5,103.5 36,103.8 36.7,104 37.2,104.2
                37.8,104.6 38.3,105 38.8,105.3 39.3,105.7 39.7,106.1 40.2,106.6 40.6,107.1 41,107.7 41.3,108.1 41.9,109.1 42.7,110 43.7,111
                44.7,112 45.9,113.1 47.2,114.1 48.7,115 50.3,116 51.9,116.9 53.8,117.7 55.8,118.4 58,119.1 60.2,119.6 62.6,120 65.2,120.3
                67.8,120.4 68.6,120.3 69.5,120.2 70.4,120 71.4,119.9 72.3,119.6 73.4,119.3 74.4,119 75.5,118.6 76.6,118.2 77.7,117.8
                78.8,117.3 79.9,116.9 81,116.5 82.1,116 83.1,115.6 	"/>
                            <polygon class="cat_yellow_st3" points="52.6,122.8 50.9,122.5 49.3,122.1 47.8,121.5 46.4,120.9 45.2,120.3 44,119.5 42.9,118.7 41.8,117.8
                40.9,117 40,116.1 39.3,115.4 38.6,114.6 38,113.8 37.4,113.2 36.9,112.7 36.5,112.3 36.4,112 36.1,111.9 35.8,111.6 35.6,111.4
                35.3,111.1 34.9,110.8 34.5,110.6 34.1,110.4 33.7,110.1 33.3,109.9 32.8,109.7 32.3,109.6 31.8,109.4 31.3,109.3 30.7,109.2
                30.2,109.2 29.6,109.3 29,109.5 28.3,109.7 27.7,110.1 27.2,110.5 26.6,111 26,111.5 25.6,112.1 25.2,112.8 24.9,113.5 24.6,114.2
                24.5,115 24.5,115.8 24.6,116.7 24.8,117.5 25.2,118.4 25,118.2 24.9,118 24.7,117.7 24.5,117.3 24.4,117 24.2,116.7 24.1,116.3
                23.9,115.9 23.7,115.5 23.7,115.1 23.6,114.7 23.4,114.3 23.4,113.9 23.3,113.5 23.3,113.1 23.3,112.7 23.3,111.8 23.5,110.8
                23.7,109.9 24.1,109 24.5,108.1 24.9,107.3 25.5,106.6 26.1,105.9 26.8,105.3 27.6,104.8 28.3,104.3 29.1,103.9 30,103.6 31,103.4
                31.9,103.2 32.9,103.1 33.6,103.2 34.2,103.2 34.9,103.4 35.5,103.5 36,103.8 36.7,104 37.2,104.2 37.8,104.6 38.3,105 38.8,105.3
                39.3,105.7 39.7,106.1 40.2,106.6 40.6,107.1 41,107.7 41.3,108.1 41.9,109.1 42.7,110 43.7,111 44.7,112 45.8,113.1 47.2,114
                48.6,115 50.2,116 51.9,116.9 53.7,117.7 55.7,118.4 57.9,119 60.2,119.6 62.6,120 65.1,120.2 67.8,120.4 67.3,120.6 66.8,120.8
                66.1,121.1 65.4,121.3 64.5,121.6 63.7,121.9 62.7,122.1 61.7,122.3 60.7,122.5 59.5,122.7 58.4,122.8 57.2,122.9 56.1,123
                54.9,123 53.7,122.9 	"/>
                            <polygon class="cat_yellow_st4" points="56.9,133.2 56,134.1 54.7,134.4 53.2,134.3 51.6,133.7 50.2,132.6 49.3,131.4 49,130.1 49.3,128.9
                50.2,128 51.4,127.7 53,127.8 54.6,128.4 55.9,129.5 56.8,130.7 57.1,132 56.9,133.2 	"/>
                        </g>
                        <polygon id="body_8_" class="cat_yellow_st2" points="180.4,0 178.9,0.1 177.4,0.4 175.8,0.9 174.6,1.7 173.4,2.6 172.4,3.7 171.5,5
                170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 168.8,11 166.4,16.4 164.2,21.6
                161.6,27.7 160.5,29.6 159.3,31.3 158.1,32.6 156.6,33.7 155.2,34.5 153.8,35 152.4,35.5 151.1,35.7 126.3,35.7 125.4,35.5
                124.1,35.2 122.7,34.7 121.3,34 119.8,33 118.4,31.7 117,30 115.9,27.9 113.5,22.6 111,16.7 108.8,11.4 106.6,6.4 106.6,6.4
                106.6,6.4 106.6,6.4 106.6,6.4 106.6,6.4 106.6,6.4 106.6,6.4 106.6,6.4 105.9,5.1 105,3.8 104,2.8 102.8,1.8 101.5,1 100,0.5
                98.4,0.2 96.9,0 94.9,0.2 92.9,0.8 91.1,1.7 89.6,2.9 88.4,4.4 87.4,6 86.8,7.9 86.5,9.9 86.5,21.3 86.5,29.6 86.5,37.9 86.5,46.3
                86.5,54.6 86.5,62.9 86.5,71.2 86.5,79.5 86.5,87.9 86.5,96.2 86.5,104.6 86.5,112.9 86.5,120 86.9,123.8 88,127.5 89.9,130.8
                92.3,133.7 95.1,136.1 98.4,137.9 102,139.2 105.9,139.7 117.3,139.7 159.7,139.7 172,139.7 175.8,139.2 179.3,138 182.5,136.1
                185.2,133.7 187.4,130.8 189.1,127.5 190.1,123.8 190.5,120 190.5,112.9 190.5,105.2 190.5,97.6 190.5,89.9 190.5,82.3 190.5,74.7
                190.5,67.1 190.5,59.5 190.5,51.8 190.5,44.2 190.5,36.5 190.5,28.9 190.5,21.3 190.5,9.9 190.3,7.9 189.7,6 188.8,4.4 187.5,2.9
                186,1.7 184.3,0.8 182.4,0.2 	"/>
                        <polygon id="body_light_part_11_" class="cat_yellow_st5" points="180.4,0 178.9,0.1 177.4,0.4 175.8,0.9 174.6,1.7 173.4,2.6 172.4,3.7
                171.5,5 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 170.8,6.4 168.8,11 166.4,16.4
                164.2,21.6 161.6,27.7 160.5,29.6 159.3,31.3 158.1,32.6 156.6,33.7 155.2,34.5 153.8,35 152.4,35.5 151.1,35.7 138.9,35.7
                138.9,139.7 159.7,139.7 172,139.7 175.8,139.2 179.3,138 182.5,136.1 185.2,133.7 187.4,130.8 189.1,127.5 190.1,123.8 190.5,120
                190.5,112.9 190.5,105.2 190.5,97.6 190.5,89.9 190.5,82.3 190.5,74.7 190.5,67.1 190.5,59.5 190.5,51.8 190.5,44.2 190.5,36.5
                190.5,28.9 190.5,21.3 190.5,9.9 190.3,7.9 189.7,6 188.8,4.4 187.5,2.9 186,1.7 184.3,0.8 182.4,0.2 	"/>
                        <polygon id="shadow_11_" class="cat_yellow_st1" points="138.9,139.7 113.7,139.7 109.9,139.3 106.4,138.1 103.2,136.2 100.5,133.9
                98.3,130.9 96.7,127.6 95.7,123.9 95.3,120 95.3,88.7 96.5,89.2 100.1,90.2 105.4,91.2 111.8,91.8 115.3,91.7 118.8,91.2
                122.5,90.6 124.6,90 126.5,89.2 128.4,88.4 130,87.4 131.6,86.2 133.1,84.9 134.6,83.5 136.2,81.9 137.5,80.1 138.9,77.8 	"/>
                        <polygon id="light_3_11_" class="cat_yellow_st3" points="190.5,9.9 190.5,120 190.2,123.9 189.2,127.6 187.6,130.9 185.5,133.9 182.8,136.2
                179.7,138.1 176.2,139.3 172.4,139.7 171.9,139.7 171.3,139.7 170.8,139.7 170.2,139.7 169.7,139.7 169.1,139.7 168.5,139.7
                168,139.7 173.1,138.9 177.1,137.3 180.5,134.7 182.9,131.5 184.7,127.9 185.8,124.2 186.4,120.6 186.6,117.3 186.6,12.7
                186.6,11.4 186.6,10.2 186.6,9.1 186.5,8.1 186.2,7.1 185.9,6.2 185.3,5.2 184.5,4.2 183.4,3.3 181.9,2.6 180.3,2.2 178.5,2.1
                176.6,2.2 174.9,2.6 173.2,3.4 171.9,4.4 172.9,3.2 173.9,2.1 175,1.3 176.1,0.8 177.3,0.4 178.4,0.2 179.5,0 180.6,0 182.7,0.2
                184.5,0.9 186.2,1.7 187.7,2.9 188.9,4.4 189.7,6.1 190.4,7.9 	"/>
                        <polygon id="light_2_11_" class="cat_yellow_st6" points="186.6,9.9 186.6,120 186.2,123.9 185.2,127.6 183.6,130.9 181.5,133.9 178.8,136.2
                175.7,138.1 172.3,139.3 168.5,139.7 167.1,139.7 165.8,139.7 164.4,139.7 163.1,139.7 161.8,139.7 160.4,139.7 159.1,139.7
                157.8,139.7 165.7,139 171.7,137.2 176.1,134.5 179.1,131.2 181,127.5 182,123.8 182.5,120.3 182.6,117.3 182.6,12.7 182.6,11.2
                182.6,9.4 182.4,7.7 182.1,6 181.6,4.5 180.7,3.3 179.4,2.5 177.7,2.1 179.4,2 181.2,2.1 182.7,2.7 183.9,3.6 185.1,4.8 185.9,6.3
                186.4,8 	"/>
                        <polygon id="light_3_9_" class="cat_yellow_st5" points="86.4,9.9 86.4,120 86.7,123.9 87.8,127.6 89.3,130.9 91.5,133.9 94.2,136.2
                97.3,138.1 100.7,139.3 104.5,139.7 105.1,139.7 105.6,139.7 106.2,139.7 106.7,139.7 107.3,139.7 107.8,139.7 108.4,139.7
                108.9,139.7 103.9,138.9 99.8,137.3 96.4,134.7 94,131.5 92.3,127.9 91.2,124.2 90.5,120.6 90.4,117.3 90.4,12.7 90.4,11.4
                90.4,10.2 90.4,9.1 90.5,8.1 90.7,7.1 91.1,6.2 91.6,5.2 92.4,4.2 93.5,3.3 95.1,2.6 96.6,2.2 98.5,2.1 100.3,2.2 102,2.6
                103.7,3.4 105.1,4.4 104,3.2 103,2.1 102,1.3 100.9,0.8 99.7,0.4 98.5,0.2 97.4,0 96.3,0 94.3,0.2 92.4,0.9 90.8,1.7 89.3,2.9
                88.1,4.4 87.2,6.1 86.6,7.9 	"/>
                        <polygon id="light_2_3_" class="cat_yellow_st5" points="90.4,9.9 90.4,120 90.7,123.9 91.7,127.6 93.3,130.9 95.5,133.9 98.2,136.2
                101.2,138.1 104.7,139.3 108.5,139.7 109.8,139.7 111.2,139.7 112.5,139.7 113.8,139.7 115.1,139.7 116.5,139.7 117.8,139.7
                119.1,139.7 111.3,139 105.2,137.2 100.9,134.5 97.8,131.2 95.9,127.5 94.9,123.8 94.4,120.3 94.3,117.3 94.3,12.7 94.3,11.2
                94.3,9.4 94.5,7.7 94.8,6 95.4,4.5 96.2,3.3 97.5,2.5 99.3,2.1 97.5,2 95.8,2.1 94.3,2.7 93,3.6 91.9,4.8 91.1,6.3 90.5,8 	"/>
                        <polygon id="shadow_on_light_part_11_" class="cat_yellow_st2" points="138.9,139.7 164.2,139.7 168,139.3 171.5,138.1 174.7,136.2
                177.4,133.9 179.6,130.9 181.2,127.6 182.2,123.9 182.6,120 182.6,88.7 181.3,89.2 177.8,90.2 172.5,91.2 166.2,91.8 162.6,91.7
                159,91.2 155.4,90.6 153.2,90 151.4,89.2 149.5,88.4 147.8,87.4 146.2,86.2 144.8,84.9 143.3,83.5 141.7,81.9 140.4,80.1
                138.9,77.8 	"/>
                        <g id="pow_left_11_">
                            <polygon class="cat_yellow_st7" points="128.6,120.8 128.5,122.8 128.1,124.7 127.7,126.5 127,128.3 126.2,130 125.3,131.5 124.2,133.1
                    123,134.4 121.6,135.6 120.2,136.7 118.6,137.7 116.9,138.5 115.2,139.1 113.4,139.6 111.5,139.9 109.6,140 107.7,139.9
                    105.7,139.6 103.9,139.1 102.2,138.5 100.6,137.7 99,136.7 97.5,135.6 96.2,134.4 95,133.1 93.8,131.5 92.9,130 92.1,128.3
                    91.5,126.5 91,124.7 90.7,122.8 90.5,120.8 90.5,120.8 90.6,120.7 90.6,120.5 90.6,120.4 90.7,120.4 90.7,120.2 90.7,120.1
                    90.7,120 90.7,120 90.7,119.8 90.7,119.7 90.7,119.6 90.7,119.6 90.7,119.4 90.7,119.3 90.7,119.2 90.9,121.1 91.3,122.7
                    91.9,124.4 92.6,125.9 93.4,127.4 94.4,128.7 95.5,130 96.7,131.1 98,132.1 99.5,133 101,133.8 102.6,134.4 104.2,134.9
                    106,135.3 107.7,135.5 109.6,135.6 111.5,135.5 113.2,135.3 115,134.9 116.6,134.4 118.2,133.8 119.7,133 121.2,132.1
                    122.4,131.1 123.7,130 124.8,128.7 125.8,127.4 126.5,125.9 127.3,124.4 127.8,122.7 128.2,121.1 128.5,119.2 128.5,119.3
                    128.5,119.4 128.5,119.6 128.5,119.6 128.5,119.7 128.5,119.8 128.5,120 128.5,120 128.5,120.1 128.5,120.2 128.5,120.4
                    128.5,120.4 128.5,120.5 128.6,120.7 128.6,120.8 		"/>
                            <polygon class="cat_yellow_st7" points="105,126.3 105,126.3 105,126.3 105,126.3 105,126.3 104.4,126.5 103.9,126.8 103.5,127.3
                    103.4,127.9 103.4,136.9 103.5,137.6 103.9,138.1 104.4,138.5 105,138.5 105,138.5 105,138.5 105,138.5 105,138.5 105.6,138.5
                    106.1,138.1 106.5,137.6 106.6,136.9 106.6,127.9 106.5,127.3 106.1,126.8 105.6,126.5 		"/>
                            <polygon class="cat_yellow_st7" points="114.2,126.3 114.2,126.3 114.2,126.3 114.2,126.3 114.2,126.3 113.5,126.5 113.1,126.8 112.7,127.3
                    112.6,127.9 112.6,136.9 112.7,137.6 113.1,138.1 113.5,138.5 114.2,138.5 114.2,138.5 114.2,138.5 114.2,138.5 114.2,138.5
                    114.8,138.5 115.3,138.1 115.6,137.6 115.8,136.9 115.8,127.9 115.6,127.3 115.3,126.8 114.8,126.5 		"/>
                        </g>
                        <g id="pow_right_11_">
                            <polygon class="cat_yellow_st8" points="186.6,120.8 186.4,122.8 186.1,124.7 185.6,126.5 185,128.3 184.2,130 183.2,131.5 182.2,133.1
                    180.9,134.4 179.6,135.6 178.2,136.7 176.6,137.7 174.9,138.5 173.2,139.1 171.3,139.6 169.5,139.9 167.5,140 165.6,139.9
                    163.7,139.6 161.9,139.1 160.1,138.5 158.5,137.7 157,136.7 155.4,135.6 154.2,134.4 152.9,133.1 151.8,131.5 150.8,130
                    150,128.3 149.4,126.5 148.9,124.7 148.6,122.8 148.5,120.8 148.5,120.8 148.5,120.7 148.5,120.5 148.5,120.4 148.6,120.4
                    148.6,120.2 148.6,120.1 148.6,120 148.6,120 148.6,119.8 148.6,119.7 148.6,119.6 148.6,119.6 148.6,119.4 148.6,119.3
                    148.7,119.2 148.9,121.1 149.3,122.7 149.8,124.4 150.5,125.9 151.4,127.4 152.4,128.7 153.5,130 154.7,131.1 156,132.1
                    157.4,133 158.9,133.8 160.5,134.4 162.2,134.9 163.9,135.3 165.7,135.5 167.5,135.6 169.4,135.5 171.2,135.3 172.9,134.9
                    174.6,134.4 176.2,133.8 177.7,133 179.1,132.1 180.4,131.1 181.6,130 182.8,128.7 183.7,127.4 184.5,125.9 185.2,124.4
                    185.8,122.7 186.2,121.1 186.4,119.2 186.4,119.3 186.4,119.4 186.5,119.6 186.5,119.6 186.5,119.7 186.5,119.8 186.5,120
                    186.5,120 186.5,120.1 186.5,120.2 186.5,120.4 186.5,120.4 186.5,120.5 186.6,120.7 186.6,120.8 		"/>
                            <polygon class="cat_yellow_st8" points="163,126.3 163,126.3 163,126.3 163,126.3 163,126.3 162.4,126.5 161.9,126.8 161.5,127.3
                    161.4,127.9 161.4,136.9 161.5,137.6 161.9,138.1 162.4,138.5 163,138.5 163,138.5 163,138.5 163,138.5 163,138.5 163.5,138.5
                    164.1,138.1 164.4,137.6 164.6,136.9 164.6,127.9 164.4,127.3 164.1,126.8 163.5,126.5 		"/>
                            <polygon class="cat_yellow_st8" points="172.1,126.3 172.1,126.3 172.1,126.3 172.1,126.3 172.1,126.3 171.5,126.5 171,126.8 170.7,127.3
                    170.5,127.9 170.5,136.9 170.7,137.6 171,138.1 171.5,138.5 172.1,138.5 172.1,138.5 172.1,138.5 172.1,138.5 172.1,138.5
                    172.8,138.5 173.2,138.1 173.5,137.6 173.7,136.9 173.7,127.9 173.5,127.3 173.2,126.8 172.8,126.5 		"/>
                        </g>
                        <polygon id="blick_10_" class="cat_yellow_st3" points="182.6,132.5 183.6,130.9 184.6,128.8 185.5,126.3 186.2,124.1 186.4,121.8
                186.6,119.5 186.7,117.1 186.7,114.6 186.9,111.9 187.3,109 187.9,106.2 188.6,103.8 189.3,102.2 189.9,101.4 190.4,101.9
                190.5,104 190.5,106.9 190.5,109.6 190.5,112 190.5,114.2 190.5,116 190.5,117.4 190.6,120 190.5,121.8 190.2,123.9 189.6,126.4
                188.6,129 187.2,131.6 185.1,134.2 183.2,136.2 182.1,136.8 181.5,136.8 181.3,136.2 181.4,135.2 181.7,134.2 182,133.5 182.3,133
                    "/>
                        <polygon id="ear_23_" class="cat_yellow_st7" points="94.5,14.8 94.6,14.1 94.8,13.4 95.2,12.9 95.7,12.3 96.2,11.9 96.9,11.6 97.7,11.4
                98.4,11.4 98.9,11.4 99.6,11.6 100.1,11.8 100.6,12.1 101,12.5 101.4,13 101.7,13.6 101.9,14.1 106.2,27.8 94.5,27.8 	"/>
                        <polygon id="nose_11_" class="cat_yellow_st3" points="140.4,76.4 139.4,76.4 138.5,76.4 137.7,76.4 136.7,76.4 134.8,76 133.3,74.9
                132.2,73.4 131.9,71.5 131.9,71.5 131.9,71.5 131.9,71.5 131.9,71.5 132,70.4 132.7,69.5 133.5,69 134.6,68.8 136.6,68.8
                138.5,68.8 140.5,68.8 142.5,68.8 143.6,69 144.4,69.5 145,70.4 145.2,71.5 145.2,71.5 145.2,71.5 145.2,71.5 145.2,71.5
                144.9,73.4 143.8,74.9 142.3,76 	"/>
                        <polygon id="ear_22_" class="cat_yellow_st9" points="182.6,14.8 182.4,14.1 182.2,13.4 181.8,12.9 181.2,12.3 180.7,11.9 180.1,11.6
                179.3,11.4 178.6,11.4 178,11.4 177.4,11.6 176.9,11.8 176.3,12.1 175.9,12.5 175.5,13 175.2,13.6 175,14.1 170.7,27.8 182.6,27.8
                    "/>
                        <polygon class="cat_yellow_st4" points="130.8,64.2 134,62.6 137.1,60 140.2,56.4 143,52.3 145.5,47.9 147.3,43.6 148.4,39.6 148.7,36.1
                145.1,36.1 141.5,36.1 138.1,36.1 135,36.1 132.3,36.1 130.3,36.1 128.9,36.1 128.5,36.1 128.4,36.9 128.3,38.8 127.8,41.7
                126.9,45.1 125.4,48.6 123.3,52 120.2,54.9 116.1,56.9 116.5,58.8 118,60.4 120.3,61.7 123.1,62.7 125.9,63.4 128.4,63.9
                130.2,64.1 130.8,64.2 	"/>
                        <polygon class="cat_yellow_st10" points="109.1,22 109.7,23.8 110.4,25.6 111,27.4 111.5,29.3 111.9,31.2 112.2,33 112.4,34.8 112.4,36.4
                111.9,38 110.5,40.7 108.3,44 105.4,47.9 101.8,51.8 97.5,55.6 92.6,58.8 87.3,61.4 87.3,66.7 87.3,71.3 87.3,75.3 87.3,78.5
                87.3,81 87.3,82.9 87.3,83.9 87.3,84.3 87.6,84.5 88.7,84.9 90.4,85.5 92.6,86 95.2,86.4 98.2,86.4 101.5,85.9 105,84.9
                108.2,83.4 112.4,80.9 117.1,77.3 121.8,72.3 126.1,65.8 129.5,57.7 131.4,47.8 131.4,35.8 128.7,35.8 126.4,35.6 124.4,35.2
                122.6,34.8 121,34.2 119.5,33.5 118.1,32.7 116.7,31.8 114.8,30.4 113.2,28.8 111.9,27.2 110.9,25.6 110.1,24.2 109.5,23.1
                109.2,22.3 109.1,22 	"/>
                        <polygon id="light_1_11_" class="cat_yellow_st11" points="138.9,35.7 138.9,39.7 124.6,39.7 122,39.4 119.7,38.5 117.5,37 115.4,34.8
                113.4,31.9 111.5,28.4 102.3,7.3 101.7,6.1 100.9,5 100,4.1 99.1,3.4 98,2.9 96.9,2.5 95.7,2.3 94.6,2.3 93.3,2.4 92.1,2.8
                90.8,3.3 89.6,4.1 88.6,5.2 87.7,6.4 87,7.8 86.6,9.4 86.9,7.7 87.4,6 88.3,4.5 89.6,3 91.1,1.8 92.8,0.8 94.7,0.2 96.9,0
                98.4,0.2 100,0.6 101.4,1.1 102.7,1.8 104,2.8 105,3.8 105.9,5.1 106.6,6.4 106.9,7.1 107.7,9 115.9,28.1 116.2,28.6 116.5,29.1
                116.8,29.6 117,30.1 117.3,30.5 117.7,30.9 118.1,31.4 118.4,31.7 119.4,32.7 120.5,33.6 121.8,34.3 123.1,34.9 124.3,35.2
                125.6,35.5 126.7,35.6 127.7,35.7 	"/>
                        <polygon id="light_1_14_" class="cat_yellow_st11" points="138.9,35.7 138.9,39.7 153,39.7 155.6,39.4 157.9,38.5 160.1,37 162.1,34.8
                164.1,31.9 166,28.4 175,7.3 175.7,6.1 176.4,5 177.3,4.1 178.2,3.4 179.3,2.9 180.4,2.5 181.5,2.3 182.7,2.3 184,2.4 185.1,2.8
                186.4,3.3 187.6,4.1 188.6,5.2 189.4,6.4 190.1,7.8 190.5,9.4 190.3,7.7 189.8,6 188.9,4.5 187.6,3 186.2,1.8 184.4,0.8 182.5,0.2
                180.4,0 178.9,0.2 177.4,0.6 176,1.1 174.6,1.8 173.4,2.8 172.4,3.8 171.5,5.1 170.8,6.4 170.5,7.1 169.7,9 161.6,28.1 161.3,28.6
                161.1,29.1 160.8,29.6 160.5,30.1 160.2,30.5 159.8,30.9 159.5,31.4 159.2,31.7 158.2,32.7 157.1,33.6 155.8,34.3 154.6,34.9
                153.3,35.2 152.1,35.5 151,35.6 150.1,35.7 	"/>
                        <g class="eye_right">
                            <circle id="eye_right_shadow_1_" class="cat_yellow_st7" cx="111" cy="68.8" r="15.5"/>
                            <circle class="eyeball cat_yellow_st3" cx="112.8" cy="65.2" r="15.5"/>
                            <circle class="pupil" cx="113" cy="65" r="5.9"/>
                            <polygon class="cat_yellow_st12" points="127.1,71.2 126.6,72.3 125.9,73.5 125.3,74.4 124.6,75.4 123.8,76.2
                    122.9,77 122,77.7 121,78.3 120.1,78.9 119.1,79.4 118.1,79.8 117,80.1 116,80.4 114.9,80.5 113.9,80.6 112.9,80.7 111.3,80.6
                    109.8,80.4 108.3,80 106.8,79.5 105.4,78.8 104.3,78.1 103.1,77.2 101.9,76.1 100.9,75.1 100,73.9 99.3,72.6 98.7,71.2 98,69.9
                    97.6,68.3 97.4,66.8 97.4,65.2 97.4,64.8 97.4,64.4 97.4,63.9 97.4,63.4 97.5,62.7 97.6,62.1 97.8,61.4 97.9,60.8 98,60
                    98.3,59.3 98.7,58.6 98.9,57.9 99.3,57.3 99.7,56.6 100.3,56 101,55.3 100.1,56.6 99.6,57.9 99.3,59.3 99.1,60.8 99.2,62.3
                    99.3,63.8 99.7,65.3 100.3,66.8 101,68.2 101.8,69.7 102.8,71 103.9,72.2 105,73.2 106.3,74.2 107.6,75 109,75.6 110.3,76
                    111.6,76.2 112.8,76.4 114.1,76.4 115.5,76.4 116.9,76.2 118.2,76 119.4,75.7 120.7,75.4 121.9,74.9 123,74.4 124,73.9
                    124.9,73.3 125.9,72.6 126.6,71.9 		"/>
                        </g>
                        <g class="eye_left">
                            <circle id="eye_right_shadow_4_" class="cat_yellow_st7" cx="162" cy="68.8" r="15.5"/>
                            <circle class="eyeball cat_yellow_st3" cx="163.8" cy="65.2" r="15.5"/>
                            <circle class="pupil" cx="164" cy="65" r="5.9"/>
                            <polygon class="cat_yellow_st12" points="178.1,71.2 177.6,72.3 176.9,73.5 176.3,74.4 175.6,75.4 174.8,76.2
                    173.9,77 173,77.7 172,78.3 171.1,78.9 170.1,79.4 169.1,79.8 168,80.1 167,80.4 165.9,80.5 164.9,80.6 163.9,80.7 162.3,80.6
                    160.8,80.4 159.3,80 157.8,79.5 156.4,78.8 155.3,78.1 154.1,77.2 152.9,76.1 151.9,75.1 151,73.9 150.3,72.6 149.7,71.2
                    149,69.9 148.6,68.3 148.4,66.8 148.4,65.2 148.4,64.8 148.4,64.4 148.4,63.9 148.4,63.4 148.5,62.7 148.6,62.1 148.8,61.4
                    148.9,60.8 149,60 149.3,59.3 149.7,58.6 149.9,57.9 150.3,57.3 150.7,56.6 151.3,56 152,55.3 151.1,56.6 150.6,57.9 150.3,59.3
                    150.1,60.8 150.2,62.3 150.3,63.8 150.7,65.3 151.3,66.8 152,68.2 152.8,69.7 153.8,71 154.9,72.2 156,73.2 157.3,74.2 158.6,75
                    160,75.6 161.3,76 162.6,76.2 163.8,76.4 165.1,76.4 166.5,76.4 167.9,76.2 169.2,76 170.4,75.7 171.7,75.4 172.9,74.9 174,74.4
                    175,73.9 175.9,73.3 176.9,72.6 177.6,71.9 		"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="cat_brown cats">
                <svg version="1.1" id="cat_brown" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 -15 191.4 173.3" style="transform-origin: center bottom;" xml:space="preserve">
                        <style type="text/css">
                            .cat_brown_st0{fill:#7D5A38;}
                            .cat_brown_st1{fill:#917050;}
                            .cat_brown_st2{fill:#D5C0AA;}
                            .cat_brown_st3{fill:#AD9781;}
                            .cat_brown_st4{fill:#FFFFFF;enable-background:new    ;}
                            .cat_brown_st5{fill:#F0E5DF;}
                            .cat_brown_st6{opacity:0.61;fill:#F0E5DF;}
                            .cat_brown_st7{opacity:0.48;fill:#876A4E;}
                            .cat_brown_st8{fill:#5D3918;}
                            .cat_brown_st9{fill:#FFFFFF;}
                            .cat_brown_st10{fill:#E0D3D0;}
                            .cat_brown_st11{opacity:0.6;fill:#D5C0AA;}
                            .cat_brown_st12{opacity:0.35;fill:#FFFFFF;}
                            .cat_brown_st13{fill:#846241;}
                            .cat_brown_st14{fill:#77624C;}
                            .cat_brown_st15{opacity:0.2;fill:#996666;enable-background:new    ;}
                        </style>
                    <g class="cat_body">
                        <g class="cat_tail">
                            <polygon class="cat_brown_st0" points="145.2,145.4 145.2,146.6 145,147.6 144.8,148.6 144.4,149.6 144,150.6 143.4,151.5 142.8,152.3
                142.1,153.1 141.3,153.8 140.5,154.4 139.6,154.9 138.6,155.4 137.7,155.8 136.7,156 135.6,156.2 134.4,156.2 133.9,156.2
                133.3,156.2 132.8,156.1 132.1,155.9 131.6,155.8 131.1,155.7 130.5,155.5 130.1,155.2 129.6,154.9 129.1,154.7 128.6,154.4
                128.1,154 127.7,153.8 127.3,153.4 126.9,153.1 126.6,152.6 125.3,151.6 124,150.5 122.7,149.4 121.3,148.5 119.7,147.6
                118.2,146.7 116.6,145.9 114.9,145.2 113.3,144.6 111.7,144 110,143.5 108.4,143.1 106.7,142.8 105.1,142.5 103.4,142.5
                101.9,142.4 100.9,142.4 99.8,142.5 98.8,142.7 97.5,142.9 96.4,143.2 95.1,143.5 93.8,143.9 92.5,144.3 91.3,144.7 89.9,145.2
                88.6,145.7 87.3,146.2 85.9,146.7 84.6,147.3 83.2,147.8 82,148.3 80.3,149 78.6,149.7 76.9,150.4 75.1,151 73.3,151.7 71.4,152.4
                69.6,153 67.8,153.5 66,154 64.2,154.6 62.4,155 60.5,155.4 58.7,155.7 56.9,155.9 55.2,156.1 53.4,156.2 51.9,156.3 50.4,156.3
                48.9,156.2 47.4,156.1 45.9,156 44.3,155.8 42.8,155.5 41.3,155.2 39.8,154.9 38.3,154.5 36.6,154 35,153.4 33.2,152.8 31.4,152.1
                29.5,151.3 27.5,150.4 25.9,149.6 24.2,148.7 22.6,147.8 21.1,146.9 19.6,145.9 18.1,144.9 16.7,143.9 15.4,142.8 14,141.8
                12.8,140.7 11.6,139.6 10.5,138.5 9.4,137.4 8.3,136.2 7.3,135.1 6.4,134.1 6.1,133.7 5.8,133.2 5.5,132.9 5.2,132.4 5,132
                4.8,131.5 4.5,131.1 4.3,130.7 4.2,130.2 4,129.8 3.9,129.3 3.8,128.8 3.7,128.4 3.6,127.8 3.5,127.2 3.5,126.7 3.6,125.7
                3.8,124.6 4,123.5 4.4,122.6 4.9,121.7 5.4,120.8 6,119.9 6.7,119.2 7.4,118.4 8.3,117.8 9.2,117.3 10.1,116.9 11.1,116.5
                12.1,116.2 13.2,116 14.3,116 15.1,116 15.8,116 16.5,116.2 17.2,116.4 17.8,116.6 18.6,116.9 19.2,117.2 19.8,117.5 20.3,118
                21,118.4 21.5,118.9 22,119.3 22.5,119.9 23,120.4 23.4,121 23.7,121.6 24.5,122.6 25.4,123.7 26.4,124.7 27.5,126 28.9,127.1
                30.4,128.3 32,129.3 33.8,130.4 35.6,131.4 37.8,132.3 40,133.2 42.4,133.8 44.9,134.4 47.7,134.9 50.5,135.2 53.4,135.3
                54.5,135.2 55.6,135.1 56.7,134.9 58,134.7 59.2,134.3 60.5,133.8 61.8,133.5 63.1,132.9 64.5,132.4 65.8,131.9 67.2,131.4
                68.6,130.8 69.9,130.2 71.2,129.6 72.6,129.1 73.9,128.6 75.9,127.7 77.8,127 79.7,126.2 81.6,125.5 83.5,124.7 85.3,124.1
                87.2,123.5 89,123 91,122.6 92.8,122.1 94.9,121.7 96.9,121.4 99,121.3 101.2,121.2 103.4,121.2 105.6,121.4 107.5,121.4
                109.4,121.7 111.3,122 113.3,122.4 115.3,122.9 117.5,123.5 119.6,124.2 121.7,125 123.9,126 126.2,127.1 128.5,128.3 130.8,129.6
                133.2,131.1 135.5,132.7 138,134.4 140.4,136.5 141,136.8 141.3,137.3 141.8,137.7 142.2,138.3 142.7,138.7 143.1,139.2
                143.5,139.8 143.7,140.4 144.1,141 144.4,141.5 144.6,142.1 144.9,142.8 145,143.4 145.1,144.1 145.2,144.7 	"/>
                            <polygon class="cat_brown_st1" points="71.9,129.4 70.9,129.9 70,130.5 69,131.4 67.8,132.5 66.8,133.7 65.5,134.9 64.4,136.2 63,137.7
                61.8,139.2 60.2,140.6 58.8,142.1 57.2,143.5 55.5,144.9 53.8,146.1 52,147.3 50.1,148.3 48.3,149.2 46.5,150 44.9,150.6
                43.3,151.3 41.7,151.7 40.3,152 38.9,152.3 37.4,152.5 36.1,152.5 34.7,152.5 33.5,152.3 32.3,152.1 31.1,151.9 29.9,151.4
                28.7,150.9 27.5,150.4 25.9,149.5 24.2,148.6 22.7,147.7 21.2,146.7 19.6,145.8 18.1,144.8 16.8,143.8 15.4,142.8 14.1,141.7
                12.9,140.6 11.6,139.5 10.5,138.5 9.4,137.3 8.3,136.2 7.3,135.1 6.4,134.1 6.1,133.7 5.8,133.2 5.5,132.8 5.3,132.3 5,131.9
                4.8,131.4 4.6,130.9 4.4,130.4 4.3,129.9 4,129.4 3.9,128.9 3.8,128.4 3.7,128 3.6,127.5 3.5,127.1 3.5,126.7 3.6,125.7 3.8,124.6
                4,123.5 4.4,122.6 4.9,121.7 5.4,120.8 6,119.9 6.7,119.2 7.4,118.4 8.3,117.8 9.2,117.3 10.1,116.9 11.1,116.5 12.1,116.2
                13.2,116 14.3,116 15.1,116 15.8,116 16.5,116.2 17.2,116.4 17.8,116.6 18.6,116.9 19.2,117.2 19.8,117.5 20.3,118 21,118.4
                21.5,118.9 22,119.3 22.5,119.9 23,120.4 23.4,121 23.7,121.6 24.5,122.6 25.4,123.7 26.4,124.7 27.5,126 28.9,127.1 30.4,128.3
                32,129.3 33.8,130.4 35.6,131.4 37.8,132.3 40,133.2 42.4,133.8 44.9,134.4 47.7,134.9 50.5,135.2 53.4,135.3 54.3,135.2
                55.3,135.1 56.4,134.9 57.5,134.7 58.5,134.4 59.7,134.1 60.9,133.8 62.1,133.3 63.3,132.9 64.5,132.4 65.7,131.9 66.9,131.4
                68.2,130.9 69.5,130.4 70.6,129.9 	"/>
                            <polygon class="cat_brown_st2" points="36.4,138 34.5,137.7 32.7,137.2 31.1,136.6 29.5,135.9 28.1,135.2 26.8,134.3 25.5,133.4 24.4,132.4
                23.3,131.5 22.3,130.5 21.5,129.6 20.7,128.8 20,128 19.3,127.2 18.8,126.6 18.4,126.2 18.2,126 17.9,125.7 17.6,125.5 17.3,125.2
                16.9,125 16.5,124.6 16.2,124.3 15.6,124.1 15.2,123.8 14.8,123.5 14.2,123.3 13.6,123.2 13,123 12.5,122.9 11.9,122.8 11.4,122.8
                10.6,122.9 10,123.1 9.2,123.3 8.5,123.8 7.9,124.2 7.3,124.7 6.7,125.4 6.1,126 5.7,126.9 5.3,127.6 5,128.4 4.9,129.3 4.9,130.2
                5,131.2 5.2,132 5.7,133.1 5.5,132.9 5.3,132.6 5.2,132.3 4.9,131.9 4.8,131.5 4.6,131.2 4.4,130.8 4.3,130.3 4,129.9 4,129.4
                3.9,129 3.7,128.5 3.7,128.1 3.6,127.6 3.5,127.2 3.5,126.7 3.6,125.7 3.8,124.6 4,123.5 4.4,122.6 4.9,121.6 5.4,120.7 6,119.9
                6.7,119 7.4,118.4 8.3,117.8 9.2,117.3 10.1,116.9 11.1,116.5 12.1,116.2 13.2,116 14.3,116 15.1,116 15.8,116 16.5,116.2
                17.2,116.4 17.8,116.6 18.6,116.9 19.2,117.2 19.8,117.5 20.3,118 21,118.4 21.5,118.9 22,119.3 22.5,119.9 23,120.4 23.4,121
                23.7,121.6 24.5,122.6 25.4,123.7 26.4,124.7 27.5,126 28.8,127.1 30.3,128.1 31.9,129.3 33.7,130.4 35.6,131.4 37.7,132.3
                39.9,133.1 42.3,133.8 44.9,134.4 47.6,134.8 50.4,135.1 53.4,135.3 52.9,135.6 52.3,135.8 51.6,136.1 50.8,136.3 49.8,136.7
                48.9,137 47.7,137.2 46.6,137.4 45.5,137.7 44.2,137.9 42.8,138 41.6,138.1 40.3,138.2 38.9,138.2 37.6,138.1 	"/>
                        </g>
                        <polygon id="body_9_" class="cat_brown_st1" points="179.6,0.4 178,0.4 176.3,0.8 174.6,1.3 173.2,2.2 171.8,3.3 170.7,4.5 169.6,6
                            168.9,7.5 168.9,7.5 168.9,7.5 168.9,7.5 168.9,7.5 168.9,7.5 169,7.5 169,7.5 169,7.5 166.6,12.7 164,18.7 161.5,24.5 158.5,31.4
                            157.4,33.5 156.1,35.4 154.6,36.9 153.1,38.1 151.4,38.9 149.8,39.6 148.3,40.1 146.8,40.4 119,40.4 118,40.1 116.6,39.8
                            115.1,39.2 113.4,38.4 111.8,37.4 110.1,35.9 108.6,34 107.4,31.7 104.7,25.7 101.9,19 99.4,13.1 97,7.6 97,7.6 97,7.6 97,7.6
                            97,7.6 97,7.6 97,7.6 97,7.6 97,7.6 96.1,6.1 95.2,4.6 94,3.5 92.7,2.4 91.2,1.5 89.5,0.9 87.8,0.5 86,0.4 83.8,0.7 81.6,1.3
                            79.6,2.2 77.9,3.7 76.5,5.2 75.4,7.1 74.8,9.3 74.5,11.5 74.5,24.2 74.5,33.5 74.5,42.9 74.5,52.2 74.5,61.6 74.5,70.8 74.5,80.2
                            74.5,89.5 74.5,98.8 74.5,108.2 74.5,117.5 74.5,126.9 74.5,134.8 74.9,139.2 76.2,143.3 78.2,146.9 80.8,150.2 84.1,152.9
                            87.8,154.9 91.8,156.4 96.1,157 108.9,157 156.5,157 170.2,157 174.4,156.4 178.5,155 182,152.9 185.1,150.2 187.6,146.9
                            189.5,143.3 190.6,139.2 191,134.8 191,126.9 191,118.3 191,109.7 191,101.2 191,92.6 191,84.1 191,75.6 191,67 191,58.5 191,49.9
                            191,41.3 191,32.8 191,24.2 191,11.5 190.7,9.3 190.1,7.1 189.1,5.2 187.7,3.6 185.9,2.2 184.1,1.3 182,0.7 	"/>
                        <polygon id="body_light_part_7_" class="cat_brown_st3" points="179.6,0.4 178,0.4 176.3,0.8 174.6,1.3 173.2,2.2 171.8,3.3 170.7,4.5
                            169.6,6 168.9,7.5 168.9,7.5 168.9,7.5 168.9,7.5 168.9,7.5 168.9,7.5 169,7.5 169,7.5 169,7.5 166.6,12.7 164,18.7 161.5,24.5
                            158.5,31.4 157.4,33.5 156.1,35.4 154.6,36.9 153.1,38.1 151.4,38.9 149.8,39.6 148.3,40.1 146.8,40.4 133.2,40.4 133.2,157
                            156.5,157 170.2,157 174.4,156.4 178.5,155 182,152.9 185.1,150.2 187.6,146.9 189.5,143.3 190.6,139.2 191,134.8 191,126.9
                            191,118.3 191,109.7 191,101.2 191,92.6 191,84.1 191,75.6 191,67 191,58.5 191,49.9 191,41.3 191,32.8 191,24.2 191,11.5
                            190.7,9.3 190.1,7.1 189.1,5.2 187.7,3.6 185.9,2.2 184.1,1.3 182,0.7 	"/>
                        <polygon id="shadow_7_" class="cat_brown_st0" points="133.2,157 104.9,157 100.7,156.5 96.7,155.2 93.1,153.1 90.1,150.4 87.6,147.1
                            85.9,143.4 84.7,139.2 84.2,134.9 84.2,99.8 85.6,100.3 89.7,101.5 95.5,102.6 102.8,103.2 106.7,103.1 110.7,102.6 114.8,101.9
                            117.2,101.3 119.2,100.4 121.4,99.4 123.2,98.3 125,97 126.6,95.5 128.3,94 130.1,92.1 131.6,90.1 133.2,87.6 	"/><polygon id="light_3_7_" class="cat_brown_st4" points="191,11.5 191,134.9 190.7,139.2 189.5,143.4 187.7,147.1 185.3,150.4 182.3,153.1
                            178.9,155.2 175,156.5 170.8,157 170.1,157 169.5,157 168.9,157 168.2,157 167.6,157 167,157 166.4,157 165.7,157 171.4,156
                            176,154.3 179.9,151.4 182.5,147.8 184.4,143.7 185.7,139.5 186.4,135.6 186.6,131.9 186.6,14.6 186.6,13.1 186.6,11.8 186.6,10.6
                            186.5,9.4 186.2,8.4 185.8,7.3 185.2,6.2 184.3,5.1 183,4 181.4,3.3 179.6,2.8 177.5,2.7 175.5,2.8 173.5,3.3 171.7,4.2 170.1,5.3
                            171.3,3.9 172.4,2.8 173.6,1.9 174.8,1.3 176.2,0.8 177.5,0.5 178.7,0.4 179.9,0.4 182.3,0.7 184.3,1.3 186.2,2.3 187.8,3.7
                            189.2,5.3 190.1,7.2 190.9,9.3 	"/>
                        <polygon id="light_2_7_" class="cat_brown_st5" points="186.6,11.5 186.6,134.9 186.2,139.2 185.1,143.4 183.3,147.1 180.9,150.4
                            177.9,153.1 174.4,155.2 170.5,156.5 166.3,157 164.8,157 163.3,157 161.8,157 160.3,157 158.8,157 157.3,157 155.8,157 154.4,157
                            163.2,156.2 169.9,154.1 174.8,151.1 178.2,147.3 180.4,143.3 181.5,139.1 182,135.2 182.1,131.9 182.1,14.6 182.1,12.9
                            182.1,10.9 182,9 181.6,7.1 181,5.5 180,4 178.6,3.1 176.6,2.8 178.6,2.6 180.5,2.8 182.3,3.4 183.7,4.3 184.9,5.7 185.8,7.4
                            186.4,9.4 	"/>
                        <polygon id="light_3_10_" class="cat_brown_st6" points="74.3,11.5 74.3,134.9 74.7,139.2 75.8,143.4 77.6,147.1 80,150.4 83,153.1
                            86.5,155.2 90.3,156.5 94.6,157 95.3,157 95.9,157 96.5,157 97.2,157 97.8,157 98.4,157 99,157 99.6,157 93.9,156 89.4,154.3
                            85.5,151.4 82.9,147.8 80.9,143.7 79.7,139.5 79,135.6 78.8,131.9 78.8,14.6 78.8,13.1 78.8,11.8 78.8,10.6 78.8,9.4 79.1,8.4
                            79.6,7.3 80.2,6.2 81.1,5.1 82.4,4 84,3.3 85.8,2.8 87.9,2.7 89.9,2.8 91.9,3.3 93.7,4.2 95.3,5.3 94.1,3.9 93,2.8 91.8,1.9
                            90.6,1.3 89.2,0.8 87.9,0.5 86.7,0.4 85.5,0.4 83.1,0.7 81.1,1.3 79.2,2.3 77.6,3.7 76.2,5.3 75.2,7.2 74.5,9.3 	"/>
                        <polygon id="light_2_9_" class="cat_brown_st3" points="78.8,11.5 78.8,134.9 79.1,139.2 80.3,143.4 82.1,147.1 84.5,150.4 87.5,153.1
                            91,155.2 94.8,156.5 99.1,157 100.6,157 102.1,157 103.6,157 105,157 106.5,157 108,157 109.5,157 111,157 102.2,156.2 95.4,154.1
                            90.6,151.1 87.2,147.3 85,143.3 83.9,139.1 83.3,135.2 83.3,131.9 83.3,14.6 83.3,12.9 83.3,10.9 83.4,9 83.8,7.1 84.4,5.5 85.4,4
                            86.8,3.1 88.8,2.8 86.8,2.6 84.8,2.8 83.1,3.4 81.7,4.3 80.5,5.7 79.6,7.4 79,9.4 	"/>
                        <polygon id="shadow_on_light_part_7_" class="cat_brown_st7" points="133.2,157 161.5,157 165.7,156.5 169.6,155.2 173.2,153.1 176.3,150.4
                            178.7,147.1 180.5,143.4 181.7,139.2 182.1,134.9 182.1,99.8 180.7,100.3 176.7,101.5 170.8,102.6 163.7,103.2 159.7,103.1
                            155.7,102.6 151.7,101.9 149.2,101.3 147.2,100.4 145,99.4 143.1,98.3 141.3,97 139.8,95.5 138.1,94 136.3,92.1 134.8,90.1
                            133.2,87.6 	"/>
                        <g id="pow_left_7_">
                            <polygon class="cat_brown_st8" points="121.6,135.8 121.4,138 121.1,140.1 120.5,142.2 119.9,144.2 119,146.1 117.9,147.8 116.7,149.5
                                    115.3,151 113.8,152.4 112.2,153.6 110.4,154.7 108.5,155.5 106.6,156.3 104.6,156.8 102.5,157.2 100.3,157.3 98.1,157.2
                                    96,156.8 94,156.3 92,155.5 90.2,154.7 88.4,153.6 86.8,152.4 85.3,151 83.9,149.5 82.6,147.8 81.6,146.1 80.7,144.2 80,142.2
                                    79.4,140.1 79.1,138 78.9,135.8 78.9,135.7 79,135.6 79,135.5 79,135.3 79.1,135.3 79.1,135.1 79.1,135 79.1,134.9 79.1,134.8
                                    79.1,134.7 79.1,134.6 79.1,134.4 79.1,134.4 79.1,134.2 79.1,134.1 79.2,134.1 79.3,136.1 79.8,138 80.4,139.8 81.2,141.5
                                    82.2,143.2 83.2,144.6 84.5,146.1 85.9,147.3 87.4,148.5 88.9,149.4 90.7,150.3 92.4,151 94.3,151.6 96.2,152 98.3,152.3
                                    100.3,152.4 102.4,152.3 104.3,152 106.3,151.6 108.2,151 110,150.3 111.7,149.4 113.3,148.5 114.7,147.3 116.1,146.1
                                    117.3,144.6 118.4,143.2 119.3,141.5 120.1,139.8 120.7,138 121.1,136.1 121.4,134.1 121.4,134.1 121.4,134.2 121.5,134.4
                                    121.5,134.4 121.5,134.6 121.5,134.7 121.5,134.8 121.5,134.9 121.5,135 121.5,135.1 121.5,135.3 121.5,135.3 121.5,135.5
                                    121.6,135.6 121.6,135.7 		"/>
                            <polygon class="cat_brown_st8" points="95.2,141.9 95.2,141.9 95.2,141.9 95.2,141.9 95.2,141.9 94.5,142.1 94,142.5 93.5,143.1 93.4,143.7
                                    93.4,153.9 93.5,154.6 94,155.2 94.5,155.5 95.2,155.7 95.2,155.7 95.2,155.7 95.2,155.7 95.2,155.7 95.8,155.5 96.4,155.2
                                    96.8,154.6 97,153.9 97,143.7 96.8,143.1 96.4,142.5 95.8,142.1 		"/>
                            <polygon class="cat_brown_st8" points="105.5,141.9 105.5,141.9 105.5,141.9 105.5,141.9 105.5,141.9 104.7,142.1 104.2,142.5 103.8,143.1
                                    103.7,143.7 103.7,153.9 103.8,154.6 104.2,155.2 104.7,155.5 105.5,155.7 105.5,155.7 105.5,155.7 105.5,155.7 105.5,155.7
                                    106.1,155.5 106.7,155.2 107,154.6 107.2,153.9 107.2,143.7 107,143.1 106.7,142.5 106.1,142.1 		"/>
                        </g>
                        <g id="pow_right_7_">
                            <polygon class="cat_brown_st8" points="186.6,135.8 186.4,138 186.1,140.1 185.5,142.2 184.8,144.2 183.9,146.1 182.9,147.8 181.7,149.5
                                    180.3,151 178.7,152.4 177.2,153.6 175.3,154.7 173.5,155.5 171.5,156.3 169.5,156.8 167.5,157.2 165.2,157.3 163.1,157.2
                                    160.9,156.8 158.9,156.3 157,155.5 155.2,154.7 153.4,153.6 151.7,152.4 150.3,151 148.8,149.5 147.6,147.8 146.5,146.1
                                    145.6,144.2 144.9,142.2 144.4,140.1 144,138 143.9,135.8 143.9,135.7 144,135.6 144,135.5 144,135.3 144,135.3 144,135.1
                                    144,135 144,134.9 144,134.8 144,134.7 144,134.6 144,134.4 144,134.4 144,134.2 144,134.1 144.1,134.1 144.3,136.1 144.8,138
                                    145.4,139.8 146.2,141.5 147.2,143.2 148.2,144.6 149.4,146.1 150.8,147.3 152.3,148.5 154,149.4 155.6,150.3 157.4,151
                                    159.3,151.6 161.2,152 163.2,152.3 165.2,152.4 167.4,152.3 169.3,152 171.3,151.6 173.2,151 174.9,150.3 176.6,149.4
                                    178.2,148.5 179.6,147.3 181.1,146.1 182.3,144.6 183.4,143.2 184.3,141.5 185.1,139.8 185.7,138 186.2,136.1 186.4,134.1
                                    186.4,134.1 186.4,134.2 186.5,134.4 186.5,134.4 186.5,134.6 186.5,134.7 186.5,134.8 186.5,134.9 186.5,135 186.5,135.1
                                    186.5,135.3 186.5,135.3 186.5,135.5 186.6,135.6 186.6,135.7 		"/>
                            <polygon class="cat_brown_st8" points="160.2,141.9 160.2,141.9 160.2,141.9 160.2,141.9 160.2,141.9 159.4,142.1 158.9,142.5 158.5,143.1
                                    158.4,143.7 158.4,153.9 158.5,154.6 158.9,155.2 159.4,155.5 160.2,155.7 160.2,155.7 160.2,155.7 160.2,155.7 160.2,155.7
                                    160.8,155.5 161.4,155.2 161.8,154.6 161.9,153.9 161.9,143.7 161.8,143.1 161.4,142.5 160.8,142.1 		"/>
                            <polygon class="cat_brown_st8" points="170.4,141.9 170.4,141.9 170.4,141.9 170.4,141.9 170.4,141.9 169.6,142.1 169.1,142.5 168.8,143.1
                                    168.6,143.7 168.6,153.9 168.8,154.6 169.1,155.2 169.6,155.5 170.4,155.7 170.4,155.7 170.4,155.7 170.4,155.7 170.4,155.7
                                    171.1,155.5 171.7,155.2 172,154.6 172.2,153.9 172.2,143.7 172,143.1 171.7,142.5 171.1,142.1 		"/>
                        </g>
                        <polygon id="blick_6_" class="cat_brown_st9" points="182.1,148.9 183.3,147.1 184.4,144.8 185.4,141.9 186.2,139.5 186.4,136.8 186.6,134.3
                                186.8,131.7 186.8,128.9 186.9,125.8 187.4,122.6 188.1,119.4 188.9,116.8 189.7,114.9 190.3,114 190.9,114.6 191,116.9 191,120.2
                                191,123.2 191,126 191,128.4 191,130.4 191,132 191.1,134.9 191,136.8 190.7,139.2 190,142 188.9,144.9 187.3,147.9 185,150.9
                                182.9,153 181.6,153.7 180.9,153.7 180.7,153 180.8,151.9 181.1,150.9 181.5,150 181.8,149.4 	"/>
                        <polygon id="nose_7_" class="cat_brown_st10" points="134.8,85.9 133.8,85.9 132.8,85.9 131.7,85.9 130.7,85.9 128.6,85.5 126.9,84.4
                                125.7,82.6 125.3,80.5 125.3,80.5 125.3,80.5 125.3,80.5 125.3,80.5 125.4,79.3 126.2,78.3 127.1,77.7 128.3,77.5 130.5,77.5
                                132.8,77.5 135,77.5 137.2,77.5 138.4,77.7 139.3,78.3 140.1,79.3 140.2,80.5 140.2,80.5 140.2,80.5 140.2,80.5 140.2,80.5
                                139.8,82.6 138.6,84.4 136.9,85.5 	"/>
                        <polygon class="cat_brown_st11" points="74.3,64.7 74.7,61.4 75.9,58.1 77.7,54.8 80.1,51.7 83.1,48.9 86.4,46.3 90.1,44 94.1,42.1 97.1,41
                                99.7,39.8 102.1,38.6 103.9,36.9 105,34.7 105.5,31.8 105,28 103.6,23.1 101.7,17.8 99.8,13 97.9,8.8 95.9,5.3 93.6,2.6 90.9,0.8
                                87.6,0 83.8,0.2 80.4,1.4 77.9,3.4 76.3,6.1 75.2,9.4 74.7,13.3 74.5,17.7 74.5,22.5 74.5,27.6 74.5,33.2 74.5,39.3 74.5,45.5
                                74.4,51.4 74.4,56.6 74.4,60.9 74.3,63.7 74.3,64.7 	"/>
                        <polygon class="cat_brown_st12" points="191,64.7 190.6,61.4 189.4,58.1 187.6,54.9 185.2,51.8 182.3,49 179,46.4 175.3,44.1 171.3,42.3
                                168.3,41.1 165.7,40 163.4,38.6 161.7,37 160.5,34.7 160.2,31.8 160.6,28 162.1,23.1 164,17.9 165.8,13.2 167.7,9.2 169.7,5.9
                                172,3.3 174.7,1.6 177.9,0.7 181.7,0.6 185.2,1.5 187.7,3.3 189.3,5.9 190.3,9.2 190.9,13 191,17.4 191,22.2 191,27.3 191,32.9
                                191,39 191,45.2 191,51.2 191,56.5 191,60.8 191,63.7 191,64.7 	"/>
                        <polygon id="ear_15_" class="cat_brown_st13" points="83.4,16.9 83.5,16.1 83.7,15.4 84.1,14.8 84.7,14.2 85.3,13.7 86.1,13.3 86.9,13.2
                                87.7,13.1 88.3,13.2 89,13.3 89.7,13.6 90.2,13.9 90.7,14.4 91.1,14.9 91.4,15.6 91.7,16.1 96.5,31.5 83.4,31.5 	"/>
                        <polygon id="light_1_7_" class="cat_brown_st2" points="133.2,40.4 133.2,44.9 117.2,44.9 114.2,44.5 111.7,43.5 109.1,41.8 106.9,39.3
                                104.6,36.2 102.4,32.2 92.2,8.5 91.4,7.2 90.6,6 89.6,5 88.5,4.2 87.3,3.7 86,3.2 84.8,2.9 83.5,2.9 82,3.1 80.7,3.5 79.3,4.1
                                77.9,5 76.8,6.1 75.8,7.5 75,9.1 74.5,10.9 74.8,9 75.4,7.1 76.4,5.5 77.8,3.7 79.6,2.4 81.5,1.3 83.6,0.7 86,0.4 87.8,0.5 89.5,1
                                91.1,1.6 92.6,2.4 94,3.5 95.2,4.6 96.1,6.1 97,7.6 97.3,8.3 98.3,10.4 107.4,31.9 107.7,32.4 108,33 108.4,33.5 108.6,34.1
                                109,34.5 109.4,35 109.8,35.5 110.1,35.9 111.3,37 112.5,38 113.9,38.8 115.4,39.4 116.8,39.8 118.2,40.1 119.5,40.3 120.5,40.4
                                "/>
                        <polygon id="light_1_15_" class="cat_brown_st11" points="132.8,40.4 132.8,44.9 148.8,44.9 151.7,44.5 154.3,43.5 156.8,41.8 159.1,39.3
                                161.3,36.2 163.6,32.2 173.8,8.5 174.5,7.2 175.3,6 176.3,5 177.5,4.2 178.7,3.7 179.9,3.2 181.1,2.9 182.5,2.9 183.9,3.1
                                185.3,3.5 186.7,4.1 188,5 189.2,6.1 190.1,7.5 191,9.1 191.4,10.9 191.1,9 190.5,7.1 189.5,5.5 188.1,3.7 186.4,2.4 184.4,1.3
                                182.3,0.7 179.9,0.4 178.1,0.5 176.5,1 174.8,1.6 173.3,2.4 171.9,3.5 170.8,4.6 169.8,6.1 169,7.6 168.6,8.3 167.7,10.4
                                158.5,31.9 158.2,32.4 157.9,33 157.6,33.5 157.3,34.1 157,34.5 156.5,35 156.1,35.5 155.8,35.9 154.6,37 153.4,38 152,38.8
                                150.6,39.4 149.1,39.8 147.7,40.1 146.4,40.3 145.4,40.4 	"/>
                        <polygon id="ear_14_" class="cat_brown_st14" points="182.1,16.9 182,16.1 181.7,15.4 181.3,14.8 180.6,14.2 180,13.7 179.3,13.3 178.5,13.2
                                177.7,13.1 177,13.2 176.3,13.3 175.7,13.6 175.1,13.9 174.7,14.4 174.2,14.9 173.8,15.6 173.6,16.1 168.8,31.5 182.1,31.5 	"/>
                        <polygon class="cat_brown_st12" points="146.2,50.6 149.8,49.2 153.6,48.5 157.3,48.2 161,48.2 164.5,48.5 167.7,49.5 170.5,51.1 172.7,53.4
                                170.6,53.5 168,53.4 165.2,53.3 162.3,53.2 159.3,53.2 156.5,53.4 154,53.8 151.8,54.7 150.2,55.3 148.8,55.2 147.9,54.6
                                147.1,53.7 146.7,52.7 146.4,51.7 146.3,50.9 146.2,50.6 	"/>
                        <g class="eye_right">
                            <circle class="eye_right_shadow cat_brown_st8" cx="101.4" cy="77.3" r="17.2"/>
                            <circle class="eyeball cat_brown_st9" cx="103.4" cy="73.3" r="17.2"/>
                            <circle class="pupil" cx="105" cy="72" r="6.5"/>
                            <polygon id="eye_right_blick_7_" class="cat_brown_st15" points="119.3,79.9 118.7,81.1 118,82.4 117.2,83.5 116.5,84.5 115.6,85.4
                                114.6,86.3 113.6,87.1 112.5,87.8 111.5,88.5 110.4,89 109.3,89.5 108.1,89.8 107,90.1 105.8,90.2 104.6,90.3 103.6,90.4
                                101.8,90.3 100.1,90.1 98.4,89.6 96.8,89.1 95.2,88.3 94,87.5 92.6,86.5 91.3,85.3 90.2,84.2 89.2,82.9 88.4,81.4 87.7,79.9
                                87.1,78.4 86.6,76.7 86.4,75.1 86.3,73.2 86.3,72.8 86.3,72.3 86.4,71.8 86.4,71.2 86.5,70.5 86.5,69.8 86.8,69 86.9,68.4
                                87.1,67.5 87.4,66.7 87.7,66 88,65.2 88.5,64.5 88.9,63.7 89.6,63 90.3,62.3 89.4,63.7 88.8,65.1 88.4,66.7 88.3,68.4 88.3,70
                                88.5,71.7 88.9,73.3 89.6,75.1 90.4,76.6 91.3,78.2 92.3,79.6 93.6,81 94.8,82.1 96.2,83.2 97.6,84.1 99.2,84.7 100.7,85.2
                                102.1,85.4 103.4,85.6 104.9,85.7 106.4,85.6 107.9,85.5 109.4,85.3 110.8,84.9 112.2,84.5 113.5,84 114.8,83.5 115.9,82.9
                                116.9,82.3 117.9,81.5 118.7,80.7 "/>
                        </g>
                        <g class="eye_left">
                            <circle class="eye_left_shadow cat_brown_st8" cx="159.4" cy="77.3" r="17.2"/>
                            <circle class="eyeball cat_brown_st9" cx="161.4" cy="73.3" r="17.2"/>
                            <circle class="pupil" cx="162" cy="72" r="6.5"/>
                            <polygon id="eye_left_blick" class="cat_brown_st15" points="177.3,79.9 176.7,81.1 176,82.4 175.2,83.5 174.5,84.5 173.6,85.4 172.6,86.3
                                171.6,87.1 170.5,87.8 169.5,88.5 168.4,89 167.3,89.5 166.1,89.8 165,90.1 163.8,90.2 162.6,90.3 161.6,90.4 159.8,90.3
                                158.1,90.1 156.4,89.6 154.8,89.1 153.2,88.3 152,87.5 150.6,86.5 149.3,85.3 148.2,84.2 147.2,82.9 146.4,81.4 145.7,79.9
                                145.1,78.4 144.6,76.7 144.4,75.1 144.3,73.2 144.3,72.8 144.3,72.3 144.4,71.8 144.4,71.2 144.5,70.5 144.5,69.8 144.8,69
                                144.9,68.4 145.1,67.5 145.4,66.7 145.7,66 146,65.2 146.5,64.5 146.9,63.7 147.6,63 148.3,62.3 147.4,63.7 146.8,65.1
                                146.4,66.7 146.3,68.4 146.3,70 146.5,71.7 146.9,73.3 147.6,75.1 148.4,76.6 149.3,78.2 150.3,79.6 151.6,81 152.8,82.1
                                154.2,83.2 155.6,84.1 157.2,84.7 158.7,85.2 160.1,85.4 161.4,85.6 162.9,85.7 164.4,85.6 165.9,85.5 167.4,85.3 168.8,84.9
                                170.2,84.5 171.5,84 172.8,83.5 173.9,82.9 174.9,82.3 175.9,81.5 176.7,80.7 	"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="cat_orange cats">
                <svg version="1.1" id="cat_orange" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 -15 236.7 173.9" style="transform-origin: center bottom;" xml:space="preserve">
                        <style type="text/css">
                            .cat_orange_st0{opacity:0.6;}
                            .cat_orange_st1{fill:#CC6600;}
                            .cat_orange_st2{fill:#FF9900;}
                            .cat_orange_st3{fill:#FFCC66;}
                            .cat_orange_st4{opacity:0.57;fill:#FFFFFF;}
                            .cat_orange_st5{opacity:0.42;fill:#FFCC00;}
                            .cat_orange_st6{fill:#FFFFFF;}
                            .cat_orange_st7{opacity:0.7;fill:#FFCC66;}
                            .cat_orange_st8{opacity:0.5;fill:#FF9900;}
                            .cat_orange_st9{fill:#993300;}
                            .cat_orange_st10{opacity:0.2;fill:#FF9900;enable-background:new    ;}
                        </style>
                    <g class="cat_shadow cat_orange_st0">
                        <polygon points="120.1,127.3 120.7,127.1 121.5,126.8 122.2,126.6 123.1,126.4 124,126.2 124.9,126 125.9,125.8 126.9,125.7
                128,125.5 129,125.4 130.1,125.3 131.1,125.2 132.2,125.1 133.1,125 134.1,125 135.1,125 135.5,125 136.1,125 136.5,125 136.9,125
                137.3,125.1 137.6,125.1 138,125.1 138.2,125.2 138.5,125.3 138.7,125.3 139,125.4 139.2,125.5 139.4,125.5 139.5,125.6
                139.7,125.7 139.8,125.8 140.2,126 140.7,126.2 141.2,126.5 141.9,126.7 142.6,126.9 143.5,127 144.4,127.2 145.4,127.4
                146.4,127.5 147.5,127.6 148.6,127.7 149.7,127.8 151,127.9 152.2,127.9 153.5,127.9 154.7,128 155.6,128 156.6,127.9 157.6,127.9
                158.8,127.9 159.9,127.8 161.1,127.7 162.4,127.6 163.7,127.6 165,127.5 166.4,127.4 167.8,127.3 169.2,127.1 170.6,127 172,126.9
                173.4,126.8 174.9,126.7 176.5,126.5 178.4,126.4 180.3,126.2 182.1,126.1 184,126 185.9,125.8 187.7,125.7 189.6,125.6
                191.4,125.5 193.3,125.3 195,125.2 196.8,125.2 198.4,125.1 200.1,125 201.7,125 203.1,125 204.4,125 205.8,125 207,125 208.2,125
                209.4,125 210.5,125.1 211.7,125.1 212.7,125.2 213.8,125.3 215,125.4 216,125.5 217.1,125.6 218.3,125.7 219.4,125.9 220.5,126
                221.7,126.2 222.6,126.4 223.6,126.6 224.4,126.8 225.2,127 225.9,127.2 226.6,127.4 227.2,127.6 227.7,127.9 228.2,128.1
                228.7,128.3 229.1,128.6 229.4,128.8 229.7,129 229.9,129.3 230.1,129.5 230.4,129.8 230.4,129.9 230.4,129.9 230.5,130
                230.4,130.1 230.3,130.2 230.3,130.3 230.3,130.4 230.2,130.5 230.1,130.6 230,130.7 229.8,130.8 229.6,130.9 229.4,131
                229.2,131.1 229,131.2 228.7,131.4 228,131.6 227.3,131.8 226.5,132 225.6,132.3 224.8,132.4 223.8,132.6 222.8,132.8 221.8,133
                220.8,133.1 219.7,133.3 218.7,133.4 217.7,133.5 216.7,133.6 215.6,133.6 214.6,133.7 213.7,133.7 213,133.7 212.4,133.7
                211.9,133.6 211.4,133.6 211.1,133.5 210.6,133.5 210.2,133.4 209.9,133.3 209.7,133.2 209.4,133.2 209.2,133 209.1,132.9
                209,132.8 208.9,132.7 208.9,132.6 208.9,132.5 208.9,132.2 208.7,132 208.4,131.8 208.1,131.5 207.6,131.3 207,131 206.2,130.8
                205.3,130.6 204.3,130.3 203,130.1 201.6,130 200,129.8 198.2,129.7 196.2,129.6 193.9,129.5 191.5,129.5 190.6,129.5 189.6,129.5
                188.5,129.6 187.3,129.6 186.1,129.7 184.8,129.8 183.5,129.9 182.1,130 180.7,130.1 179.2,130.2 177.8,130.4 176.3,130.5
                174.9,130.6 173.4,130.7 172,130.8 170.6,130.9 168.5,131.1 166.5,131.3 164.4,131.5 162.4,131.6 160.5,131.8 158.6,131.9
                156.6,132 154.8,132.2 152.9,132.3 151.1,132.3 149.2,132.4 147.4,132.5 145.5,132.5 143.6,132.5 141.8,132.5 140,132.5
                138.5,132.5 137,132.4 135.6,132.4 134.2,132.3 132.7,132.2 131.2,132 129.8,131.9 128.5,131.7 127.2,131.5 125.9,131.3 124.6,131
                123.4,130.7 122.2,130.4 121.2,130.1 120,129.7 119.1,129.3 118.9,129.2 118.8,129.1 118.7,129 118.6,128.9 118.5,128.8
                118.5,128.6 118.4,128.5 118.5,128.4 118.5,128.3 118.6,128.2 118.7,128 118.9,127.9 119.1,127.8 119.4,127.6 119.7,127.5 	"/>
                        <polygon id="body_5_" points="10.6,158.6 12,158.6 13.6,158.5 15.4,158.4 17,158.2 18.8,158 20.4,157.7 22,157.4 23.6,157.1
                23.6,157.1 23.6,157.1 23.6,157.1 23.6,157.1 23.6,157.1 23.5,157.1 23.5,157.1 23.5,157.1 28.3,156 33.9,154.7 39.3,153.4
                45.5,151.9 47.7,151.5 49.8,151.1 51.9,150.7 53.9,150.5 55.7,150.3 57.4,150.2 59,150 60.4,150 83.7,150 84.5,150 85.5,150.1
                86.4,150.2 87.3,150.4 88.2,150.6 88.7,151 88.9,151.4 88.7,151.9 87.6,153.2 86.3,154.6 85,155.9 84,157.1 84,157.1 84,157.1
                84,157.1 84,157.1 84,157.1 84,157.1 84,157.1 84,157.1 83.8,157.4 83.9,157.7 84.2,158 84.7,158.2 85.5,158.4 86.6,158.5
                87.8,158.6 89.2,158.6 91.2,158.6 93.4,158.4 95.6,158.2 97.8,157.9 99.9,157.6 101.8,157.2 103.6,156.7 105.1,156.2 112.2,153.5
                117.4,151.5 122.5,149.4 127.7,147.4 132.9,145.4 138.1,143.4 143.3,141.4 148.5,139.4 153.6,137.4 158.8,135.3 164,133.3
                169.2,131.3 173.6,129.6 175.7,128.7 176.9,127.8 177.2,127 176.8,126.3 175.6,125.7 173.6,125.3 171,125 167.7,124.8 157,124.8
                117,124.8 105.5,124.8 101.6,125 97.5,125.2 93.3,125.7 89.3,126.3 85.3,127 81.7,127.8 78.5,128.7 75.7,129.6 71.3,131.3
                66.5,133.2 61.8,135 57,136.9 52.3,138.7 47.5,140.6 42.8,142.4 38,144.2 33.3,146.1 28.5,147.9 23.7,149.8 19,151.6 14.2,153.5
                7.2,156.2 6.2,156.7 5.5,157.2 5.3,157.6 5.6,157.9 6.3,158.2 7.3,158.4 8.8,158.6 	"/>
                    </g>
                    <g class="cat_body">
                        <g class="tail_right cat_tail">
                            <polygon class="cat_orange_st1" points="116.8,119 116.9,118.5 117,117.9 117.1,117.4 117.3,116.9 117.5,116.4 117.7,115.9 118,115.4
                118.2,115 118.6,114.6 118.9,114.1 119.3,113.7 119.6,113.3 120,112.9 120.4,112.6 120.8,112.2 122.8,110.6 124.8,109.1
                126.7,107.8 128.7,106.6 130.6,105.5 132.5,104.5 134.4,103.6 136.2,102.8 138,102.1 139.7,101.6 141.5,101 143.2,100.7
                144.8,100.3 146.4,100.1 147.9,99.9 149.5,99.8 151.3,99.7 153.2,99.7 154.8,99.7 156.7,99.9 158.3,100.1 160,100.4 161.4,100.8
                163.1,101.1 164.6,101.6 166.2,102.1 167.7,102.6 169.3,103.2 170.8,103.8 172.5,104.4 173.9,105 175.6,105.7 176.7,106.2
                177.8,106.6 178.9,107.1 180,107.6 181.1,108 182.2,108.5 183.3,108.9 184.4,109.3 185.5,109.8 186.6,110.1 187.7,110.4
                188.7,110.7 189.8,111 190.7,111.1 191.6,111.2 192.5,111.2 194.9,111.2 197.3,111 199.5,110.6 201.6,110.1 203.5,109.5
                205.3,108.8 207.2,108.1 208.6,107.2 210.1,106.3 211.5,105.5 212.7,104.5 213.8,103.5 214.8,102.6 215.6,101.7 216.3,100.8
                217,99.9 217.3,99.5 217.7,99 218,98.6 218.5,98.1 218.8,97.7 219.3,97.3 219.8,97 220.2,96.6 220.7,96.4 221.3,96.1 221.8,95.9
                222.4,95.7 222.9,95.5 223.5,95.4 224,95.4 224.8,95.3 225.7,95.4 226.5,95.5 227.4,95.8 228.2,96.1 229,96.4 229.7,96.9
                230.4,97.4 231,98 231.6,98.6 232.1,99.3 232.6,100 232.9,100.8 233.2,101.6 233.4,102.4 233.6,103.3 233.7,104.2 233.7,104.6
                233.6,105.1 233.5,105.5 233.4,105.9 233.4,106.3 233.3,106.7 233.1,107.1 233,107.4 232.9,107.8 232.6,108.2 232.4,108.5
                232.3,108.9 232,109.3 231.8,109.6 231.5,109.9 231.3,110.2 230.5,111.1 229.7,112.1 228.8,113 227.9,113.9 227,114.8 226,115.7
                225,116.6 223.9,117.5 222.8,118.4 221.6,119.2 220.4,120.1 219.2,120.9 218,121.6 216.6,122.3 215.2,123.1 213.8,123.7
                212.2,124.5 210.7,125.1 209.2,125.7 207.8,126.2 206.4,126.7 205,127.1 203.7,127.4 202.4,127.7 201.2,128 200,128.1 198.7,128.3
                197.5,128.4 196.3,128.5 195.1,128.6 193.7,128.6 192.5,128.5 191,128.4 189.6,128.3 188.1,128.1 186.6,127.8 185.2,127.5
                183.7,127.2 182.1,126.7 180.7,126.3 179.1,125.9 177.6,125.3 176.1,124.8 174.7,124.2 173.2,123.7 171.7,123.1 170.3,122.5
                169,122 167.9,121.6 166.8,121.2 165.7,120.7 164.6,120.3 163.5,119.8 162.4,119.5 161.3,119 160.2,118.7 159.1,118.4 158.1,118.1
                157,117.8 156.2,117.6 155.1,117.3 154.3,117.3 153.4,117.1 152.5,117.1 151.2,117.2 149.9,117.3 148.6,117.5 147.1,117.7
                145.8,118.1 144.5,118.4 143.1,119 141.8,119.5 140.4,120.1 139.1,120.7 137.8,121.4 136.6,122.1 135.4,122.9 134.3,123.8
                133.3,124.7 132.2,125.6 131.9,125.9 131.6,126.2 131.3,126.5 130.9,126.7 130.6,127 130.1,127.2 129.7,127.5 129.3,127.7
                128.9,127.9 128.5,128.1 128.1,128.2 127.6,128.3 127.1,128.4 126.7,128.5 126.1,128.5 125.7,128.5 124.8,128.5 123.9,128.3
                123.1,128.1 122.3,127.8 121.5,127.5 120.7,127 120.1,126.5 119.4,125.9 118.8,125.3 118.3,124.6 117.9,123.9 117.5,123.1
                117.2,122.3 117,121.4 116.8,120.6 116.8,119.6 	"/>
                            <polygon class="cat_orange_st2" points="178.3,106.8 179.3,107.2 180.3,107.7 181.3,108.1 182.4,108.5 183.3,108.9 184.3,109.3 185.4,109.6
                186.4,110 187.4,110.3 188.3,110.5 189.2,110.8 190.1,111 191,111.1 191.8,111.2 192.5,111.2 194.9,111.2 197.3,111 199.5,110.6
                201.6,110.1 203.5,109.5 205.3,108.8 207.2,108.1 208.6,107.2 210.1,106.3 211.5,105.5 212.7,104.5 213.8,103.5 214.8,102.6
                215.6,101.7 216.3,100.8 217,99.9 217.3,99.5 217.7,99 218,98.6 218.5,98.1 218.8,97.7 219.3,97.3 219.8,97 220.2,96.6 220.7,96.4
                221.3,96.1 221.8,95.9 222.4,95.7 222.9,95.5 223.5,95.4 224,95.4 224.8,95.3 225.7,95.4 226.5,95.5 227.4,95.8 228.2,96.1
                229,96.4 229.7,96.9 230.4,97.4 231,98 231.6,98.6 232.1,99.3 232.6,100 232.9,100.8 233.2,101.6 233.4,102.4 233.6,103.3
                233.7,104.2 233.7,104.5 233.6,104.9 233.5,105.2 233.4,105.6 233.4,106 233.2,106.4 233.1,106.8 232.9,107.2 232.8,107.7
                232.6,108 232.4,108.5 232.2,108.8 232,109.2 231.8,109.6 231.5,109.9 231.3,110.2 230.5,111.1 229.7,112.1 228.8,112.9
                227.9,113.9 227,114.8 225.9,115.7 224.9,116.5 223.8,117.4 222.7,118.3 221.6,119.1 220.4,119.9 219.1,120.7 217.8,121.5
                216.6,122.3 215.2,123 213.8,123.7 212.9,124.2 211.9,124.5 210.8,124.9 210,125.1 208.9,125.3 207.9,125.4 206.8,125.4
                205.7,125.4 204.5,125.3 203.4,125 202.2,124.8 200.9,124.5 199.5,123.9 198.2,123.4 196.7,122.8 195.2,122 193.6,121.2
                192.1,120.2 190.8,119.2 189.4,118.1 188.1,116.9 186.9,115.7 185.7,114.5 184.6,113.2 183.5,112.1 182.5,111 181.5,109.9
                180.6,109 179.6,108.1 178.8,107.4 178.1,106.8 177.2,106.4 	"/>
                            <polygon class="cat_orange_st3" points="205.6,113.6 204.5,113.7 203.4,113.7 202.3,113.6 201.2,113.5 200.1,113.4 199.1,113.2 198.1,113
                197.2,112.9 196.3,112.6 195.4,112.4 194.7,112.1 194,111.9 193.4,111.7 192.9,111.5 192.5,111.2 195,111.1 197.3,110.9
                199.5,110.5 201.7,110 203.6,109.4 205.5,108.8 207.2,108 208.8,107.2 210.3,106.3 211.6,105.4 212.8,104.5 213.8,103.5
                214.8,102.6 215.6,101.7 216.3,100.8 217,99.9 217.3,99.5 217.7,99 218,98.6 218.5,98.1 218.8,97.7 219.3,97.3 219.8,97
                220.2,96.6 220.7,96.4 221.3,96.1 221.8,95.9 222.4,95.7 222.9,95.5 223.5,95.4 224,95.4 224.8,95.3 225.7,95.4 226.5,95.5
                227.4,95.8 228.2,96.1 229,96.4 229.7,96.9 230.4,97.4 231,97.9 231.6,98.6 232.1,99.2 232.6,99.9 232.9,100.8 233.2,101.6
                233.4,102.4 233.6,103.3 233.7,104.2 233.7,104.6 233.6,104.9 233.5,105.3 233.5,105.7 233.4,106 233.3,106.4 233.2,106.8
                233.1,107.1 232.9,107.5 232.8,107.9 232.6,108.2 232.5,108.5 232.3,108.8 232.2,109 232,109.3 231.9,109.4 232.3,108.6
                232.4,107.9 232.6,107.1 232.5,106.3 232.4,105.6 232.2,104.9 231.9,104.3 231.5,103.6 231.1,103.1 230.6,102.6 230.1,102.1
                229.6,101.8 229,101.4 228.4,101.2 227.8,101 227.2,101 226.8,101 226.2,101 225.8,101.1 225.3,101.3 224.8,101.4 224.4,101.6
                224,101.8 223.7,102 223.2,102.2 222.9,102.4 222.6,102.7 222.2,103 222.1,103.2 221.8,103.4 221.5,103.5 221.4,103.8 221,104.1
                220.6,104.6 220,105.2 219.5,105.9 218.8,106.6 218.2,107.4 217.4,108.2 216.5,108.9 215.5,109.7 214.5,110.4 213.4,111.2
                212.2,111.8 211,112.3 209.6,112.9 208.1,113.2 206.6,113.5 	"/>
                            <polygon class="cat_orange_st4" points="215.9,101.5 217.9,98.6 220,96.7 222.2,95.6 224.5,95.2 226.6,95.5 228.6,96.2 230.3,97.3 231.6,98.6
                232.6,100.1 233.4,101.8 233.8,103.6 233.7,105.6 233,107.8 231.4,110.3 229,113.1 225.6,116.2 223.4,114.7 221.5,113 219.9,111.1
                218.5,109.1 217.5,107.1 216.7,105.2 216.2,103.3 215.9,101.5 	"/>
                        </g>
                        <polygon id="body_14_" class="cat_orange_st1" points="165.7,0 164.4,0.1 162.9,0.4 161.6,0.8 160.3,1.5 159.2,2.4 158.4,3.4 157.6,4.6
                156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 155.1,10.2 152.9,15.2 150.8,20
                148.4,25.6 147.4,27.4 146.3,28.9 145.1,30.2 143.8,31.1 142.4,31.8 141.1,32.4 139.9,32.8 138.6,33 115.7,33 114.8,32.8
                113.7,32.5 112.4,32.1 111.1,31.4 109.7,30.5 108.4,29.3 107.1,27.7 106.1,25.8 103.9,20.9 101.6,15.4 99.5,10.5 97.5,5.9
                97.5,5.9 97.5,5.9 97.5,5.9 97.5,5.9 97.5,5.9 97.5,5.9 97.5,5.9 97.5,5.9 96.9,4.7 96.1,3.5 95.1,2.6 94,1.7 92.8,0.9 91.4,0.4
                90,0.1 88.5,0 86.7,0.2 84.8,0.7 83.2,1.5 81.8,2.7 80.6,4 79.8,5.6 79.2,7.3 79,9.2 79,19.7 79,27.4 79,35.1 79,42.8 79,50.5
                79,58.1 79,65.8 79,73.5 79,81.2 79,88.9 79,96.6 79,104.3 79,110.9 79.3,114.5 80.4,117.9 82,120.9 84.2,123.6 86.9,125.8
                90,127.5 93.3,128.6 96.9,129.2 107.4,129.2 146.6,129.2 158,129.2 161.4,128.6 164.8,127.5 167.7,125.8 170.2,123.6 172.3,120.9
                173.9,117.9 174.8,114.5 175.2,110.9 175.2,104.3 175.2,97.2 175.2,90.2 175.2,83.1 175.2,76.1 175.2,69.1 175.2,62 175.2,55
                175.2,47.9 175.2,40.9 175.2,33.8 175.2,26.7 175.2,19.7 175.2,9.2 175,7.3 174.4,5.6 173.5,4 172.4,2.6 170.9,1.5 169.4,0.7
                167.7,0.2 	"/>
                        <polygon id="body_light_part_2_" class="cat_orange_st2" points="165.7,0 164.4,0.1 162.9,0.4 161.6,0.8 160.3,1.5 159.2,2.4 158.4,3.4
                157.6,4.6 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 156.9,5.9 155.1,10.2 152.9,15.2
                150.8,20 148.4,25.6 147.4,27.4 146.3,28.9 145.1,30.2 143.8,31.1 142.4,31.8 141.1,32.4 139.9,32.8 138.6,33 127.4,33
                127.4,129.2 146.6,129.2 158,129.2 161.4,128.6 164.8,127.5 167.7,125.8 170.2,123.6 172.3,120.9 173.9,117.9 174.8,114.5
                175.2,110.9 175.2,104.3 175.2,97.2 175.2,90.2 175.2,83.1 175.2,76.1 175.2,69.1 175.2,62 175.2,55 175.2,47.9 175.2,40.9
                175.2,33.8 175.2,26.7 175.2,19.7 175.2,9.2 175,7.3 174.4,5.6 173.5,4 172.4,2.6 170.9,1.5 169.4,0.7 167.7,0.2 	"/>
                        <polygon id="light_1_10_" class="cat_orange_st5" points="127.1,33 127.1,36.7 140.3,36.7 142.7,36.4 144.9,35.6 146.9,34.2 148.8,32.1
                150.7,29.5 152.5,26.3 160.9,6.7 161.5,5.6 162.3,4.6 163.1,3.8 163.9,3.1 165,2.7 166,2.3 167,2.1 168.1,2.1 169.3,2.2 170.4,2.6
                171.6,3.1 172.7,3.8 173.6,4.8 174.4,5.9 175.1,7.2 175.5,8.7 175.2,7.1 174.7,5.6 173.9,4.2 172.7,2.8 171.4,1.7 169.7,0.7
                168,0.2 166,0 164.5,0.1 163.1,0.5 161.8,1 160.6,1.7 159.4,2.6 158.4,3.5 157.6,4.7 157,5.9 156.7,6.5 155.9,8.3 148.4,26
                148.1,26.4 147.9,26.9 147.6,27.4 147.4,27.8 147.1,28.2 146.7,28.5 146.4,29 146.1,29.3 145.2,30.2 144.1,31 143,31.7 141.8,32.2
                140.6,32.6 139.4,32.8 138.4,32.9 137.5,33 	"/>
                        <polygon id="light_3_2_" class="cat_orange_st6" points="175.2,9.2 175.2,111 174.9,114.6 173.9,117.9 172.4,121 170.5,123.7 167.9,125.9
                165.1,127.7 162,128.8 158.4,129.2 157.9,129.2 157.4,129.2 156.9,129.2 156.3,129.2 155.8,129.2 155.4,129.2 154.8,129.2
                154.3,129.2 159,128.3 162.8,126.9 165.9,124.5 168.1,121.6 169.7,118.2 170.8,114.8 171.3,111.5 171.5,108.5 171.5,11.7
                171.5,10.5 171.5,9.4 171.5,8.4 171.4,7.5 171.2,6.6 170.8,5.7 170.3,4.8 169.5,3.9 168.6,3 167.2,2.4 165.7,2 164,1.9 162.3,2
                160.7,2.4 159.1,3.1 157.9,4.1 158.9,2.9 159.8,2 160.7,1.2 161.8,0.7 162.9,0.4 163.9,0.1 165,0 166,0 167.9,0.2 169.5,0.8
                171.1,1.6 172.5,2.7 173.6,4.1 174.4,5.6 175,7.3 	"/>
                        <polygon id="light_2_2_" class="cat_orange_st3" points="171.5,9.2 171.5,111 171.2,114.6 170.2,117.9 168.7,121 166.8,123.7 164.2,125.9
                161.4,127.7 158.3,128.8 154.7,129.2 153.5,129.2 152.3,129.2 151,129.2 149.9,129.2 148.6,129.2 147.4,129.2 146,129.2
                144.9,129.2 152.1,128.5 157.8,126.8 161.8,124.3 164.6,121.2 166.4,117.9 167.3,114.4 167.7,111.2 167.8,108.5 167.8,11.7
                167.8,10.3 167.8,8.7 167.7,7.1 167.3,5.6 166.8,4.2 166.1,3 164.9,2.3 163.3,2 164.9,1.8 166.4,2 167.9,2.5 169.1,3.3 170.1,4.4
                170.8,5.8 171.3,7.4 	"/>
                        <polygon id="light_3_4_" class="cat_orange_st7" points="79,9.2 79,111 79.3,114.6 80.3,117.9 81.8,121 83.7,123.7 86.3,125.9 89.1,127.7
                92.2,128.8 95.8,129.2 96.3,129.2 96.8,129.2 97.3,129.2 97.9,129.2 98.4,129.2 98.8,129.2 99.4,129.2 99.9,129.2 95.2,128.3
                91.4,126.9 88.3,124.5 86.1,121.6 84.5,118.2 83.4,114.8 82.9,111.5 82.7,108.5 82.7,11.7 82.7,10.5 82.7,9.4 82.7,8.4 82.8,7.5
                83,6.6 83.4,5.7 83.9,4.8 84.7,3.9 85.6,3 87,2.4 88.5,2 90.2,1.9 91.9,2 93.5,2.4 95.1,3.1 96.3,4.1 95.3,2.9 94.4,2 93.5,1.2
                92.4,0.7 91.3,0.4 90.3,0.1 89.2,0 88.2,0 86.3,0.2 84.7,0.8 83.1,1.6 81.7,2.7 80.6,4.1 79.8,5.6 79.2,7.3 	"/>
                        <polygon id="light_2_8_" class="cat_orange_st8" points="82.7,9.2 82.7,111 83,114.6 84,117.9 85.5,121 87.4,123.7 90,125.9 92.8,127.7
                95.9,128.8 99.5,129.2 100.7,129.2 101.9,129.2 103.2,129.2 104.3,129.2 105.6,129.2 106.8,129.2 108.2,129.2 109.3,129.2
                102.1,128.5 96.4,126.8 92.4,124.3 89.6,121.2 87.8,117.9 86.9,114.4 86.5,111.2 86.4,108.5 86.4,11.7 86.4,10.3 86.4,8.7
                86.5,7.1 86.9,5.6 87.4,4.2 88.1,3 89.3,2.3 90.9,2 89.3,1.8 87.8,2 86.3,2.5 85.1,3.3 84.1,4.4 83.4,5.8 82.9,7.4 	"/>
                        <polygon id="shadow_on_light_part_2_" class="cat_orange_st2" points="127.4,129.2 150.8,129.2 154.3,128.8 157.6,127.7 160.5,125.9
                162.9,123.7 165,121 166.4,117.9 167.5,114.6 167.8,111 167.8,82 166.6,82.4 163.3,83.4 158.5,84.3 152.6,84.8 149.3,84.8
                146,84.3 142.7,83.7 140.6,83.2 138.9,82.5 137.2,81.7 135.6,80.8 134.1,79.7 132.8,78.5 131.4,77.2 130,75.7 128.7,74 127.4,71.9
                    "/>
                        <g id="pow_left_2_">
                            <polygon class="cat_orange_st9" points="117.9,111.7 117.7,113.5 117.4,115.3 117,117 116.4,118.6 115.7,120.1 114.8,121.6 113.8,123
                    112.6,124.2 111.4,125.3 110.1,126.4 108.6,127.2 107.1,128 105.5,128.6 103.8,129 102.1,129.3 100.2,129.4 98.5,129.3 96.7,129
                    95,128.6 93.4,128 92,127.2 90.5,126.4 89.1,125.3 87.9,124.2 86.7,123 85.7,121.6 84.8,120.1 84.1,118.6 83.5,117 83.1,115.3
                    82.8,113.5 82.6,111.7 82.6,111.6 82.7,111.5 82.7,111.4 82.7,111.3 82.8,111.2 82.8,111.1 82.8,111 82.8,111 82.8,110.9
                    82.8,110.7 82.8,110.7 82.8,110.6 82.8,110.5 82.8,110.4 82.8,110.3 82.9,110.2 83,111.9 83.4,113.4 83.9,115 84.5,116.4
                    85.3,117.8 86.2,119 87.3,120.1 88.4,121.2 89.6,122.1 90.9,122.9 92.3,123.6 93.8,124.2 95.3,124.7 96.9,125 98.6,125.3
                    100.2,125.3 102,125.3 103.6,125 105.2,124.7 106.8,124.2 108.2,123.6 109.6,122.9 111,122.1 112.1,121.2 113.3,120.1 114.3,119
                    115.2,117.8 115.9,116.4 116.6,115 117.1,113.4 117.5,111.9 117.7,110.2 117.7,110.3 117.7,110.4 117.8,110.5 117.8,110.6
                    117.8,110.7 117.8,110.7 117.8,110.9 117.8,111 117.8,111 117.8,111.1 117.8,111.2 117.8,111.3 117.8,111.4 117.9,111.5
                    117.9,111.6 		"/>
                            <polygon class="cat_orange_st9" points="96.1,116.8 96.1,116.8 96.1,116.8 96.1,116.8 96.1,116.8 95.5,116.9 95,117.2 94.7,117.7 94.6,118.2
                    94.6,126.6 94.7,127.2 95,127.7 95.5,128 96.1,128.1 96.1,128.1 96.1,128.1 96.1,128.1 96.1,128.1 96.6,128 97.1,127.7
                    97.4,127.2 97.5,126.6 97.5,118.2 97.4,117.7 97.1,117.2 96.6,116.9 		"/>
                            <polygon class="cat_orange_st9" points="104.5,116.8 104.5,116.8 104.5,116.8 104.5,116.8 104.5,116.8 103.9,116.9 103.5,117.2 103.2,117.7
                    103,118.2 103,126.6 103.2,127.2 103.5,127.7 103.9,128 104.5,128.1 104.5,128.1 104.5,128.1 104.5,128.1 104.5,128.1 105.1,128
                    105.5,127.7 105.8,127.2 106,126.6 106,118.2 105.8,117.7 105.5,117.2 105.1,116.9 		"/>
                        </g>
                        <g id="pow_right_2_">
                            <polygon class="cat_orange_st1" points="171.5,111.7 171.3,113.5 171,115.3 170.5,117 170,118.6 169.3,120.1 168.4,121.6 167.5,123
                    166.2,124.2 165,125.3 163.7,126.4 162.2,127.2 160.7,128 159.1,128.6 157.4,129 155.7,129.3 153.9,129.4 152.1,129.3 150.3,129
                    148.6,128.6 147,128 145.5,127.2 144.1,126.4 142.7,125.3 141.5,124.2 140.3,123 139.3,121.6 138.4,120.1 137.7,118.6 137.1,117
                    136.6,115.3 136.4,113.5 136.2,111.7 136.2,111.6 136.3,111.5 136.3,111.4 136.3,111.3 136.4,111.2 136.4,111.1 136.4,111
                    136.4,111 136.4,110.9 136.4,110.7 136.4,110.7 136.4,110.6 136.4,110.5 136.4,110.4 136.4,110.3 136.4,110.2 136.6,111.9
                    136.9,113.4 137.5,115 138.1,116.4 138.9,117.8 139.8,119 140.8,120.1 141.9,121.2 143.2,122.1 144.5,122.9 145.9,123.6
                    147.4,124.2 149,124.7 150.6,125 152.1,125.3 153.9,125.3 155.6,125.3 157.3,125 158.9,124.7 160.3,124.2 161.8,123.6
                    163.3,122.9 164.6,122.1 165.7,121.2 167,120.1 167.9,119 168.8,117.8 169.5,116.4 170.2,115 170.8,113.4 171.1,111.9
                    171.3,110.2 171.3,110.3 171.3,110.4 171.4,110.5 171.4,110.6 171.4,110.7 171.4,110.7 171.4,110.9 171.4,111 171.4,111
                    171.4,111.1 171.4,111.2 171.4,111.3 171.4,111.4 171.5,111.5 171.5,111.6 		"/>
                            <polygon class="cat_orange_st1" points="149.7,116.8 149.7,116.8 149.7,116.8 149.7,116.8 149.7,116.8 149.1,116.9 148.6,117.2 148.2,117.7
                    148.2,118.2 148.2,126.6 148.2,127.2 148.6,127.7 149.1,128 149.7,128.1 149.7,128.1 149.7,128.1 149.7,128.1 149.7,128.1
                    150.2,128 150.7,127.7 151,127.2 151.2,126.6 151.2,118.2 151,117.7 150.7,117.2 150.2,116.9 		"/>
                            <polygon class="cat_orange_st1" points="158.1,116.8 158.1,116.8 158.1,116.8 158.1,116.8 158.1,116.8 157.6,116.9 157,117.2 156.8,117.7
                    156.7,118.2 156.7,126.6 156.8,127.2 157,127.7 157.6,128 158.1,128.1 158.1,128.1 158.1,128.1 158.1,128.1 158.1,128.1
                    158.7,128 159.1,127.7 159.5,127.2 159.6,126.6 159.6,118.2 159.5,117.7 159.1,117.2 158.7,116.9 		"/>
                        </g>
                        <polygon id="blick_2_" class="cat_orange_st6" points="167.8,122.5 168.7,121 169.7,119.1 170.5,116.8 171.1,114.7 171.3,112.6 171.5,110.4
                171.6,108.2 171.6,106 171.7,103.5 172.1,100.8 172.8,98.2 173.4,96 174.1,94.4 174.6,93.7 175,94.2 175.2,96.1 175.2,98.8
                175.2,101.3 175.2,103.5 175.2,105.6 175.2,107.2 175.2,108.5 175.2,111 175,112.6 174.9,114.6 174.2,116.8 173.4,119.2 172,121.7
                170.2,124.1 168.4,125.9 167.3,126.4 166.8,126.4 166.6,125.9 166.7,125 167,124.1 167.3,123.4 167.5,122.9 	"/>
                        <polygon id="ear_5_" class="cat_orange_st9" points="86.3,13.6 86.4,13 86.6,12.4 87,11.9 87.4,11.4 87.9,11 88.6,10.7 89.2,10.6 89.9,10.5
                90.4,10.6 91,10.7 91.5,10.9 92,11.2 92.3,11.6 92.7,12 93,12.5 93.2,13 97.2,25.7 86.3,25.7 	"/>
                        <polygon id="ear_4_" class="cat_orange_st1" points="167.8,13.6 167.7,13 167.5,12.4 167.1,11.9 166.6,11.4 166.1,11 165.5,10.7 164.8,10.6
                164.2,10.5 163.5,10.6 163.1,10.7 162.5,10.9 162,11.2 161.7,11.6 161.3,12 160.9,12.5 160.7,13 156.8,25.7 167.8,25.7 	"/>
                        <polygon id="nose_2_" class="cat_orange_st3" points="128.7,70.6 127.8,70.6 127,70.6 126.2,70.6 125.3,70.6 123.6,70.2 122.2,69.3
                121.2,67.8 120.9,66.1 120.9,66.1 120.9,66.1 120.9,66.1 120.9,66.1 121,65.1 121.6,64.3 122.3,63.8 123.4,63.5 125.2,63.5
                127,63.5 128.9,63.5 130.7,63.5 131.7,63.8 132.5,64.3 133,65.1 133.2,66.1 133.2,66.1 133.2,66.1 133.2,66.1 133.2,66.1
                132.9,67.8 131.9,69.3 130.5,70.2 	"/>

                        <g class="eye_left">
                            <circle class="cat_orange_st1" cx="149.2" cy="63.8" r="14.2"/>
                            <circle class="eyeball cat_orange_st6" cx="150.7" cy="60.1" r="14.2"/>
                            <polygon class="cat_orange_st10" points="163.7,65.6 163.1,66.6 162.5,67.7 162,68.5 161.3,69.4 160.6,70.2
                159.8,70.9 159,71.5 158.1,72.1 157.3,72.6 156.3,73.1 155.4,73.5 154.5,73.7 153.5,74 152.5,74.1 151.5,74.2 150.7,74.3
                149.2,74.2 147.9,74 146.4,73.6 145.1,73.2 143.8,72.6 142.7,71.8 141.6,71 140.6,70.1 139.7,69.1 138.8,68 138.2,66.8 137.6,65.6
                137.1,64.4 136.7,63 136.5,61.6 136.4,60.1 136.4,59.7 136.4,59.4 136.5,58.9 136.5,58.4 136.6,57.8 136.6,57.2 136.8,56.6
                136.9,56.1 137.1,55.4 137.3,54.7 137.6,54.1 137.9,53.5 138.3,52.8 138.6,52.2 139.1,51.7 139.7,51.1 139,52.2 138.5,53.4
                138.2,54.7 138,56.1 138.1,57.5 138.3,58.8 138.6,60.2 139.1,61.6 139.8,62.9 140.5,64.2 141.4,65.4 142.4,66.5 143.5,67.4
                144.6,68.3 145.8,69.1 147.1,69.6 148.2,69.9 149.5,70.2 150.7,70.3 151.8,70.4 153,70.3 154.3,70.2 155.4,70 156.7,69.7
                157.8,69.4 158.9,69 160,68.5 160.9,68 161.7,67.5 162.5,66.9 163.1,66.3 	"/>
                            <circle class="pupil" cx="152" cy="60" r="5.4"/>
                        </g>

                        <g  class="eye_right">
                            <circle class="cat_orange_st9" cx="101.4" cy="63.8" r="14.2"/>
                            <circle class="eyeball cat_orange_st6" cx="102.9" cy="60.1" r="14.2"/>
                            <polygon class="cat_orange_st10" points="115.9,65.6 115.4,66.6 114.8,67.7 114.3,68.5 113.6,69.4 112.9,70.2
                112.1,70.9 111.3,71.5 110.4,72.1 109.5,72.6 108.6,73.1 107.7,73.5 106.7,73.7 105.7,74 104.8,74.1 103.8,74.2 103,74.3
                101.5,74.2 100.1,74 98.7,73.6 97.4,73.2 96.1,72.6 95,71.8 93.9,71 92.9,70.1 92,69.1 91.1,68 90.5,66.8 89.9,65.6 89.4,64.4
                89,63 88.8,61.6 88.7,60.1 88.7,59.7 88.7,59.4 88.8,58.9 88.8,58.4 88.9,57.8 88.9,57.2 89.1,56.6 89.2,56.1 89.4,55.4 89.6,54.7
                89.9,54.1 90.2,53.5 90.6,52.8 90.9,52.2 91.4,51.7 92,51.1 91.3,52.2 90.8,53.4 90.5,54.7 90.3,56.1 90.4,57.5 90.6,58.8
                90.9,60.2 91.4,61.6 92.1,62.9 92.8,64.2 93.7,65.4 94.7,66.5 95.8,67.4 96.9,68.3 98.1,69.1 99.4,69.6 100.5,69.9 101.7,70.2
                102.9,70.3 104.1,70.4 105.3,70.3 106.6,70.2 107.7,70 108.9,69.7 110.1,69.4 111.2,69 112.2,68.5 113.2,68 114,67.5 114.8,66.9
                115.4,66.3 	"/>
                            <circle class="pupil" cx="103" cy="60" r="5.4"/>
                        </g>
                        <polygon class="cat_orange_st5" points="127.2,70.9 127.1,71.5 126.9,72.1 126.6,72.9 126.1,73.6 125.5,74.4 124.8,75.2 124,76 123.1,76.8
                122.1,77.6 121,78.4 119.8,79.2 118.6,80 117.3,80.7 115.9,81.3 114.4,81.9 112.9,82.4 111.3,82.9 109.7,83.2 108.1,83.5
                106.4,83.6 104.7,83.7 103,83.6 101.3,83.4 99.6,83 97.8,82.5 96.1,81.9 94.4,81 92.7,80 91,78.8 89.3,77.4 87.7,75.8 86.1,74
                86.1,76.5 86.3,78.8 86.7,81 87.3,83.2 88.1,85.2 89.1,87.1 90.3,88.9 91.6,90.6 93,92.3 94.6,93.9 96.2,95.3 98,96.8 99.8,98.1
                101.7,99.4 103.6,100.7 105.5,101.9 107.5,103 109.4,104.1 111.3,105.2 113.2,106.2 115.1,107.2 116.8,108.2 118.5,109.2
                120.1,110.1 121.5,111.1 122.8,112 124,113 125,113.9 125.9,114.9 126.5,115.8 127,116.8 127.2,117.9 127.2,116.2 127.2,114.5
                127.2,112.8 127.2,111.2 127.2,109.6 127.2,107.9 127.2,106.3 127.2,104.7 127.2,103.1 127.2,101.5 127.2,100 127.2,98.4
                127.2,96.9 127.2,95.4 127.2,93.8 127.2,92.4 127.2,90.9 127.2,89.4 127.2,88 127.2,86.5 127.2,85.1 127.2,83.7 127.2,82.4
                127.2,81 127.2,79.7 127.2,78.4 127.2,77.1 127.2,75.8 127.2,74.5 127.2,73.3 127.2,72.1 127.2,70.9 	"/>
                        <polygon class="cat_orange_st3" points="127.2,70.9 127.2,71.5 127.4,72.1 127.8,72.9 128.2,73.6 128.8,74.4 129.5,75.2 130.3,76 131.2,76.8
                132.2,77.6 133.3,78.4 134.5,79.2 135.8,80 137.1,80.7 138.5,81.3 139.9,81.9 141.4,82.4 143,82.9 144.6,83.2 146.2,83.5
                147.9,83.6 149.6,83.7 151.3,83.6 153,83.4 154.8,83 156.5,82.5 158.2,81.9 159.9,81 161.7,80 163.3,78.8 165,77.4 166.6,75.8
                168.2,74 168.3,76.5 168.1,78.8 167.7,81 167,83.2 166.2,85.2 165.2,87.1 164.1,88.9 162.8,90.6 161.3,92.3 159.8,93.9 158.1,95.3
                156.3,96.8 154.5,98.1 152.7,99.4 150.7,100.7 148.8,101.9 146.8,103 144.9,104.1 143,105.2 141.1,106.2 139.3,107.2 137.5,108.2
                135.9,109.2 134.3,110.1 132.8,111.1 131.5,112 130.3,113 129.3,113.9 128.5,114.9 127.8,115.8 127.4,116.8 127.2,117.9
                127.2,116.2 127.2,114.5 127.2,112.8 127.2,111.2 127.2,109.6 127.2,107.9 127.2,106.3 127.2,104.7 127.2,103.1 127.2,101.5
                127.2,100 127.2,98.4 127.2,96.9 127.2,95.4 127.2,93.8 127.2,92.4 127.2,90.9 127.2,89.4 127.2,88 127.2,86.5 127.2,85.1
                127.2,83.7 127.2,82.4 127.2,81 127.2,79.7 127.2,78.4 127.2,77.1 127.2,75.8 127.2,74.5 127.2,73.3 127.2,72.1 127.2,70.9 	"/>
                        <polygon id="light_1_2_" class="cat_orange_st3" points="127.4,33 127.4,36.7 114.2,36.7 111.8,36.4 109.6,35.6 107.6,34.2 105.7,32.1
                103.8,29.5 102,26.3 93.6,6.7 93,5.6 92.2,4.6 91.4,3.8 90.6,3.1 89.5,2.7 88.5,2.3 87.5,2.1 86.4,2.1 85.2,2.2 84.1,2.6 82.9,3.1
                81.8,3.8 80.9,4.8 80.1,5.9 79.4,7.2 79,8.7 79.3,7.1 79.8,5.6 80.6,4.2 81.8,2.8 83.1,1.7 84.8,0.7 86.5,0.2 88.5,0 90,0.1
                91.4,0.5 92.7,1 93.9,1.7 95.1,2.6 96.1,3.5 96.9,4.7 97.5,5.9 97.8,6.5 98.6,8.3 106.1,26 106.4,26.4 106.6,26.9 106.9,27.4
                107.1,27.8 107.4,28.2 107.8,28.5 108.1,29 108.4,29.3 109.3,30.2 110.4,31 111.5,31.7 112.7,32.2 113.9,32.6 115.1,32.8
                116.1,32.9 117,33 	"/>
                    </g>
                    </svg>
            </div>

            <div class="cat_green cats">
                <svg version="1.1" id="cat_green" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 -15 376.5 271.3" style="transform-origin: center bottom;" xml:space="preserve">
                        <style type="text/css">
                            .cat_green_st0{opacity:0.6;}
                            .cat_green_st1{fill:#336600;}
                            .cat_green_st2{fill:#669900;}
                            .cat_green_st3{fill:#CCFF00;}
                            .cat_green_st4{opacity:0.4;fill:#CCFF00;}
                            .cat_green_st5{fill:#99CC00;}
                            .cat_green_st6{fill:#3D4801;}
                            .cat_green_st7{fill:#F8FFC5;enable-background:new    ;}
                            .cat_green_st8{fill:#FFFFFF;}
                            .cat_green_st9{fill:#272E02;}
                            .cat_green_st10{opacity:0.32;fill:#CCCC33;enable-background:new    ;}
                        </style>
                    <g class="cat_shadow cat_green_st0">
                        <polygon points="192.4,202.1 193.5,201.7 194.7,201.3 196,200.9 197.4,200.6 198.9,200.2 200.4,199.9 202,199.6 203.8,199.3
                205.5,199.1 207.2,198.9 208.9,198.7 210.7,198.5 212.4,198.4 214,198.3 215.6,198.2 217.2,198.2 218,198.2 218.8,198.2
                219.5,198.3 220.2,198.3 220.9,198.4 221.4,198.4 222,198.5 222.4,198.6 222.9,198.7 223.3,198.8 223.8,198.9 224,199 224.4,199.1
                224.6,199.2 224.9,199.3 225,199.5 225.7,199.9 226.5,200.3 227.3,200.6 228.4,201 229.7,201.3 231.1,201.6 232.5,201.9
                234.2,202.1 235.9,202.4 237.7,202.6 239.5,202.7 241.4,202.9 243.5,203 245.5,203.1 247.6,203.1 249.7,203.1 251.1,203.1
                252.8,203.1 254.3,203.1 256.3,203 258.2,202.9 260.2,202.7 262.3,202.6 264.5,202.5 266.6,202.3 268.9,202.1 271.2,202
                273.5,201.8 275.9,201.6 278.2,201.4 280.6,201.2 282.9,201 285.7,200.8 288.8,200.5 291.8,200.3 294.9,200.1 298,199.8
                301.2,199.6 304.2,199.4 307.3,199.2 310.2,199 313.3,198.8 316.2,198.6 319.1,198.5 321.8,198.4 324.6,198.3 327.2,198.3
                329.6,198.2 331.8,198.2 334,198.2 336,198.2 337.9,198.3 340,198.3 341.8,198.4 343.7,198.4 345.5,198.6 347.3,198.7 349.2,198.8
                350.9,199 352.8,199.2 354.7,199.4 356.5,199.7 358.4,200 360.3,200.3 361.9,200.6 363.4,200.9 364.8,201.2 366.1,201.5
                367.3,201.9 368.4,202.2 369.4,202.6 370.2,203 371.1,203.4 371.8,203.7 372.5,204.1 373,204.5 373.5,204.9 373.9,205.3
                374.2,205.7 374.6,206.1 374.7,206.3 374.7,206.4 374.8,206.5 374.7,206.7 374.6,206.9 374.5,207 374.5,207.2 374.3,207.3
                374.2,207.5 374,207.7 373.7,207.8 373.4,208 373.1,208.2 372.7,208.4 372.4,208.5 371.9,208.7 370.8,209.1 369.5,209.5
                368.2,209.9 366.8,210.2 365.4,210.5 363.8,210.9 362.1,211.2 360.5,211.4 358.8,211.7 357,211.9 355.3,212.1 353.7,212.3
                352,212.4 350.2,212.5 348.6,212.5 347,212.6 346,212.5 345,212.5 344.2,212.5 343.4,212.4 342.8,212.3 342,212.2 341.4,212.1
                340.9,212 340.5,211.8 340,211.7 339.7,211.5 339.5,211.4 339.3,211.2 339.1,211 339.1,210.8 339.1,210.6 339.1,210.2 338.8,209.8
                338.3,209.4 337.8,209 337,208.6 336,208.2 334.7,207.8 333.2,207.4 331.6,207.1 329.5,206.7 327.1,206.4 324.4,206.2 321.5,206
                318.1,205.8 314.4,205.7 310.4,205.7 308.9,205.7 307.3,205.7 305.5,205.8 303.6,205.9 301.5,206 299.4,206.2 297.2,206.3
                294.8,206.5 292.5,206.7 290.2,206.9 287.8,207.1 285.3,207.3 282.9,207.5 280.6,207.7 278.2,207.9 275.9,208.1 272.4,208.4
                269.1,208.6 265.7,208.9 262.4,209.2 259.2,209.4 256,209.7 252.8,209.9 249.8,210.1 246.6,210.2 243.6,210.4 240.5,210.5
                237.5,210.6 234.4,210.7 231.3,210.7 228.2,210.7 225.3,210.6 222.8,210.6 220.5,210.5 218,210.4 215.7,210.3 213.2,210.1
                210.9,209.9 208.5,209.6 206.3,209.3 204.1,209 202,208.6 199.9,208.2 197.9,207.7 195.9,207.2 194.2,206.6 192.4,206 190.8,205.3
                190.5,205.1 190.3,205 190.1,204.8 190,204.6 189.7,204.4 189.7,204.3 189.6,204.1 189.7,203.9 189.8,203.7 189.9,203.5
                190.1,203.2 190.5,203 190.8,202.8 191.3,202.5 191.8,202.3 	"/>
                        <polygon id="body_7_" points="11.5,253.8 13.9,253.8 16.6,253.6 19.4,253.5 22.2,253.1 25,252.8 27.7,252.3 30.5,251.8 33,251.3
                33,251.3 33,251.3 33,251.3 33,251.3 33,251.3 32.8,251.3 32.8,251.3 32.8,251.3 40.9,249.4 50.1,247.2 58.9,245.2 69.3,242.7
                72.8,242 76.4,241.3 79.8,240.8 83,240.3 86.1,240 88.9,239.8 91.5,239.6 93.8,239.5 132.3,239.5 133.6,239.6 135.2,239.7
                136.8,239.9 138.3,240.2 139.7,240.6 140.5,241.1 140.9,241.8 140.5,242.6 138.7,244.8 136.6,247.1 134.6,249.3 132.8,251.2
                132.8,251.2 132.8,251.2 132.8,251.2 132.8,251.2 132.8,251.2 132.8,251.2 132.8,251.2 132.8,251.2 132.5,251.8 132.6,252.3
                133.1,252.7 134,253.1 135.3,253.4 137.1,253.6 139.1,253.7 141.4,253.8 144.7,253.7 148.4,253.5 152,253.1 155.7,252.6
                159.1,252.1 162.3,251.4 165.2,250.6 167.7,249.8 280.9,205.8 284.3,204.3 286.3,202.8 286.8,201.5 286.1,200.4 284.1,199.4
                280.8,198.7 276.6,198.2 271.1,197.9 253.4,197.9 187.3,197.9 168.3,197.9 161.8,198.2 155,198.6 148.1,199.4 141.5,200.4
                135,201.5 129.1,202.8 123.7,204.3 119.1,205.8 5.9,249.8 4.2,250.6 3.1,251.4 2.9,252.1 3.3,252.7 4.4,253.1 6.1,253.5 8.5,253.7
                    "/>
                    </g>
                    <g class="cat_body">
                        <g class="tail_right cat_tail">
                            <polygon class="cat_green_st1" points="186.3,184.6 186.4,183.8 186.5,182.8 186.7,182 187,181.3 187.3,180.5 187.6,179.8 188,179
                188.3,178.3 188.9,177.6 189.4,176.9 190,176.3 190.5,175.6 191.1,175.2 191.7,174.6 192.4,174 195.4,171.5 198.7,169.2
                201.5,167.2 204.7,165.2 207.6,163.5 210.6,162.1 213.5,160.7 216.3,159.4 219,158.4 221.7,157.5 224.5,156.6 227,156.1
                229.6,155.6 232.1,155.3 234.3,154.9 236.9,154.8 239.7,154.6 242.6,154.6 245.2,154.7 248,154.9 250.6,155.3 253.1,155.7
                255.4,156.2 258.1,156.8 260.3,157.5 262.8,158.3 265,159.1 267.6,160 270,160.9 272.5,162 274.8,162.8 277.4,163.9 279.1,164.6
                280.8,165.3 282.5,166 284.3,166.8 285.9,167.5 287.6,168.2 289.4,168.9 291,169.5 292.7,170.2 294.5,170.7 296.2,171.2
                297.7,171.7 299.3,172.1 300.7,172.3 302.2,172.4 303.6,172.5 307.3,172.4 310.9,172.1 314.4,171.5 317.6,170.7 320.7,169.7
                323.4,168.8 326.3,167.7 328.7,166.3 330.9,164.9 333.1,163.5 334.9,162.1 336.5,160.6 338.2,159.1 339.4,157.7 340.6,156.3
                341.6,155 342.1,154.3 342.6,153.5 343.1,152.8 343.8,152.1 344.4,151.5 345.1,150.9 345.9,150.4 346.6,149.8 347.4,149.5
                348.2,149.1 349,148.8 349.9,148.4 350.8,148.2 351.7,148 352.5,148 353.7,147.8 355.1,148 356.3,148.2 357.7,148.5 359,149
                360.2,149.6 361.3,150.2 362.4,151 363.3,151.9 364.2,152.8 365,154 365.7,155.1 366.3,156.2 366.8,157.5 367.1,158.8 367.3,160.2
                367.5,161.6 367.5,162.3 367.3,162.9 367.2,163.6 367.1,164.2 367,164.9 366.9,165.5 366.7,166 366.4,166.6 366.2,167.2
                365.8,167.8 365.5,168.3 365.3,168.9 364.9,169.4 364.6,169.9 364.1,170.4 363.8,170.9 362.5,172.3 361.3,173.8 359.9,175.3
                358.5,176.6 357,178.1 355.5,179.5 354,180.9 352.3,182.1 350.5,183.5 348.8,184.9 346.8,186.2 345,187.5 343,188.5 341,189.7
                338.8,190.8 336.5,191.9 334.2,193 331.8,194.1 329.6,194.9 327.3,195.7 325.1,196.4 323,197.1 321,197.5 318.9,198 317.1,198.5
                315.2,198.7 313.2,199.1 311.3,199.2 309.5,199.3 307.6,199.4 305.5,199.4 303.6,199.3 301.3,199.2 299,198.9 296.8,198.6
                294.5,198.2 292.2,197.8 290,197.2 287.5,196.5 285.2,195.8 282.8,195.1 280.5,194.4 278.1,193.6 275.9,192.7 273.6,191.9
                271.4,190.9 269.1,190 267.1,189.2 265.4,188.5 263.8,187.9 262,187.2 260.3,186.5 258.6,185.8 256.9,185.3 255.2,184.6 253.5,184
                251.8,183.5 250.3,183.1 248.6,182.6 247.2,182.3 245.6,181.9 244.3,181.9 242.9,181.7 241.5,181.7 239.4,181.8 237.6,181.9
                235.5,182.1 233.2,182.5 231.2,183.1 229.1,183.6 226.9,184.5 225,185.3 222.8,186.2 220.7,187.2 218.8,188.3 216.8,189.3
                215,190.6 213.4,192 211.7,193.4 210.1,194.8 209.7,195.2 209.1,195.7 208.6,196.2 208,196.5 207.5,197 206.9,197.3 206.3,197.7
                205.6,198 205,198.4 204.3,198.6 203.6,198.8 202.9,198.9 202.1,199.2 201.4,199.3 200.6,199.3 200,199.3 198.5,199.3 197.1,199.1
                195.9,198.7 194.6,198.2 193.4,197.7 192.3,197 191.2,196.2 190.2,195.2 189.3,194.3 188.5,193.3 187.8,192.1 187.3,190.8
                186.8,189.6 186.5,188.3 186.3,187 186.3,185.5 	"/>
                            <polygon class="cat_green_st2" points="281.6,165.7 283.1,166.3 284.6,167 286.2,167.7 287.9,168.2 289.4,168.9 290.9,169.4 292.5,170
                294.1,170.6 295.6,171 297.1,171.4 298.4,171.8 299.8,172.1 301.2,172.3 302.5,172.4 303.6,172.5 307.3,172.4 310.9,172.1
                314.4,171.5 317.6,170.7 320.7,169.7 323.4,168.8 326.3,167.7 328.7,166.3 330.9,164.9 333.1,163.5 334.9,162.1 336.5,160.6
                338.2,159.1 339.4,157.7 340.6,156.3 341.6,155 342.1,154.3 342.6,153.5 343.1,152.8 343.8,152.1 344.4,151.5 345.1,150.9
                345.9,150.4 346.6,149.8 347.4,149.5 348.2,149.1 349,148.8 349.9,148.4 350.8,148.2 351.7,148 352.5,148 353.7,147.8 355.1,148
                356.3,148.2 357.7,148.5 359,149 360.2,149.6 361.3,150.2 362.4,151 363.3,151.9 364.2,152.8 365,154 365.7,155.1 366.3,156.2
                366.8,157.5 367.1,158.8 367.3,160.2 367.5,161.6 367.5,162.1 367.3,162.6 367.2,163.1 367.1,163.7 367,164.3 366.8,165
                366.5,165.7 366.3,166.3 366.1,167 365.8,167.5 365.5,168.2 365.1,168.8 364.8,169.3 364.5,169.9 364.1,170.4 363.8,170.9
                362.5,172.3 361.3,173.8 359.9,175.2 358.5,176.6 357,178 355.3,179.4 353.9,180.7 352.2,182 350.4,183.4 348.8,184.7 346.8,186
                344.9,187.2 342.8,188.4 340.9,189.6 338.8,190.7 336.5,191.9 335.3,192.6 333.7,193.1 332,193.7 330.6,194.1 329.1,194.3
                327.5,194.5 325.8,194.5 324,194.5 322.3,194.3 320.4,194 318.6,193.6 316.6,193 314.4,192.2 312.4,191.4 310.2,190.4 307.8,189.2
                305.2,188 303,186.4 301,184.8 298.8,183.1 296.8,181.3 294.9,179.4 293,177.5 291.2,175.5 289.6,173.8 288.1,172.1 286.5,170.4
                285.1,168.9 283.7,167.7 282.4,166.5 281.3,165.7 279.9,165 	"/>
                            <polygon class="cat_green_st3" points="323.8,176.1 322.2,176.2 320.4,176.2 318.7,176.1 317.1,176 315.4,175.8 313.8,175.5 312.3,175.3
                310.8,175.1 309.5,174.7 308.1,174.4 307,173.9 305.8,173.6 305,173.2 304.1,172.9 303.6,172.5 307.4,172.3 310.9,171.9
                314.4,171.4 317.8,170.6 320.9,169.6 323.7,168.7 326.3,167.5 328.9,166.3 331.1,164.9 333.2,163.4 335.1,162.1 336.5,160.6
                338.2,159.1 339.4,157.7 340.6,156.3 341.6,155 342.1,154.3 342.6,153.5 343.1,152.8 343.8,152.1 344.4,151.5 345.1,150.9
                345.9,150.4 346.6,149.8 347.4,149.5 348.2,149.1 349,148.8 349.9,148.4 350.8,148.2 351.7,148 352.5,148 353.7,147.8 355.1,148
                356.3,148.2 357.7,148.5 359,149 360.2,149.6 361.3,150.2 362.4,151 363.3,151.8 364.2,152.8 365,153.9 365.7,155 366.3,156.2
                366.8,157.5 367.1,158.8 367.3,160.2 367.5,161.6 367.5,162.2 367.3,162.7 367.2,163.3 367.2,163.8 367,164.4 366.9,165
                366.8,165.6 366.5,166.1 366.3,166.7 366.1,167.3 365.8,167.8 365.6,168.2 365.4,168.7 365.1,169 364.9,169.4 364.7,169.6
                365.3,168.5 365.5,167.3 365.7,166 365.6,164.9 365.5,163.7 365.1,162.7 364.7,161.7 364.1,160.7 363.4,159.9 362.6,159.1
                361.8,158.4 361.1,157.8 360.2,157.2 359.2,156.9 358.3,156.6 357.4,156.5 356.7,156.5 355.9,156.6 355.3,156.8 354.5,157
                353.7,157.2 353.1,157.5 352.5,157.8 351.9,158.2 351.2,158.5 350.8,158.8 350.2,159.3 349.6,159.7 349.4,160 348.9,160.4
                348.6,160.6 348.5,160.9 347.9,161.5 347.2,162.3 346.3,163.1 345.5,164.2 344.4,165.3 343.4,166.5 342.2,167.8 340.8,168.9
                339.3,170.1 337.7,171.2 336,172.4 334.2,173.3 332.2,174.3 330,175.1 327.8,175.5 325.4,176 	"/>
                            <polygon class="cat_green_st4" points="336.7,162.6 335.9,164.5 336.1,168 336.7,172.4 337.3,177.5 337.4,182.6 336.4,187.3 334,191.1
                329.7,193.6 337.9,189.7 345.8,185.6 353,181.2 359.2,176.5 363.8,171.6 366.6,166.3 366.9,160.7 364.5,154.8 361,151 357,149.8
                352.9,150.5 348.8,152.5 344.9,155.3 341.4,158.3 338.6,160.9 336.7,162.6 	"/>
                            <polygon class="cat_green_st4" points="328.6,168.5 329.5,170.3 329.9,173.2 330,176.9 329.7,181 329.1,185.3 328.2,189.3 327,192.7
                325.5,195.1 323.8,195.8 322,196.4 320.1,196.8 318.3,197.2 316.6,197.4 315.2,197.6 314.3,197.7 314,197.7 314.7,197.2
                316.5,195.6 319,193.2 321.8,189.8 324.6,185.6 326.9,180.6 328.4,174.9 328.6,168.5 	"/>
                        </g>
                        <polygon id="body_13_" class="cat_green_st2" points="262,0 259.9,0.1 257.7,0.6 255.7,1.3 253.7,2.3 252.1,3.7 250.7,5.2 249.4,7.2 248.4,9
                248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 245.6,15.8 242.1,23.5 239,30.9 235.1,39.7 233.5,42.4
                231.9,44.8 230.1,46.7 228,48.2 226,49.4 223.9,50.2 221.9,50.9 220,51.2 184.5,51.2 183.1,50.9 181.3,50.4 179.4,49.7 177.3,48.7
                175.1,47.3 173.2,45.4 171.2,43 169.6,40 166.2,32.4 162.5,23.9 159.4,16.2 156.2,9.2 156.2,9.2 156.2,9.2 156.2,9.2 156.2,9.2
                156.2,9.2 156.2,9.2 156.2,9.2 156.2,9.2 155.3,7.3 154,5.4 152.5,3.9 150.8,2.5 148.9,1.4 146.7,0.7 144.5,0.2 142.3,0 139.4,0.3
                136.6,1.2 134.1,2.3 131.9,4.2 130.1,6.3 128.8,8.6 127.9,11.4 127.5,14.3 127.5,30.5 127.5,42.4 127.5,54.3 127.5,66.3
                127.5,78.3 127.5,90.1 127.5,102.1 127.5,114 127.5,125.9 127.5,137.9 127.5,149.8 127.5,161.9 127.5,171.9 128.1,177.5
                129.6,182.7 132.3,187.5 135.7,191.6 139.8,195 144.5,197.7 149.6,199.5 155.3,200.3 171.5,200.3 232.5,200.3 250.1,200.3
                255.4,199.5 260.5,197.8 265,195 269,191.6 272.2,187.5 274.7,182.7 276.2,177.5 276.7,171.9 276.7,161.9 276.7,150.7 276.7,139.8
                276.7,129 276.7,117.9 276.7,107.1 276.7,96.2 276.7,85.2 276.7,74.3 276.7,63.4 276.7,52.4 276.7,41.4 276.7,30.5 276.7,14.3
                276.4,11.4 275.6,8.6 274.1,6.3 272.3,4.1 270.1,2.3 267.8,1.2 265,0.3 	"/>
                        <polygon id="body_light_part_13_" class="cat_green_st5" points="262,0 259.9,0.1 257.7,0.6 255.7,1.3 253.7,2.3 252.1,3.7 250.7,5.2
                249.4,7.2 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 248.4,9 245.6,15.8 242.1,23.5 239,30.9 235.1,39.7
                233.5,42.4 231.9,44.8 230.1,46.7 228,48.2 226,49.4 223.9,50.2 221.9,50.9 220,51.2 202.6,51.2 202.6,200.3 232.5,200.3
                250.1,200.3 255.4,199.5 260.5,197.8 265,195 269,191.6 272.2,187.5 274.7,182.7 276.2,177.5 276.7,171.9 276.7,161.9 276.7,150.7
                276.7,139.8 276.7,129 276.7,117.9 276.7,107.1 276.7,96.2 276.7,85.2 276.7,74.3 276.7,63.4 276.7,52.4 276.7,41.4 276.7,30.5
                276.7,14.3 276.4,11.4 275.6,8.6 274.1,6.3 272.3,4.1 270.1,2.3 267.8,1.2 265,0.3 	"/>
                        <polygon id="shadow_13_" class="cat_green_st6" points="202.6,200.3 166.4,200.3 161,199.7 155.9,198 151.4,195.2 147.6,191.9 144.3,187.7
                142.1,182.8 140.6,177.6 140,172.1 140,127.1 141.9,127.8 147,129.3 154.5,130.7 163.7,131.5 168.6,131.4 173.9,130.7 179.1,129.9
                182.1,129.1 184.7,127.9 187.5,126.6 189.8,125.2 192.2,123.6 194.1,121.8 196.3,119.7 198.7,117.4 200.6,114.8 202.6,111.5 	"/>
                        <polygon id="light_3_13_" class="cat_green_st7" points="276.7,14.3 276.7,172.1 276.3,177.6 274.7,182.8 272.3,187.7 269.4,191.9
                265.4,195.2 261.1,198 256.2,199.7 250.7,200.3 250,200.3 249.2,200.3 248.4,200.3 247.4,200.3 246.6,200.3 245.9,200.3
                245.2,200.3 244.4,200.3 251.7,199.1 257.5,196.7 262.3,193.1 265.6,188.5 268.3,183.3 269.9,178 270.7,172.9 271.1,168.2
                271.1,18.2 271.1,16.2 271.1,14.5 271.1,13.1 270.8,11.6 270.6,10.2 270,8.8 269.1,7.5 267.9,6 266.4,4.6 264.3,3.7 262,3.1
                259.4,2.9 256.8,3.1 254.3,3.7 251.8,4.9 250,6.4 251.5,4.5 252.9,3 254.3,1.9 256,1.2 257.7,0.6 259.1,0.2 260.9,0 262.5,0
                265.4,0.3 267.9,1.3 270.5,2.4 272.5,4.2 274.2,6.4 275.6,8.7 276.4,11.4 	"/>
                        <polygon id="light_2_13_" class="cat_green_st3" points="271.1,14.3 271.1,172.1 270.6,177.6 269,182.8 266.7,187.7 263.8,191.9 259.7,195.2
                255.4,198 250.6,199.7 245,200.3 243.1,200.3 241.2,200.3 239.2,200.3 237.5,200.3 235.5,200.3 233.5,200.3 231.6,200.3
                229.7,200.3 240.9,199.3 249.7,196.6 256,192.8 260.3,188 263.1,182.7 264.6,177.4 265,172.4 265.3,168.2 265.3,18.2 265.3,16
                265.3,13.6 265,11 264.6,8.6 263.8,6.5 262.6,4.6 260.8,3.5 258.3,3 260.8,2.8 263.2,3 265.4,3.8 267.2,5.1 268.9,6.8 270,8.9
                270.7,11.5 	"/>
                        <polygon id="light_3_3_" class="cat_green_st4" points="127.2,14.3 127.2,172.1 127.7,177.6 129.3,182.8 131.6,187.7 134.5,191.9
                138.6,195.2 142.8,198 147.7,199.7 153.3,200.3 154,200.3 154.8,200.3 155.6,200.3 156.5,200.3 157.3,200.3 158,200.3 158.7,200.3
                159.5,200.3 152.2,199.1 146.4,196.7 141.7,193.1 138.3,188.5 135.7,183.3 134,178 133.2,172.9 132.9,168.2 132.9,18.2 132.9,16.2
                132.9,14.5 132.9,13.1 133.1,11.6 133.3,10.2 133.9,8.8 134.8,7.5 136,6 137.5,4.6 139.6,3.7 141.9,3.1 144.6,2.9 147.1,3.1
                149.7,3.7 152.1,4.9 154,6.4 152.5,4.5 151.1,3 149.7,1.9 147.9,1.2 146.2,0.6 144.8,0.2 143.1,0 141.4,0 138.6,0.3 136,1.3
                133.5,2.4 131.5,4.2 129.7,6.4 128.4,8.7 127.5,11.4 	"/>
                        <polygon id="shadow_on_light_part_13_" class="cat_green_st2" points="202.6,200.3 239,200.3 244.4,199.7 249.4,198 253.9,195.2 257.7,191.9
                260.9,187.7 263.2,182.8 264.8,177.6 265.3,172.1 265.3,127.1 263.4,127.8 258.3,129.3 250.9,130.7 241.8,131.5 236.7,131.4
                231.4,130.7 226.3,129.9 223.1,129.1 220.4,127.9 217.8,126.6 215.3,125.2 213.1,123.6 211.1,121.8 208.8,119.7 206.6,117.4
                204.7,114.8 202.6,111.5 	"/>
                        <g id="pow_right_13_">
                            <polygon class="cat_green_st1" points="271.1,173.2 270.7,176 270.3,178.8 269.6,181.4 268.8,183.9 267.6,186.3 266.2,188.5 264.8,190.7
                    262.8,192.7 260.9,194.4 258.8,195.9 256.6,197.3 254.3,198.5 251.8,199.4 249.2,200.1 246.5,200.6 243.7,200.7 240.9,200.6
                    238.2,200.1 235.5,199.4 232.9,198.5 230.7,197.3 228.4,195.9 226.3,194.4 224.5,192.7 222.6,190.7 221,188.5 219.7,186.3
                    218.6,183.9 217.7,181.4 217,178.8 216.5,176 216.3,173.2 216.3,173.1 216.4,173 216.4,172.8 216.4,172.6 216.5,172.5
                    216.5,172.3 216.5,172.2 216.5,172.1 216.5,171.9 216.5,171.7 216.5,171.6 216.5,171.5 216.5,171.4 216.5,171.1 216.5,171
                    216.6,170.9 216.8,173.6 217.4,175.9 218.2,178.3 219.3,180.5 220.4,182.6 221.8,184.5 223.4,186.3 225.2,187.9 227,189.3
                    229.1,190.6 231.3,191.8 233.5,192.7 236.1,193.4 238.6,194 240.9,194.3 243.7,194.4 246.3,194.3 248.9,194 251.5,193.4
                    253.7,192.7 256,191.8 258.3,190.6 260.3,189.3 262,187.9 264,186.3 265.4,184.5 266.8,182.6 267.9,180.5 269,178.3 269.9,175.9
                    270.5,173.6 270.7,170.9 270.7,171 270.7,171.1 270.8,171.4 270.8,171.5 270.8,171.6 270.8,171.7 270.8,171.9 270.8,172.1
                    270.8,172.2 270.8,172.3 270.8,172.5 270.8,172.6 270.8,172.8 271.1,173 271.1,173.1 		"/>
                            <polygon class="cat_green_st1" points="237.2,181.1 237.2,181.1 237.2,181.1 237.2,181.1 237.2,181.1 236.3,181.3 235.5,181.8 234.9,182.5
                    234.9,183.3 234.9,196.3 234.9,197.2 235.5,198 236.3,198.5 237.2,198.6 237.2,198.6 237.2,198.6 237.2,198.6 237.2,198.6
                    238,198.5 238.7,198 239.2,197.2 239.4,196.3 239.4,183.3 239.2,182.5 238.7,181.8 238,181.3 		"/>
                            <polygon class="cat_green_st1" points="250.3,181.1 250.3,181.1 250.3,181.1 250.3,181.1 250.3,181.1 249.4,181.3 248.6,181.8 248.2,182.5
                    248,183.3 248,196.3 248.2,197.2 248.6,198 249.4,198.5 250.3,198.6 250.3,198.6 250.3,198.6 250.3,198.6 250.3,198.6
                    251.3,198.5 251.8,198 252.3,197.2 252.5,196.3 252.5,183.3 252.3,182.5 251.8,181.8 251.3,181.3 		"/>
                        </g>
                        <polygon id="blick_11_" class="cat_green_st8" points="265.3,189.9 266.7,187.7 268.2,184.7 269.6,181.1 270.5,177.9 270.7,174.6
                271.1,171.2 271.3,167.9 271.3,164.3 271.4,160.5 271.9,156.2 272.9,152.2 274,148.9 275,146.5 275.8,145.3 276.4,146.1
                276.7,149.1 276.7,153.2 276.7,157 276.7,160.6 276.7,163.7 276.7,166.3 276.7,168.3 276.9,172.1 276.5,174.6 276.3,177.6
                275.2,181.2 274,184.9 271.8,188.6 269,192.4 266.2,195.1 264.6,196 263.8,196 263.4,195.1 263.7,193.8 264,192.4 264.6,191.3
                264.9,190.6 	"/>
                        <polygon id="ear_27_" class="cat_green_st9" points="138.9,21.1 139,20.2 139.3,19.2 139.9,18.4 140.6,17.6 141.4,17 142.5,16.6 143.4,16.3
                144.4,16.2 145.2,16.3 146.2,16.6 147,16.9 147.7,17.4 148.3,18 148.8,18.7 149.2,19.5 149.5,20.2 155.7,39.9 138.9,39.9 	"/>
                        <polygon id="ear_26_" class="cat_green_st1" points="265.3,21.1 265,20.2 264.8,19.2 264.2,18.4 263.4,17.6 262.6,17 261.7,16.6 260.5,16.3
                259.6,16.2 258.6,16.3 258,16.6 257.2,16.9 256.4,17.4 255.8,18 255.2,18.7 254.6,19.5 254.3,20.2 248.2,39.9 265.3,39.9 	"/>
                        <polygon id="light_1_13_" class="cat_green_st5" points="202.6,51.2 202.6,56.9 182.1,56.9 178.4,56.4 175,55.2 171.9,52.9 168.9,49.8
                166.1,45.8 163.2,40.7 150.1,10.4 149.2,8.7 148.1,7.2 146.9,5.9 145.5,4.9 143.8,4.2 142.3,3.6 140.7,3.2 139,3.2 137.1,3.4
                135.5,3.9 133.7,4.8 131.9,5.9 130.4,7.4 129.3,9 128.2,11.1 127.6,13.4 128,11 128.8,8.6 129.9,6.5 131.8,4.3 134,2.5 136.4,1.2
                139.2,0.3 142.3,0 144.5,0.2 146.7,0.8 148.8,1.5 150.7,2.5 152.5,3.9 154,5.4 155.3,7.3 156.2,9.2 156.7,10.1 157.9,12.9
                169.6,40.2 170,40.9 170.4,41.7 170.8,42.4 171.2,43.1 171.7,43.7 172.2,44.3 172.7,45 173.2,45.4 174.7,46.8 176.2,48.1 178,49.1
                179.9,49.9 181.6,50.5 183.5,50.9 185.1,51.1 186.5,51.2 	"/>
                        <polygon id="light_1_3_" class="cat_green_st3" points="182.1,56.9 178.4,56.4 175,55.2 171.9,52.9 168.9,49.8 166.1,45.8 163.2,40.7
                150.1,10.4 149.2,8.7 148.1,7.2 146.9,5.9 145.5,4.9 143.8,4.2 142.3,3.6 140.7,3.2 139,3.2 137.1,3.4 135.5,3.9 133.7,4.8
                131.9,5.9 130.4,7.4 129.3,9 128.2,11.1 127.6,13.4 128,11 128.8,8.6 129.9,6.5 131.8,4.3 134,2.5 136.4,1.2 139.2,0.3 142.3,0
                144.5,0.2 146.7,0.8 148.8,1.5 150.7,2.5 152.5,3.9 154,5.4 155.3,7.3 156.2,9.2 156.7,10.1 157.9,12.9 169.6,40.2 170,40.9
                170.4,41.7 170.8,42.4 171.2,43.1 171.7,43.7 172.2,44.3 172.7,45 173.2,45.4 174.7,46.8 176.2,48.1 178,49.1 179.9,49.9
                181.6,50.5 183.5,50.9 185.1,51.1 186.5,51.2 192.6,52.8 188.4,55.7 	"/>
                        <polygon class="cat_green_st4" points="159.9,60.4 173.2,61.3 187.1,63.2 200.9,65.8 214.3,69.1 226.5,72.8 237.1,76.8 245.7,80.9
                251.5,85.1 255.6,89.4 259,94.2 261.7,99.2 263.7,104.5 264.9,109.9 265.1,115.2 264.3,120.5 262.5,125.6 259.2,130.5 254.8,134.4
                249.3,137.4 242.8,139.4 235.4,140.7 227.1,141.1 218.1,140.9 208.5,139.9 198.8,138.7 189.6,137.3 181,135.7 173.1,133.7
                165.9,131.1 159.5,127.8 154,123.8 149.5,118.7 145.3,111.5 141.7,102.7 139.3,92.9 138.4,83.1 139.5,74.1 143.2,66.8 149.8,61.9
                159.9,60.4 	"/>
                        <ellipse class="cat_green_st3" cx="245.9" cy="59.7" rx="7.2" ry="7"/>
                        <polygon id="nose_13_" class="cat_green_st7" points="204.7,109.5 203.3,109.5 202,109.5 200.7,109.5 199.5,109.5 196.7,108.9 194.5,107.4
                193,105.1 192.5,102.5 192.5,102.5 192.5,102.5 192.5,102.5 192.5,102.5 192.7,100.9 193.7,99.6 194.7,98.8 196.3,98.5 199.2,98.5
                202,98.5 204.9,98.5 207.7,98.5 209.3,98.8 210.5,99.6 211.4,100.9 211.6,102.5 211.6,102.5 211.6,102.5 211.6,102.5 211.6,102.5
                211.2,105.1 209.5,107.4 207.3,108.9 	"/>
                        <g id="pow_left_13_">
                            <polygon class="cat_green_st9" points="187.8,173.2 187.6,176 187.2,178.8 186.5,181.4 185.6,183.9 184.4,186.3 183,188.5 181.5,190.7
                    179.8,192.7 177.8,194.4 175.7,195.9 173.5,197.3 171.1,198.5 168.5,199.4 166,200.1 163.3,200.6 160.5,200.7 157.8,200.6
                    155.1,200.1 152.4,199.4 149.9,198.5 147.7,197.3 145.4,195.9 143.2,194.4 141.4,192.7 139.6,190.7 137.9,188.5 136.6,186.3
                    135.5,183.9 134.6,181.4 133.9,178.8 133.4,176 133.2,173.2 133.2,173.1 133.3,173 133.3,172.8 133.3,172.6 133.4,172.5
                    133.4,172.3 133.4,172.2 133.4,172.1 133.4,171.9 133.4,171.7 133.4,171.6 133.4,171.5 133.4,171.4 133.4,171.1 133.4,171
                    133.5,170.9 133.8,173.6 134.3,175.9 135.2,178.3 136.1,180.5 137.4,182.6 138.8,184.5 140.4,186.3 142.1,187.9 144,189.3
                    146.1,190.6 148.3,191.8 150.5,192.7 152.9,193.4 155.4,194 157.9,194.3 160.5,194.4 163.2,194.3 165.7,194 168.3,193.4
                    170.6,192.7 172.9,191.8 175,190.6 177.1,189.3 179,187.9 180.8,186.3 182.3,184.5 183.7,182.6 184.9,180.5 185.9,178.3
                    186.7,175.9 187.3,173.6 187.6,170.9 187.6,171 187.6,171.1 187.6,171.4 187.6,171.5 187.6,171.6 187.6,171.7 187.6,171.9
                    187.6,172.1 187.6,172.2 187.6,172.3 187.6,172.5 187.6,172.6 187.6,172.8 187.8,173 187.8,173.1 		"/>
                            <polygon class="cat_green_st9" points="154,181.1 154,181.1 154,181.1 154,181.1 154,181.1 153.1,181.3 152.4,181.8 151.8,182.5
                    151.7,183.3 151.7,196.3 151.8,197.2 152.4,198 153.1,198.5 154,198.6 154,198.6 154,198.6 154,198.6 154,198.6 154.9,198.5
                    155.6,198 156,197.2 156.2,196.3 156.2,183.3 156,182.5 155.6,181.8 154.9,181.3 		"/>
                            <polygon class="cat_green_st9" points="167.1,181.1 167.1,181.1 167.1,181.1 167.1,181.1 167.1,181.1 166.2,181.3 165.5,181.8 165.1,182.5
                    164.8,183.3 164.8,196.3 165.1,197.2 165.5,198 166.2,198.5 167.1,198.6 167.1,198.6 167.1,198.6 167.1,198.6 167.1,198.6
                    168.1,198.5 168.6,198 169.1,197.2 169.3,196.3 169.3,183.3 169.1,182.5 168.6,181.8 168.1,181.3 		"/>
                        </g>
                        <g class="eye_right">
                            <circle class="cat_green_st9" cx="162.3" cy="98.8" r="22"/>
                            <circle class="eyeball cat_green_st8" cx="164.6" cy="93.2" r="22"/>
                            <polygon class="cat_green_st10" points="184.9,101.7 184.1,103.4 183.1,104.9 182.2,106.2 181.3,107.6 180.1,108.8
                178.8,110 177.6,111 176.2,111.8 174.8,112.6 173.5,113.3 172,113.9 170.5,114.4 169,114.7 167.6,114.9 166.1,115.1 164.7,115.2
                162.4,115.1 160.3,114.7 158.1,114.1 156,113.4 154.2,112.5 152.4,111.3 150.7,110.2 149.1,108.7 147.7,107.2 146.4,105.4
                145.4,103.7 144.4,101.7 143.6,99.8 143,97.7 142.7,95.5 142.6,93.2 142.6,92.6 142.6,92 142.7,91.4 142.7,90.6 142.8,89.7
                142.9,88.8 143.2,87.8 143.4,86.9 143.6,85.9 144,84.9 144.4,83.9 144.9,83 145.5,81.9 146.1,81 146.9,80.1 147.8,79.1 146.6,81
                145.8,82.8 145.4,84.9 145.1,86.9 145.2,89.1 145.5,91.3 146.1,93.3 146.9,95.5 147.9,97.6 148.9,99.5 150.3,101.4 152,103.1
                153.6,104.5 155.4,105.9 157.2,107.1 159.1,107.9 161,108.4 162.7,108.8 164.6,109 166.4,109.1 168.3,109 170.3,108.9 172.1,108.6
                174,108.1 175.7,107.6 177.5,106.9 179.1,106.2 180.6,105.4 181.7,104.6 183,103.8 184.1,102.8 	"/>
                            <circle class="pupil" cx="165" cy="92" r="8.3"/>
                        </g>
                        <g class="eye_left">
                            <circle class="cat_green_st9" cx="236.4" cy="98.8" r="22"/>
                            <circle class="eyeball cat_green_st8" cx="238.7" cy="93.2" r="22"/>
                            <polygon class="cat_green_st10" points="258.8,101.7 258.1,103.4 257.2,104.9 256.4,106.2 255.2,107.6 254,108.8
                252.9,110 251.7,111 250.3,111.8 248.9,112.6 247.4,113.3 246,113.9 244.7,114.4 243.1,114.7 241.5,114.9 240,115.1 238.7,115.2
                236.4,115.1 234.3,114.7 232.1,114.1 230.1,113.4 228.1,112.5 226.3,111.3 224.7,110.2 223.1,108.7 221.6,107.2 220.3,105.4
                219.4,103.7 218.5,101.7 217.7,99.8 217.1,97.7 216.7,95.5 216.6,93.2 216.6,92.6 216.6,92 216.7,91.4 216.7,90.6 216.8,89.7
                217,88.8 217.2,87.8 217.4,86.9 217.7,85.9 218,84.9 218.5,83.9 218.9,83 219.5,81.9 220,81 220.8,80.1 221.7,79.1 220.6,81
                219.9,82.8 219.4,84.9 219.2,86.9 219.3,89.1 219.5,91.3 220,93.3 220.8,95.5 221.8,97.6 223,99.5 224.4,101.4 226,103.1
                227.5,104.5 229.4,105.9 231.2,107.1 233.2,107.9 234.9,108.4 236.9,108.8 238.7,109 240.4,109.1 242.3,109 244.4,108.9 246,108.6
                248,108.1 249.7,107.6 251.5,106.9 253.1,106.2 254.6,105.4 255.8,104.6 257.2,103.8 258.1,102.8 	"/>
                            <circle class="pupil" cx="240" cy="92" r="8.3"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="cat_violet cats">
                <svg version="1.1" id="cat_violet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="35 -35.2 242.1 246.3" style="transform-origin: center bottom;" xml:space="preserve">
                        <style type="text/css">
                            .cat_violet_st0{opacity:0.6;}
                            .cat_violet_st1{fill:#590B3C;}
                            .cat_violet_st2{opacity:0.54;fill:#D570A3;enable-background:new    ;}
                            .cat_violet_st3{fill:#D570A3;}
                            .cat_violet_st4{fill:#4A0125;}
                            .cat_violet_st5{fill:#781245;}
                            .cat_violet_st6{opacity:0.6;fill:#36011B;enable-background:new    ;}
                            .cat_violet_st7{fill:#A43D71;}
                            .cat_violet_st8{opacity:0.5;fill:#A43D71;enable-background:new    ;}
                            .cat_violet_st9{fill:#FEE0EE;}
                            .cat_violet_st10{opacity:0.48;fill:#4A0125;enable-background:new    ;}
                            .cat_violet_st11{fill:#36001A;}
                            .cat_violet_st12{fill:#FFFFFF;}
                            .cat_violet_st13{opacity:0.54;}
                            .cat_violet_st14{fill:#BB8496;}
                        </style>
                    <g class="cat_shadow cat_violet_st0">
                        <polygon points="223.2,157 224.1,156.7 224.8,156.5 225.2,156.3 225.6,156 225.9,155.8 226,155.6 225.9,155.4 225.7,155.3
                225.4,155.1 225,154.9 224.4,154.8 223.6,154.7 222.8,154.6 221.9,154.6 220.9,154.5 219.6,154.5 219,154.5 218.3,154.5
                217.6,154.6 216.8,154.6 216.1,154.6 215.4,154.7 214.6,154.7 213.9,154.8 213.1,154.8 212.4,154.9 211.6,154.9 210.8,155
                210.1,155.1 209.4,155.2 208.6,155.3 207.9,155.4 205.6,155.6 203.4,155.8 201.1,156.1 198.7,156.3 196.3,156.5 194,156.7
                191.5,156.9 189.1,157 186.8,157.2 184.4,157.3 182.2,157.4 180,157.5 177.9,157.6 175.9,157.6 174,157.7 172.2,157.7 171,157.7
                170,157.6 168.8,157.6 167.6,157.6 166.6,157.5 165.4,157.4 164.3,157.3 163.1,157.3 162,157.2 161,157 159.8,156.9 158.7,156.8
                157.7,156.7 156.6,156.6 155.5,156.4 154.4,156.3 153.1,156.2 151.7,156 150.3,155.9 148.8,155.7 147.4,155.6 145.8,155.4
                144.3,155.3 142.6,155.2 141,155 139.3,154.9 137.7,154.8 135.8,154.7 134.1,154.7 132.3,154.6 130.4,154.6 128.6,154.5
                126.9,154.5 125.1,154.5 123.4,154.5 121.7,154.6 119.9,154.6 118,154.6 116.2,154.7 114.2,154.8 112.2,154.8 110.1,154.9
                107.9,155 105.6,155.2 103.1,155.3 100.6,155.5 97.8,155.7 94.9,155.9 92.4,156 89.8,156.2 87.3,156.4 84.9,156.6 82.4,156.9
                80.1,157.1 77.6,157.3 75.3,157.6 72.9,157.8 70.7,158.1 68.6,158.3 66.3,158.6 64.3,158.8 62.2,159.1 60.2,159.3 58.2,159.6
                57.7,159.7 56.9,159.8 56.3,159.8 55.7,159.9 55.1,160 54.5,160.2 53.8,160.3 53.3,160.4 52.7,160.5 52.2,160.6 51.7,160.7
                51.2,160.8 50.8,160.9 50.2,161 49.7,161.1 49.3,161.2 48.6,161.5 47.9,161.7 47.4,162 47,162.2 46.8,162.4 46.7,162.6 46.7,162.8
                47,163 47.2,163.1 47.7,163.3 48.3,163.4 48.9,163.5 49.8,163.6 50.7,163.6 51.8,163.7 52.9,163.7 53.9,163.7 54.7,163.7
                55.6,163.6 56.6,163.6 57.5,163.5 58.5,163.5 59.4,163.4 60.4,163.3 61.3,163.2 62.3,163.1 63.3,163 64.2,162.9 65.2,162.8
                66.1,162.7 67.1,162.5 67.9,162.4 69.6,162.2 71.4,161.9 73.5,161.7 75.7,161.4 78.1,161.2 80.7,160.9 83.3,160.7 86.2,160.4
                89.1,160.2 92.2,160 95.3,159.8 98.6,159.6 101.9,159.5 105.3,159.4 108.8,159.3 112.1,159.3 113.3,159.3 114.4,159.3 115.5,159.4
                116.7,159.4 117.9,159.5 118.9,159.6 120.1,159.7 121.2,159.8 122.3,159.9 123.4,160.1 124.4,160.2 125.6,160.3 126.6,160.5
                127.7,160.6 128.8,160.7 129.9,160.8 131.4,161 132.9,161.2 134.5,161.4 136,161.5 137.6,161.7 139.2,161.8 140.8,162 142.5,162.1
                144.3,162.2 146.1,162.3 148.1,162.4 150.1,162.4 152.3,162.5 154.7,162.5 157.2,162.5 159.8,162.5 162,162.4 164.3,162.4
                166.7,162.3 169.2,162.2 171.9,162.1 174.8,162 177.7,161.8 180.8,161.6 184,161.4 187.4,161.2 190.9,160.9 194.5,160.6
                198.4,160.3 202.3,159.9 206.5,159.5 210.7,159 211.7,158.9 212.5,158.8 213.4,158.7 214.3,158.6 215.2,158.5 216,158.4
                216.9,158.3 217.6,158.1 218.5,158 219.2,157.9 220,157.7 220.7,157.6 221.4,157.5 222.1,157.3 222.6,157.2 	"/>
                        <polygon id="body_1_" points="147.8,190 146,190 144.3,189.9 142.9,189.8 142,189.6 141.3,189.3 140.9,189.1 141,188.7
                141.3,188.4 141.3,188.4 141.3,188.4 141.3,188.4 141.3,188.4 141.3,188.4 141.4,188.4 141.4,188.4 141.4,188.4 142.9,187.2
                144.7,185.8 146.4,184.5 148.5,182.9 148.9,182.4 148.9,182 148.5,181.7 147.6,181.4 146.5,181.2 145.2,181.1 143.9,180.9
                142.4,180.9 111.2,180.9 109.8,180.9 107.9,181 105.8,181.1 103.3,181.3 100.6,181.6 97.6,181.9 94.4,182.3 91.2,182.9 83.5,184.2
                75.1,185.7 67.6,187.1 60.6,188.4 60.6,188.4 60.6,188.4 60.6,188.4 60.6,188.4 60.6,188.4 60.6,188.4 60.6,188.4 60.6,188.4
                58.5,188.7 56.2,189 54,189.3 51.7,189.5 49.3,189.7 46.9,189.9 44.7,190 42.6,190 40.3,189.9 38.3,189.8 36.9,189.6 36.1,189.2
                35.7,188.9 36,188.5 36.9,188 38.3,187.5 135.4,159.4 139.3,158.4 143.9,157.5 149.1,156.6 154.7,155.9 160.4,155.3 166.2,154.8
                171.8,154.5 177.2,154.4 191.5,154.4 245,154.4 260.4,154.4 264.7,154.5 268.2,154.8 270.5,155.3 271.8,155.9 272,156.6
                271.2,157.5 269.3,158.4 266.4,159.4 169.3,187.5 167.3,188 164.9,188.5 162.2,188.9 159.3,189.3 156.4,189.6 153.5,189.8
                150.6,189.9 	"/>
                    </g>
                    <g class="cat_body">
                        <g class="cat_tail">
                            <polygon class="cat_violet_st1" points="224.4,143.9 224.4,145.2 224.2,146.4 223.9,147.5 223.5,148.6 223,149.7 222.4,150.7 221.7,151.6
                220.9,152.5 220,153.3 219.1,154 218.1,154.6 217,155.1 215.9,155.5 214.8,155.8 213.6,156 212.3,156 211.7,156 211,156
                210.4,155.9 209.7,155.7 209.1,155.6 208.5,155.4 207.9,155.2 207.4,154.9 206.8,154.6 206.3,154.3 205.7,154 205.2,153.6
                204.7,153.3 204.3,152.9 203.8,152.5 203.4,152 202,150.8 200.6,149.6 199.1,148.4 197.5,147.3 195.8,146.3 194.1,145.4
                192.3,144.5 190.4,143.7 188.6,143 186.7,142.3 184.9,141.8 183,141.3 181.1,141 179.3,140.7 177.5,140.6 175.8,140.5 174.6,140.5
                173.4,140.7 172.2,140.8 170.8,141.1 169.5,141.4 168.1,141.8 166.7,142.2 165.2,142.6 163.8,143.1 162.3,143.7 160.8,144.2
                159.3,144.8 157.8,145.4 156.3,146 154.8,146.6 153.3,147.2 151.5,147.9 149.5,148.7 147.6,149.5 145.6,150.2 143.6,151
                141.5,151.7 139.5,152.4 137.4,153 135.4,153.6 133.3,154.2 131.3,154.7 129.2,155.1 127.2,155.4 125.2,155.7 123.2,155.9
                121.3,156 119.6,156.1 117.8,156.1 116.1,156 114.5,155.9 112.8,155.8 111.1,155.5 109.4,155.3 107.7,154.9 106,154.5 104.2,154.1
                102.4,153.5 100.5,152.9 98.5,152.2 96.5,151.4 94.4,150.5 92.2,149.5 90.3,148.6 88.4,147.6 86.6,146.6 84.9,145.6 83.2,144.5
                81.6,143.4 80,142.2 78.5,141 77,139.8 75.6,138.6 74.3,137.4 73,136.1 71.8,134.9 70.6,133.6 69.5,132.3 68.4,131.1 68.1,130.7
                67.7,130.2 67.4,129.8 67.1,129.3 66.9,128.8 66.6,128.3 66.3,127.8 66.1,127.3 65.9,126.8 65.7,126.3 65.6,125.8 65.5,125.2
                65.4,124.7 65.3,124.1 65.2,123.5 65.2,122.9 65.3,121.7 65.5,120.5 65.8,119.3 66.2,118.2 66.7,117.2 67.3,116.2 68,115.2
                68.8,114.4 69.6,113.6 70.6,112.9 71.6,112.3 72.6,111.8 73.7,111.4 74.9,111.1 76.1,110.9 77.3,110.8 78.2,110.9 79,110.9
                79.8,111.1 80.6,111.3 81.3,111.6 82.1,111.9 82.8,112.2 83.5,112.6 84.1,113.1 84.8,113.5 85.4,114.1 85.9,114.6 86.5,115.2
                87,115.8 87.5,116.5 87.9,117.1 88.7,118.3 89.7,119.5 90.9,120.7 92.2,122 93.7,123.3 95.4,124.6 97.2,125.8 99.2,127
                101.3,128.2 103.7,129.2 106.2,130.1 108.9,130.9 111.7,131.6 114.8,132.1 118,132.4 121.3,132.5 122.5,132.4 123.7,132.3
                125,132.1 126.4,131.8 127.8,131.4 129.2,130.9 130.7,130.5 132.2,129.9 133.7,129.3 135.2,128.7 136.7,128.1 138.3,127.5
                139.8,126.8 141.3,126.2 142.8,125.6 144.3,125 146.5,124 148.6,123.2 150.8,122.3 152.9,121.5 155,120.7 157.1,120 159.2,119.3
                161.3,118.7 163.5,118.2 165.6,117.7 167.9,117.3 170.1,117 172.5,116.8 174.9,116.7 177.4,116.7 179.9,116.9 182,117 184.1,117.3
                186.3,117.6 188.5,118.1 190.8,118.6 193.2,119.3 195.6,120.1 198,121 200.5,122.1 203,123.3 205.6,124.6 208.2,126.1 210.9,127.8
                213.5,129.6 216.3,131.6 219,133.8 219.6,134.3 220.1,134.8 220.6,135.3 221.1,135.9 221.6,136.4 222,137 222.5,137.6 222.8,138.2
                223.2,138.9 223.5,139.5 223.8,140.2 224,140.9 224.2,141.6 224.3,142.4 224.4,143.1 	"/>
                            <polygon class="cat_violet_st2" points="142,125.9 140.9,126.5 139.9,127.2 138.7,128.2 137.5,129.4 136.3,130.7 134.9,132.1 133.6,133.6
                132.1,135.2 130.6,136.9 128.9,138.5 127.3,140.2 125.5,141.8 123.6,143.3 121.7,144.7 119.7,146.1 117.6,147.2 115.5,148.2
                113.5,149.1 111.7,149.8 109.9,150.5 108.1,151 106.5,151.3 104.9,151.6 103.3,151.8 101.8,151.8 100.3,151.8 98.9,151.6
                97.5,151.4 96.2,151.1 94.8,150.6 93.5,150.1 92.2,149.5 90.3,148.5 88.5,147.5 86.7,146.5 85,145.4 83.3,144.3 81.6,143.2
                80.1,142.1 78.6,140.9 77.1,139.7 75.7,138.5 74.3,137.3 73,136.1 71.8,134.8 70.6,133.6 69.5,132.3 68.4,131.1 68.1,130.7
                67.8,130.2 67.5,129.7 67.2,129.2 66.9,128.7 66.6,128.1 66.4,127.6 66.2,127 66,126.5 65.8,125.9 65.6,125.3 65.5,124.8
                65.4,124.3 65.3,123.8 65.2,123.3 65.2,122.9 65.3,121.7 65.5,120.5 65.8,119.3 66.2,118.2 66.7,117.2 67.3,116.2 68,115.2
                68.8,114.4 69.6,113.6 70.6,112.9 71.6,112.3 72.6,111.8 73.7,111.4 74.9,111.1 76.1,110.9 77.3,110.8 78.2,110.9 79,110.9
                79.8,111.1 80.6,111.3 81.3,111.6 82.1,111.9 82.8,112.2 83.5,112.6 84.1,113.1 84.8,113.5 85.4,114.1 85.9,114.6 86.5,115.2
                87,115.8 87.5,116.5 87.9,117.1 88.7,118.3 89.7,119.5 90.9,120.7 92.2,122 93.7,123.3 95.4,124.6 97.2,125.8 99.2,127
                101.3,128.2 103.7,129.2 106.2,130.1 108.9,130.9 111.7,131.6 114.8,132.1 118,132.4 121.3,132.5 122.3,132.4 123.4,132.3
                124.6,132.1 125.8,131.9 127,131.5 128.3,131.2 129.6,130.8 131,130.3 132.4,129.8 133.8,129.3 135.1,128.7 136.5,128.2
                137.9,127.6 139.3,127 140.6,126.5 	"/>
                            <polygon class="cat_violet_st3" points="102.1,135.6 100,135.2 98,134.7 96.1,134 94.4,133.2 92.8,132.4 91.3,131.4 89.9,130.4 88.6,129.3
                87.4,128.3 86.3,127.2 85.4,126.2 84.5,125.2 83.7,124.3 83,123.5 82.4,122.8 81.9,122.3 81.7,122 81.4,121.8 81,121.5 80.7,121.2
                80.3,120.9 79.8,120.5 79.4,120.2 78.8,119.9 78.3,119.6 77.8,119.3 77.2,119.1 76.6,118.9 75.9,118.7 75.3,118.6 74.6,118.5
                74,118.5 73.2,118.6 72.4,118.8 71.6,119.1 70.8,119.6 70.1,120.1 69.4,120.7 68.7,121.4 68.1,122.1 67.6,123 67.2,123.9
                66.9,124.8 66.8,125.8 66.7,126.8 66.9,127.9 67.1,128.9 67.6,130 67.4,129.8 67.2,129.5 67,129.1 66.8,128.7 66.6,128.3
                66.4,127.9 66.2,127.4 66,126.9 65.8,126.4 65.7,125.9 65.6,125.4 65.4,124.9 65.4,124.4 65.3,123.9 65.2,123.4 65.2,122.9
                65.3,121.7 65.5,120.5 65.8,119.3 66.2,118.2 66.7,117.1 67.3,116.1 68,115.2 68.8,114.3 69.6,113.6 70.6,112.9 71.6,112.3
                72.6,111.8 73.7,111.4 74.9,111.1 76.1,110.9 77.3,110.8 78.2,110.9 79,110.9 79.8,111.1 80.6,111.3 81.3,111.6 82.1,111.9
                82.8,112.2 83.5,112.6 84.1,113.1 84.8,113.5 85.4,114.1 85.9,114.6 86.5,115.2 87,115.8 87.5,116.5 87.9,117.1 88.7,118.3
                89.7,119.5 90.9,120.7 92.2,122 93.6,123.3 95.3,124.5 97.1,125.8 99.1,127 101.3,128.1 103.6,129.1 106.1,130 108.8,130.8
                111.7,131.5 114.7,132 117.9,132.3 121.3,132.5 120.7,132.8 120,133.1 119.2,133.4 118.3,133.7 117.2,134.1 116.1,134.4
                114.9,134.7 113.6,134.9 112.3,135.2 110.9,135.4 109.4,135.6 108,135.7 106.5,135.8 105,135.8 103.5,135.7 	"/>
                        </g>
                        <polygon id="body" class="cat_violet_st4" points="263.1,-19.1 261.2,-19 259.3,-18.6 257.4,-18 255.8,-17 254.3,-15.8 253,-14.5 251.9,-12.8
                251,-11.1 251,-11.1 251,-11.1 251,-11.1 251,-11.1 251,-11.1 251.1,-11.1 251.1,-11.1 251.1,-11.1 248.5,-5.2 245.5,1.6
                242.7,8.1 239.4,15.8 238.1,18.2 236.6,20.3 235,22 233.2,23.3 231.4,24.3 229.6,25 227.9,25.6 226.2,25.9 195,25.9 193.8,25.6
                192.2,25.2 190.5,24.6 188.7,23.7 186.8,22.5 185,20.8 183.3,18.7 181.9,16.1 178.9,9.4 175.7,1.9 172.9,-4.8 170.2,-11 170.2,-11
                170.2,-11 170.2,-11 170.2,-11 170.2,-11 170.2,-11 170.2,-11 170.2,-11 169.3,-12.7 168.2,-14.3 166.9,-15.6 165.4,-16.8
                163.7,-17.8 161.8,-18.5 159.9,-18.9 157.9,-19.1 155.4,-18.8 152.9,-18.1 150.7,-17 148.8,-15.4 147.2,-13.6 146,-11.5
                145.2,-9.1 144.9,-6.6 144.9,7.7 144.9,18.2 144.9,28.7 144.9,39.2 144.9,49.7 144.9,60.1 144.9,70.6 144.9,81.1 144.9,91.6
                144.9,102.1 144.9,112.6 144.9,123.1 144.9,132 145.4,136.9 146.8,141.5 149.1,145.6 152.1,149.3 155.7,152.3 159.9,154.6
                164.4,156.2 169.3,156.9 183.6,156.9 237.1,156.9 252.5,156.9 257.3,156.2 261.8,154.7 265.8,152.3 269.2,149.3 272,145.6
                274.1,141.5 275.4,136.9 275.9,132 275.9,123.1 275.9,113.4 275.9,103.8 275.9,94.2 275.9,84.6 275.9,75 275.9,65.4 275.9,55.8
                275.9,46.2 275.9,36.6 275.9,26.9 275.9,17.3 275.9,7.7 275.9,-6.6 275.6,-9.1 274.9,-11.5 273.7,-13.6 272.1,-15.5 270.2,-17
                268.1,-18.1 265.7,-18.8 	"/>
                        <polygon id="body_light_part" class="cat_violet_st5" points="263.1,-19.1 261.2,-19 259.3,-18.6 257.4,-18 255.8,-17 254.3,-15.8 253,-14.5
                251.9,-12.8 251,-11.1 251,-11.1 251,-11.1 251,-11.1 251,-11.1 251,-11.1 251.1,-11.1 251.1,-11.1 251.1,-11.1 248.5,-5.2
                245.5,1.6 242.7,8.1 239.4,15.8 238.1,18.2 236.6,20.3 235,22 233.2,23.3 231.4,24.3 229.6,25 227.9,25.6 226.2,25.9 210.9,25.9
                210.9,156.9 237.1,156.9 252.5,156.9 257.3,156.2 261.8,154.7 265.8,152.3 269.2,149.3 272,145.6 274.1,141.5 275.4,136.9
                275.9,132 275.9,123.1 275.9,113.4 275.9,103.8 275.9,94.2 275.9,84.6 275.9,75 275.9,65.4 275.9,55.8 275.9,46.2 275.9,36.6
                275.9,26.9 275.9,17.3 275.9,7.7 275.9,-6.6 275.6,-9.1 274.9,-11.5 273.7,-13.6 272.1,-15.5 270.2,-17 268.1,-18.1 265.7,-18.8
                "/>
                        <polygon id="shadow" class="cat_violet_st6" points="210.9,156.9 179.1,156.9 174.3,156.4 169.9,154.9 165.9,152.5 162.5,149.5 159.7,145.8
                157.7,141.6 156.4,137 155.9,132.1 155.9,92.6 157.5,93.2 162,94.5 168.6,95.8 176.7,96.5 181.1,96.4 185.6,95.8 190.2,95
                192.9,94.3 195.2,93.3 197.6,92.2 199.7,91 201.7,89.5 203.5,87.9 205.4,86.1 207.4,84 209.1,81.8 210.9,78.9 	"/>
                        <polygon id="light_1" class="cat_violet_st7" points="210.9,25.9 210.9,30.9 192.9,30.9 189.6,30.5 186.7,29.4 183.9,27.5 181.3,24.7
                178.8,21.1 176.3,16.7 164.8,-9.9 164,-11.4 163,-12.8 161.9,-13.9 160.7,-14.8 159.3,-15.4 157.9,-15.9 156.5,-16.2 155,-16.2
                153.4,-16.1 151.9,-15.6 150.3,-14.9 148.8,-13.9 147.5,-12.6 146.4,-11.1 145.5,-9.3 145,-7.3 145.3,-9.4 146,-11.5 147.1,-13.4
                148.7,-15.3 150.6,-16.8 152.8,-18.1 155.2,-18.8 157.9,-19.1 159.9,-18.9 161.8,-18.4 163.6,-17.7 165.3,-16.8 166.9,-15.6
                168.2,-14.3 169.3,-12.7 170.2,-11 170.6,-10.2 171.6,-7.8 181.9,16.3 182.3,16.9 182.6,17.6 183,18.2 183.3,18.8 183.7,19.3
                184.2,19.8 184.6,20.4 185,20.8 186.3,22.1 187.7,23.2 189.3,24.1 190.9,24.8 192.5,25.3 194.1,25.6 195.5,25.8 196.7,25.9 	"/>
                        <polygon id="light_1_8_" class="cat_violet_st8" points="210.2,25.9 210.2,30.9 228.2,30.9 231.5,30.5 234.4,29.4 237.2,27.5 239.8,24.7
                242.3,21.1 244.8,16.7 256.3,-9.9 257.1,-11.4 258.1,-12.8 259.2,-13.9 260.4,-14.8 261.8,-15.4 263.2,-15.9 264.6,-16.2
                266.1,-16.2 267.7,-16.1 269.2,-15.6 270.8,-14.9 272.3,-13.9 273.6,-12.6 274.7,-11.1 275.6,-9.3 276.1,-7.3 275.8,-9.4
                275.1,-11.5 274,-13.4 272.4,-15.3 270.5,-16.8 268.3,-18.1 265.9,-18.8 263.2,-19.1 261.2,-18.9 259.3,-18.4 257.5,-17.7
                255.8,-16.8 254.2,-15.6 252.9,-14.3 251.8,-12.7 250.9,-11 250.5,-10.2 249.5,-7.8 239.2,16.3 238.8,16.9 238.5,17.6 238.1,18.2
                237.8,18.8 237.4,19.3 236.9,19.8 236.5,20.4 236.1,20.8 234.8,22.1 233.4,23.2 231.8,24.1 230.2,24.8 228.6,25.3 227,25.6
                225.6,25.8 224.4,25.9 	"/>
                        <polygon id="light_3" class="cat_violet_st9" points="275.9,-6.6 275.9,132.1 275.5,137 274.2,141.6 272.2,145.8 269.5,149.5 266.1,152.5
                262.2,154.9 257.9,156.4 253.1,156.9 252.4,156.9 251.7,156.9 251,156.9 250.3,156.9 249.6,156.9 248.9,156.9 248.2,156.9
                247.5,156.9 253.9,155.8 259,153.8 263.3,150.6 266.3,146.6 268.5,142 269.9,137.3 270.7,132.8 270.9,128.7 270.9,-3.1 270.9,-4.8
                270.9,-6.3 270.9,-7.6 270.8,-8.9 270.5,-10.1 270,-11.3 269.3,-12.5 268.3,-13.8 266.9,-15 265,-15.8 263,-16.3 260.7,-16.5
                258.4,-16.3 256.2,-15.8 254.1,-14.8 252.4,-13.5 253.7,-15.1 255,-16.4 256.3,-17.4 257.7,-18.1 259.2,-18.6 260.6,-18.9
                262,-19.1 263.4,-19.1 266,-18.8 268.3,-18 270.4,-16.9 272.3,-15.4 273.8,-13.5 274.9,-11.4 275.7,-9.1 	"/>
                        <polygon id="light_2" class="cat_violet_st3" points="270.9,-6.6 270.9,132.1 270.5,137 269.2,141.6 267.2,145.8 264.5,149.5 261.1,152.5
                257.2,154.9 252.9,156.4 248.1,156.9 246.4,156.9 244.7,156.9 243,156.9 241.4,156.9 239.7,156.9 238,156.9 236.3,156.9
                234.7,156.9 244.6,156 252.2,153.7 257.7,150.3 261.5,146.1 263.9,141.5 265.2,136.8 265.8,132.4 265.9,128.7 265.9,-3.1 265.9,-5
                265.9,-7.2 265.7,-9.4 265.3,-11.5 264.6,-13.4 263.5,-15 261.9,-16 259.7,-16.4 261.9,-16.6 264.1,-16.4 266,-15.7 267.6,-14.6
                269,-13.1 270,-11.2 270.7,-9 	"/>
                        <path id="light" class="cat_violet_st2" d="M176.2,155.8l-7.7-2.5l-5.5-3.4l-3.8-4.2l-2.4-4.6l-1.3-4.7l-0.5-4.5l0.1-3.7V-3.3v-1.9v-2.2v-2.2
                l0.3-2.1l0.6-1.9l1.1-1.6l1.6-1l2.2-0.4l1.3,0.1l2.2,0.5l2.1,1l1.7,1.3l-1.2-1.6l-1.3-1.3l-1.3-1l-1.4-0.7l-1.5-0.5l-1.5-0.1h-1.4
                h-1.4l-2.6,0.1l-2.3,0.7l-2.1,1.1l-1.9,1.5l-1.5,1.9l-1.1,2.1l-0.5,2.3l0.1,2.5v138.4l0.1,4.9l1.2,4.6l1.9,4.2l2.7,3.7l3.4,3
                l3.9,2.4l4.3,1.8l4.8,0.8h0.7h0.7h0.7h0.7h0.7h0.7h0.7h0.1h0.6h1.1h1.7h1.7h1.6h1.7h1.7h1.7h1.6L176.2,155.8L176.2,155.8z"/>
                        <polygon id="shadow_on_light_part" class="cat_violet_st10" points="210.9,156.9 242.7,156.9 247.5,156.4 251.9,154.9 255.9,152.5
                259.3,149.5 262.1,145.8 264.1,141.6 265.4,137 265.9,132.1 265.9,92.6 264.3,93.2 259.8,94.5 253.2,95.8 245.2,96.5 240.7,96.4
                236.2,95.8 231.7,95 228.9,94.3 226.6,93.3 224.2,92.2 222.1,91 220.1,89.5 218.3,87.9 216.4,86.1 214.4,84 212.7,81.8 210.9,78.9
                    "/>
                        <polygon id="stripe_3_" class="cat_violet_st2" points="144.9,101.7 156.5,106.1 157.5,106.4 158.2,106.5 160.5,106.2 165.9,105.4
                173.4,104.4 182.1,103.2 191.2,102.1 199.8,101.2 207.1,100.7 212.2,100.7 215.3,101.2 217.4,101.7 218.5,102.2 218.7,102.8
                218.1,103.5 216.7,104.1 214.7,104.8 212,105.5 208.1,106.5 201.7,108.4 161.6,120.4 157.8,121.2 156.8,121.2 144.9,118.1 	"/>
                        <polygon id="stripe_2_" class="cat_violet_st2" points="183.6,120.5 185.8,119.6 189.2,118.4 193.5,116.9 198.9,115.3 205.2,113.9
                212.3,112.7 220.4,112 229.2,111.9 237.9,112.2 245.4,112.6 251.7,113 256.9,113.5 260.9,113.9 263.8,114.2 265.5,114.5 266,114.5
                275.9,112.1 275.9,132.9 266.5,134.3 265.4,134.2 263.6,133.8 260.3,132.8 255.8,131.5 250.4,130 244.5,128.4 238.4,127
                232.5,125.9 227.1,125.2 221.1,124.9 213.6,124.5 205.4,124 197.3,123.5 190.3,123 185.1,122.3 182.6,121.4 	"/>
                        <polygon id="stripe_1_" class="cat_violet_st2" points="230,137.5 226,138.4 222.4,139.4 219,140.4 215.5,141.5 211.6,142.8 207.2,144.2
                201.9,145.8 195.5,147.7 192.8,148.6 190.3,149.5 187.6,150.4 184.6,151.1 181.1,151.7 176.7,152 171.4,151.9 164.9,151.4
                163.7,151.1 161.8,150.3 147.6,142.8 147,141.6 146.5,140.3 146,139 145.6,137.6 145.3,136.3 145.1,134.9 144.9,133.5 144.9,132.1
                144.9,130.9 144.9,129.8 144.9,128.6 144.9,127.5 144.9,126.3 144.9,125.2 144.9,124.1 144.9,122.9 155.7,128.4 157,128.5
                160.4,128.9 165.5,129.4 172,130 179.5,130.5 187.6,130.9 195.8,131.1 203.7,131.1 210.8,130.9 216.9,130.7 222,130.6 226.3,130.6
                229.9,130.7 233,130.8 235.8,131 238.3,131.2 240.4,131.5 241.8,131.9 242.3,132.5 242,133.2 240.7,134.1 238.3,135.1 234.7,136.2
                    "/>
                        <polygon id="stripe" class="cat_violet_st2" points="208,147.2 212.8,146.5 219.9,145.6 228.5,144.5 237.8,143.4 246.9,142.3 255,141.3
                261.2,140.4 264.6,139.8 266.5,139.2 268.3,138.4 270,137.4 271.6,136.5 272.9,135.5 274,134.8 274.6,134.3 274.9,134.1
                270.3,146.7 270.1,146.7 269.3,146.6 268.2,146.5 266.7,146.5 264.9,146.9 262.8,147.5 260.6,148.6 258.2,150.2 254.7,151.7
                249.3,152.5 242.7,152.7 235.4,152.5 228.2,152.1 221.6,151.6 216.2,151.1 212.7,150.9 209.4,150.9 207,150.8 205.3,150.3
                204.4,149.7 204.2,149.1 204.8,148.4 206.1,147.8 	"/>
                        <g id="pow_left">
                            <polygon class="cat_violet_st11" points="197.9,133.1 197.7,135.6 197.3,138 196.7,140.3 195.9,142.5 194.9,144.6 193.7,146.6 192.4,148.5
                    190.8,150.2 189.1,151.7 187.3,153.1 185.3,154.3 183.2,155.3 181,156.1 178.7,156.7 176.4,157.1 173.9,157.2 171.5,157.1
                    169.1,156.7 166.8,156.1 164.6,155.3 162.6,154.3 160.6,153.1 158.7,151.7 157.1,150.2 155.5,148.5 154.1,146.6 152.9,144.6
                    151.9,142.5 151.1,140.3 150.5,138 150.1,135.6 149.9,133.1 149.9,133 150,132.9 150,132.7 150,132.6 150.1,132.5 150.1,132.3
                    150.1,132.2 150.1,132.1 150.1,132 150.1,131.8 150.1,131.7 150.1,131.6 150.1,131.5 150.1,131.3 150.1,131.2 150.2,131.1
                    150.4,133.4 150.9,135.5 151.6,137.6 152.5,139.5 153.6,141.4 154.8,143 156.2,144.6 157.7,146 159.4,147.3 161.2,148.4
                    163.1,149.4 165.1,150.2 167.2,150.8 169.4,151.3 171.6,151.6 173.9,151.7 176.3,151.6 178.5,151.3 180.7,150.8 182.8,150.2
                    184.8,149.4 186.7,148.4 188.5,147.3 190.1,146 191.7,144.6 193.1,143 194.3,141.4 195.3,139.5 196.2,137.6 196.9,135.5
                    197.4,133.4 197.7,131.1 197.7,131.2 197.7,131.3 197.8,131.5 197.8,131.6 197.8,131.7 197.8,131.8 197.8,132 197.8,132.1
                    197.8,132.2 197.8,132.3 197.8,132.5 197.8,132.6 197.8,132.7 197.9,132.9 197.9,133 		"/>
                            <polygon class="cat_violet_st11" points="168.2,140 168.2,140 168.2,140 168.2,140 168.2,140 167.4,140.2 166.8,140.6 166.3,141.3 166.2,142
                    166.2,153.4 166.3,154.2 166.8,154.9 167.4,155.3 168.2,155.4 168.2,155.4 168.2,155.4 168.2,155.4 168.2,155.4 168.9,155.3
                    169.6,154.9 170,154.2 170.2,153.4 170.2,142 170,141.3 169.6,140.6 168.9,140.2 		"/>
                            <polygon class="cat_violet_st11" points="179.7,140 179.7,140 179.7,140 179.7,140 179.7,140 178.9,140.2 178.3,140.6 177.9,141.3 177.7,142
                    177.7,153.4 177.9,154.2 178.3,154.9 178.9,155.3 179.7,155.4 179.7,155.4 179.7,155.4 179.7,155.4 179.7,155.4 180.5,155.3
                    181.1,154.9 181.5,154.2 181.7,153.4 181.7,142 181.5,141.3 181.1,140.6 180.5,140.2 		"/>
                        </g>
                        <g id="pow_right">
                            <polygon class="cat_violet_st11" points="270.9,133.1 270.7,135.6 270.3,138 269.7,140.3 268.9,142.5 267.9,144.6 266.7,146.6 265.4,148.5
                    263.8,150.2 262.1,151.7 260.3,153.1 258.3,154.3 256.2,155.3 254,156.1 251.7,156.7 249.4,157.1 246.9,157.2 244.5,157.1
                    242.1,156.7 239.8,156.1 237.6,155.3 235.6,154.3 233.6,153.1 231.7,151.7 230.1,150.2 228.5,148.5 227.1,146.6 225.9,144.6
                    224.9,142.5 224.1,140.3 223.5,138 223.1,135.6 222.9,133.1 222.9,133 223,132.9 223,132.7 223,132.6 223.1,132.5 223.1,132.3
                    223.1,132.2 223.1,132.1 223.1,132 223.1,131.8 223.1,131.7 223.1,131.6 223.1,131.5 223.1,131.3 223.1,131.2 223.2,131.1
                    223.4,133.4 223.9,135.5 224.6,137.6 225.5,139.5 226.6,141.4 227.8,143 229.2,144.6 230.7,146 232.4,147.3 234.2,148.4
                    236.1,149.4 238.1,150.2 240.2,150.8 242.4,151.3 244.6,151.6 246.9,151.7 249.3,151.6 251.5,151.3 253.7,150.8 255.8,150.2
                    257.8,149.4 259.7,148.4 261.5,147.3 263.1,146 264.7,144.6 266.1,143 267.3,141.4 268.3,139.5 269.2,137.6 269.9,135.5
                    270.4,133.4 270.7,131.1 270.7,131.2 270.7,131.3 270.8,131.5 270.8,131.6 270.8,131.7 270.8,131.8 270.8,132 270.8,132.1
                    270.8,132.2 270.8,132.3 270.8,132.5 270.8,132.6 270.8,132.7 270.9,132.9 270.9,133 		"/>
                            <polygon class="cat_violet_st11" points="241.2,140 241.2,140 241.2,140 241.2,140 241.2,140 240.4,140.2 239.8,140.6 239.3,141.3 239.2,142
                    239.2,153.4 239.3,154.2 239.8,154.9 240.4,155.3 241.2,155.4 241.2,155.4 241.2,155.4 241.2,155.4 241.2,155.4 241.9,155.3
                    242.6,154.9 243,154.2 243.2,153.4 243.2,142 243,141.3 242.6,140.6 241.9,140.2 		"/>
                            <polygon class="cat_violet_st11" points="252.7,140 252.7,140 252.7,140 252.7,140 252.7,140 251.9,140.2 251.3,140.6 250.9,141.3 250.7,142
                    250.7,153.4 250.9,154.2 251.3,154.9 251.9,155.3 252.7,155.4 252.7,155.4 252.7,155.4 252.7,155.4 252.7,155.4 253.5,155.3
                    254.1,154.9 254.5,154.2 254.7,153.4 254.7,142 254.5,141.3 254.1,140.6 253.5,140.2 		"/>
                        </g>
                        <polygon id="blick" class="cat_violet_st12" points="265.9,147.8 267.2,145.8 268.4,143.2 269.6,140 270.4,137.2 270.7,134.3 270.9,131.4
                271.1,128.4 271.1,125.3 271.3,121.9 271.8,118.2 272.6,114.7 273.5,111.7 274.4,109.6 275.1,108.6 275.7,109.3 275.9,111.9
                275.9,115.5 275.9,118.9 275.9,122 275.9,124.8 275.9,127 275.9,128.8 276,132.1 275.8,134.3 275.5,137 274.7,140.1 273.5,143.4
                271.7,146.7 269.1,150 266.7,152.4 265.3,153.2 264.5,153.2 264.3,152.4 264.4,151.2 264.8,150 265.2,149 265.5,148.4 	"/>
                        <polygon id="ear" class="cat_violet_st11" points="154.9,-0.5 155,-1.4 155.3,-2.2 155.8,-2.9 156.4,-3.6 157.1,-4.1 158,-4.5 158.9,-4.7
                159.8,-4.8 160.5,-4.7 161.3,-4.5 162,-4.2 162.6,-3.8 163.1,-3.3 163.6,-2.7 164,-2 164.3,-1.4 169.7,15.9 154.9,15.9 	"/>
                        <polygon id="nose" class="cat_violet_st7" points="212.7,77.1 211.5,77.1 210.4,77.1 209.3,77.1 208.1,77.1 205.7,76.6 203.8,75.3 202.4,73.3
                202,71 202,71 202,71 202,71 202,71 202.2,69.6 203,68.5 204,67.8 205.4,67.5 207.9,67.5 210.4,67.5 212.9,67.5 215.4,67.5
                216.8,67.8 217.8,68.5 218.6,69.6 218.8,71 218.8,71 218.8,71 218.8,71 218.8,71 218.4,73.3 217,75.3 215.1,76.6 	"/>
                        <g id="hair" class="cat_violet_st13">
                            <polygon class="cat_violet_st3" points="205.6,48.3 205.3,49.8 204.9,50.9 204.3,51.5 203.8,51.7 203.2,51.6 202.7,51.3 202.3,50.7
                    202.1,49.9 201.8,48.5 201.2,46.3 200.4,43.5 199.4,40.4 198.4,37.4 197.4,34.6 196.5,32.5 195.7,31.3 195.7,31.3 195.7,31.3
                    195.7,31.3 195.7,31.3 195.7,31.3 195.7,31.2 195.7,31.2 195.7,31.2 194.9,30.7 193.7,30.2 192.3,29.6 190.8,28.9 189.2,27.7
                    187.6,26.1 186.1,23.9 184.8,20.8 186,22.1 187.3,23.2 188.6,24 190,24.7 191.3,25.3 192.6,25.6 193.9,25.8 195.1,25.9
                    200.4,25.9 201.2,26.7 202.1,27.5 203,28.2 203.8,29 204.6,29.6 205.2,30.2 205.6,30.7 205.8,31.1 206,31.9 206.1,33.5
                    206.1,35.7 206.1,38.3 206.1,41 206,43.7 205.8,46.2 		"/>
                            <polygon class="cat_violet_st3" points="212.6,54.4 213.3,52.7 213.9,49.9 214.3,46.5 214.6,42.7 214.9,39 215,35.6 215.2,32.9 215.4,31.3
                    215.4,30.3 215,29.3 214.4,28.4 213.6,27.6 212.8,26.9 212.1,26.4 211.6,26 211.5,25.9 200.2,25.9 200.4,26.1 200.8,26.6
                    201.5,27.4 202.3,28.3 203.2,29.2 204.1,30.1 205,30.8 205.7,31.3 206.4,32.1 207.2,33.5 207.9,35.4 208.6,37.5 209.2,39.7
                    209.7,41.6 210.1,43.2 210.2,44.1 210.3,45.1 210.4,46.7 210.6,48.6 210.9,50.6 211.2,52.4 211.6,53.8 212,54.6 		"/>
                            <polygon class="cat_violet_st3" points="234.9,22.5 234.1,23.2 233.1,24 231.9,25 230.6,26.1 229.3,27.2 227.9,28.5 226.6,29.8 225.4,31.2
                    224.4,32.6 223.7,34.2 223.1,35.9 222.8,37.7 222.4,39.6 221.9,41.6 221.3,43.7 220.5,45.9 219.9,46.9 219.2,47.6 218.6,47.9
                    217.9,47.8 217.3,47.3 216.8,46.4 216.6,45.1 216.5,43.4 216.4,38.3 215.9,34.2 215.1,31.2 214.1,28.9 213.2,27.4 212.3,26.5
                    211.7,26 211.5,25.9 226.3,25.9 227.1,25.9 228,25.7 229,25.5 230.1,25.2 231.3,24.8 232.5,24.2 233.7,23.4 		"/>
                        </g>
                        <polygon id="ear_1_" class="cat_violet_st11" points="265.9,-0.5 265.7,-1.4 265.4,-2.2 264.9,-2.9 264.2,-3.6 263.5,-4.1 262.7,-4.5
                261.8,-4.7 260.9,-4.8 260.1,-4.7 259.4,-4.5 258.7,-4.2 258,-3.8 257.5,-3.3 257,-2.7 256.6,-2 256.3,-1.4 250.9,15.9 265.9,15.9
                    "/>
                        <g class="eye_right">
                            <circle id="eye_right_shadow" class="cat_violet_st11" cx="175.5" cy="67.5" r="19.4"/>
                            <circle class="eyeball cat_violet_st12" cx="177.4" cy="62.9" r="19.4"/>
                            <circle class="pupil" cx="178" cy="60" r="7.3"/>
                            <polygon id="eye_right_blick" class="cat_violet_st14" points="195,70.8 194.3,72.2 193.5,73.6 192.7,74.8 191.8,76 190.8,77 189.7,78
                    188.6,78.9 187.4,79.7 186.2,80.4 185,81 183.7,81.5 182.4,81.9 181.1,82.2 179.8,82.4 178.5,82.5 177.3,82.6 175.3,82.5
                    173.4,82.2 171.5,81.7 169.7,81.1 168,80.3 166.5,79.3 165,78.2 163.6,76.9 162.3,75.6 161.2,74.1 160.3,72.5 159.5,70.8
                    158.8,69.1 158.3,67.2 158,65.3 157.9,63.3 157.9,62.8 157.9,62.3 158,61.7 158,61 158.1,60.2 158.2,59.4 158.4,58.6 158.6,57.8
                    158.8,56.9 159.1,56 159.5,55.1 159.9,54.3 160.4,53.4 160.9,52.6 161.6,51.8 162.4,51 161.4,52.6 160.7,54.2 160.3,56
                    160.1,57.8 160.2,59.7 160.4,61.6 160.9,63.4 161.6,65.3 162.5,67.1 163.5,68.9 164.7,70.5 166.1,72 167.5,73.3 169.1,74.5
                    170.7,75.5 172.4,76.2 174,76.7 175.6,77 177.2,77.2 178.8,77.3 180.5,77.2 182.2,77.1 183.8,76.8 185.4,76.4 187,76 188.5,75.4
                    189.9,74.8 191.2,74.1 192.3,73.4 193.4,72.6 194.3,71.7 		"/>
                        </g>
                        <g class="eye_left">
                            <circle id="eye_left_shadow" class="cat_violet_st11" cx="240.8" cy="67.5" r="19.4"/>
                            <circle class="eyeball cat_violet_st12" cx="242.7" cy="62.9" r="19.4"/>
                            <circle class="pupil" cx="243" cy="60" r="7.3"/>
                            <polygon id="eye_left_blick" class="cat_violet_st14" points="260.3,70.8 259.6,72.2 258.8,73.6 258,74.8 257.1,76 256.1,77 255,78
                    253.9,78.9 252.7,79.7 251.5,80.4 250.3,81 249,81.5 247.7,81.9 246.4,82.2 245.1,82.4 243.8,82.5 242.6,82.6 240.6,82.5
                    238.7,82.2 236.8,81.7 235,81.1 233.3,80.3 231.8,79.3 230.3,78.2 228.9,76.9 227.6,75.6 226.5,74.1 225.6,72.5 224.8,70.8
                    224.1,69.1 223.6,67.2 223.3,65.3 223.2,63.3 223.2,62.8 223.2,62.3 223.3,61.7 223.3,61 223.4,60.2 223.5,59.4 223.7,58.6
                    223.9,57.8 224.1,56.9 224.4,56 224.8,55.1 225.2,54.3 225.7,53.4 226.2,52.6 226.9,51.8 227.7,51 226.7,52.6 226,54.2 225.6,56
                    225.4,57.8 225.5,59.7 225.7,61.6 226.2,63.4 226.9,65.3 227.8,67.1 228.8,68.9 230,70.5 231.4,72 232.8,73.3 234.4,74.5
                    236,75.5 237.7,76.2 239.3,76.7 240.9,77 242.5,77.2 244.1,77.3 245.8,77.2 247.5,77.1 249.1,76.8 250.7,76.4 252.3,76
                    253.8,75.4 255.2,74.8 256.5,74.1 257.6,73.4 258.7,72.6 259.6,71.7 		"/>
                        </g>
                    </g>
                    </svg>
            </div>

            <div class="front_tree_1">
                <svg version="1.1" id="_x31_plan_tree" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="577 -1203.8 1367 1977.9" style="enable-background:new 577 -1203.8 1367 1977.9;" xml:space="preserve">
                        <style type="text/css">
                            .front_tree_1_st0{opacity:0.6;enable-background:new    ;}
                            .front_tree_1_st1{fill:#172301;}
                            .front_tree_1_st2{fill:#141A01;}
                            .front_tree_1_st3{fill:#3D5603;}
                            .front_tree_1_st4{fill:#476203;}
                            .front_tree_1_st5{fill:#171E01;}
                            .front_tree_1_st6{fill:#1D2A03;}
                            .front_tree_1_st7{fill:#311A03;}
                            .front_tree_1_st8{fill:#1E2B01;}
                            .front_tree_1_st9{fill:#2E3F01;}
                            .front_tree_1_st10{fill:#352B20;}
                            .front_tree_1_st11{fill:#0C0D01;}
                            .front_tree_1_st12{fill:#2E3514;}
                            .front_tree_1_st13{fill:#243501;}
                            .front_tree_1_st14{fill:#1C2301;}
                            .front_tree_1_st15{fill:#283A00;}
                            .front_tree_1_st16{fill:#060702;}
                            .front_tree_1_st17{fill:#253203;}
                            .front_tree_1_st18{fill:#0D1000;}
                            .front_tree_1_st19{fill:#040401;}
                            .front_tree_1_st20{fill:#273704;}
                            .front_tree_1_st21{fill:#3D5600;}
                            .front_tree_1_st22{fill:#111701;}
                            .front_tree_1_st23{fill:#526F01;}
                            .front_tree_1_st24{fill:#532D02;}
                            .front_tree_1_st25{fill:#512A09;}
                            .front_tree_1_st26{fill:#523B2A;}
                            .front_tree_1_st27{fill:#422910;}
                            .front_tree_1_st28{fill:#090701;}
                            .front_tree_1_st29{fill:#121212;}
                            .front_tree_1_st30{fill:#5D3301;}
                            .front_tree_1_st31{fill:#57402D;}
                            .front_tree_1_st32{fill:#6D4611;}
                            .front_tree_1_st33{fill:#0C0601;}
                            .front_tree_1_st34{fill:#4D2A00;}
                            .front_tree_1_st35{fill:#607A00;}
                        </style>
                    <polygon id="shadow" class="front_tree_1_st0" points="1644.8,487.3 833.3,774.1 577,774.1 1582.3,477.9 1658.3,478.3 "/>
                    <g id="tree">
                        <polygon class="front_tree_1_st1" points="1831.7,-917.8 1776.7,-962.5 1792.1,-1056 1743.8,-1143.8 1644.3,-1193.9 1479.6,-1185.2
                1481,-1100.9 1415.7,-1111.1 1399,-990.9 1353.1,-900.6 1348,-790.1 1292.2,-727.5 1303.8,-605 1392.1,-402 1399,-302.2
                1523.3,-253.9 1546.9,-185.8 1578.2,-166.3 1701.8,-240.7 1751.5,-302.2 1827,-335.4 1887.6,-544.2 1930.8,-775.9 	"/>
                        <polygon class="front_tree_1_st2" points="1534.3,-344.5 1571.1,-313.5 1593,-306.8 1596.8,-404.5 	"/>
                        <polygon class="front_tree_1_st1" points="1640.8,-441.5 1637.5,-605 1545.3,-605 1473.8,-573.8 1596.8,-404.5 	"/>
                        <polygon class="front_tree_1_st3" points="1827,-335.4 1808.3,-450 1679.3,-363.8 1751.5,-302.2 	"/>
                        <polygon class="front_tree_1_st4" points="1751.5,-302.2 1799.6,-352.5 1808.3,-450 1827,-335.4 	"/>
                        <polygon class="front_tree_1_st5" points="1461.1,-426.2 1473.6,-573.8 1303.8,-605 1391.9,-402 	"/>
                        <polygon class="front_tree_1_st6" points="1596.8,-404.5 1473.8,-573.8 1534.3,-344.5 	"/>
                        <polygon class="front_tree_1_st7" points="1593,-306.8 1596.8,-404.5 1632.6,-261.9 	"/>
                        <polygon class="front_tree_1_st8" points="1751.5,-302.2 1701.8,-240.7 1678.3,-276 	"/>
                        <polygon class="front_tree_1_st9" points="1798,-605 1755,-534.5 1808.3,-450 1887.6,-544.2 	"/>
                        <polygon class="front_tree_1_st10" points="1596.3,-234 1546.9,-185.8 1578.2,-166.3 	"/>
                        <polygon class="front_tree_1_st11" points="1392.1,-402 1443,-330.8 1461.3,-426.2 	"/>
                        <polygon class="front_tree_1_st8" points="1578.2,-166.3 1701.8,-240.7 1647.8,-230.3 1629.8,-271.5 1596.3,-234 	"/>
                        <polygon class="front_tree_1_st8" points="1443,-330.8 1399,-302.2 1523.3,-253.9 	"/>
                        <polygon class="front_tree_1_st12" points="1392.1,-402 1443,-330.8 1399,-302.2 	"/>
                        <polygon class="front_tree_1_st13" points="1755,-534.5 1808.3,-450 1679.3,-363.8 1692.6,-504.5 	"/>
                        <polygon class="front_tree_1_st8" points="1692.6,-504.5 1679.3,-363.8 1640.3,-441.5 	"/>
                        <polygon class="front_tree_1_st14" points="1692.6,-503.5 1647,-605 1637.5,-605 1640.8,-439.6 	"/>
                        <polygon class="front_tree_1_st15" points="1798,-605 1647,-605 1692.6,-503 1755,-532.2 	"/>
                        <polygon class="front_tree_1_st16" points="1661.8,-293.5 1595.8,-404.5 1630.1,-270.8 	"/>
                        <polygon class="front_tree_1_st17" points="1751.5,-302.2 1661.8,-293.5 1678.3,-276 	"/>
                        <polygon class="front_tree_1_st2" points="1647.8,-230.3 1678.3,-276 1701.8,-240.7 	"/>
                        <polygon class="front_tree_1_st2" points="1523.3,-253.9 1596.3,-234 1546.9,-185.8 	"/>
                        <polygon class="front_tree_1_st18" points="1523.3,-253.9 1571.1,-313.5 1534.3,-344.5 	"/>
                        <polygon class="front_tree_1_st19" points="1596.3,-234 1593,-306.8 1571.1,-313.5 1523.3,-253.9 	"/>
                        <polygon class="front_tree_1_st20" points="1661.8,-293.5 1679.3,-363.8 1751.5,-302.2 	"/>
                        <polygon class="front_tree_1_st2" points="1640.3,-441.5 1595.8,-404.5 1661.8,-293.5 1679.3,-363.8 	"/>
                        <polygon class="front_tree_1_st21" points="1887.6,-544.2 1808.3,-450 1827,-335.4 	"/>
                        <polygon class="front_tree_1_st22" points="1461.3,-426.2 1443,-330.8 1534.3,-344.5 	"/>
                        <polygon class="front_tree_1_st2" points="1534.3,-344.5 1523.3,-253.9 1443,-330.8 	"/>
                        <polygon class="front_tree_1_st2" points="1534.3,-344.5 1473.8,-573.8 1461.3,-426.2 	"/>
                        <polygon class="front_tree_1_st19" points="1647.8,-230.3 1629.8,-271.5 1661.8,-293.5 1678.3,-276 	"/>
                        <polygon class="front_tree_1_st23" points="1838,-440.9 1887.6,-544.2 1827,-335.4 	"/>
                        <polygon class="front_tree_1_st7" points="1582.3,477.1 1578.3,349.6 1591.3,-7.3 1578.3,-97.3 1584.8,-123.8 1578.3,-216.5 1596.3,-234
                1593,-306.8 1629.8,-271.5 1647.3,-257.5 1647.6,-87.8 1652.8,-63.3 1652.8,33.3 1657.6,477.1 1644.4,486.5 1638.4,484.5
                1615.2,490 1590.9,483.4 	"/>
                        <g>
                            <polygon class="front_tree_1_st24" points="1654.3,-63.3 1644.8,154.1 1648.3,-87.8 		"/>
                        </g>
                        <polygon class="front_tree_1_st25" points="1656.3,254.8 1648,386.9 1657.4,379.9 	"/>
                        <polygon class="front_tree_1_st26" points="1644.8,486.5 1658.3,477.1 1657.4,379.9 1648,386.9 	"/>
                        <polygon class="front_tree_1_st7" points="1644.8,154.1 1656.3,254.8 1648,386.9 	"/>
                        <polygon class="front_tree_1_st7" points="1584.8,-123.8 1607.8,23.5 1591.3,-7.3 1578.3,-97.3 	"/>
                        <polygon class="front_tree_1_st27" points="1591,483.4 1596.8,254.8 1624.7,224.1 1615.3,490 	"/>
                        <polygon class="front_tree_1_st28" points="1591.3,-7.3 1607.8,23.5 1596.8,254.8 	"/>
                        <polygon class="front_tree_1_st29" points="1591.3,-7.3 1596.8,254.8 1582.3,477.1 1578.3,349.6 	"/>
                        <polygon class="front_tree_1_st30" points="1622,21.3 1641.3,76.5 1644.8,154.1 1648,386.9 1624.7,224.1 	"/>
                        <polygon class="front_tree_1_st31" points="1615.3,490 1638.6,484.5 1624.7,224.1 	"/>
                        <polygon class="front_tree_1_st27" points="1644.8,486.5 1648,386.9 1624.7,224.1 1638.6,484.5 	"/>
                        <polygon class="front_tree_1_st32" points="1646.8,-87.5 1646.8,-257.5 1629.8,-271.5 1644.3,154.1 	"/>
                        <polygon class="front_tree_1_st33" points="1607.8,23.5 1596.3,-234 1578.3,-216.5 1584.8,-123.8 	"/>
                        <polygon class="front_tree_1_st34" points="1629.8,-271.5 1641.3,76.5 1622,21.3 1599.6,-160.8 1593,-306.8 	"/>
                        <polygon class="front_tree_1_st15" points="1887.6,-544.2 1798,-605 1930.8,-775.9 	"/>
                        <path class="front_tree_1_st3" d="M1792.1-1056c0,0-91.4-46.6-90.1-48.6s41.8-39.2,41.8-39.2L1792.1-1056z"/>
                        <polygon class="front_tree_1_st13" points="1774,-963.9 1718,-929.3 1702,-1104.6 1792.1,-1056 	"/>
                        <polygon class="front_tree_1_st3" points="1831.7,-917.8 1718,-929.3 1774,-963.9 	"/>
                        <polygon class="front_tree_1_st23" points="1841.3,-848.3 1718,-929.3 1831.7,-917.8 	"/>
                        <polygon class="front_tree_1_st13" points="1798,-605 1841.3,-848.3 1930.8,-775.9 	"/>
                        <polygon class="front_tree_1_st5" points="1544,-607.9 1637.5,-605 1482.3,-737.6 1473.8,-573.8 	"/>
                        <polygon class="front_tree_1_st2" points="1292.2,-727.5 1482.3,-737.6 1473.8,-573.8 1303.8,-605 	"/>
                        <polygon class="front_tree_1_st6" points="1482.3,-737.6 1348,-790.1 1292.2,-727.5 	"/>
                        <polygon class="front_tree_1_st2" points="1482.3,-737.6 1451.3,-900.6 1353.1,-900.6 1348,-790.1 	"/>
                        <polygon class="front_tree_1_st6" points="1399,-990.9 1535.3,-892.6 1451.3,-900.6 1353.1,-900.6 	"/>
                        <polygon class="front_tree_1_st5" points="1415.7,-1111.1 1535.3,-892.6 1399,-990.9 	"/>
                        <polygon class="front_tree_1_st14" points="1535.3,-892.6 1647,-605 1482.3,-737.6 1451.3,-900.6 	"/>
                        <polygon class="front_tree_1_st8" points="1535.3,-892.6 1718,-929.3 1647,-605 	"/>
                        <polygon class="front_tree_1_st9" points="1718,-929.3 1647,-605 1798,-605 1841.3,-848.3 	"/>
                        <polygon class="front_tree_1_st9" points="1481,-1100.9 1535.3,-892.6 1415.7,-1111.1 	"/>
                        <polygon class="front_tree_1_st23" points="1702,-1104.6 1617.3,-1157.3 1644.3,-1193.9 1743.8,-1143.8 	"/>
                        <polygon class="front_tree_1_st3" points="1617.3,-1157.3 1479.6,-1185.2 1644.3,-1193.9 	"/>
                        <polygon class="front_tree_1_st9" points="1479.6,-1185.2 1617.3,-1157.3 1481,-1100.9 	"/>
                        <polygon class="front_tree_1_st8" points="1535.3,-892.6 1617.3,-1157.3 1481,-1100.9 	"/>
                        <polygon class="front_tree_1_st35" points="1831.7,-917.8 1930.8,-775.9 1841.3,-848.3 	"/>
                    </g>
                    </svg>
            </div>

            <div class="cat_blue cats">
                <svg version="1.1" id="cat_blue" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="76 -86.3 324.2 328.4" style="transform-origin: center bottom;" xml:space="preserve">
                        <style type="text/css">
                            .cat_blue_st0{opacity:0.6;}
                            .cat_blue_st1{fill:#0E3C9E;}
                            .cat_blue_st2{fill:#487BE1;}
                            .cat_blue_st3{fill:#73A6FF;}
                            .cat_blue_st4{fill:#477BE1;}
                            .cat_blue_st5{opacity:0.5;fill:#2558BE;enable-background:new    ;}
                            .cat_blue_st6{opacity:0.5;fill:#0A3DA3;enable-background:new    ;}
                            .cat_blue_st7{fill:#E3ECFF;}
                            .cat_blue_st8{opacity:0.7;fill:#E3ECFF;enable-background:new    ;}
                            .cat_blue_st9{fill:#FFFFFF;}
                            .cat_blue_st10{fill:#EBE8F4;}
                            .cat_blue_st11{opacity:0.2;fill:#FFFFFF;enable-background:new    ;}
                        </style>
                    <g class="cat_shadow cat_blue_st0">
                        <polygon points="320.6,169 321.7,168.4 322.5,167.9 323.1,167.4 323.5,166.9 323.8,166.5 323.8,166 323.7,165.6 323.4,165.2
                322.9,164.9 322.3,164.6 321.4,164.3 320.4,164.1 319.3,163.9 318,163.8 316.6,163.7 314.8,163.7 314,163.7 313.1,163.7
                312.2,163.7 311.1,163.8 310.2,163.9 309.2,164 308.2,164.1 307.2,164.2 306.2,164.3 305.2,164.4 304.2,164.6 303.1,164.8
                302.2,164.9 301.3,165.1 300.3,165.2 299.3,165.5 296.4,166 293.4,166.5 290.3,167 287.2,167.5 284,167.9 281,168.3 277.7,168.7
                274.5,169.1 271.4,169.4 268.2,169.7 265.4,169.9 262.4,170.1 259.5,170.3 256.8,170.4 254.3,170.4 251.9,170.5 250.3,170.5
                248.9,170.4 247.3,170.3 245.7,170.2 244.2,170.1 242.7,169.9 241.1,169.7 239.4,169.6 238,169.3 236.5,169.1 234.9,168.9
                233.4,168.6 231.9,168.3 230.4,168.1 228.9,167.8 227.4,167.5 225.6,167.2 223.6,166.9 221.7,166.5 219.6,166.2 217.6,165.9
                215.4,165.6 213.3,165.3 211,165 208.8,164.8 206.5,164.5 204.2,164.3 201.7,164.1 199.3,164 196.8,163.8 194.3,163.7 191.8,163.7
                189.6,163.7 187.2,163.7 184.8,163.7 182.5,163.7 180.1,163.8 177.6,163.9 175.1,164 172.5,164.2 169.8,164.4 167,164.5
                164.1,164.8 161,165.1 157.7,165.4 154.3,165.7 150.6,166.1 146.8,166.5 143.4,166.9 140,167.4 136.7,167.8 133.5,168.2
                130.3,168.7 127.1,169.2 123.9,169.7 120.8,170.3 117.8,170.8 114.8,171.3 112,171.8 109.1,172.4 106.4,172.9 103.7,173.5
                101.1,174.1 98.5,174.6 97.8,174.8 96.8,175 96,175.2 95.2,175.4 94.5,175.6 93.6,175.8 92.8,176 92.1,176.3 91.4,176.5
                90.7,176.7 90.1,176.9 89.4,177.2 88.9,177.4 88.2,177.7 87.6,177.9 87,178.2 86.1,178.7 85.3,179.2 84.7,179.7 84.3,180.2
                84.1,180.7 84,181.1 84.1,181.5 84.5,181.9 84.8,182.2 85.6,182.5 86.4,182.8 87.3,183 88.5,183.2 89.8,183.3 91.3,183.4
                92.8,183.5 94.1,183.4 95.2,183.4 96.4,183.3 97.7,183.2 98.9,183.1 100.3,183 101.5,182.9 102.8,182.7 104,182.5 105.3,182.3
                106.6,182 107.7,181.8 109.1,181.5 110.3,181.3 111.6,181 112.6,180.7 114.8,180.2 117.2,179.7 119.8,179.1 122.7,178.6 125.9,178
                129.3,177.4 132.8,176.9 136.6,176.4 140.5,175.9 144.6,175.4 148.7,175 153.1,174.7 157.5,174.4 162.1,174.2 166.7,174 171.3,174
                172.8,174 174.3,174.1 175.9,174.2 177.5,174.3 179.1,174.5 180.5,174.7 182.2,174.9 183.7,175.1 185.2,175.4 186.7,175.6
                188.2,175.9 189.9,176.2 191.3,176.5 192.8,176.7 194.3,177 195.8,177.3 197.9,177.7 200,178 202.2,178.4 204.4,178.8 206.5,179.1
                208.7,179.4 210.9,179.7 213.3,180 215.8,180.2 218.2,180.4 221,180.6 223.7,180.8 226.7,180.8 229.9,180.9 233.3,180.9
                236.8,180.8 239.8,180.8 242.9,180.6 246.1,180.5 249.5,180.3 253.1,180.1 256.9,179.7 260.9,179.4 264.9,179 269.2,178.5
                298.9,174.4 304.5,173.4 305.7,173.2 306.8,173 307.9,172.8 309.1,172.5 310.3,172.3 311.3,172 312.5,171.7 313.5,171.5
                314.6,171.2 315.5,170.9 316.6,170.6 317.4,170.3 318.3,170 319.2,169.6 319.9,169.3 	"/>
                        <polygon id="body_3_" points="230.4,240.3 227.9,240.2 225.7,240.1 223.6,239.8 222.3,239.4 221.3,238.8 220.7,238.3 220.7,237.5
                221,236.8 221,236.8 221,236.8 221,236.8 221,236.8 221,236.8 221.1,236.8 221.1,236.8 221.1,236.8 222.8,234.2 224.7,231.2
                226.6,228.4 228.8,225 229.2,224 229,223 228.3,222.3 227,221.7 225.5,221.3 223.6,221 221.9,220.7 219.8,220.6 177.7,220.6
                175.8,220.7 173.3,220.9 170.4,221.2 167.2,221.6 163.6,222.1 159.7,222.8 155.5,223.7 151.4,224.9 141.5,227.8 130.6,231.1
                120.9,234 111.9,236.7 111.9,236.7 111.9,236.7 111.9,236.7 111.9,236.7 111.9,236.7 111.9,236.7 111.9,236.7 111.9,236.7
                109.1,237.5 106.3,238.2 103.4,238.7 100.3,239.3 97.1,239.7 93.9,240 91,240.2 88.1,240.3 85,240.1 82.3,239.8 80.2,239.4
                79.1,238.7 78.5,237.9 78.7,236.9 79.7,235.9 81.5,234.8 213.6,170 220.3,168.2 227.6,166.6 235.1,165.3 242.8,164.3 250.3,163.6
                257.5,163.3 276.8,163.3 349.1,163.3 370,163.3 375.8,163.6 380.6,164.3 383.9,165.3 385.9,166.6 386.4,168.2 385.7,170
                383.4,172.1 379.8,174.2 258.6,234.8 256,235.9 253,236.9 249.5,237.9 245.7,238.7 241.8,239.4 238,239.8 234.1,240.1 	"/>
                    </g>
                    <g class="cat_body">
                        <g class="cat_tail">
                            <polygon class="cat_blue_st1" points="327.7,149.7 327.7,151.5 327.5,153.1 327.1,154.5 326.5,156 325.9,157.5 325.1,158.8 324.1,160
                323.1,161.2 321.8,162.3 320.6,163.3 319.3,164.1 317.8,164.7 316.4,165.3 314.9,165.7 313.3,165.9 311.5,165.9 310.7,165.9
                309.8,165.9 309,165.8 308,165.5 307.2,165.4 306.4,165.1 305.6,164.9 305,164.5 304.2,164.1 303.5,163.7 302.7,163.3 302,162.7
                301.3,162.3 300.8,161.8 300.1,161.2 299.6,160.6 297.7,159 295.8,157.4 293.8,155.8 291.7,154.3 289.4,152.9 287.1,151.7
                284.7,150.5 282.2,149.5 279.8,148.5 277.2,147.6 274.8,146.9 272.3,146.2 269.7,145.8 267.3,145.4 264.9,145.3 262.6,145.2
                261,145.2 259.4,145.4 257.8,145.6 255.9,146 254.2,146.4 252.3,146.9 250.4,147.4 248.4,148 246.5,148.7 244.5,149.5 242.5,150.1
                240.5,150.9 238.5,151.7 236.5,152.5 234.5,153.3 232.5,154.1 230,155.1 227.4,156.2 224.8,157.2 222.1,158.2 219.5,159.2
                216.6,160.2 214,161.1 211.1,161.9 208.5,162.7 205.7,163.5 203,164.2 200.2,164.7 197.5,165.1 194.8,165.5 192.1,165.8
                189.6,165.9 187.3,166.1 184.9,166.1 182.6,165.9 180.5,165.8 178.2,165.7 175.9,165.3 173.6,165 171.3,164.5 169.1,163.9
                166.7,163.4 164.2,162.6 161.7,161.8 159,160.8 156.3,159.8 153.5,158.6 150.6,157.2 148,156 145.5,154.7 143.1,153.3 140.8,152
                138.5,150.5 136.4,149.1 134.2,147.4 132.2,145.8 130.2,144.2 128.3,142.6 126.6,141 124.8,139.3 123.2,137.7 121.6,135.9
                120.2,134.2 118.7,132.6 118.3,132 117.7,131.4 117.3,130.8 116.9,130.2 116.7,129.5 116.3,128.8 115.9,128.1 115.6,127.5
                115.3,126.8 115.1,126.1 114.9,125.5 114.8,124.7 114.7,124 114.5,123.2 114.4,122.4 114.4,121.6 114.5,120 114.8,118.4
                115.2,116.8 115.7,115.3 116.4,113.9 117.2,112.6 118.1,111.3 119.2,110.2 120.3,109.1 121.6,108.2 123,107.4 124.3,106.7
                125.8,106.2 127.4,105.8 129,105.5 130.6,105.4 131.8,105.5 132.9,105.5 134,105.8 135,106 136,106.4 137,106.8 138,107.2
                138.9,107.8 139.7,108.4 140.7,109 141.5,109.8 142.1,110.5 142.9,111.3 143.6,112.1 144.3,113 144.8,113.8 145.9,115.4 147.2,117
                148.8,118.6 150.6,120.4 152.6,122.1 154.9,123.9 157.3,125.5 160,127.1 162.8,128.7 166,130 169.3,131.2 173,132.3 176.7,133.2
                180.9,133.9 185.1,134.3 189.6,134.4 191.2,134.3 192.8,134.2 194.5,133.9 196.4,133.5 198.3,133 200.2,132.3 202.2,131.8
                204.2,131 206.2,130.2 208.2,129.4 210.2,128.5 212.4,127.7 214.4,126.8 216.4,126 218.4,125.2 220.4,124.4 223.3,123.1 226.2,122
                229.1,120.8 231.9,119.7 234.7,118.6 237.5,117.7 240.4,116.8 243.2,116 246.1,115.3 248.9,114.6 252,114.1 255,113.7 258.2,113.4
                261.4,113.3 264.8,113.3 268.1,113.5 270.9,113.7 273.7,114.1 276.7,114.5 279.6,115.1 282.7,115.8 285.9,116.8 289.1,117.8
                292.4,119 295.7,120.5 299.1,122.1 302.5,123.9 306,125.9 309.7,128.1 313.1,130.6 316.9,133.2 320.5,136.2 321.3,136.9 322,137.5
                322.7,138.2 323.3,139 324,139.7 324.5,140.5 325.2,141.3 325.6,142.1 326.1,143 326.5,143.8 326.9,144.8 327.2,145.7 327.5,146.6
                327.6,147.7 327.7,148.7 	"/>
                            <polygon class="cat_blue_st2" points="217.3,125.6 215.8,126.4 214.5,127.3 212.9,128.7 211.3,130.3 209.7,132 207.8,133.9 206.1,135.9
                204,138.1 202,140.3 199.8,142.5 197.6,144.8 195.2,146.9 192.7,148.9 190.1,150.8 187.4,152.7 184.6,154.1 181.8,155.5
                179.1,156.7 176.7,157.6 174.3,158.6 171.9,159.2 169.7,159.6 167.6,160 165.4,160.3 163.4,160.3 161.4,160.3 159.6,160
                157.7,159.8 155.9,159.4 154.1,158.7 152.3,158 150.6,157.2 148,155.9 145.6,154.5 143.2,153.2 140.9,151.7 138.6,150.3
                136.4,148.8 134.4,147.3 132.3,145.7 130.3,144.1 128.5,142.5 126.6,140.9 124.8,139.3 123.2,137.5 121.6,135.9 120.2,134.2
                118.7,132.6 118.3,132 117.9,131.4 117.5,130.7 117.1,130 116.7,129.4 116.3,128.5 116,127.9 115.7,127.1 115.5,126.4 115.2,125.6
                114.9,124.8 114.8,124.1 114.7,123.5 114.5,122.8 114.4,122.1 114.4,121.6 114.5,120 114.8,118.4 115.2,116.8 115.7,115.3
                116.4,113.9 117.2,112.6 118.1,111.3 119.2,110.2 120.3,109.1 121.6,108.2 123,107.4 124.3,106.7 125.8,106.2 127.4,105.8
                129,105.5 130.6,105.4 131.8,105.5 132.9,105.5 134,105.8 135,106 136,106.4 137,106.8 138,107.2 138.9,107.8 139.7,108.4
                140.7,109 141.5,109.8 142.1,110.5 142.9,111.3 143.6,112.1 144.3,113 144.8,113.8 145.9,115.4 147.2,117 148.8,118.6 150.6,120.4
                152.6,122.1 154.9,123.9 157.3,125.5 160,127.1 162.8,128.7 166,130 169.3,131.2 173,132.3 176.7,133.2 180.9,133.9 185.1,134.3
                189.6,134.4 190.9,134.3 192.4,134.2 194,133.9 195.6,133.6 197.2,133.1 199,132.7 200.7,132.2 202.6,131.5 204.4,130.8
                206.3,130.2 208.1,129.4 209.9,128.7 211.8,127.9 213.7,127.1 215.4,126.4 	"/>
                            <polygon class="cat_blue_st3" points="163.8,138.6 161,138.1 158.3,137.4 155.8,136.5 153.5,135.4 151.4,134.3 149.4,133 147.5,131.6
                145.7,130.2 144.1,128.8 142.7,127.3 141.5,126 140.3,124.7 139.2,123.5 138.2,122.4 137.4,121.4 136.8,120.8 136.5,120.4
                136.1,120.1 135.6,119.7 135.2,119.3 134.6,118.9 134,118.4 133.4,118 132.6,117.6 131.9,117.2 131.3,116.8 130.5,116.5
                129.7,116.2 128.7,116 127.9,115.8 127,115.7 126.2,115.7 125.1,115.8 124,116.1 123,116.5 121.9,117.2 121,117.8 120,118.6
                119.1,119.6 118.3,120.5 117.6,121.7 117.1,122.9 116.7,124.1 116.5,125.5 116.4,126.8 116.7,128.3 116.9,129.6 117.6,131.1
                117.3,130.8 117.1,130.4 116.8,129.9 116.5,129.4 116.3,128.8 116,128.3 115.7,127.6 115.5,126.9 115.2,126.3 115.1,125.6
                114.9,124.9 114.7,124.3 114.7,123.6 114.5,122.9 114.4,122.2 114.4,121.6 114.5,120 114.8,118.4 115.2,116.8 115.7,115.3
                116.4,113.8 117.2,112.5 118.1,111.3 119.2,110.1 120.3,109.1 121.6,108.2 123,107.4 124.3,106.7 125.8,106.2 127.4,105.8
                129,105.5 130.6,105.4 131.8,105.5 132.9,105.5 134,105.8 135,106 136,106.4 137,106.8 138,107.2 138.9,107.8 139.7,108.4
                140.7,109 141.5,109.8 142.1,110.5 142.9,111.3 143.6,112.1 144.3,113 144.8,113.8 145.9,115.4 147.2,117 148.8,118.6 150.6,120.4
                152.4,122.1 154.7,123.7 157.1,125.5 159.8,127.1 162.8,128.5 165.9,129.9 169.2,131.1 172.8,132.2 176.7,133.1 180.7,133.8
                185,134.2 189.6,134.4 188.8,134.8 187.8,135.2 186.8,135.7 185.6,136.1 184.1,136.6 182.6,137 181,137.4 179.3,137.7 177.5,138.1
                175.6,138.3 173.6,138.6 171.7,138.7 169.7,138.9 167.7,138.9 165.7,138.7 	"/>
                        </g>
                        <polygon id="body_2_" class="cat_blue_st4" points="379.6,-68.7 377.1,-68.6 374.5,-68.1 372,-67.3 369.8,-65.9 367.8,-64.3 366.1,-62.6
                364.6,-60.3 363.4,-58 363.4,-58 363.4,-58 363.4,-58 363.4,-58 363.4,-58 363.5,-58 363.5,-58 363.5,-58 360,-50.1 356,-41
                352.3,-32.3 347.8,-22 346.1,-18.7 344.1,-15.9 341.9,-13.6 339.5,-11.9 337.1,-10.6 334.7,-9.6 332.4,-8.8 330.2,-8.4 288.3,-8.4
                286.7,-8.8 284.6,-9.4 282.3,-10.2 279.9,-11.4 277.4,-13 274.9,-15.3 272.7,-18.1 270.8,-21.6 266.8,-30.5 262.5,-40.6
                258.7,-49.6 255.1,-57.9 255.1,-57.9 255.1,-57.9 255.1,-57.9 255.1,-57.9 255.1,-57.9 255.1,-57.9 255.1,-57.9 255.1,-57.9
                253.9,-60.1 252.4,-62.3 250.7,-64 248.7,-65.6 246.4,-67 243.8,-67.9 241.3,-68.5 238.6,-68.7 235.3,-68.3 231.9,-67.4 229,-65.9
                226.4,-63.8 224.3,-61.4 222.7,-58.5 221.6,-55.3 221.2,-52 221.2,-32.8 221.2,-18.7 221.2,-4.7 221.2,9.4 221.2,23.5 221.2,37.4
                221.2,51.5 221.2,65.6 221.2,79.6 221.2,93.7 221.2,107.8 221.2,121.8 221.2,133.8 221.9,140.3 223.7,146.5 226.8,152 230.8,157
                235.7,161 241.3,164.1 247.3,166.2 253.9,167.1 273.1,167.1 344.8,167.1 365.4,167.1 371.8,166.2 377.9,164.2 383.2,161 387.8,157
                391.5,152 394.3,146.5 396.1,140.3 396.8,133.8 396.8,121.8 396.8,108.8 396.8,96 396.8,83.1 396.8,70.3 396.8,57.4 396.8,44.5
                396.8,31.7 396.8,18.8 396.8,5.9 396.8,-7.1 396.8,-19.9 396.8,-32.8 396.8,-52 396.4,-55.3 395.4,-58.5 393.8,-61.4 391.7,-63.9
                389.1,-65.9 386.3,-67.4 383.1,-68.3 	"/>
                        <polygon id="body_light_part_3_" class="cat_blue_st3" points="379.6,-68.7 377.1,-68.6 374.5,-68.1 372,-67.3 369.8,-65.9 367.8,-64.3
                366.1,-62.6 364.6,-60.3 363.4,-58 363.4,-58 363.4,-58 363.4,-58 363.4,-58 363.4,-58 363.5,-58 363.5,-58 363.5,-58 360,-50.1
                356,-41 352.3,-32.3 347.8,-22 346.1,-18.7 344.1,-15.9 341.9,-13.6 339.5,-11.9 337.1,-10.6 334.7,-9.6 332.4,-8.8 330.2,-8.4
                309.7,-8.4 309.7,167.1 344.8,167.1 365.4,167.1 371.8,166.2 377.9,164.2 383.2,161 387.8,157 391.5,152 394.3,146.5 396.1,140.3
                396.8,133.8 396.8,121.8 396.8,108.8 396.8,96 396.8,83.1 396.8,70.3 396.8,57.4 396.8,44.5 396.8,31.7 396.8,18.8 396.8,5.9
                396.8,-7.1 396.8,-19.9 396.8,-32.8 396.8,-52 396.4,-55.3 395.4,-58.5 393.8,-61.4 391.7,-63.9 389.1,-65.9 386.3,-67.4
                383.1,-68.3 	"/>
                        <polygon id="body_light_part_1_" class="cat_blue_st5" points="239.1,-69 241.6,-68.9 244.2,-68.4 246.7,-67.6 248.9,-66.2 250.9,-64.6
                252.6,-62.9 254.1,-60.6 255.3,-58.3 255.3,-58.3 255.3,-58.3 255.3,-58.3 255.3,-58.3 255.3,-58.3 255.2,-58.3 255.2,-58.3
                255.2,-58.3 258.7,-50.4 262.7,-41.3 266.4,-32.6 270.9,-22.3 272.6,-19 274.6,-16.2 276.7,-13.9 279.2,-12.2 281.6,-10.9
                284,-9.9 286.3,-9.1 288.5,-8.4 301.4,-8.4 301.3,0.2 301.3,4.7 301.1,9.4 300.9,15.2 300.6,19.9 300.3,24.6 299.9,28.8
                299.5,32.2 298.9,36.1 298,41 296.7,46.7 295.4,50.9 293.9,54.5 292.5,57.6 290.8,60.5 288.4,63.6 285.2,66.9 282.9,68.7 280,70.6
                276.6,72.4 272.8,74 269.5,75.1 266.5,76 263.2,76.7 259.3,77.2 254,77.3 250.3,77.1 246.8,76.7 243.1,76.2 239,75.5 235.6,74.8
                231.3,73.7 228.3,72.8 224.6,71.6 221.9,70.5 221.9,44.2 221.9,31.4 221.9,18.5 221.9,5.6 221.9,-7.4 221.9,-20.2 221.9,-33.1
                221.9,-52.3 222.3,-55.6 223.3,-58.8 224.9,-61.7 227,-64.2 229.6,-66.2 232.4,-67.7 235.6,-68.6 	"/>
                        <polygon id="shadow_3_" class="cat_blue_st6" points="309.7,167.1 267,167.1 260.6,166.5 254.7,164.5 249.3,161.2 244.8,157.2 241,152.3
                238.4,146.6 236.6,140.5 235.9,133.9 235.9,81 238.1,81.8 244.1,83.5 253,85.3 263.8,86.2 269.7,86.1 275.7,85.3 281.9,84.2
                285.5,83.3 288.6,81.9 291.8,80.4 294.6,78.8 297.3,76.8 299.7,74.7 302.3,72.3 305,69.4 307.2,66.5 309.7,62.6 	"/>
                        <polygon id="light_1_4_" class="cat_blue_st7" points="309.7,-8.4 309.7,-1.7 285.5,-1.7 281.1,-2.3 277.2,-3.7 273.5,-6.3 270,-10
                266.6,-14.9 263.3,-20.7 247.9,-56.4 246.8,-58.4 245.5,-60.3 244,-61.8 242.4,-63 240.5,-63.8 238.6,-64.4 236.7,-64.8
                234.7,-64.8 232.6,-64.7 230.6,-64 228.4,-63.1 226.4,-61.8 224.7,-60 223.2,-58 222,-55.6 221.3,-52.9 221.7,-55.7 222.7,-58.5
                224.1,-61.1 226.3,-63.6 228.8,-65.6 231.8,-67.4 235,-68.3 238.6,-68.7 241.3,-68.5 243.8,-67.8 246.3,-66.8 248.5,-65.6
                250.7,-64 252.4,-62.3 253.9,-60.1 255.1,-57.9 255.6,-56.8 257,-53.6 270.8,-21.3 271.3,-20.5 271.7,-19.5 272.3,-18.7
                272.7,-17.9 273.2,-17.3 273.9,-16.6 274.4,-15.8 274.9,-15.3 276.7,-13.5 278.6,-12 280.7,-10.8 282.8,-9.9 285,-9.2 287.1,-8.8
                289,-8.6 290.6,-8.4 	"/>
                        <polygon id="light_1_5_" class="cat_blue_st8" points="308.5,-8.4 308.5,-1.7 332.6,-1.7 337,-2.3 340.9,-3.7 344.7,-6.3 348.2,-10
                351.5,-14.9 354.9,-20.7 370.3,-56.4 371.3,-58.4 372.7,-60.3 374.2,-61.8 375.8,-63 377.6,-63.8 379.5,-64.4 381.4,-64.8
                383.4,-64.8 385.5,-64.7 387.6,-64 389.7,-63.1 391.7,-61.8 393.5,-60 394.9,-58 396.1,-55.6 396.8,-52.9 396.4,-55.7 395.5,-58.5
                394,-61.1 391.8,-63.6 389.3,-65.6 386.3,-67.4 383.1,-68.3 379.5,-68.7 376.8,-68.5 374.3,-67.8 371.9,-66.8 369.6,-65.6
                367.5,-64 365.7,-62.3 364.2,-60.1 363,-57.9 362.5,-56.8 361.2,-53.6 347.3,-21.3 346.8,-20.5 346.4,-19.5 345.9,-18.7
                345.5,-17.9 344.9,-17.3 344.3,-16.6 343.7,-15.8 343.2,-15.3 341.5,-13.5 339.6,-12 337.4,-10.8 335.3,-9.9 333.1,-9.2 331,-8.8
                329.1,-8.6 327.5,-8.4 	"/>
                        <path id="light_1_1_" class="cat_blue_st9" d="M273.8-6.5l-3.5-3.8l-3.4-4.9l-3.4-6l-15.4-36l-1.1-2l-1.3-1.9l-1.4-1.5l-1.6-1.2l-1.8-0.8
                l-1.9-0.7l-1.9-0.4h-2l-2.1,0.1l-2,0.7l-2.1,0.9l-2,1.4l-1.7,1.8l-1.5,2l-1.2,2.4l-0.7,2.7l0.4-2.8l0.9-2.8l1.5-2.6l2.1-2.6l2.5-2
                l2.9-1.8l3.2-0.9l3.6-0.4l2.7,0.3l2.5,0.7l2.4,0.9l2.3,1.2l2.1,1.6l1.7,1.8l1.5,2.2l1.2,2.3l0.5,1.1l1.3,3.2l13.8,32.6l0.5,0.8
                l0.4,0.9l0.5,0.8l0.4,0.8l0.9,0.8l0.7,0.7l0.5,0.8l0.5,0.5l1.7,1.8l1.9,1.5l2.1,1.2l2.1,0.9l2.1,0.7l2.1,0.4l1.9,0.3l1.6,0.1
                l-1.4,1.3l-2.8,1.7l-3.6,1.3L279-4.3L276.4-5L273.8-6.5L273.8-6.5L273.8-6.5z"/>
                        <polygon id="shadow_on_light_part_3_" class="cat_blue_st2" points="309.7,167.1 352.3,167.1 358.7,166.5 364.6,164.5 370,161.2
                374.5,157.2 378.3,152.3 380.9,146.6 382.7,140.5 383.4,133.9 383.4,81 381.2,81.8 375.2,83.5 366.3,85.3 355.6,86.2 349.6,86.1
                343.6,85.3 337.5,84.2 333.8,83.3 330.7,81.9 327.5,80.4 324.7,78.8 322,76.8 319.6,74.7 317,72.3 314.3,69.4 312.1,66.5
                309.7,62.6 	"/>
                        <polygon id="ear_7_" class="cat_blue_st1" points="234.6,-43.8 234.7,-45 235.1,-46.1 235.8,-47 236.6,-48 237.5,-48.6 238.8,-49.2
                240,-49.4 241.2,-49.6 242.1,-49.4 243.2,-49.2 244.1,-48.8 244.9,-48.2 245.6,-47.6 246.3,-46.7 246.8,-45.8 247.2,-45
                254.4,-21.8 234.6,-21.8 	"/>
                        <polygon id="nose_3_" class="cat_blue_st7" points="312.1,60.2 310.5,60.2 309,60.2 307.5,60.2 305.9,60.2 302.7,59.5 300.1,57.8
                298.3,55.1 297.7,52 297.7,52 297.7,52 297.7,52 297.7,52 298,50.1 299.1,48.7 300.4,47.7 302.3,47.3 305.6,47.3 309,47.3
                312.3,47.3 315.7,47.3 317.6,47.7 318.9,48.7 320,50.1 320.2,52 320.2,52 320.2,52 320.2,52 320.2,52 319.7,55.1 317.8,57.8
                315.3,59.5 	"/>
                        <g class="eye_left">
                            <circle id="eye_left_shadow_3_" class="cat_blue_st1" cx="349.3" cy="47.7" r="25.9"/>
                            <circle class="eyeball cat_blue_st9" cx="352" cy="41" r="25.9"/>
                            <polygon id="eye_left_blick_3_" class="cat_blue_st10" points="375.9,51.1 374.9,53 373.8,54.8 372.8,56.4 371.6,58.1 370.2,59.4
                368.8,60.7 367.3,61.9 365.7,63 364.1,64 362.5,64.8 360.7,65.4 359,66 357.2,66.4 355.5,66.6 353.7,66.8 352.1,66.9 349.5,66.8
                346.9,66.4 344.4,65.7 341.9,64.9 339.7,63.8 337.7,62.5 335.6,61 333.8,59.3 332,57.5 330.6,55.5 329.4,53.4 328.3,51.1
                327.3,48.8 326.7,46.3 326.3,43.7 326.1,41 326.1,40.4 326.1,39.7 326.3,38.9 326.3,38 326.4,36.9 326.5,35.8 326.8,34.7
                327.1,33.7 327.3,32.5 327.7,31.3 328.3,30 328.8,29 329.5,27.8 330.2,26.7 331.1,25.6 332.2,24.6 330.8,26.7 329.9,28.8
                329.4,31.3 329.1,33.7 329.2,36.2 329.5,38.8 330.2,41.2 331.1,43.7 332.3,46.1 333.6,48.5 335.2,50.7 337.1,52.7 339,54.4
                341.1,56 343.3,57.4 345.6,58.3 347.7,59 349.9,59.4 352,59.7 354.1,59.8 356.4,59.7 358.7,59.5 360.8,59.1 363,58.6 365.1,58.1
                367.1,57.3 369,56.4 370.8,55.5 372.2,54.6 373.7,53.5 374.9,52.3 	"/>
                            <circle class="pupil" cx="352" cy="38" r="9.8"/>
                        </g>
                        <g class="eye_right">
                            <circle id="eye_right_shadow_3_" class="cat_blue_st1" cx="262.2" cy="47.7" r="25.9"/>
                            <circle class="eyeball cat_blue_st9" cx="264.9" cy="41" r="25.9"/>
                            <polygon id="eye_right_blick_3_" class="cat_blue_st10" points="288.7,51.1 287.8,53 286.7,54.8 285.7,56.4 284.5,58.1 283.1,59.4
                281.6,60.7 280.2,61.9 278.6,63 277,64 275.3,64.8 273.6,65.4 271.9,66 270.1,66.4 268.4,66.6 266.6,66.8 265,66.9 262.3,66.8
                259.8,66.4 257.2,65.7 254.8,64.9 252.6,63.8 250.5,62.5 248.5,61 246.7,59.3 244.9,57.5 243.4,55.5 242.2,53.4 241.2,51.1
                240.2,48.8 239.6,46.3 239.2,43.7 239,41 239,40.4 239,39.7 239.2,38.9 239.2,38 239.3,36.9 239.4,35.8 239.7,34.7 240,33.7
                240.2,32.5 240.6,31.3 241.2,30 241.7,29 242.4,27.8 243,26.7 244,25.6 245.1,24.6 243.7,26.7 242.8,28.8 242.2,31.3 242,33.7
                242.1,36.2 242.4,38.8 243,41.2 244,43.7 245.2,46.1 246.5,48.5 248.1,50.7 250,52.7 251.9,54.4 254,56 256.2,57.4 258.5,58.3
                260.6,59 262.7,59.4 264.9,59.7 267,59.8 269.3,59.7 271.6,59.5 273.7,59.1 275.9,58.6 278,58.1 280,57.3 281.9,56.4 283.7,55.5
                285.1,54.6 286.6,53.5 287.8,52.3 	"/>
                            <circle class="pupil" cx="266" cy="38" r="9.8"/>
                        </g>
                        <polygon id="ear_6_" class="cat_blue_st2" points="383.4,-43.8 383.1,-45 382.7,-46.1 382,-47 381.1,-48 380.1,-48.6 379.1,-49.2
                377.9,-49.4 376.7,-49.6 375.6,-49.4 374.6,-49.2 373.7,-48.8 372.8,-48.2 372.1,-47.6 371.4,-46.7 370.9,-45.8 370.5,-45
                363.3,-21.8 383.4,-21.8 	"/>
                        <polygon id="blick_3_" class="cat_blue_st9" points="383.4,154.9 385.1,152.3 386.7,148.8 388.3,144.5 389.4,140.7 389.8,136.9 390.1,133
                390.3,128.9 390.3,124.8 390.6,120.2 391.3,115.3 392.3,110.6 393.5,106.6 394.8,103.8 395.7,102.4 396.5,103.4 396.8,106.8
                396.8,111.7 396.8,116.2 396.8,120.4 396.8,124.1 396.8,127.1 396.8,129.5 396.9,133.9 396.6,136.9 396.2,140.5 395.2,144.6
                393.5,149.1 391.1,153.5 387.6,157.9 384.4,161.1 382.6,162.2 381.5,162.2 381.2,161.1 381.3,159.5 381.9,157.9 382.4,156.6
                382.8,155.8 	"/>
                        <polygon class="cat_blue_st11" points="281.2,167.3 274.5,166.8 270.7,165.6 269.6,163.9 270.5,161.9 273.3,159.8 277.3,157.9 282.2,156.4
                287.6,155.6 295.9,153.9 302.3,150.6 307.1,146.1 310.9,140.7 314.1,134.7 317,128.7 320.1,122.9 323.9,117.8 328.7,113.9
                334.4,111.7 340.7,111.2 347.2,112.1 353.3,114.6 358.7,118.4 362.9,123.7 365.5,130.2 368.1,136.5 372,141.3 376.7,144.5
                381.9,146.3 386.9,146.7 391.2,145.9 394.5,143.8 396.1,140.6 395.3,145.5 393.6,150.2 391,154.7 387.6,158.8 383.3,162.2
                378.3,164.9 372.6,166.7 366.2,167.3 356.4,167.3 343.8,167.3 329.8,167.3 315.6,167.3 302.4,167.3 291.5,167.3 284,167.3 	"/>
                        <g id="pow_left_3_">
                            <polygon class="cat_blue_st1" points="292.2,135.2 292,138.6 291.4,141.8 290.6,144.9 289.5,147.8 288.2,150.7 286.6,153.3 284.9,155.9
                    282.7,158.2 280.4,160.2 278,162.1 275.3,163.7 272.5,165 269.6,166.1 266.5,166.9 263.4,167.4 260.1,167.5 256.8,167.4
                    253.6,166.9 250.5,166.1 247.6,165 244.9,163.7 242.2,162.1 239.7,160.2 237.5,158.2 235.4,155.9 233.5,153.3 231.9,150.7
                    230.6,147.8 229.5,144.9 228.7,141.8 228.2,138.6 227.9,135.2 227.9,135.1 228,135 228,134.7 228,134.6 228.2,134.4 228.2,134.2
                    228.2,134 228.2,133.9 228.2,133.8 228.2,133.5 228.2,133.4 228.2,133.2 228.2,133.1 228.2,132.8 228.2,132.7 228.3,132.6
                    228.6,135.7 229.2,138.5 230.2,141.3 231.4,143.8 232.9,146.4 234.5,148.5 236.3,150.7 238.4,152.5 240.6,154.3 243,155.8
                    245.6,157.1 248.3,158.2 251.1,159 254,159.6 257,160 260.1,160.2 263.3,160 266.2,159.6 269.2,159 272,158.2 274.7,157.1
                    277.2,155.8 279.6,154.3 281.8,152.5 283.9,150.7 285.8,148.5 287.4,146.4 288.7,143.8 289.9,141.3 290.9,138.5 291.6,135.7
                    292,132.6 292,132.7 292,132.8 292.1,133.1 292.1,133.2 292.1,133.4 292.1,133.5 292.1,133.8 292.1,133.9 292.1,134 292.1,134.2
                    292.1,134.4 292.1,134.6 292.1,134.7 292.2,135 292.2,135.1 		"/>
                            <polygon class="cat_blue_st1" points="252.4,144.5 252.4,144.5 252.4,144.5 252.4,144.5 252.4,144.5 251.4,144.8 250.5,145.3 249.9,146.2
                    249.7,147.2 249.7,162.5 249.9,163.5 250.5,164.5 251.4,165 252.4,165.1 252.4,165.1 252.4,165.1 252.4,165.1 252.4,165.1
                    253.4,165 254.3,164.5 254.8,163.5 255.1,162.5 255.1,147.2 254.8,146.2 254.3,145.3 253.4,144.8 		"/>
                            <polygon class="cat_blue_st1" points="267.8,144.5 267.8,144.5 267.8,144.5 267.8,144.5 267.8,144.5 266.8,144.8 266,145.3 265.4,146.2
                    265.2,147.2 265.2,162.5 265.4,163.5 266,164.5 266.8,165 267.8,165.1 267.8,165.1 267.8,165.1 267.8,165.1 267.8,165.1
                    268.9,165 269.7,164.5 270.2,163.5 270.5,162.5 270.5,147.2 270.2,146.2 269.7,145.3 268.9,144.8 		"/>
                        </g>
                        <polygon id="light_2_4_" class="cat_blue_st7" points="390.1,-52 390.1,133.9 389.5,140.5 387.8,146.6 385.1,152.3 381.5,157.2 376.9,161.2
                371.7,164.5 365.9,166.5 359.5,167.1 357.2,167.1 354.9,167.1 352.7,167.1 350.5,167.1 348.2,167.1 346,167.1 343.7,167.1
                341.5,167.1 354.8,165.9 365,162.9 372.4,158.3 377.5,152.7 380.7,146.5 382.4,140.2 383.2,134.3 383.4,129.4 383.4,-47.3
                383.4,-49.8 383.4,-52.8 383.1,-55.7 382.6,-58.5 381.6,-61.1 380.1,-63.2 378,-64.6 375.1,-65.1 378,-65.4 380.9,-65.1
                383.5,-64.2 385.6,-62.7 387.5,-60.7 388.9,-58.1 389.8,-55.2 	"/>
                        <polygon class="cat_blue_st11" points="397.8,137.5 391.6,136.9 386,135.3 381.1,132.8 376.8,129.5 373.3,125.5 370.8,120.9 369.2,115.9
                368.6,110.4 369.2,105 370.8,99.9 373.3,95.3 376.8,91.3 381.1,88 386,85.5 391.6,83.9 397.8,83.4 397.8,90.1 397.8,95.9
                397.8,101.3 397.8,106.7 397.8,112.6 397.8,119.4 397.8,127.5 	"/>
                        <polygon id="light_3_5_" class="cat_blue_st9" points="396.8,-52 396.8,133.9 396.2,140.5 394.5,146.6 391.8,152.3 388.2,157.2 383.6,161.2
                378.4,164.5 372.6,166.5 366.2,167.1 365.3,167.1 364.3,167.1 363.4,167.1 362.5,167.1 361.5,167.1 360.6,167.1 359.6,167.1
                358.7,167.1 367.3,165.7 374.1,163 379.9,158.7 383.9,153.3 386.8,147.2 388.7,140.9 389.8,134.8 390.1,129.4 390.1,-47.3
                390.1,-49.6 390.1,-51.6 390.1,-53.3 389.9,-55.1 389.5,-56.7 388.9,-58.3 387.9,-59.9 386.6,-61.6 384.7,-63.2 382.2,-64.3
                379.5,-65 376.4,-65.2 373.3,-65 370.4,-64.3 367.5,-63 365.3,-61.2 367,-63.4 368.8,-65.1 370.5,-66.4 372.4,-67.4 374.4,-68.1
                376.3,-68.5 378.1,-68.7 380,-68.7 383.5,-68.3 386.6,-67.3 389.4,-65.8 391.9,-63.8 393.9,-61.2 395.4,-58.4 396.5,-55.3 	"/>
                        <g id="pow_right_3_">
                            <polygon class="cat_blue_st1" points="390.1,135.2 389.8,138.6 389.3,141.8 388.5,144.9 387.4,147.8 386,150.7 384.4,153.3 382.7,155.9
                    380.5,158.2 378.3,160.2 375.9,162.1 373.2,163.7 370.4,165 367.4,166.1 364.3,166.9 361.2,167.4 357.9,167.5 354.7,167.4
                    351.5,166.9 348.4,166.1 345.4,165 342.8,163.7 340.1,162.1 337.5,160.2 335.4,158.2 333.2,155.9 331.4,153.3 329.8,150.7
                    328.4,147.8 327.3,144.9 326.5,141.8 326,138.6 325.7,135.2 325.7,135.1 325.9,135 325.9,134.7 325.9,134.6 326,134.4 326,134.2
                    326,134 326,133.9 326,133.8 326,133.5 326,133.4 326,133.2 326,133.1 326,132.8 326,132.7 326.1,132.6 326.4,135.7 327.1,138.5
                    328,141.3 329.2,143.8 330.7,146.4 332.3,148.5 334.2,150.7 336.2,152.5 338.5,154.3 340.9,155.8 343.4,157.1 346.1,158.2
                    348.9,159 351.9,159.6 354.8,160 357.9,160.2 361.1,160 364.1,159.6 367,159 369.8,158.2 372.5,157.1 375.1,155.8 377.5,154.3
                    379.6,152.5 381.8,150.7 383.6,148.5 385.2,146.4 386.6,143.8 387.8,141.3 388.7,138.5 389.4,135.7 389.8,132.6 389.8,132.7
                    389.8,132.8 389.9,133.1 389.9,133.2 389.9,133.4 389.9,133.5 389.9,133.8 389.9,133.9 389.9,134 389.9,134.2 389.9,134.4
                    389.9,134.6 389.9,134.7 390.1,135 390.1,135.1 		"/>
                            <polygon class="cat_blue_st1" points="350.3,144.5 350.3,144.5 350.3,144.5 350.3,144.5 350.3,144.5 349.2,144.8 348.4,145.3 347.7,146.2
                    347.6,147.2 347.6,162.5 347.7,163.5 348.4,164.5 349.2,165 350.3,165.1 350.3,165.1 350.3,165.1 350.3,165.1 350.3,165.1
                    351.2,165 352.1,164.5 352.7,163.5 352.9,162.5 352.9,147.2 352.7,146.2 352.1,145.3 351.2,144.8 		"/>
                            <polygon class="cat_blue_st1" points="365.7,144.5 365.7,144.5 365.7,144.5 365.7,144.5 365.7,144.5 364.6,144.8 363.8,145.3 363.3,146.2
                    363,147.2 363,162.5 363.3,163.5 363.8,164.5 364.6,165 365.7,165.1 365.7,165.1 365.7,165.1 365.7,165.1 365.7,165.1 366.7,165
                    367.5,164.5 368.1,163.5 368.3,162.5 368.3,147.2 368.1,146.2 367.5,145.3 366.7,144.8 		"/>
                        </g>
                    </g>
                    </svg>
            </div>

        </div>

    </div>

</div>

<script src="/libs/js/parallax.min.js"></script>
<script>
    const PAGE = 'index';
    const PAGE_VERSION = 'pc';
    let scene = document.getElementById('scene');
    let parallaxInstance = new Parallax(scene);
</script>
<script src="/libs/js/anime.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/index.js"></script>
</body>
</html>