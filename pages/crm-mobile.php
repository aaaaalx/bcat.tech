<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/icons/favicon_32x32.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="img/icons/favicon_64x64.ico" type="image/x-icon" sizes="64x64">
    <title>Black cats</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/style-mobile.css">
</head>
<body class="main-page">

<div class="loader">
    <img class="loader-icon" src="/img/ring-loader.gif">
</div>

<header class="controls">
    <div class="logo">
        <a href="<?= to('/'); ?>"><img src="/img/menu-icons/logo.png" alt="logo"></a>
    </div>

    <div class="menu"></div>

    <div class="menu-wrap hidden">
        <span class="button-close"></span>
        <div class="lang-mobile">
            <ul>
                <?php foreach($langsList as $key => $lang): ?>
                    <?php
                    $activeItem = '';
                    if($currentLang == $lang){
                        $activeItem = ' class="active"';
                    }
                    ?>
                    <li<?= $activeItem; ?>><a href="<?= to($key); ?>"><?= $lang; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <nav class="main-menu-min">
            <ul>
                <li><a href="<?= to('app'); ?>"><?= getTranslate('app development'); ?></a></li>
                <li><a href="<?= to('promotion'); ?>"><?= getTranslate('promotion'); ?></a></li>
                <li><a href="<?= to('web'); ?>"><?= getTranslate('web development'); ?></a></li>
                <li><a href="<?= to('crm'); ?>"><?= getTranslate('CRM'); ?></a></li>
                <li><a href="#"><?= getTranslate('Your "ninja" project'); ?></a></li>
                <li><a href="<?= to('design'); ?>"><?= getTranslate('Design'); ?></a></li>
                <li><a href="#"><?= getTranslate('Game development'); ?></a></li>
                <li><a href="<?= to('outsource-and-outstaff'); ?>"><?= getTranslate('Outsource & outstaff'); ?></a></li>
            </ul>
        </nav>
        <div class="buttons">
            <ul>
                <li><a href="//blog.bcat.tech" target="_blank"><?= getTranslate('blog'); ?></a></li>
                <li><a href="#"><?= getTranslate('projects'); ?></a></li>
                <li><a href="<?= to('contacts'); ?>"><?= getTranslate('Contact us'); ?></a></a></li>
            </ul>
        </div>
    </div>

    <div class="button-top-right">
        <div class="item-wrap">
            <a href="<?= to('contacts'); ?>"><?= getTranslate('Contact us'); ?></a>
        </div>
    </div>

</header>

<div class="body-wrap">

    <div class="title">
        <h1>Promote your business</h1>
    </div>

    <div class="content-svg">
        <object type="image/svg+xml" data="/img/svg/crm_page/crm-mobile.svg"></object>
    </div>

    <div class="content-mobile">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
    </div>

    <div class="content-bottom">
        <a class="button-center" href="<?= to('/'); ?>"><div class="item-wrap"><?= getTranslate('Back to home page'); ?></div></a>
    </div>

</div>

<script>
    const PAGE = 'crm';
    const PAGE_VERSION = 'mobile';
</script>
<script src="/libs/js/anime.min.js"></script>
<script type="text/javascript" src="/libs/js/jquery-3.1.1.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/crm-mobile.js"></script>
</body>
</html>