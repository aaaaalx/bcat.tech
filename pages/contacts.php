<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/icons/favicon_32x32.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="img/icons/favicon_64x64.ico" type="image/x-icon" sizes="64x64">
    <title>Black cats</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/contacts.css">
</head>
<body>

<div class="loader">
    <img class="loader-icon" src="/img/ring-loader.gif">
</div>

<header class="controls">
    <div class="logo">
        <a href="<?= to('/'); ?>"><img src="/img/menu-icons/logo.png" alt="logo"></a>
    </div>
    <div class="lang">
        <nav class="lang-nav">
            <div class="lang-nav-wrap">
                <span class="selected-lang"><?= $currentLang; ?></span>
                <ul class="hidden-menu">
                    <?php foreach($langsList as $key => $lang): ?>
                        <?php if($currentLang !== $lang): ?>
                            <li><a href="<?= to($key); ?>"><?= $lang; ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <span class="arrow-icon"><img src="/img/arrow-icon.png"></span>
            </div>
        </nav>
    </div>
    <div class="path">
        <a href="<?= to('/'); ?>">Home</a>
        <span>/</span>
        <span><?= getTranslate('Contacts us'); ?></span>
    </div>
    <div class="button-bottom-left">
        <div class="item-wrap">
            <a href="//blog.bcat.tech" target="_blank"><?= getTranslate('blog'); ?></a>
        </div>
    </div>
    <div class="button-top-right">
        <div class="item-wrap">
            <a href="<?= to('/'); ?>"><?= getTranslate('Back to home'); ?></a>
        </div>
    </div>
    <div class="button-bottom-right">
        <div class="item-wrap">
            <a href="#"><?= getTranslate('projects'); ?></a>
        </div>
    </div>
    <div class="button-bottom-center">
        <a data-modal-target="form" href="#"><div class="item-wrap"><?= getTranslate('drop us a line'); ?></div></a>
    </div>
</header>

<div class="body-wrap">

    <div class="contacts-bg">

        <div class="map">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="654 -144 1920 1080" style="enable-background:new 654 -144 1920 1080;" xml:space="preserve">
	            <style type="text/css">
                    .map_st0{opacity:0.77;}
                    .map_st1{fill:#ED9329;}
                    .map_st2{fill:#D15F40;}
                    .map_st3{fill:#B96695;}
                    .map_st4{fill:#8CAF2A;}
                    .map_st5{fill:#A9DC0E;}
                    .map_st6{fill:#F1A535;}
                    .map_st7{fill:#F9CE61;}
                    .map_st8{fill:#85A811;}
                    .map_st9{fill:#8BAA07;}
                    .map_st10{fill:#D3EC81;}
                    .map_st11{fill:#A0D123;}
                    .map_st12{fill:#FDDA4B;}
                    .map_st13{fill:#D3ED82;}
                    .map_st14{fill:#C2E559;}
                    .map_st15{fill:#B54671;}
                    .map_st16{fill:#A84683;}
                    .map_st17{fill:#C86F8D;}
                    .map_st18{fill:#F8CB4D;}
                    .map_st19{fill:#FAD78E;}
                    .map_st20{fill:#F4BF5D;}
                    .map_st21{fill:#F2B04A;}
                    .map_st22{fill:#F6C15D;}
                    .map_st23{fill:#F7C779;}
                    .map_st24{fill:#FAD264;}
                    .map_st25{fill:#F9D166;}
                    .map_st26{fill:#F5B632;}
                    .map_st27{fill:#EA9F5F;}
                    .map_st28{fill:#EBB288;}
                    .map_st29{fill:#E7A269;}
                    .map_st30{fill:#E9B392;}
                    .map_st31{fill:#EAAE6D;}
                    .map_st32{fill:#F1C16C;}
                    .map_st33{fill:#E6A76E;}
                    .map_st34{fill:#E2A792;}
                    .map_st35{fill:#E7A96C;}
                    .map_st36{fill:#E7AB64;}
                    .map_st37{fill:#E6AE85;}
                    .map_st38{fill:#F6C787;}
                    .map_st39{fill:#F1B170;}
                    .map_st40{fill:#EB9C54;}
                    .map_st41{fill:#EC9A50;}
                    .map_st42{fill:#EDB287;}
                    .map_st43{fill:#E8A174;}
                    .map_st44{fill:#E79C73;}
                    .map_st45{fill:#F1AB4E;}
                    .map_st46{fill:#F2A24B;}
                    .map_st47{fill:#F2B275;}
                    .map_st48{fill:#D97E6C;}
                    .map_st49{fill:#E68543;}
                    .map_st50{fill:#F3A64E;}
                    .map_st51{fill:#E38C5D;}
                    .map_st52{fill:#F2AB57;}
                    .map_st53{fill:#C44B54;}
                    .map_st54{fill:#ED963A;}
                    .map_st55{fill:#EFA349;}
                    .map_st56{fill:#DB7B5D;}
                    .map_st57{fill:#C86E90;}
                    .map_st58{fill:#BE4A6B;}
                    .map_st59{fill:#BD5281;}
                    .map_st60{fill:#CD7795;}
                    .map_st61{fill:#C6688D;}
                    .map_st62{fill:#C35A7E;}
                    .map_st63{fill:#D0819F;}
                    .map_st64{fill:#C86A8C;}
                    .map_st65{fill:#CA708E;}
                    .map_st66{fill:#D3869D;}
                    .map_st67{fill:#CF81A0;}
                    .map_st68{fill:#C877A0;}
                    .map_st69{fill:#AF3D81;}
                    .map_st70{fill:#AA3279;}
                    .map_st71{fill:#C5596F;}
                    .map_st72{fill:#AD3070;}
                    .map_st73{fill:#AB4490;}
                    .map_st74{fill:#B85C9B;}
                    .map_st75{fill:#A74F9B;}
                    .map_st76{fill:#B56CAE;}
                    .map_st77{fill:#99368F;}
                    .map_st78{fill:#B14084;}
                    .map_st79{fill:#BB4C77;}
                    .map_st80{fill:#CF5D53;}
                    .map_st81{fill:#EC9D57;}
                    .map_st82{fill:#AA72B8;}
                    .map_st83{fill:#A262AF;}
                    .map_st84{fill:#9D73C0;}
                    .map_st85{fill:#9888D3;}
                    .map_st86{fill:#B0B5EC;}
                    .map_st87{fill:#AC9FDC;}
                    .map_st88{fill:#C86B7D;}
                    .map_st89{fill:#C96D7E;}
                    .map_st90{fill:#F1B35F;}
                    .map_st91{fill:#F2B35F;}
                    .map_st92{fill:#F3B45D;}
                    .map_st93{fill:#BC5B82;}
                    .map_st94{fill:#C56684;}
                    .map_st95{fill:#C083AF;}
                    .map_st96{fill:#C36A8D;}
                    .map_st97{fill:#C25772;}
                    .map_st98{fill:#AC6AA8;}
                    .map_st99{fill:#A062AA;}
                    .map_st100{fill:#B36BA2;}
                    .map_st101{fill:#9A74BF;}
                    .map_st102{fill:#AEA1DB;}
                    .map_st103{fill:#998DD6;}
                    .map_st104{fill:#B75C90;}
                    .map_st105{fill:#CD6C7E;}
                    .map_st106{fill:#C15578;}
                    .map_st107{fill:#C66986;}
                    .map_st108{fill:#CA6D86;}
                    .map_st109{fill:#C96C87;}
                    .map_st110{fill:#C56787;}
                    .map_st111{fill:#C56688;}
                    .map_st112{fill:#B64F88;}
                    .map_st113{fill:#EBA55C;}
                    .map_st114{fill:#EFAC5D;}
                    .map_st115{fill:#C65D6F;}
                    .map_st116{fill:#B5DF3F;}
                    .map_st117{fill:#B7E03C;}
                    .map_st118{fill:#BBE338;}
                    .map_st119{fill:#BEE63C;}
                    .map_st120{fill:#CEEC6A;}
                    .map_st121{fill:#D6EF80;}
                    .map_st122{fill:#BAE334;}
                    .map_st123{fill:#CFEC71;}
                    .map_st124{fill:#B7E038;}
                    .map_st125{fill:#CBE874;}
                    .map_st126{fill:#BBE146;}
                    .map_st127{fill:#BDE150;}
                    .map_st128{fill:#BBE432;}
                    .map_st129{fill:#CEEB6E;}
                    .map_st130{fill:#B7E03A;}
                    .map_st131{fill:#B2E022;}
                    .map_st132{fill:#B8E13E;}
                    .map_st133{fill:#C7E959;}
                    .map_st134{fill:#BAE338;}
                    .map_st135{fill:#AAD62E;}
                    .map_st136{fill:#B4DA4D;}
                    .map_st137{fill:#B4DA50;}
                    .map_st138{fill:#BAD963;}
                    .map_st139{fill:#BBD668;}
                    .map_st140{fill:#B1CF6B;}
                    .map_st141{fill:#B0CF6A;}
                    .map_st142{fill:#ACC967;}
                    .map_st143{fill:#94B53A;}
                    .map_st144{fill:#A5C159;}
                    .map_st145{fill:#BBD075;}
                    .map_st146{fill:#ABC250;}
                    .map_st147{fill:#B5CD62;}
                    .map_st148{fill:#B6CE69;}
                    .map_st149{fill:#D6E4A3;}
                    .map_st150{fill:#D6E199;}
                    .map_st151{fill:#C4D15E;}
                    .map_st152{fill:#CFDE85;}
                    .map_st153{fill:#ABC145;}
                    .map_st154{fill:#AAC03C;}
                    .map_st155{fill:#D2D54F;}
                    .map_st156{fill:#E1E596;}
                    .map_st157{fill:#DCE287;}
                    .map_st158{fill:#E3EBA9;}
                    .map_st159{fill:#DFE37B;}
                    .map_st160{fill:#E2E796;}
                    .map_st161{fill:#E7E576;}
                    .map_st162{fill:#EEECA0;}
                    .map_st163{fill:#DBDA55;}
                    .map_st164{fill:#CCD046;}
                    .map_st165{fill:#B4C124;}
                    .map_st166{fill:#B6C53C;}
                    .map_st167{fill:#B3C759;}
                    .map_st168{fill:#B8CD7A;}
                    .map_st169{fill:#A8BE44;}
                    .map_st170{fill:#9EBC44;}
                    .map_st171{fill:#A1BE56;}
                    .map_st172{fill:#FDE67B;}
                    .map_st173{fill:#FCDD49;}
                    .map_st174{fill:#FDDB46;}
                    .map_st175{fill:#FDD942;}
                    .map_st176{fill:#FDD844;}
                    .map_st177{fill:#FCDF4E;}
                    .map_st178{fill:#FCE272;}
                    .map_st179{fill:#FBE057;}
                    .map_st180{fill:#FCE685;}
                    .map_st181{fill:#FDE995;}
                    .map_st182{fill:#FDE278;}
                    .map_st183{fill:#FDE27F;}
                    .map_st184{fill:#FDD943;}
                    .map_st185{fill:#FAE060;}
                    .map_st186{fill:#F9E266;}
                    .map_st187{fill:#F9E062;}
                    .map_st188{fill:#FBEC9E;}
                    .map_st189{fill:#F9E36F;}
                    .map_st190{fill:#FAEFAA;}
                    .map_st191{fill:#FAD958;}
                    .map_st192{fill:#FCD348;}
                    .map_st193{fill:#FDD43F;}
                    .map_st194{fill:#FDD441;}
                    .map_st195{fill:#FDE280;}
                    .map_st196{fill:#FAE158;}
                    .map_st197{fill:#FAE155;}
                    .map_st198{fill:#FBDF4C;}
                    .map_st199{fill:#EEE466;}
                    .map_st200{fill:#ECE155;}
                    .map_st201{fill:#F2E76D;}
                    .map_st202{fill:#F1E770;}
                    .map_st203{fill:#F4E565;}
                    .map_st204{fill:#F4E45F;}
                    .map_st205{fill:#F6E468;}
                    .map_st206{fill:#F6E560;}
                    .map_st207{fill:#F6DB31;}
                    .map_st208{fill:#F5E352;}
                    .map_st209{fill:#F3E252;}
                    .map_st210{fill:#FDDA3A;}
                    .map_st211{fill:#FDDA37;}
                    .map_st212{fill:#FEE46C;}
                    .map_st213{fill:#FDDB3B;}
                    .map_st214{fill:#FDE77E;}
                    .map_st215{fill:#FDEA8D;}
                    .map_st216{fill:#F9E144;}
                    .map_st217{fill:#E5DA2D;}
                    .map_st218{fill:#E9E36C;}
                    .map_st219{fill:#E9DB32;}
                    .map_st220{fill:#F4D918;}
                    .map_st221{fill:#F7D919;}
                    .map_st222{fill:#E3DA3A;}
                    .map_st223{fill:#EDE049;}
                    .map_st224{fill:#EDD920;}
                    .map_st225{fill:#AE4C90;}
                    .map_st226{fill:#A756A0;}
                    .map_st227{fill:#A45AA7;}
                    .map_st228{fill:#A756A1;}
                    .map_st229{fill:#BD5B88;}
                    .map_st230{fill:#D88440;}
                    .map_st231{fill:#EBAA47;}
                    .map_st232{fill:#E69E4C;}
                    .map_st233{fill:#BF4F60;}
                    .map_st234{fill:#B84362;}
                    .map_st235{fill:#BF505C;}
                    .map_st236{fill:#CD6C57;}
                    .map_st237{fill:#E79847;}
                    .map_st238{fill:#F3BC47;}
                    .map_st239{fill:#F5BC47;}
                    .map_st240{fill:#F6BD44;}
                    .map_st241{fill:#F6CB3E;}
                    .map_st242{fill:#F7C13A;}
                    .map_st243{fill:#EF9A2E;}
                    .map_st244{fill:#F0A236;}
                    .map_st245{fill:#CC5953;}
                    .map_st246{fill:#DD6B32;}
                    .map_st247{fill:#E37A2E;}
                    .map_st248{fill:#BB384C;}
                    .map_st249{fill:#C34444;}
                    .map_st250{fill:#AE2556;}
                    .map_st251{fill:#EF9D38;}
                    .map_st252{fill:#EE9B34;}
                    .map_st253{fill:#EF952B;}
                    .map_st254{fill:#EEA03B;}
                    .map_st255{fill:#EC9D38;}
                    .map_st256{fill:#B12952;}
                    .map_st257{fill:#C74D41;}
                    .map_st258{fill:#BB3A4C;}
                    .map_st259{fill:#BA3A50;}
                    .map_st260{fill:#9B166F;}
                    .map_st261{fill:#991671;}
                    .map_st262{fill:#90338F;}
                    .map_st263{fill:#942B85;}
                    .map_st264{fill:#AD326C;}
                    .map_st265{fill:#AB316D;}
                    .map_st266{fill:#B33E6D;}
                    .map_st267{fill:#C04F6D;}
                    .map_st268{fill:#B9466A;}
                    .map_st269{fill:#C2516A;}
                    .map_st270{fill:#A03981;}
                    .map_st271{fill:#BA4263;}
                    .map_st272{fill:#B8456B;}
                    .map_st273{fill:#B63D66;}
                    .map_st274{fill:#8E53AA;}
                    .map_st275{fill:#8D5CB2;}
                    .map_st276{fill:#9755A2;}
                    .map_st277{fill:#A25094;}
                    .map_st278{fill:#9B5BA4;}
                    .map_st279{fill:#B24779;}
                    .map_st280{fill:#AC5691;}
                    .map_st281{fill:#A5236B;}
                    .map_st282{fill:#8948A7;}
                    .map_st283{fill:#7A88E4;}
                    .map_st284{fill:#F8BC28;}
                    .map_st285{fill:#F6B527;}
                    .map_st286{fill:#F2AF31;}
                    .map_st287{fill:#F5B929;}
                    .map_st288{fill:#F1B438;}
                    .map_st289{fill:#EFAE3A;}
                    .map_st290{fill:#E99E3A;}
                    .map_st291{fill:#E79D3C;}
                    .map_st292{fill:#E28D3B;}
                    .map_st293{fill:#ABDE06;}
                    .map_st294{fill:#AADD06;}
                    .map_st295{fill:#B2E025;}
                    .map_st296{fill:#A6D80D;}
                    .map_st297{fill:#ADDF05;}
                    .map_st298{fill:#AAD922;}
                    .map_st299{fill:#A5D51A;}
                    .map_st300{fill:#A1D029;}
                    .map_st301{fill:#A7D42A;}
                    .map_st302{fill:#95B93C;}
                    .map_st303{fill:#AABF2E;}
                    .map_st304{fill:#B2C225;}
                    .map_st305{fill:#A3B715;}
                    .map_st306{fill:#8EAD1B;}
                    .map_st307{fill:#8AAA0B;}
                    .map_st308{fill:#85A918;}
                    .map_st309{fill:#C4C619;}
                    .map_st310{fill:#B2BE15;}
                    .map_st311{fill:#CED035;}
                    .map_st312{fill:#D9D328;}
                    .map_st313{fill:#D0DB58;}
                    .map_st314{fill:#DEE269;}
                    .map_st315{fill:#E2E269;}
                    .map_st316{fill:#9ABE46;}
                    .map_st317{fill:#F7DC4C;}
                    .map_st318{fill:#F9DA3C;}
                    .map_st319{fill:#FBCD23;}
                    .map_st320{fill:#FDCC12;}
                    .map_st321{fill:#FDD21A;}
                    .map_st322{fill:#FCD41A;}
                    .map_st323{fill:#FBD728;}
                    .map_st324{fill:#FCD71C;}
                    .map_st325{fill:#FAD929;}
                    .map_st326{fill:#FDD20D;}
                    .map_st327{fill:#FCD30D;}
                    .map_st328{fill:#FAD613;}
                    .map_st329{fill:#FAD812;}
                    .map_st330{fill:#F7D813;}
                    .map_st331{fill:#F6D812;}
                    .map_st332{fill:#D9CD15;}
                    .map_st333{fill:#E0D41E;}
                    .map_st334{fill:#ECD514;}
                    .map_st335{fill:#EED616;}
                    .map_st336{fill:#E9D71C;}
                </style>
                <g id="map" class="map_st0">
                    <polygon class="map_st1" points="1797,394.4 1776.7,384.4 1736.7,412.9 1750.7,424.4 	"/>
                    <path class="map_st2" d="M2359.9,241l0.1-0.1l-64.5-4.1l-34.8,11.9l-45.8-32.5l-99.1,13.8l-68.7-27.8l-49,8.5l25.3-9.9l-34.5-13.3
			h-18.5l-8.8,2.5l2,4.4l-9.8-2.1l-3.8,3.3l-12.1-0.3l-17.5,4.6l-12.6-1.8l-6.8,7.2h-6.8l-2.8,1.2l-13.9,5l7.1,9.7l5.8-2.6l13.3,6.7
			l-0.1,0.1l57.8,10.5l-76.8,7.3l-13.5-1.8l-12-5.5l4.8,9l-7.3,1.1l-3.3,4.6l-2.6-3.4l-2.7-1.9h-8l-5.3-5.3l-101-7.8l-47.2,30h-9.3
			l-1-7.2l5.2-6.9l9.4-8.1l-18.4-7.8l-63,8.4l-19,10.5l-33.9,9.7l-16.8,20.5l13.4,1.8l12.9-1.8l-2,14.5l18.3-2.1l16-17.1l9.2-8.1
			l7.6-0.7l-9.6,13.5l18.3-8.5l39.6,4.1l-0.7,0.4h0.1l-9.9,4l-6.3-1.5l-17,1.6l11.2,3.4l-2.2,5.6l-6.3,0.9l-3.5-2h-8.4l0.8,2.3
			l-2.5,5.3l-7.4,5l-57.1,5.9l0.6-0.4l4.8-9.8l-2.4-6.3l-19.4,16l-28,25.6l-4,2.4l-4.1,3.5l-9,3.3l4.3,6.4l-3.3,8.6l-11,11.2
			l55.8-10.3l-2.2-3.2l5.3-5.1l11.8-1.2l22.5-1.5l2.8,12.8l10.1-4l1.9-8.8l21.9,8.7l2.3,2.8l-1.2,6.2l6.2-6.8l-4-4.9l7.3,1l-20.3-7.9
			l-3.3-5.4l1.2-5.8l63.8,9.7l-7.3,8.6l7.9,1.1l0.5,3.9l-1.5,0.8l3.5,1.1l0.8,3.3v1.3l5.8-0.7l4,2.7l6.7,11.7l-22.9,7.1l-18.5-4.3
			l-9.3,2.8l-3.3,8l-13.8-3.5l-4.4-2.8l-13.6-2l-12.5-3.3l5.4-5.8l-3.8-3.4l1.4-11.3l-5.7-0.2l-5,0.3l-10.5,2.3l-36.5,6.2h-49
			l-45.5,23l20,35.5l52,0.5l44,19l-2.5,28l25.5,16.5l51.5,29.5l-23.3,43.3l25.5,17.5h30l9.5-15l31-39l22,3l18.5-12l-10.3-22.5l-2-9.6
			l16.3-10.9h0.5l31-50.8l-55.5,0.1l-13.5-11.8l-19.8-18.5l-2.6-15.6l51.4,34.1l33.7-24l-19.7-8.3l3.2-4.1l19.9-0.6l49.5,4.7l18,10.8
			l32.9-2.5v-0.1l63.6-4.4l64,11l5.3,25.5l53.8,6.8l2,29.5l22,28.8l-6.3-55.5l39.6-13l-33.5-12.9l55.8-27.9l-1.5-13.9l1.4-4.6
			l13.1-2.8l-10-2l-10.4,2.5l-4.9-2.5h-6l15.6-41.5l7.9-34.5l32.3,18.8l27.3-24l9.5,8.4l24-3.6l4.7,4.4h17.1l-2.8-7.8l6.8-3.2h8.8
			l6,5.6l4-1.6h4v-7l8-1.9v4.9l54-11.5l0.1-0.2l13.9-1.3v-6l4.8,4h10.3l14,3.8L2359.9,241z M1689,469.8l-0.8-5.6h6.9l5.6,2.9l5.9-4.7
			l0.8,9.5l-18.7-1.6l-7.3,29.5l1.5-34.9L1689,469.8z M1685.6,378.4h-0.3v-0.1l1.5,0L1685.6,378.4z M1723.4,346.1l-23.3-3.1
			l-25.2,3.1l0.1,0.3l-0.3,0l-0.1-0.3h0.2l-0.4-0.4l-3.5-7l2.4-2.5v-2.9h4l1.8-3l4.5-3.2l15.5,0.4l21.2-6.2l-3,6.7l-4.8,2.3l7.8,3
			l5.3,1.5l11.3,3.7L1723.4,346.1z M1794.3,346.3l-5.3,7l-14.7-5.5l2.5-9l-13.3-10.8l-4.4-3.7l2.7-4.5l4-2.5l8-2.3l10.8,5l-8.9,2.4
			l11.1,5.6l1.3,5.2l5.4-1.9l3.5,3.5l-7.1,5L1794.3,346.3z"/>
                    <polygon class="map_st3" points="2480,575 2485.5,573.9 2468.7,552.7 2468.3,552.4 2441,532.3 2435,531.9 2425.2,525.4 2410,525.4
			2390.8,509.3 2378.7,507.6 2380.2,509 2363.7,495.9 2365.2,504.4 2370.2,512.4 2367.7,525.8 2354.3,532.4 2354.2,532.4
			2342.6,516.2 2329.2,511.9 2329.2,503.4 2301.6,500.2 2291.2,504.7 2291.2,512.4 2283.5,511.2 2275.5,503.2 2269.5,507.7
			2255.7,517.2 2245.7,529.8 2226.7,529 2223.7,532.1 2214.5,537 2211.2,544.4 2243.2,576.9 2246.7,582.7 2264.6,591.2 2272.2,584
			2285.5,582.5 2285.7,582.7 2303.1,580.5 2306.7,578.4 2320.2,578.4 2323.2,576.4 2349.9,576.4 2381.6,588.4 2386.2,576.4
			2385.2,570.4 2458.2,604.2 2485.5,600.9 2480.2,588.4 	"/>
                    <polygon class="map_st1" points="2211.2,467.4 2195.7,458.4 2211.7,445.7 2231,447.9 2221.5,437.7 2183.7,445.9 2171,458.2
			2170.7,458.2 2141,459.2 2146.2,471.2 2185.5,477.1 2185.2,476.9 2190.1,467.5 	"/>
                    <polygon class="map_st1" points="1918.6,172 1936.1,172.5 1951.5,170.4 1949.8,165.5 1953.7,162.4 1939.6,159.9 1937.8,156.2
			1933.7,152.7 1928.5,155.4 1926,150.9 1906,152.7 1923.8,163.9 	"/>
                    <path class="map_st4" d="M998,430.2l13-12l15.7-3.3l-17-2.8l-14,7.8h-0.1l-22-11.2l-4.2-9.3l3.8-6h10.5l8.3-17.2l30.4,4.5l7.7,10.7
			l0.2-9.5l47-36.7l56.7-3.3l10.7-1.3l7-6.3l17-1.4l22.3-7l-46.3-1.7l33.7-6.8l38-2.8l37.3-11.6l-39.3,2.8l15-8.3l-17.4-4l0.1-0.2
			h-0.2l-24.2-7.7l-39,7.8l-64-0.1l2.2,2.9l-2.2-2.5l-20.2-2.5l2.2-5.3l-5.2-6.3l5.5-7.7l17.6-11.7l-15.3-17.2l-26,20.2l-22.6-15.3
			l-58.5-14.5l-47.8,7.5l-39.8-16.2l-1.9-1.8l-51.3,4.5h-23.2l-5.3,4l-20.6,6.4l0.4,14.6l-7.5,7.5l-24.5,5.8l24.5,12.6l42.5-6.8
			l101.8-6.1l-45.8,13.3l8.3,17l28,2.3l-5.8,23.4h24.4L870,338.9l23.7,35.7l19.2,3.8l3,5.3l21.3,15.7l-4.8,8.6l9.4,7.6l19.5,3.9h0.4
			L998,430.2z M1102.1,314.3h0.6h-0.4H1102.1z M1017.2,336.7l-28.1-4.4l-0.8-5.2l8.8,1.2l-2.9-6.4l-1.3-1.6l6-2.1h0.7l11.8-0.5l0,0
			l6.8-0.3l-8.2-15.8l5.3,5.2l9.4,1.4l3.7,5.4l14.9,3l55.9-1.8l-61.5,10.6l-3,5.3L1017.2,336.7z"/>
                    <polygon class="map_st1" points="1449.7,152.4 1443.2,152.4 1423.9,161.6 1420.8,161.8 1395.5,150.7 1418.2,145.4 1443.2,145.4
			1427.7,138.6 1392.1,133.2 1374.5,128.4 1353.5,143.4 1321.2,136.7 1278.4,155.9 1267.5,181.9 1248.7,176.9 1221,181.7
			1242.7,181.9 1254.2,192.9 1256.1,192.9 1268.7,196 1276,206.2 1307.9,207.9 1297.2,232.9 1324.5,242.2 1287.7,244.4 1294.7,269.4
			1304.2,269.4 1309.2,272.4 1318,267.5 1326.3,263.9 1313.3,254.9 1335,255.7 1366.5,230.9 1366.5,231 1372.1,237.7 1401.5,236.2
			1393.2,233.4 1393.2,228.9 1409.1,220.7 1410.5,220.4 1407.2,207.4 1418.2,201.9 1418.7,201.9 1418.7,201.4 1428.5,192.4
			1430.2,174.7 1446.6,160.8 1447.4,160.7 1456.7,146.8 	"/>
                    <polygon class="map_st1" points="1414.5,212.9 1434.2,204.4 1422.2,204.4 	"/>
                    <polygon class="map_st1" points="1236.5,210.4 1208,197.4 1184.7,216.4 1203,222.4 1211.9,240.1 1176,256.9 1196.7,259.5 1226.2,277.2
			1224,263.2 1240.2,266.1 1244.2,253.6 1231,240.7 1266.2,237.7 1263.2,236.8 1263.2,231.4 1236.4,229.3 1235.5,229.1 	"/>
                    <polygon class="map_st1" points="1171.4,246.2 1181.7,239.9 1165.2,242.2 	"/>
                    <polygon class="map_st1" points="1774.2,530.9 1763.2,539.9 1729,545.7 1761,554.7 1787.5,557.2 1792.2,540.4 1776,495.4 	"/>
                    <polygon class="map_st1" points="1936.1,172.9 1924.5,182.4 1954.7,182.4 1939.6,179.1 	"/>
                    <polygon class="map_st1" points="2232.5,368.4 2238.2,363.4 2255.7,363.4 2264.3,354.8 2259.8,347.5 2250.3,344.9 2252.4,354.4
			2240.7,351.5 2234,358.3 2221.5,359.4 2223.2,365.4 2206.7,365.4 2218.2,368.4 	"/>
                    <polygon class="map_st1" points="2338.7,492.2 2343.5,487.6 2396.5,494.2 2345,481 2328.5,473.3 2289.7,467.4 2250.7,467.4
			2280.8,482.2 2311,485.9 2307.2,490.2 	"/>
                    <g>
                        <polygon class="map_st5" points="992.5,673.9 1028.4,658.2 1006.5,647 		"/>
                        <polygon class="map_st5" points="1269.3,496.9 1271,497.4 1239.4,472.9 1164.7,464.9 1174.4,453.2 1115,445.9 1094,434.2
				1043.4,455.4 1017,482.6 1032.7,499.2 1034.3,491.6 1041.2,492.1 1041.2,496.4 1032.7,512.6 1062.5,519.1 1043.2,545.4
				1045.7,560.4 1022.4,584.2 1007.7,593.9 991.4,618.9 994.7,622.6 989.4,638.2 1028.4,658.2 1018,646.6 1021,643.3 1035.7,638.2
				1049.7,628.4 1047.2,627.5 1047.2,615.9 1069.9,606.9 1057,599.9 1102.4,584.2 1102.7,583.9 1102.6,584.1 1109.8,582.8
				1122.4,588.4 1137.2,588.4 1137,579.4 1152.2,579.4 1171.7,560.6 1186,545.9 1220,539.6 1238.2,529.4 1245.4,529.4 1255.2,507.4
				1271.1,497.5 		"/>
                    </g>
                    <polygon class="map_st1" points="1287.7,136.7 1319.2,133.4 1291.9,129.5 1237.5,119.5 1236.5,118 1209.7,139.5 1177.2,150.2
			1208.7,153.8 1179.2,161.4 1179.2,167.4 1167.5,176.9 1176.5,176.8 1185.3,188.4 1200.2,192.9 1194,184.9 1215.2,176.4
			1215.1,176.4 1213.6,170.9 1221.9,163.4 1221.8,163.2 1222,163.4 1246,150.7 1244.6,150.2 1285,143.4 1263.8,142.2 1298.7,138.8
			"/>
                    <polygon class="map_st1" points="1698.1,334.2 1704.6,331.3 1709.7,329.2 1703,326.4 1699,327.6 1692,330.4 	"/>
                    <polygon class="map_st1" points="1630.2,342.2 1630.2,346.4 1638.7,353.4 1642.2,354.2 1639.5,356.2 1641.5,360 1651,361.6
			1649.6,355.9 1653.5,354.4 1652.3,351.8 1645.3,345.3 1664.4,344.6 1621.3,338 	"/>
                    <polygon class="map_st1" points="1037.7,325.4 1043.4,316.6 1026.7,324.2 	"/>
                    <polygon class="map_st1" points="1026.7,324.2 1018.2,317.4 1011.4,317.7 1017.2,336.7 1019.1,329.2 	"/>
                    <polygon class="map_st1" points="1014.7,400.9 1030.2,391.4 1022.2,387 	"/>
                </g>
                <g id="map_copy">
                    <polygon class="map_st6" points="1609.7,254.9 1566.4,281.5 1564.4,296 1582.7,293.8 1598.7,276.8 1608,268.7 1615.6,268 1606,281.5
			1624.2,273 1643.8,252.6 	"/>
                    <polygon class="map_st7" points="1586,338 1582.1,346.3 1590.7,347.2 1588.8,356 1578.7,360 1576,347.3 1553.5,348.8 	"/>
                    <polygon class="map_st8" points="900.5,296.5 919,321.5 894.2,321.5 	"/>
                    <polygon class="map_st9" points="901.4,298.3 911,264.9 863.7,278.8 872,295.8 	"/>
                    <polygon class="map_st10" points="1245.4,488.3 1271.1,497.5 1255.2,507.5 1245.4,529.5 1238.2,529.5 1220,539.7 1191,527.3
			1185.6,514.4 	"/>
                    <polygon class="map_st11" points="1032.7,512.7 1067.6,478.8 1017,482.7 1032.7,499.3 1034.3,491.7 1041.2,492.2 1041.2,496.5 	"/>
                    <polygon class="map_st12" points="1389.7,187.4 1421,161.9 1316,170.9 	"/>
                    <polygon class="map_st13" points="1187.7,516.7 1164.7,465 1122.4,500.3 	"/>
                    <polygon class="map_st14" points="1116.4,476.7 1122.4,500.3 1087.7,518.3 	"/>
                    <polygon class="map_st3" points="2354.2,532.3 2342.6,516 2329.2,512 2329.2,503.5 2301.6,500.3 2291.2,504.8 2291.2,512.5 2278.9,533
				"/>
                    <polygon class="map_st15" points="2279.1,533.3 2291.2,512.5 2283.5,511.3 2275.5,503.3 2269.5,507.8 2255.7,517.3 	"/>
                    <polygon class="map_st16" points="2295.2,544.3 2278.9,533 2350.2,532.3 2359.7,546 	"/>
                    <polygon class="map_st17" points="2255.7,517.3 2246.7,541.5 2295.2,543.9 	"/>
                    <polygon class="map_st18" points="1516.2,395.5 1509.2,427.5 1493.7,378.5 	"/>
                    <polygon class="map_st7" points="1578.7,360 1516.2,395.5 1493.7,378.5 	"/>
                    <polygon class="map_st19" points="1538.2,307.5 1555.2,307.5 1508.2,335 	"/>
                    <polygon class="map_st20" points="1654.7,326.5 1600.7,335 1553.2,308 	"/>
                    <polygon class="map_st21" points="1611.7,302 1654.7,326.5 1553.2,308 	"/>
                    <polygon class="map_st22" points="1612.6,385.7 1516.2,395.5 1578.7,360 1583.7,359.8 1589.4,359.8 1588.1,371.3 1591.8,374.5
			1586.5,380.3 1599,383.5 	"/>
                    <polygon class="map_st23" points="1707.7,413 1688,394.5 1685.2,378.3 1661.7,385.5 1643.2,381.3 1634,384 1630.7,392 1617,388.5
			1612.6,385.7 1516.2,395.5 	"/>
                    <polygon class="map_st24" points="1509.2,427.5 1557.2,413 1516.2,395.5 	"/>
                    <polygon class="map_st25" points="1553.2,446.5 1557.2,413 1509.2,427.5 	"/>
                    <polygon class="map_st26" points="1658.7,446.8 1704.2,436.3 1707.7,413 1557.2,415.5 	"/>
                    <polygon class="map_st27" points="1776.7,424.8 1704.2,436.3 1707.7,413 1721.2,424.8 	"/>
                    <polygon class="map_st28" points="1745.7,475.5 1776.7,424.8 1704.2,436.3 	"/>
                    <polygon class="map_st29" points="1707.7,475.5 1704.2,436.3 1745.7,475.5 	"/>
                    <polygon class="map_st26" points="1651.2,458 1658.7,445.5 1553.2,446.5 	"/>
                    <polygon class="map_st30" points="1731,496 1688.7,470.5 1745.2,475.5 1729,486.3 	"/>
                    <polygon class="map_st31" points="1658.7,445.5 1682.9,465 1681.5,500 1651.2,458 	"/>
                    <polygon class="map_st32" points="1576.2,491 1644.7,509 1651.2,458 	"/>
                    <polygon class="map_st33" points="1700.7,527.5 1681.5,500 1644.7,509 	"/>
                    <polygon class="map_st34" points="1731,496 1722.7,530.5 1700.7,527.5 1681.5,500 	"/>
                    <polygon class="map_st35" points="1627.7,520.5 1700.7,527.5 1644.7,509 	"/>
                    <polygon class="map_st36" points="1604.7,564 1627.7,520.5 1669.7,566.5 	"/>
                    <polygon class="map_st37" points="1700.7,527.5 1669.7,566.5 1627.7,520.5 	"/>
                    <polygon class="map_st38" points="1654.7,326.5 1674.7,346.3 1674.7,346.3 1600.7,335 	"/>
                    <polygon class="map_st39" points="1684.7,378.5 1774.2,376 1765.7,357 	"/>
                    <polygon class="map_st40" points="1736.7,413 1684.7,378.5 1770.4,389 	"/>
                    <polygon class="map_st41" points="1848.2,353 1774.2,376 1765.7,357 	"/>
                    <polygon class="map_st42" points="1750.7,424.5 1776.7,384.5 1736.7,413 	"/>
                    <polygon class="map_st43" points="1797,394.5 1750.7,424.5 1776.7,384.5 	"/>
                    <polygon class="map_st44" points="1848,352.3 1874.1,389 1841.2,391.5 1823.2,380.8 1773.7,376 	"/>
                    <polygon class="map_st45" points="1658.7,296 1691.1,291.5 1654.7,326.5 	"/>
                    <polygon class="map_st46" points="1765.7,357 1776.7,339 1736.7,338.7 1723.2,346.3 1699.9,343.2 1674.7,346.3 	"/>
                    <polygon class="map_st47" points="1848,353.5 1803.2,330.5 1789.8,340 1794.2,346.5 1789,353.5 1774.2,348 1765.7,357 	"/>
                    <polygon class="map_st48" points="1873.2,389 1937.7,384.5 1848,353.5 	"/>
                    <polygon class="map_st49" points="1803.2,330.5 1903.7,286.5 1827.2,277.5 	"/>
                    <polygon class="map_st50" points="1691.1,291.5 1827.2,277.5 1803.2,330.5 1775.6,322.5 1784.5,320.2 1773.7,315.2 1765.7,317.5 	"/>
                    <polygon class="map_st51" points="1848,353.5 1992.7,343.8 1803.2,330.5 	"/>
                    <polygon class="map_st52" points="1827.2,277.5 1734,232.4 1740.7,271.5 	"/>
                    <polygon class="map_st53" points="1953.7,266.9 1992.7,343.8 1903.7,286.5 	"/>
                    <polygon class="map_st54" points="1835,240.3 1827.2,277.5 1734,232.4 	"/>
                    <polygon class="map_st55" points="1691.1,291.5 1663.1,277.5 1740.7,271.5 	"/>
                    <polygon class="map_st56" points="1827.2,277.5 1917.7,256.2 1953.7,266.9 	"/>
                    <polygon class="map_st57" points="2056,345.8 2020.5,286.5 1992.7,343.8 	"/>
                    <polygon class="map_st58" points="1937.7,384.5 2056,345.8 1992.7,343.8 	"/>
                    <polygon class="map_st59" points="2069.7,358.3 2056,345.8 1937.7,384.5 	"/>
                    <polygon class="map_st60" points="2001.7,395.5 1937.7,384.5 2069.7,358.3 	"/>
                    <polygon class="map_st61" points="2084.7,404.8 2069.7,358.3 2001.7,395.5 	"/>
                    <polygon class="map_st62" points="2007,421 2001.7,395.5 2084.7,404.8 	"/>
                    <polygon class="map_st63" points="2060.7,427.8 2084.7,404.8 2007,421 	"/>
                    <polygon class="map_st64" points="2078.5,430.5 2084.7,404.8 2060.7,427.8 	"/>
                    <polygon class="map_st65" points="2062.7,457.3 2060.7,427.8 2078.5,430.5 	"/>
                    <polygon class="map_st66" points="2084.7,486 2062.7,457.3 2078.5,430.5 	"/>
                    <polygon class="map_st67" points="2084.7,404.8 2118.2,417.5 2078.5,430.5 	"/>
                    <polygon class="map_st68" points="2084.7,404.8 2140.5,376.8 2069.7,358.3 	"/>
                    <polygon class="map_st69" points="2139,363 2140.3,358.3 2153.4,355.5 2143.4,353.5 2133.1,356 2128.2,353.5 2128.2,353.5
			2122.2,353.5 2137.8,312 2056,345.8 2069.7,358.5 	"/>
                    <polygon class="map_st70" points="2020.5,286.5 2137.8,312 2056,345.8 	"/>
                    <polygon class="map_st71" points="1953.7,266.9 1961.5,235.9 2020.5,286.5 	"/>
                    <polygon class="map_st72" points="2045.8,224.3 2060.7,283.5 2020.5,286.5 	"/>
                    <polygon class="map_st73" points="2185.5,257.4 2145.7,277.5 2060.7,283.5 	"/>
                    <polygon class="map_st74" points="2045.8,224.3 2084.7,248.7 2185.5,257.4 2060.7,283.5 	"/>
                    <polygon class="map_st75" points="2145.7,277.5 2178,296.3 2205.2,272.3 	"/>
                    <polygon class="map_st76" points="2185.5,257.4 2205.2,272.3 2145.7,277.5 	"/>
                    <polygon class="map_st77" points="2214.7,216.3 2185.5,257.4 2115.6,229.9 	"/>
                    <polygon class="map_st78" points="2047,202.3 2084.7,248.7 2045.8,224.3 	"/>
                    <polygon class="map_st71" points="1903.7,225.4 1961.5,235.9 2045.8,224.3 	"/>
                    <polygon class="map_st79" points="1937.8,195.3 1903.8,225.3 2023.2,200.8 1988.7,187.4 1970.2,187.4 1961.5,189.9 1963.5,194.3
			1953.7,192.3 1950,195.6 	"/>
                    <polygon class="map_st80" points="1884.7,243.3 1917.7,256.2 1961.5,235.9 	"/>
                    <polygon class="map_st81" points="1827,277.5 1844.6,250.2 1850.9,247.3 1848.2,245.4 1840.2,245.4 1835,240.3 	"/>
                    <polygon class="map_st82" points="2214.7,216.3 2257.5,273.8 2185.5,257.4 	"/>
                    <polygon class="map_st83" points="2205.2,272.3 2257.5,273.8 2185.5,257.4 	"/>
                    <polygon class="map_st84" points="2214.7,216.3 2260.5,248.7 2257.5,273.8 	"/>
                    <polygon class="map_st85" points="2295.2,236.8 2349.2,258.9 2260.5,248.7 	"/>
                    <polygon class="map_st86" points="2359.7,240.9 2349.2,258.9 2295.2,236.8 	"/>
                    <polygon class="map_st87" points="2257.5,273.8 2264.5,270.5 2273.2,270.5 2279.2,276 2283.2,274.5 2287.2,274.5 2287.2,267.4
			2295.2,265.6 2295.2,270.5 2349.2,258.9 2260.5,248.7 	"/>
                    <polygon class="map_st88" points="1787.5,557.3 1763.2,540 1761,554.8 	"/>
                    <polygon class="map_st89" points="1774.2,531 1792.2,540.5 1763.2,540 	"/>
                    <polygon class="map_st90" points="1609.7,240.8 1668.8,245.3 1672.7,232.4 	"/>
                    <polygon class="map_st91" points="1624.2,273 1625.3,266.1 1637,259.8 1643.8,252.6 1668.1,277.5 	"/>
                    <polygon class="map_st92" points="1611.6,302 1619.1,297 1621.7,291.8 1620.8,289.5 1629.2,289.5 1632.7,291.5 1639,290.5 1641.1,285
			1630,281.7 1647,280 1653.2,281.5 1663.1,277.5 1658.7,296 	"/>
                    <polygon class="map_st93" points="2294.2,568.3 2359.7,546 2246.7,541.5 	"/>
                    <polygon class="map_st94" points="2243.2,577 2286,582.5 2246.7,541.5 	"/>
                    <polygon class="map_st95" points="2294.2,568.3 2349.9,576.5 2386.2,576.5 2359.7,546 	"/>
                    <polygon class="map_st96" points="2285.7,582.8 2303.1,580.5 2306.7,578.5 2320.2,578.5 2323.2,576.5 2350.2,576.5 2294.2,568.3 	"/>
                    <polygon class="map_st97" points="2211.2,544.5 2246.7,541.5 2255.7,517.3 2245.7,529.9 2226.7,529 2223.7,532.2 2214.5,537 	"/>
                    <polygon class="map_st98" points="2354.2,532.3 2409,532 2363.7,496 2365.2,504.5 2370.2,512.5 2367.7,525.8 	"/>
                    <polygon class="map_st99" points="2417.2,568.5 2350.2,532.3 2405,531.8 	"/>
                    <polygon class="map_st100" points="2359.7,546 2417.2,568.5 2350.2,532.3 	"/>
                    <polygon class="map_st101" points="2454.7,544.5 2452,563.5 2405,531.8 	"/>
                    <polygon class="map_st102" points="2485.5,601 2452,563.5 2417.2,568.5 2458.2,604.3 	"/>
                    <polygon class="map_st103" points="2454.7,544.5 2468.7,552.8 2485.5,574 2480,575 2480.2,588.5 2485.5,601 2452,563.5 	"/>
                    <polygon class="map_st104" points="2250.7,467.5 2280.8,482.3 2311,486 2345,481 2328.5,473.3 2289.7,467.5 	"/>
                    <polygon class="map_st105" points="2140.5,529 2160.5,526.8 2162,556.5 	"/>
                    <polygon class="map_st106" points="2091.2,577 2143.2,588.5 2098.3,600 	"/>
                    <polygon class="map_st107" points="2205,480 2212.2,472 2232.5,477.8 	"/>
                    <polygon class="map_st108" points="2185.5,477.2 2171.7,468.3 2146.2,471.3 	"/>
                    <polygon class="map_st109" points="2141,459.3 2171.7,468.3 2170.7,458.3 	"/>
                    <polygon class="map_st110" points="2190.1,467.5 2211.2,467.5 2195.7,458.5 	"/>
                    <polygon class="map_st111" points="2183.7,446 2195.7,458.5 2170.7,458.5 	"/>
                    <polygon class="map_st112" points="2171.7,384.5 2169,396.8 2182.7,399.5 	"/>
                    <polygon class="map_st113" points="1800.7,194.9 1840,178.6 1773,190.9 	"/>
                    <polygon class="map_st114" points="1786.9,214.8 1771.2,221.3 1771.2,204.4 	"/>
                    <polygon class="map_st115" points="1918.6,172.1 1923.8,163.9 1953.7,162.4 1949.8,165.6 1951.5,170.4 1936.1,172.6 	"/>
                    <polygon class="map_st116" points="1087.7,518.3 1116.4,476.7 1032.7,512.7 	"/>
                    <polygon class="map_st117" points="1122.4,500.3 1116.4,476.7 1164.7,465 	"/>
                    <polygon class="map_st118" points="1245.4,489.3 1164.7,465 1187.7,516.7 	"/>
                    <polygon class="map_st119" points="1187.7,516.7 1191,526.7 1122.4,500.3 	"/>
                    <polygon class="map_st120" points="1171.7,560.7 1122.4,500.3 1191,526.7 	"/>
                    <polygon class="map_st121" points="1129.4,554.7 1122.4,500.3 1171.7,560.7 	"/>
                    <polygon class="map_st122" points="1102.4,584.3 1129.4,554.7 1087.4,539.7 	"/>
                    <polygon class="map_st123" points="1122.4,500.3 1087.4,539.7 1129.4,554.7 	"/>
                    <polygon class="map_st124" points="1087.7,518.3 1087.4,539.7 1122.4,500.3 	"/>
                    <polygon class="map_st125" points="1115,446 1116.4,476.7 1164.7,465 	"/>
                    <polygon class="map_st126" points="1174.4,453.3 1164.7,465 1115,446 	"/>
                    <polygon class="map_st127" points="1271,497.5 1245.4,489.3 1239.4,473 	"/>
                    <polygon class="map_st128" points="1191,526.7 1220,539.7 1186,546 1171.7,560.7 	"/>
                    <polygon class="map_st129" points="1087.4,539.7 1057,600 1102.4,584.3 	"/>
                    <polygon class="map_st130" points="1045.7,560.5 1043.2,545.5 1087.4,539.7 	"/>
                    <polygon class="map_st131" points="1016,616 1045.7,560.5 1057,600 	"/>
                    <polygon class="map_st132" points="989.4,638.3 1016,616 1007.7,594 991.4,619 994.7,622.7 	"/>
                    <polygon class="map_st133" points="1057,600 1069.9,607 1047.2,616 1047.2,628.5 1016,616 	"/>
                    <polygon class="map_st134" points="1028.4,658.3 989.4,638.3 1049.7,628.5 1035.7,638.3 1021,643.4 1018,646.7 	"/>
                    <polygon class="map_st135" points="1032.7,512.7 1116.4,476.7 1076,466.7 	"/>
                    <polygon class="map_st136" points="1017,482.7 1076,466.7 1063.1,480.3 	"/>
                    <polygon class="map_st137" points="1043.4,455.5 1076,466.7 1115,446 	"/>
                    <polygon class="map_st138" points="1104.9,417.3 1060.2,412 1082.5,407 	"/>
                    <polygon class="map_st139" points="1068.4,403.3 1094,398.7 1063.1,395 	"/>
                    <polygon class="map_st140" points="1026.7,415 1011,418.3 998,430.3 995.7,420 1009.7,412.2 	"/>
                    <polygon class="map_st141" points="961.4,419.5 995.7,420 998,430.3 	"/>
                    <polygon class="map_st142" points="992,376.3 983.7,393.5 973.2,393.5 969.4,399.5 937.2,399.5 	"/>
                    <polygon class="map_st143" points="870,339 893.7,374.7 919,321.3 	"/>
                    <polygon class="map_st144" points="992,376.3 937,364 893.7,374.7 	"/>
                    <polygon class="map_st145" points="1034.6,330.8 992,376.3 937,364 	"/>
                    <polygon class="map_st146" points="919,321.3 1017.2,336.8 937,364 	"/>
                    <polygon class="map_st147" points="1021.2,354.3 992,376.3 1034.6,330.8 	"/>
                    <polygon class="map_st148" points="1030.4,382 1020.7,354 992,376.3 	"/>
                    <polygon class="map_st149" points="1077.4,345.3 1030.4,382 1020.7,354 	"/>
                    <polygon class="map_st150" points="1099.9,314.8 1021.2,354.3 1037.7,325.5 	"/>
                    <polygon class="map_st151" points="1010,301.7 1015.3,306.8 1024.7,308.3 1028.5,313.7 1043.4,316.7 1102.4,314.8 	"/>
                    <polygon class="map_st152" points="1101.6,313.7 1077.4,345.3 1020.7,354 	"/>
                    <polygon class="map_st153" points="1010,301.7 919,322 900.2,297.7 	"/>
                    <polygon class="map_st154" points="911,264.9 950.4,286 900.2,297.7 	"/>
                    <polygon class="map_st155" points="1020.7,247.9 1043.4,263.2 1010,301.7 	"/>
                    <polygon class="map_st156" points="1061.5,279.7 1010,301.7 1043.4,263.2 	"/>
                    <polygon class="map_st157" points="1102.4,314.8 1084.7,293.8 1064.5,291.3 1066.7,286 1061.5,279.7 1010,301.7 	"/>
                    <polygon class="map_st158" points="1077.4,345.3 1134,342 1144.7,340.7 1151.7,334.3 1168.7,333 1191,326 1144.7,324.3 1100.7,314 	"/>
                    <polygon class="map_st159" points="1211.9,293.8 1178.4,317.5 1134,317.5 	"/>
                    <polygon class="map_st160" points="1084.7,293.5 1212.1,293.8 1140.2,317.3 1100.2,314.3 	"/>
                    <polygon class="map_st161" points="1187.7,286 1211.9,293.8 1144.5,294.5 	"/>
                    <polygon class="map_st162" points="1178.4,317.5 1216.4,314.8 1253.7,303.2 1214.4,306 1229.4,297.7 1211.9,293.8 	"/>
                    <polygon class="map_st163" points="1084.6,260.2 1043.4,263.2 1067,272 	"/>
                    <polygon class="map_st164" points="957.1,246.3 965,257.7 950.4,286 1020.7,247.9 	"/>
                    <polygon class="map_st165" points="911,264.9 957.1,246.3 965,257.7 	"/>
                    <polygon class="map_st166" points="872.7,223.9 917,265.9 914.4,240.9 	"/>
                    <polygon class="map_st167" points="807.7,271.5 917,264.9 872.7,222.9 835,260.2 	"/>
                    <polygon class="map_st168" points="772.3,237.8 835,260.2 765.2,259.9 772.7,252.4 	"/>
                    <polygon class="map_st169" points="872.7,222.9 821.4,227.4 814.4,234.6 821.4,244.9 	"/>
                    <polygon class="map_st170" points="772.3,237.8 815.4,234.6 822.4,244.9 835,260.2 	"/>
                    <polygon class="map_st171" points="740.7,265.7 765.2,259.9 807.7,271.5 765.2,278.3 	"/>
                    <polygon class="map_st172" points="1316,170.9 1421,161.9 1337.5,156.9 	"/>
                    <polygon class="map_st173" points="1278.4,155.9 1337.5,156.9 1316,170.9 	"/>
                    <polygon class="map_st174" points="1353.5,143.4 1337.5,156.9 1321.2,136.8 	"/>
                    <polygon class="map_st175" points="1374.5,128.4 1395.5,150.8 1353.5,143.4 	"/>
                    <polygon class="map_st176" points="1421,161.9 1395.5,150.8 1337.5,156.9 	"/>
                    <polygon class="map_st177" points="1283.2,192.4 1267.5,181.9 1316,170.9 	"/>
                    <polygon class="map_st178" points="1355.2,195.9 1389.7,187.4 1316,170.9 	"/>
                    <polygon class="map_st179" points="1307.9,207.9 1355.2,195.9 1283.2,192.4 	"/>
                    <polygon class="map_st180" points="1366.5,230.9 1355.2,195.9 1307.9,207.9 	"/>
                    <polygon class="map_st181" points="1366.5,230.9 1410.5,220.4 1355.2,195.9 	"/>
                    <polygon class="map_st182" points="1389.7,187.4 1418.7,201.9 1355.2,195.9 	"/>
                    <polygon class="map_st183" points="1421,161.9 1418.7,201.9 1389.7,187.4 	"/>
                    <polygon class="map_st184" points="1395.5,150.8 1400,135.4 1374.5,128.4 	"/>
                    <polygon class="map_st185" points="1329.5,224.9 1366.5,230.9 1307.9,207.9 	"/>
                    <polygon class="map_st186" points="1324.5,242.3 1329.5,224.9 1297.2,232.9 	"/>
                    <polygon class="map_st187" points="1366.5,230.9 1324.5,242.3 1329.5,224.9 	"/>
                    <polygon class="map_st188" points="1335,255.7 1324.5,242.3 1366.5,230.9 	"/>
                    <polygon class="map_st189" points="1287.7,244.4 1313.3,254.9 1324.5,242.3 	"/>
                    <polygon class="map_st189" points="1294.7,269.5 1313.3,254.9 1287.7,244.4 	"/>
                    <polygon class="map_st190" points="1326.3,263.9 1313.3,254.9 1294.7,269.5 1304.2,269.5 1309.2,272.5 1318,267.5 	"/>
                    <polygon class="map_st191" points="1372.1,237.8 1366.5,231.1 1409.6,220.6 1393.2,228.9 1393.2,233.4 1401.5,236.3 	"/>
                    <polygon class="map_st192" points="1414.5,212.9 1434.2,204.4 1422.2,204.4 	"/>
                    <polygon class="map_st193" points="1420.8,163.1 1447.4,160.8 1456.7,146.8 1449.7,152.4 1443.2,152.4 	"/>
                    <polygon class="map_st194" points="1420.8,161.8 1430.2,174.8 1428.5,192.4 1418.2,201.9 	"/>
                    <polygon class="map_st195" points="1447.4,160.1 1430.2,174.8 1420.8,161.8 	"/>
                    <polygon class="map_st196" points="1276,206.3 1307.9,207.9 1283.2,192.4 	"/>
                    <polygon class="map_st197" points="1254.2,192.4 1283.2,192.4 1276,206.3 1268.7,196.1 	"/>
                    <polygon class="map_st198" points="1221,181.8 1242.7,181.9 1254.2,192.9 1267.5,181.9 1248.7,176.9 	"/>
                    <polygon class="map_st199" points="1176,256.9 1196.7,259.6 1235.5,229.1 	"/>
                    <polygon class="map_st200" points="1226.2,277.3 1196.7,259.6 1224,263.2 	"/>
                    <polygon class="map_st201" points="1244.2,253.7 1196.7,259.6 1231,240.8 	"/>
                    <polygon class="map_st202" points="1240.2,266.2 1224,263.2 1196.7,259.6 1244.2,253.7 	"/>
                    <polygon class="map_st203" points="1235.5,229.1 1266.2,237.8 1231,240.8 1196.7,259.6 	"/>
                    <polygon class="map_st204" points="1203,222.4 1235.5,229.1 1211.9,240.1 	"/>
                    <polygon class="map_st205" points="1263.2,237.4 1263.2,231.4 1233,229.1 	"/>
                    <polygon class="map_st206" points="1208,197.4 1235.5,229.1 1203,222.4 	"/>
                    <polygon class="map_st207" points="1236.5,210.4 1235.5,229.1 1208,197.4 	"/>
                    <polygon class="map_st208" points="1203,222.4 1184.7,216.4 1208,197.4 	"/>
                    <polygon class="map_st209" points="1169.2,225.4 1203.2,225.4 1184.7,217.8 1181.5,208.1 	"/>
                    <polygon class="map_st210" points="1246.2,133.4 1236.5,119.3 1291.9,129.6 1319.2,133.4 	"/>
                    <polygon class="map_st211" points="1209.7,139.6 1246.2,130.9 1236.5,118 	"/>
                    <polygon class="map_st212" points="1319.2,133.4 1287.7,136.8 1298.7,138.8 1253.7,143.3 1246.2,133.4 	"/>
                    <polygon class="map_st213" points="1253.7,141.8 1242,150.8 1229.2,145.3 1209.7,139.6 	"/>
                    <polygon class="map_st214" points="1253.7,141.8 1285,143.4 1242,150.8 	"/>
                    <polygon class="map_st215" points="1222,163.4 1213.7,154.4 1246,150.8 	"/>
                    <polygon class="map_st216" points="1185.3,188.4 1176.2,176.4 1215.2,176.4 	"/>
                    <polygon class="map_st217" points="1089.2,264.4 1105.2,264.4 1113.7,258.2 	"/>
                    <polygon class="map_st218" points="1107,240.8 1109.2,249.1 1089.2,261.9 	"/>
                    <polygon class="map_st217" points="1122.2,247.9 1107,240.8 1109.2,249.1 	"/>
                    <polygon class="map_st219" points="1148.2,243.3 1139.2,250.2 1122.2,247.9 1141.7,239.9 1132.7,234.6 1147,235.9 	"/>
                    <polygon class="map_st220" points="1127.5,204.3 1156.5,206.1 1150.2,200.1 1134,197.1 1132.2,192.4 1108.2,192.4 1122.2,195.9 	"/>
                    <polygon class="map_st221" points="1119.6,184.9 1136.2,181.8 1138.5,187.4 	"/>
                    <polygon class="map_st222" points="1129.2,258.4 1136.2,258.4 1147,265.4 1132.2,265.4 	"/>
                    <polygon class="map_st223" points="1165.2,242.3 1181.7,239.9 1171.4,246.3 	"/>
                    <polygon class="map_st224" points="1102.2,213.4 1122.2,213.4 1112.2,217.3 1104.7,224.3 	"/>
                    <polygon class="map_st225" points="2180,353.5 2193.2,353.5 2193.2,367.5 	"/>
                    <polygon class="map_st226" points="2237.2,343.3 2244,341.5 2262.2,341.5 2237.2,332.3 	"/>
                    <polygon class="map_st227" points="2220,296.3 2238.7,308 2233.6,315 	"/>
                    <polygon class="map_st228" points="2250.3,345 2259.8,347.5 2264.3,354.8 2252.5,355.2 	"/>
                    <polygon class="map_st229" points="2211.7,445.8 2231,448 2221.5,437.8 	"/>
                    <polygon class="map_st230" points="1604.7,564 1630.2,581.5 1660.2,581.5 1669.7,566.5 	"/>
                    <polygon class="map_st231" points="1627.7,520.5 1576.2,491 1644.7,509 	"/>
                    <polygon class="map_st232" points="1651.2,458 1644.7,509 1681.5,500 	"/>
                    <polygon class="map_st233" points="1774.2,531 1776,495.5 1792.2,540.5 	"/>
                    <polygon class="map_st234" points="1787.5,557.3 1792.2,540.5 1763.2,540 	"/>
                    <polygon class="map_st235" points="1729,545.8 1761,554.8 1763.2,540 	"/>
                    <polygon class="map_st236" points="1722.7,530.5 1741.2,518.5 1731,496 	"/>
                    <polygon class="map_st237" points="1704.2,436.3 1700.7,467.3 1658.7,445.5 	"/>
                    <polygon class="map_st238" points="1651.2,458 1576.2,491 1550.7,474.5 1553.2,446.5 	"/>
                    <polygon class="map_st239" points="1557.2,415.5 1553.2,446.5 1658.7,445.5 	"/>
                    <polygon class="map_st240" points="1707.7,413 1557.2,416.5 1516.2,395.5 	"/>
                    <polygon class="map_st241" points="1509.2,427.5 1457.2,427 1437.2,391.5 1493.7,378.5 	"/>
                    <polygon class="map_st242" points="1578.7,360.5 1531.7,368.5 1482.7,368.5 1437.2,391.5 1493.7,379 	"/>
                    <polygon class="map_st26" points="1553.2,308 1600.7,335 1553.5,348.8 1531.7,346.3 1526.2,335 1506.2,335.5 	"/>
                    <polygon class="map_st1" points="1684.7,378.5 1765.7,357 1674.7,346.3 	"/>
                    <polygon class="map_st243" points="1654.7,326.5 1691.1,291.5 1776.7,339 1717.2,328.2 1720.2,321.5 1699,327.7 	"/>
                    <polygon class="map_st244" points="1611.6,302 1658.7,296 1654.7,326.5 	"/>
                    <polygon class="map_st2" points="1803.2,330.5 1903.7,286.5 1992.7,343.8 	"/>
                    <polygon class="map_st245" points="1937.7,384.5 1848,353.5 1992.7,343.8 	"/>
                    <polygon class="map_st246" points="1953.7,266.9 1903.7,286.5 1827,277.5 	"/>
                    <polygon class="map_st247" points="1884.7,243.3 1917.7,256.2 1827,277.5 1844.6,250.2 1850.9,247.3 1853.5,250.7 1856.7,246.1
			1864,244.9 1859.2,235.9 1871.2,241.4 	"/>
                    <polygon class="map_st248" points="1953.7,266.9 1992.7,343.8 2020.5,286.5 	"/>
                    <polygon class="map_st249" points="1961.5,235.9 1953.7,266.9 1917.7,256.2 	"/>
                    <polygon class="map_st250" points="2045.8,224.3 2020.5,286.5 1961.5,235.9 	"/>
                    <polygon class="map_st251" points="1691.1,291.5 1663.1,277.5 1658.7,296 	"/>
                    <polygon class="map_st252" points="1734,232.4 1740.7,271.5 1663.1,277.5 	"/>
                    <polygon class="map_st253" points="1827,277.5 1691.1,291.5 1740.7,271.5 	"/>
                    <polygon class="map_st254" points="1609.7,240.8 1643.8,252.6 1668.1,277.5 1668.8,245.3 	"/>
                    <polygon class="map_st255" points="1771.2,204.4 1800.2,194.9 1772.8,190.9 1752.3,204.4 1771.2,221.3 	"/>
                    <polygon class="map_st256" points="2045.8,224.3 2047,202.3 1998,210.8 2023.2,200.8 1903.8,225.3 	"/>
                    <polygon class="map_st257" points="1877.6,211.8 1884.6,221.4 1890.4,218.8 1903.7,225.4 1937.8,195.3 1920.3,199.9 1907.8,198.3
			1901,205.4 1894.2,205.4 1891.5,206.6 	"/>
                    <polygon class="map_st258" points="1906,152.8 1923.8,163.9 1953.7,162.4 1939.6,159.9 1937.8,156.3 1933.7,152.8 1928.5,155.4
			1926,150.9 	"/>
                    <polygon class="map_st259" points="1924.5,182.4 1936.1,172.9 1939.6,179.1 1954.7,182.4 	"/>
                    <polygon class="map_st260" points="2145.7,277.5 2137.8,312 2020.5,286.5 	"/>
                    <polygon class="map_st261" points="2185.5,257.4 2084.7,248.7 2047,202.3 2115.6,229.9 	"/>
                    <polygon class="map_st262" points="2255.7,330.3 2233.6,315 2231,328.3 2235.5,325.8 	"/>
                    <polygon class="map_st263" points="2264.3,354.8 2255.7,363.5 2238.2,363.5 2232.5,368.5 2218.2,368.5 2206.7,365.5 2223.2,365.5
			2221.5,359.5 2234,358.3 2240.7,351.5 2253.2,354.7 	"/>
                    <polygon class="map_st112" points="2182.7,365 2190.2,368 2191.5,374.3 	"/>
                    <polygon class="map_st264" points="2165.8,412.5 2184.5,417.5 2170.7,421 	"/>
                    <polygon class="map_st265" points="2198,416.8 2208.5,426 2184.5,417.5 	"/>
                    <polygon class="map_st266" points="2183.7,446 2221.5,437.8 2211.7,445.8 2195.7,458.5 	"/>
                    <polygon class="map_st267" points="2171.7,468.3 2141,459.3 2146.2,471.3 	"/>
                    <polygon class="map_st268" points="2185.2,477 2190.1,467.5 2195.7,458.5 2170.7,458.3 2171.7,468.3 	"/>
                    <polygon class="map_st269" points="2145.7,483.3 2156.3,477.2 2120.7,480.2 	"/>
                    <polygon class="map_st270" points="2307.2,490.3 2311,486 2345,481 2396.5,494.3 2343.5,487.7 2338.7,492.3 	"/>
                    <polygon class="map_st271" points="2211.2,544.5 2246.7,541.5 2243.2,577 	"/>
                    <polygon class="map_st272" points="2285.7,582.8 2294.2,568.3 2246.7,541.5 	"/>
                    <polygon class="map_st273" points="2243.2,577 2246.7,582.8 2264.6,591.3 2272.2,584 2286,582.5 	"/>
                    <polygon class="map_st274" points="2378.7,507.7 2390.8,509.4 2410,525.5 2425.2,525.5 2435,532 2441,532.4 2468.7,552.8 2454.7,544.5
			2405,531.8 	"/>
                    <polygon class="map_st275" points="2417.2,568.5 2405,531.8 2452,563.5 	"/>
                    <polygon class="map_st276" points="2458.2,604.3 2417.5,568.5 2359.7,546 2386.2,576.5 2385.2,570.5 	"/>
                    <polygon class="map_st277" points="2349.9,576.5 2381.6,588.5 2386.2,576.5 	"/>
                    <polygon class="map_st278" points="2380.7,604.3 2403.5,620.5 2417.2,612.4 	"/>
                    <polygon class="map_st279" points="2345,626.3 2318,616 2309.2,628.5 	"/>
                    <polygon class="map_st280" points="2345,646 2377.5,636 2383.7,656.5 	"/>
                    <polygon class="map_st281" points="2140.5,376.8 2139,363 2069.7,358.5 	"/>
                    <polygon class="map_st282" points="2260.5,281.5 2257.5,273.3 2205.2,272.3 2214.7,280.5 2238.7,277 2243.4,281.5 	"/>
                    <polygon class="map_st283" points="2347.5,258.9 2363.2,257.4 2363.2,251.4 2368,255.4 2378.2,255.4 2392.2,259.2 2359.6,241.1 	"/>
                    <polygon class="map_st284" points="1481.5,304 1493.7,291.5 1472,292.3 	"/>
                    <path class="map_st285" d="M1477.7,309.2l38.5,0.9l-2-41.5l-17.2,28.8L1477.7,309.2L1477.7,309.2z"/>
                    <polygon class="map_st286" points="1615.5,200.4 1626.5,193.3 1633.4,190.9 1618.2,191.8 1613.5,186.8 1616,179.8 1609.7,187.8
			1613.5,194.9 	"/>
                    <polygon class="map_st287" points="1553.2,215.3 1587.5,202.8 1558.2,202.1 1553.1,193.8 1535.7,194.8 1526.2,202.4 1545.2,202.4
			1541.2,206.6 	"/>
                    <polygon class="map_st288" points="1726.5,164.9 1740,158.4 1714.2,158.4 	"/>
                    <polygon class="map_st289" points="1730.4,165.4 1752.6,156.9 1747.5,165.4 	"/>
                    <polygon class="map_st290" points="1773,165.4 1790.2,158.4 1791.4,165.4 	"/>
                    <polygon class="map_st291" points="1776.5,152.4 1793.7,157.8 1799.4,152.4 	"/>
                    <polygon class="map_st292" points="1800.5,162.3 1793.7,157.3 1820,151.9 1819,154.6 1842,151.9 1829.4,155.6 1828.5,157.9
			1819.9,161.3 1806.5,158.6 	"/>
                    <polygon class="map_st5" points="992.5,674 1028.4,658.3 1006.5,647 	"/>
                    <polygon class="map_st293" points="989.4,638.3 1049.7,628.5 1016,616 	"/>
                    <polygon class="map_st294" points="1057,600 1087.4,539.7 1045.7,560.5 	"/>
                    <polygon class="map_st295" points="1016,616 1045.7,560.5 1022.4,584.3 1007.7,594 	"/>
                    <polygon class="map_st296" points="1087.4,539.7 1087.7,518.3 1032.7,512.7 1062.5,519.2 1043.2,545.5 	"/>
                    <polygon class="map_st297" points="1102.6,584.2 1109.8,582.9 1122.4,588.5 1137.2,588.5 1137,579.5 1152.2,579.5 1171.7,560.7
			1129.4,554.8 	"/>
                    <polygon class="map_st298" points="1245.5,489.5 1239.4,473 1164.7,465 	"/>
                    <polygon class="map_st299" points="1116.4,476.7 1115,446 1076,466.7 	"/>
                    <polygon class="map_st300" points="1043.4,455.5 1076,466.7 1017,482.7 	"/>
                    <polygon class="map_st301" points="1043.4,455.5 1094,434.3 1115,446 	"/>
                    <polygon class="map_st302" points="937.2,399.5 915.9,383.8 912.9,378.5 893.7,374.7 992,376.3 	"/>
                    <polygon class="map_st4" points="919,321 893.7,374.7 937,364 	"/>
                    <polygon class="map_st303" points="1018.2,317.5 1010,301.7 919,321.5 	"/>
                    <polygon class="map_st304" points="1010,301.7 1020.7,247.9 950.4,286 900,297.7 	"/>
                    <polygon class="map_st305" points="965,257.7 950.4,286 911,264.9 	"/>
                    <polygon class="map_st306" points="772.3,237.8 815.4,235.4 821.4,227.4 798.2,227.4 792.9,231.4 	"/>
                    <polygon class="map_st307" points="872.7,222.9 835,260.2 821.4,244.9 	"/>
                    <polygon class="map_st308" points="765.2,259.9 807.7,271.5 835,260.2 	"/>
                    <polygon class="map_st309" points="1020.7,247.9 962.2,233.4 957.1,246.3 	"/>
                    <polygon class="map_st310" points="911,264.9 957.1,246.3 962.2,233.4 914.4,240.9 	"/>
                    <polygon class="map_st311" points="1061.5,279.7 1043.4,263.2 1067,272 	"/>
                    <polygon class="map_st312" points="1084.6,260.2 1069.4,243.1 1043.4,263.2 	"/>
                    <polygon class="map_st313" points="1099.2,314.3 1144.7,324.3 1178.4,317.5 	"/>
                    <polygon class="map_st314" points="1241.2,333.5 1241.2,318.5 1220.5,333.5 	"/>
                    <polygon class="map_st315" points="1246.2,333.5 1246.2,323.5 1278.2,333 	"/>
                    <polygon class="map_st316" points="937.2,399.5 932.5,408 941.8,415.8 961.4,419.5 995.7,420 973.6,408.8 969.4,399.5 	"/>
                    <polygon class="map_st317" points="1335,255.7 1324.5,242.3 1313.3,254.9 	"/>
                    <polygon class="map_st318" points="1329.5,224.9 1307.9,207.9 1297.2,232.9 	"/>
                    <polygon class="map_st319" points="1410.5,220.4 1355.2,195.9 1418.2,201.9 1407.2,207.4 	"/>
                    <polygon class="map_st320" points="1395.5,150.8 1418.2,145.4 1443.2,145.4 1427.7,138.6 1400,135.8 	"/>
                    <polygon class="map_st321" points="1337.5,156.9 1395.5,150.6 1353.5,143.4 	"/>
                    <polygon class="map_st322" points="1278.4,155.9 1337.5,156.9 1321.2,136.8 	"/>
                    <polygon class="map_st323" points="1283.2,192.4 1316,170.9 1355.2,195.9 	"/>
                    <polygon class="map_st324" points="1278.4,155.9 1267.5,181.9 1316,170.9 	"/>
                    <polygon class="map_st325" points="1254.2,192.9 1267.5,181.9 1283.2,192.4 	"/>
                    <polygon class="map_st326" points="1252.5,141.8 1246.2,130.9 1209.7,139.6 	"/>
                    <polygon class="map_st327" points="1177.2,150.3 1213.7,154.4 1246,150.8 1229.2,145.3 1209.7,139.6 	"/>
                    <polygon class="map_st328" points="1179.2,161.4 1179.2,167.4 1167.5,176.9 1215.1,176.4 1213.6,170.9 1221.9,163.4 1213.4,154.4 	"/>
                    <polygon class="map_st221" points="1200.2,192.9 1194,184.9 1185.3,188.4 	"/>
                    <polygon class="map_st329" points="1148.2,178.4 1156.4,172.3 1170.9,169.3 1158.5,158.4 1148.2,158.4 1138.1,154.8 1122.2,157.3
			1134.2,159.1 1126,166.1 1136.7,178.4 	"/>
                    <polygon class="map_st330" points="1117.5,185.1 1124,178.4 1116.2,178.4 	"/>
                    <polygon class="map_st331" points="1102.4,184.9 1109.2,178.9 1087.2,176.4 	"/>
                    <polygon class="map_st332" points="981.2,224.4 981.2,218.4 1012,206.4 973.2,206.4 972,213.6 957.1,222.6 969.5,229.1 	"/>
                    <polygon class="map_st333" points="988.8,222.4 1011.9,213.3 1020.3,213.4 1030.7,207.9 1049.2,209.9 1049.2,224.4 1080.2,225.4
			1057.8,233.6 1037.9,232.4 1021.3,225.1 1013.2,229.4 992.2,229.4 1007.7,224.3 	"/>
                    <polygon class="map_st334" points="1020.4,197.4 1055.2,195.4 1051.5,205.9 1041.5,199.3 	"/>
                    <polygon class="map_st335" points="1077.6,197.9 1089.2,206.4 1051.2,206.4 	"/>
                    <polygon class="map_st336" points="1073.2,217.4 1093.2,217.4 1080.5,225.9 	"/>
                    <polygon class="map_st243" points="1674.6,346.5 1670.8,338.8 1673.2,336.3 1673.2,333.5 1677.2,333.5 1679,330.5 1683.4,327.3
			1654.7,326.5 	"/>
                    <polygon class="map_st243" points="1736.7,338.7 1725.5,335 1720.2,333.5 1712.5,330.5 1717.2,328.2 1776.7,339 	"/>
                    <polygon class="map_st243" points="1699,327.7 1692,330.5 1698.1,334.3 1704.6,331.3 1709.7,329.3 1703,326.5 	"/>
                    <polygon class="map_st243" points="1684.8,378.5 1678,366.7 1674,364 1668.2,364.8 1668.2,363.5 1667.5,360.3 1664,359 1665.5,358.3
			1665,354.5 1657.1,353.3 1664.4,344.7 1674.6,346.5 	"/>
                    <polygon class="map_st20" points="1577.7,341.7 1582.1,346.3 1590.7,347.2 1612.6,355.8 1614.9,358.7 1613.7,364.8 1619.9,358
			1615.8,353 1623.1,354 1602.8,346.3 1599.5,340.8 1600.7,335 	"/>
                    <polygon class="map_st242" points="1553.5,348.8 1541.7,349.8 1536.4,355 1538.5,358.2 1482.7,368.5 1493.7,357.3 1497,348.8
			1492.7,342.3 1501.7,339 1506.2,335.5 1526.2,335 1531.7,346.3 	"/>
                    <polygon class="map_st23" points="1621.3,338 1630.2,342.3 1630.2,346.5 1638.7,353.5 1642.2,354.3 1639.5,356.3 1641.5,360
			1651,361.7 1649.6,356 1653.5,354.5 1652.3,351.8 1645.3,345.3 1664.4,344.7 	"/>
                    <polygon class="map_st47" points="1776.7,339 1774.2,348 1765.7,357 	"/>
                    <polygon class="map_st47" points="1789.8,340 1796.8,335 1793.3,331.5 1788,333.3 1786.7,328.2 1775.6,322.5 1803.2,330.5
			1800.2,336.5 	"/>
                    <polygon class="map_st47" points="1776.7,339 1763.5,328.2 1759,324.5 1761.7,320 1765.7,317.5 1691.1,291.5 	"/>
                    <polygon class="map_st146" points="1043.4,316.7 1037.7,325.5 1026.7,324.3 	"/>
                    <polygon class="map_st146" points="1018.2,317.5 1026.7,324.3 1019.1,329.3 1017.2,336.8 1011.4,317.8 	"/>
                    <polygon class="map_st146" points="1011.4,317.8 999,318.3 993,320.3 994.2,322 997.1,328.3 988.3,327 989.1,332.3 919,321.5
			1004.8,314.8 	"/>
                    <polygon class="map_st151" points="1030.2,391.5 1022.5,380.8 1030.4,382 	"/>
                    <polygon class="map_st151" points="1030.2,391.5 1014.7,401 1022.2,387 	"/>
                    <polygon class="map_st237" points="1689,470 1688.2,464.5 1695.2,464.5 1658.7,445.5 	"/>
                    <polygon class="map_st237" points="1700.7,467.3 1706.6,462.7 1704.2,436.3 	"/>
                    <polygon class="map_st237" points="1688.7,470.5 1681.5,500 1731,496 	"/>
                    <polygon class="map_st287" points="1668.4,265.1 1681.7,248.2 1691.1,240.3 1672.7,232.4 1668.8,245.3 	"/>
                    <polygon class="map_st6" points="1555.2,307.5 1560,297.8 1557.6,291.5 1538.2,307.5 	"/>
                    <polygon class="map_st6" points="1676.4,255.2 1677.5,262.4 1687.2,262.4 1668.1,274.3 1668.4,265.1 	"/>
                    <polygon class="map_st90" points="1609.7,240.8 1590.7,251.2 1556.8,260.9 1540,281.5 1553.5,283.3 1566.4,281.5 1609.7,254.9
			1643.8,252.6 	"/>
                    <polygon class="map_st1" points="1754.4,376.5 1750.7,380.7 1770.4,389 1684.8,378.5 	"/>
                </g>
            </svg>

            <div class="marker marker_1">
                <div class="marker-wrap small">
                    <img src="/img/marker.png">
                    <div class="flag">
                        <img src="/img/flag-us.png">
                    </div>
                    <div class="marker-content">
                        <div class="marker-content-wrap">
                            <span class="line location"><?= getTranslate('Pennsylvania'); ?></span>
                            <span class="line email">usa@bcat.tech</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="marker marker_2">
                <div class="marker-wrap">
                    <img src="/img/marker.png">
                    <div class="flag">
                        <img src="/img/flag-uk.png">
                    </div>
                    <div class="marker-content">
                        <div class="marker-content-wrap">
                            <span class="line location"><?= getTranslate('Ukraine, Kiev, Bolshaya Okruzhnaya 4'); ?></span>
                            <span class="line phone">+38 (044) 587 78 28<br>+38 (094) 887 78 28<br>+38 (073) 887 78 28</span>
                            <span class="line email">info@bcat.tech</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<div data-modal-id="form" class="modal">
    <div class="modal-content-wrap">
        <div class="modal-logo">
            <img src="/img/modal/logo-popup.png">
        </div>
        <span class="button-close"></span>
        <div class="modal-content">
            <form action="/sendmail.php" method="post">
                <div class="form-data">
                    <input type="text" name="username" placeholder="<?= getTranslate('What’s your name?'); ?>" required>
                </div>
                <div class="form-data">
                    <input type="email" name="email" placeholder="<?= getTranslate('Your email'); ?>" required>
                </div>
                <div class="form-data">
                    <input type="tel"name="phone" placeholder="<?= getTranslate('Your phone number'); ?>">
                </div>
                <div class="form-data">
                    <textarea rows="7" name="message" placeholder="<?= getTranslate('Your message'); ?>" required></textarea>
                </div>

                <button type="submit" class="submit-button"><?= getTranslate('send message'); ?></button>

            </form>
        </div>
    </div>
</div>

<script>
    const PAGE = 'contacts';
    const PAGE_VERSION = 'pc';
</script>
<script src="/libs/js/anime.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/contacts.js"></script>
</body>
</html>