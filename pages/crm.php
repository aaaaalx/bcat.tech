<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/icons/favicon_32x32.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="img/icons/favicon_64x64.ico" type="image/x-icon" sizes="64x64">
    <title>Black cats</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/crm.css">
</head>
<body class="page">

<div class="loader">
    <img class="loader-icon" src="/img/ring-loader.gif">
</div>

<header class="controls">
    <div class="logo">
        <a href="<?= to('/'); ?>"><img src="/img/menu-icons/logo.png" alt="logo"></a>
    </div>
    <div class="lang">
        <nav class="lang-nav">
            <div class="lang-nav-wrap">
                <span class="selected-lang"><?= $currentLang; ?></span>
                <ul class="hidden-menu">
                    <?php foreach($langsList as $key => $lang): ?>
                        <?php if($currentLang !== $lang): ?>
                            <li><a href="?l=<?= $key; ?>"><?= $lang; ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <span class="arrow-icon"><img src="/img/arrow-icon.png"></span>
            </div>
        </nav>
    </div>
    <div class="path">
        <a href="<?= to('/'); ?>">Home</a>
        <span>/</span>
        <span><?= getTranslate('CRM'); ?></span>
    </div>
    <div class="button-bottom-left">
        <div class="item-wrap">
            <a href="//blog.bcat.tech" target="_blank"><?= getTranslate('blog'); ?></a>
        </div>
    </div>
    <div class="button-top-right">
        <div class="item-wrap">
            <a href="<?= to('/'); ?>"><?= getTranslate('Back to home'); ?></a>
        </div>
    </div>
</header>

<div class="body-wrap">

    <div class="page-bg">

        <div class="left-part">
            <h1>Lorem ipsum dolor sit amet</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
        </div>

        <div class="right-part">
            <div class="inner">
                <object id="svg-back" type="image/svg+xml" data="/img/svg/crm_page/back.svg"></object>
                <object id="svg-smoke" type="image/svg+xml" data="/img/svg/crm_page/smoke.svg"></object>
                <object id="svg-box-open" type="image/svg+xml" data="/img/svg/crm_page/box_open.svg"></object>
                <object id="svg-box-close" type="image/svg+xml" data="/img/svg/crm_page/box_close.svg"></object>
                <object id="svg-box-close-end" type="image/svg+xml" data="/img/svg/crm_page/box_close_end.svg"></object>
                <object id="svg-fabric" type="image/svg+xml" data="/img/svg/crm_page/fabric.svg"></object>
                <object id="svg-robots" type="image/svg+xml" data="/img/svg/crm_page/robots.svg"></object>
                <object id="svg-cat" type="image/svg+xml" data="/img/svg/crm_page/cat.svg"></object>
            </div>
        </div>

    </div>

</div>
<script>
    const PAGE = 'crm';
    const PAGE_VERSION = 'pc';
</script>
<script src="/libs/js/anime.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/crm-script.js"></script>
</body>
</html>