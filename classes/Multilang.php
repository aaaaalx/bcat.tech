<?php

class Multilang{

    private $root;
    private $langList = [];
    private $defaultLang;
    private $config;
    private $geolocation;
    private $route;
    private $translate;
    private $currentLang;
    private $currentLangKey;
    private $isLangSet;

    public function __construct(Route $Route, Config $Config, Geolocation $Geolocation)
    {
        $this->route = $Route;
        $this->config = $Config;
        $this->geolocation = $Geolocation;
        $this->langList = $this->config->langList;
        $this->root = $this->config->root;
        $this->defaultLang = $this->config->defaultLang;
        $this->getLocation();
    }

    public function getDefaultLang(): ?string
    {
        return $this->defaultLang;
    }

    public function getCurrentLang(): ?string
    {
        return $this->currentLang;
    }

    public function getCurrentLangKey(): ?string
    {
        return $this->currentLangKey;
    }

    public function getLangList(): array
    {
        return $this->langList;
    }

    public function isLangSet(): bool
    {
        return $this->isLangSet;
    }

    public function isLang($lang): bool
    {
        if(isset($this->getLangList()[$lang])){
            return true;
        }
        return false;
    }

    public function getLinkWithoutLang(string $url): string
    {
        $urlArray = parse_url($url);
        if($path = $urlArray['path']??null){
            $separator = substr($url, 0, 1) == '/'?'/':'';
            $pathArray = array_values(array_filter(explode('/', $path)));
            if($this->isLang($pathArray[0])) {
                unset($pathArray[0]);
                $url = '';
                if(isset($urlArray['scheme'])) {
                    $url .= $urlArray['scheme'] . '://';
                }
                if(isset($urlArray['host'])) {
                    $url .= $urlArray['host'];
                }
                if(isset($urlArray['port'])) {
                    $url .= ':'.$urlArray['port'];
                }
                $url .= $separator.implode('/', $pathArray);
                if(isset($urlArray['query'])) {
                    $url .= '?'.$urlArray['query'];
                }
            }

        }
        return $url;
    }

    public function getTranslate(string $string)
    {
        if(trim($string) && $this->translate){
            if(isset($this->translate[$string])){
                return $this->translate[$string];
            }
        }
        return $string;
    }

    private function findTranslate(?string $lang): Void
    {
        $path = $lang?$this->root.DIRECTORY_SEPARATOR.'translate'.DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.'translate.php':false;
        if ($path && file_exists($path)) {
            $this->translate = include_once $path;
        }
        else{
            $this->currentLangKey = array_search($this->defaultLang, $this->langList);
            $path = $this->root.DIRECTORY_SEPARATOR.'translate'.DIRECTORY_SEPARATOR.$this->currentLangKey.DIRECTORY_SEPARATOR.'translate.php';
            if (file_exists($path)) {
                $this->translate = include_once $path;
            }
            $this->currentLang = $this->defaultLang;
        }
    }

    private function getLocation(): Void
    {
        $this->route->addMultilang($this);
        $lang = $this->route->getLang();
        $this->isLangSet = $lang?true:false;
        $country = $this->geolocation->getCountry();
        if($country && isset($this->langList[$country]) && !$lang){
            $lang = $country;
        }
        if(preg_match('/^[a-zA-Z\s]+$/', $lang)){
            if(isset($this->langList[$lang])){
                $this->currentLang = $this->langList[$lang];
                $this->currentLangKey = array_search($this->langList[$lang], $this->langList);
            }
            $this->findTranslate($lang);
        }
        else{
            $this->findTranslate(null);
        }
    }

}



