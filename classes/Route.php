<?php
class Route{

    private $root;
    private $currentUrl;
    private $config;
    private $multilang;
    private $path;

    public function __construct(Config $Config)
    {
        $this->config = $Config;
        $this->root = $this->config->root;
        $this->currentUrl = parse_url((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'?"https":"http")."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
    }

    public function addMultilang(Multilang $Multilang): Void
    {
        $this->multilang = $this->multilang??$Multilang;
    }

    public function getUrlPath(): array
    {
        if(!$this->path){
            $this->path = array_filter(explode('/', $this->currentUrl['path']));
        }
        return $this->path;
    }

    public function getLang(): ?string
    {
        if($this->multilang){
            $langList = $this->multilang->getLangList();
            $pathArray = $this->getUrlPath();
            if($pathArray && $langList[$pathArray[1]]){
                return $pathArray[1];
            }
        }
        return null;
    }

    public function to($url): string
    {
        if($this->multilang){
            $separator = '/';
            $lang = '/'.$this->multilang->getCurrentLangKey();
            if (!$this->isFullUrl($url)){
                if(trim($url) == '/' || substr($url, 0, 1) == '/'){
                    $separator = '';
                }
                if ($this->multilang->getLinkWithoutLang($url) != $url){
                    $lang = '';
                }
                return $this->multilang->isLangSet()?$lang.$separator.$url:$url;
            }
        }
        return $url;
    }

    public function isFullUrl(string $url): bool
    {
        return filter_var($url,FILTER_VALIDATE_URL);
    }

}