<?php
class Geolocation
{

    const PATH = 'http://www.geoplugin.net/php.gp?ip=';
    private $country;

    public function __construct()
    {
        $ip = $this->get_client_ip();
        if ($ip && $ip !== 'UNKNOWN'){
            $geoInfo = @unserialize(file_get_contents(self::PATH.$ip));
            if (isset($geoInfo['geoplugin_countryCode'])){
                $this->country = mb_strtolower($geoInfo['geoplugin_countryCode']);
            }
        }
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    private function get_client_ip(): string
    {
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else {
            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
            } else {
                if (getenv('HTTP_X_FORWARDED')) {
                    $ipaddress = getenv('HTTP_X_FORWARDED');
                } else {
                    if (getenv('HTTP_FORWARDED_FOR')) {
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                    } else {
                        if (getenv('HTTP_FORWARDED')) {
                            $ipaddress = getenv('HTTP_FORWARDED');
                        } else {
                            if (getenv('REMOTE_ADDR')) {
                                $ipaddress = getenv('REMOTE_ADDR');
                            } else {
                                $ipaddress = 'UNKNOWN';
                            }
                        }
                    }
                }
            }
        }
        return $ipaddress;
    }
}

