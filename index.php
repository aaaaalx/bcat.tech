<?php
spl_autoload_register(function ($class_name) {
    require_once __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.$class_name . '.php';
});

require_once __DIR__.DIRECTORY_SEPARATOR.'Config.php';

$config = new Config();

$Route = new Route($config);
$Multilang = new Multilang($Route, $config, new Geolocation());
$Mobile_Detect = new Mobile_Detect;

function to(string $url){
    global $Route;
    return $Route->to($url);
}

function getTranslate(string $str){
    global $Multilang;
    return $Multilang->getTranslate($str);
}

$langsList = $Multilang->getLangList();
$currentLang = $Multilang->getCurrentLang();

$page = $Route->getUrlPath();
if($Multilang->isLangSet()){
    $page = isset($page[2])?$page[2]:'index';
}
else{
    $page = isset($page[1])?$page[1]:'index';
}

$forcedMobile = false;
if(isset($_GET['v']) && $_GET['v'] == 'mobile'){
    $forcedMobile = true;
}

if($forcedMobile || $Mobile_Detect->isMobile() || $Mobile_Detect->isTablet()){
    $path = __DIR__.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.$page.'-mobile.php';
    if(file_exists($path)){
        require_once $path;
    }
    else{
        set404();
    }
}
else{
    $path = __DIR__.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.$page.'.php';
    if(file_exists($path)){
        require_once $path;
    }
    else{
        set404();
    }
}

function set404(){
    header("HTTP/1.0 404 Not Found");
    exit;
}

function is_ajax(){
    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower(getenv('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest'));
}